<?php

$app->mount($app['api'].'/books', new Civitours\Controller\Provider\Book());

// Admin endpoints
$app->mount($app['api'].'/admin', new Civitours\Controller\Admin\Admin());

// User auth endpoints
$app->post($app['api'].'/register', "Civitours\Controller\RegistrationController::register");
$app->post($app['api'].'/resend', "Civitours\Controller\RegistrationController::resend");
$app->get($app['api'].'/confirm/{token}', "Civitours\Controller\RegistrationController::confirm");
$app->get($app['api'].'/login', "Civitours\Controller\AuthenticateController::authenticate");
$app->post($app['api'].'/login/social', "Civitours\Controller\AuthenticateController::authenticateSocial");

$app->match($app['api'].'/restore', "Civitours\Controller\AuthenticateController::restore");
$app->match($app['api'].'/reset_request', "Civitours\Controller\AuthenticateController::resetRequest");
$app->match($app['api'].'/new_password', "Civitours\Controller\AuthenticateController::newPassword");

$app->post($app['api'].'/update_password', "Civitours\Controller\AuthenticateController::updatePassword")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");
$app->post($app['api'].'/update_account', "Civitours\Controller\AuthenticateController::updateAccount")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");
$app->get($app['api'].'/renew_token', "Civitours\Controller\AuthenticateController::renewToken")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");

// Geo data routes
$app->get($app['api'].'/countries', "Civitours\Controller\CountryController::getAll");
$app->get($app['api'].'/countries/{id}', "Civitours\Controller\CountryController::get")
    ->assert('id', '\d+');
$app->get($app['api'].'/cities/{id}', "Civitours\Controller\CityController::get")
    ->assert('id', '\d+');
$app->get($app['api'].'/cities/{country}/search/{name}', "Civitours\Controller\CityController::search")
    ->assert('country', '\d+');

//Destination routes
$app->get($app['api'].'/destinations/country/list', "Civitours\Controller\DestinationController::countryList");
$app->get($app['api'].'/destinations/main/list', "Civitours\Controller\DestinationController::mainList");

$app->get($app['api'].'/destinations/country/{routeName}', "Civitours\Controller\DestinationController::getDestinations");
$app->get($app['api'].'/destinations/country/{countryCode}/cities', "Civitours\Controller\DestinationController::cityList");
$app->get($app['api'].'/destinations/city/{countryRoute}/{cityRoute}', "Civitours\Controller\DestinationController::getDestinationCity");

//Activities routes
$app->get($app['api'].'/activities/featured', "Civitours\Controller\ActivitiesController::featured");
$app->get($app['api'].'/activities/{idActivity}', "Civitours\Controller\ActivitiesController::getById")
    ->assert('idActivity', '\d+');
$app->get($app['api'].'/activities/{idActivity}/related', "Civitours\Controller\ActivitiesController::getRelated")
    ->assert('idActivity', '\d+');
$app->get($app['api'].'/activities/categories', "Civitours\Controller\ActivitiesController::getCategories");
$app->get($app['api'].'/activities/city/{idCity}', "Civitours\Controller\ActivitiesController::getForCity")
    ->assert('idCity', '\d+');
$app->get($app['api'].'/activities/{countryRoute}/{cityRoute}/{route}', "Civitours\Controller\ActivitiesController::getByRoute");

//Review routes
$app->get($app['api'].'/reviews/traveller_types', "Civitours\Controller\ReviewController::getTravellerTypes");
$app->get($app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::getById")
    ->assert('idReview', '\d+');
$app->get($app['api'].'/reviews/city/{idCity}', "Civitours\Controller\ReviewController::getForCity")
    ->assert('idCity', '\d+');
$app->get($app['api'].'/reviews/activity/{idActivity}', "Civitours\Controller\ReviewController::getForActivity")
    ->assert('idActivity', '\d+');
$app->delete($app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('idReview', '\d+');
$app->post($app['api'].'/reviews', "Civitours\Controller\ReviewController::create")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");
$app->patch($app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idReview', '\d+');
$app->get($app['api'].'/reviews/user', "Civitours\Controller\ReviewController::getForUser")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");

// Upload routes
$app->post($app['api'].'/upload/image', "Civitours\Controller\UploadController::uploadImage");

// Language routes
$app->get($app['api'].'/languages',  "Civitours\Controller\LanguageController::getList");

// Order routes
$app->get($app['api'].'/order/{orderCode}',  "Civitours\Controller\OrderController::get");
$app->post($app['api'].'/order/create',  "Civitours\Controller\OrderController::create");
$app->post($app['api'].'/order/update/{orderCode}',  "Civitours\Controller\OrderController::update");
$app->get($app['api'].'/order/{orderCode}/redsys/pay/{amount}',  "Civitours\Controller\OrderController::payRedsys");
$app->post($app['api'].'/order/{orderCode}/viewed',  "Civitours\Controller\OrderController::setViewedStatus");
$app->post($app['api'].'/redsys_notify',  "Civitours\Controller\OrderController::redsysNotification");
$app->post($app['api'].'/order/{orderCode}/stripe',  "Civitours\Controller\OrderController::payStripe");


// Search routes
$app->get($app['api'].'/search/countries',  "Civitours\Controller\SearchController::getCountries");
$app->get($app['api'].'/search/cities',  "Civitours\Controller\SearchController::getCities");
$app->get($app['api'].'/search/activities',  "Civitours\Controller\SearchController::getActivities");

// User routes
$app->get($app['api'].'/user/order',  "Civitours\Controller\OrderController::getForUser")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized");

// Contact routes
$app->post($app['api'].'/contacts/request',  "Civitours\Controller\ContactController::request");
$app->post($app['api'].'/faq/request',  "Civitours\Controller\ContactController::requestFaq");
