<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180820094047 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER SEQUENCE orders_id_seq RESTART WITH 7001");
    }

    public function down(Schema $schema)
    {
        //No down migration

    }
}
