<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180821103031 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE activity_voucher
          (
            id serial PRIMARY KEY NOT NULL,
            activity int NOT NULL,
            image varchar(1024) NULL,
            text text NULL
          )
        ");
        $this->addSql("COMMENT ON COLUMN activity_voucher.activity IS 'Identitfier for activity that voucher belongs to'");
        $this->addSql("COMMENT ON COLUMN activity_voucher.image IS 'Image of activity voucher'");
        $this->addSql("COMMENT ON COLUMN activity_voucher.text IS 'Text of voucher'");
        $this->addSql("COMMENT ON TABLE activity_voucher IS 'Table that holds vouchers for activity'");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE activity_voucher");
    }
}
