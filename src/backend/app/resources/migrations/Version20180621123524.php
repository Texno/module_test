<?php declare(strict_types=1);

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180621123524 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE categories SET name='Excursiones' WHERE id=2");
        $this->addSql("UPDATE categories SET name='Visitas Guiadas' WHERE id=3");
        $this->addSql("UPDATE categories SET name='Traslados Aeropuerto' WHERE id=4");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE categories SET name='Day trips' WHERE id=2");
        $this->addSql("UPDATE categories SET name='Guided tours' WHERE id=3");
        $this->addSql("UPDATE categories SET name='Airport transfers' WHERE id=4");
    }
}
