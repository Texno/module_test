<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180806130232 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders ALTER COLUMN status TYPE varchar(15) USING status::varchar(15)");

    }

    public function down(Schema $schema)
    {
        // no down migration

    }
}
