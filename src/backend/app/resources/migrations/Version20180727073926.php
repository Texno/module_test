<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180727073926 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE faq_requests
                   (
                        id serial PRIMARY KEY NOT NULL,
                        email varchar(50),
                        subject varchar(50),
                        description varchar(1024),
                        file varchar(1024)
                    )");
        $this->addSql("COMMENT ON COLUMN faq_requests.email IS 'Requester email'");
        $this->addSql("COMMENT ON COLUMN faq_requests.subject IS 'Request subject'");
        $this->addSql("COMMENT ON COLUMN faq_requests.description IS 'Description of the request'");
        $this->addSql("COMMENT ON COLUMN faq_requests.file IS 'Request attached filename'");
        $this->addSql("COMMENT ON TABLE faq_requests IS 'Table that contains all requests for faq'");

    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE faq_requests");
    }
}
