<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180618095338 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql(
    "CREATE TABLE traveller_types (
            id serial not null constraint traveller_types_pkey primary key,
            name varchar(255)
          )");
        $this->addSql("comment on table traveller_types is 'Table with traveller types'");
        $this->addSql("comment on column traveller_types.name is 'Name of traveller type'");
        $this->addSql("INSERT INTO traveller_types (name) VALUES ('He viajado solo'), ('He viajado en pareja'), ('Familia con hijos pequeños'), ('Familia con hijos mayores'), ('He viajado con amigos')");
        $this->addSql("TRUNCATE TABLE reviews");
        $this->addSql("ALTER TABLE reviews ADD COLUMN traveller_type integer not null");
        $this->addSql("comment on column reviews.traveller_type is 'Traveller type identifier'");
        $this->addSql("ALTER TABLE reviews ADD COLUMN country integer null");
        $this->addSql("comment on column reviews.country is 'Traveller country identifier'");
        $this->addSql("ALTER TABLE reviews ADD COLUMN city integer null");
        $this->addSql("comment on column reviews.city is 'Traveller city identifier'");
        $this->addSql("ALTER TABLE reviews ADD COLUMN ip bigint not null default 2130706433");
        $this->addSql("comment on column reviews.ip is 'Traveller ip address'");
        $this->addSql("ALTER TABLE reviews ADD COLUMN is_approved boolean not null default false");
        $this->addSql("comment on column reviews.ip is 'Traveller ip address'");
        $this->addSql("ALTER TABLE reviews ADD COLUMN \"user\" integer not null");
        $this->addSql("comment on column reviews.user is 'User that review belongs to'");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE traveller_types");
        $this->addSql("ALTER TABLE reviews DROP COLUMN traveller_type");
        $this->addSql("ALTER TABLE reviews DROP COLUMN country");
        $this->addSql("ALTER TABLE reviews DROP COLUMN city");
        $this->addSql("ALTER TABLE reviews DROP COLUMN ip");
        $this->addSql("ALTER TABLE reviews DROP COLUMN is_approved");
        $this->addSql("ALTER TABLE reviews DROP COLUMN \"user\"");
    }
}
