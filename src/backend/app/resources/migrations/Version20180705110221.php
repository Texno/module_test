<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180705110221 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE countries ADD COLUMN route_name VARCHAR(255)");
        $this->addSql("ALTER TABLE cities ADD COLUMN route_name VARCHAR(255)");
        $this->addSql("ALTER TABLE activities ADD COLUMN route_name VARCHAR(255)");
        $sqlFile = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'seo.sql');
        foreach (explode(';', $sqlFile) as $sql) {
            if(!empty($sql)) {
                $this->addSql($sql);
            }
        }
        $this->addSql("CREATE INDEX countries_route_name_index ON countries (route_name)");
        $this->addSql("CREATE INDEX cities_country_route_name_index ON cities (country, route_name)");
        $this->addSql("CREATE INDEX activities_route_name_index ON activities (route_name)");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE countries DROP COLUMN route_name");
        $this->addSql("ALTER TABLE cities DROP COLUMN route_name");
        $this->addSql("ALTER TABLE activities DROP COLUMN route_name");
        $this->addSql("DROP INDEX countries_route_name_index");
        $this->addSql("DROP INDEX cities_country_route_name_index");
        $this->addSql("DROP INDEX activities_route_name_index");
    }
}
