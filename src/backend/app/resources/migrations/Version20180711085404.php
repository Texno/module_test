<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180711085404 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE activities ADD COLUMN book_limit integer NOT NULL DEFAULT 0");

    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE activities DROP COLUMN book_limit");
    }
}
