<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180711122757 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE order_invoices
            (
                id serial PRIMARY KEY NOT NULL,
                \"order\" int NOT NULL,
                tax_name varchar(50),
                tax_id int NOT NULL,
                document_type varchar(5) NOT NULL,
                address varchar(200) NOT NULL,
                city varchar(100) NOT NULL,
                country varchar(100) NOT NULL,
                postal varchar(20) NOT NULL
            )    
        ");
        $this->addSql("CREATE INDEX order_invoice_order_index ON order_invoices (\"order\")");

    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE order_invoice");
    }
}
