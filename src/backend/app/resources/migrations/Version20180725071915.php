<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180725071915 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE cities ADD travellers integer NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE cities DROP travellers');
    }
}
