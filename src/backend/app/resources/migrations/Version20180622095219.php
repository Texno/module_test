<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180622095219 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("
            create function extract_hour_for_day(bigint[], int8range)
                returns integer[]
            stable
            language sql
            as $$
            SELECT array_agg(vals)
                FROM (select cast(date_part('hour', to_timestamp(unnest)) as integer) as vals  from unnest($1) where unnest <@ $2) AS x;
            $$;");

        $this->addSql("
            create function extract_hour(bigint[])
                returns integer[]
            stable
            language sql
            as $$
            SELECT array_agg(vals)
                FROM (select cast(date_part('hour', to_timestamp(unnest($1))) as integer) as vals) AS x;
            $$;");
    }

    public function down(Schema $schema)
    {
        $this->addSql("drop function if exists extract_hour_for_day(bigint[], int8range)");
        $this->addSql("drop function if exists extract_hour(bigint[])");
    }
}
