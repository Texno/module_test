<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180726101525 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("UPDATE traveller_types SET name='He viajado solo' WHERE id=1");
        $this->addSql("UPDATE traveller_types SET name='He viajado en pareja' WHERE id=2");
        $this->addSql("UPDATE traveller_types SET name='Familia con hijos pequeños' WHERE id=3");
        $this->addSql("UPDATE traveller_types SET name='Familia con hijos adolescentes' WHERE id=4");
        $this->addSql("UPDATE traveller_types SET name='He viajado con amigos' WHERE id=5");
    }

    public function down(Schema $schema)
    {
        $this->addSql("UPDATE traveller_types SET name='I traveling alone' WHERE id=1");
        $this->addSql("UPDATE traveller_types SET name='Couple' WHERE id=2");
        $this->addSql("UPDATE traveller_types SET name='Family with small children' WHERE id=3");
        $this->addSql("UPDATE traveller_types SET name='Family with adolescent children' WHERE id=4");
        $this->addSql("UPDATE traveller_types SET name='I travelled with friends' WHERE id=5");
    }
}
