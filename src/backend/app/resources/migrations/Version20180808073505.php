<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180808073505 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("UPDATE orders SET status='paid' WHERE status='confirmed'");
        $this->addSql("UPDATE orders SET status='unpaid' WHERE status='unconfirmed'");
        $this->addSql("ALTER TABLE orders ALTER COLUMN status SET DEFAULT 'unpaid'::character varying");
    }

    public function down(Schema $schema)
    {
        $this->addSql("UPDATE orders SET status='confirmed' WHERE status='paid'");
        $this->addSql("UPDATE orders SET status='unconfirmed' WHERE status='unpaid'");
        $this->addSql("ALTER TABLE orders ALTER COLUMN status SET DEFAULT 'confirmed'::character varying");
    }
}
