<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180615072241 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE activity_schedule ALTER COLUMN time TYPE bigint[]');

    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE activity_schedule ALTER COLUMN time TYPE integer[]');
    }
}
