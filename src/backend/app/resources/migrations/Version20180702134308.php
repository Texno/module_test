<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180702134308 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE reviews ADD COLUMN order_code VARCHAR(100)");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE reviews DROP COLUMN order_code");
    }
}
