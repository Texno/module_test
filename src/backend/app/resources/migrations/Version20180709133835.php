<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180709133835 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders ADD COLUMN status VARCHAR(10) NOT NULL DEFAULT 'confirmed'");
        $this->addSql("ALTER TABLE order_activities ADD COLUMN admin_comment VARCHAR(500)");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders DROP COLUMN status");
        $this->addSql("ALTER TABLE order_activities DROP COLUMN admin_comment");
    }
}
