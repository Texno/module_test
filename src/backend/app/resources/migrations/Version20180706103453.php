<?php declare(strict_types=1);

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180706103453 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("
            CREATE TABLE suppliers
            (
                id serial PRIMARY KEY NOT NULL,
                name varchar(50) NOT NULL,
                phone varchar(20) NOT NULL,
                email varchar(50) NOT NULL,
                contact varchar(50) NOT NULL,
                vat varchar(20) NOT NULL,
                post_code varchar(10) NOT NULL,
                registered_at timestamp DEFAULT current_timestamp NOT NULL
            )");
        $this->addSql("COMMENT ON COLUMN suppliers.name IS 'Name of the supplier'");
        $this->addSql("COMMENT ON COLUMN suppliers.phone IS 'Supplier`s phone number'");
        $this->addSql("COMMENT ON COLUMN suppliers.email IS 'Supplier`s email'");
        $this->addSql("COMMENT ON COLUMN suppliers.contact IS 'Supplier`s contact name'");
        $this->addSql("COMMENT ON COLUMN suppliers.vat IS 'Supplier`s VAT number'");
        $this->addSql("COMMENT ON COLUMN suppliers.post_code IS 'Supplier`s postal code'");
        $this->addSql("COMMENT ON COLUMN suppliers.registered_at IS 'Date of supplier registration'");
        $this->addSql("COMMENT ON TABLE suppliers IS 'Table with activity suppliers information'");

        $this->addSql("
            CREATE TABLE activity_suppliers
            (
              activity int NOT NULL,
              supplier int NOT NULL
            )");
        $this->addSql("COMMENT ON COLUMN activity_suppliers.activity IS 'Activity identifier'");
        $this->addSql("COMMENT ON COLUMN activity_suppliers.supplier IS 'Supplier identifier'");
        $this->addSql("COMMENT ON TABLE activity_suppliers IS 'Relation table between activities and suppliers'");

        $this->addSql("
          ALTER TABLE activity_suppliers
            ADD CONSTRAINT activity_suppliers_activity_supplier_pk UNIQUE (activity, supplier);");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE suppliers");
        $this->addSql("DROP TABLE activity_suppliers");
    }
}
