<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180820102000 extends AbstractMigration
{
    public function up(Schema $schema)
    {
       $this->addSql("ALTER TABLE suppliers ADD website varchar(1024) NULL");
       $this->addSql("ALTER TABLE suppliers ADD bank_details text NULL");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE suppliers DROP website");
        $this->addSql("ALTER TABLE suppliers DROP bank_details");
    }
}
