<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180723084931 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE cities ADD avatar_small varchar(1024) NULL');
        $this->addSql('ALTER TABLE countries ADD avatar_small varchar(1024) NULL');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE cities DROP avatar_small');
        $this->addSql('ALTER TABLE countries DROP avatar_small');
    }
}
