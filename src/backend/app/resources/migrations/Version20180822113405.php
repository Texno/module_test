<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180822113405 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE activities ADD is_deleted boolean DEFAULT false NOT NULL");
        $this->addSql("ALTER TABLE orders ADD is_deleted boolean DEFAULT false NOT NULL");
        $this->addSql("ALTER TABLE reports ADD is_deleted boolean DEFAULT false NOT NULL");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE activities DROP is_deleted");
        $this->addSql("ALTER TABLE orders DROP is_deleted");
        $this->addSql("ALTER TABLE reports DROP is_deleted");
    }
}
