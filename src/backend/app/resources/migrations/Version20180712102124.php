<?php declare(strict_types=1);

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180712102124 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE activities ADD blocks json DEFAULT '{}' NOT NULL");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE activities DROP blocks");
    }
}
