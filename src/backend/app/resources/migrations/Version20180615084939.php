<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180615084939 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE order_activities_prices ADD COLUMN person_type integer not null default 0");
        $this->addSql("ALTER TABLE order_activities_prices RENAME COLUMN activity_price TO activity_type");
    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE order_activities_prices RENAME COLUMN activity_type TO activity_price");
        $this->addSql("ALTER TABLE order_activities_prices DROP COLUMN person_type");
    }
}
