<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180725102211 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE contact_requests
                           (
                                id serial PRIMARY KEY NOT NULL,
                                has_reservation boolean DEFAULT false NOT NULL,
                                number varchar(50),
                                name varchar(50),
                                email varchar(50),
                                subject varchar(50),
                                destination varchar(100),
                                message varchar(1024),
                                file varchar(1024)
                            )");
        $this->addSql("COMMENT ON COLUMN contact_requests.has_reservation IS 'Contact requester has bokked reservation'");
        $this->addSql("COMMENT ON COLUMN contact_requests.number IS 'Reservation number'");
        $this->addSql("COMMENT ON COLUMN contact_requests.name IS 'Requester name'");
        $this->addSql("COMMENT ON COLUMN contact_requests.email IS 'Requester email'");
        $this->addSql("COMMENT ON COLUMN contact_requests.subject IS 'Request subject'");
        $this->addSql("COMMENT ON COLUMN contact_requests.destination IS 'Request destination name'");
        $this->addSql("COMMENT ON COLUMN contact_requests.message IS 'Message of the request'");
        $this->addSql("COMMENT ON COLUMN contact_requests.file IS 'Request attached filename'");
        $this->addSql("COMMENT ON TABLE contact_requests IS 'Table that contains all requests for contact'");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE contact_requests");
    }
}
