<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180724082431 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE activities ADD avatar_small varchar(1024) NULL');
    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE activities DROP avatar_small');
    }
}
