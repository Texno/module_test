<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180817094043 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders ADD viewed boolean DEFAULT false NOT NULL");
        $this->addSql("UPDATE orders SET viewed=true");

    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE orders DROP viewed");
    }
}
