<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180625140835 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE cities ADD COLUMN latitude DECIMAL(10,6) not null default 0");
        $this->addSql("ALTER TABLE cities ADD COLUMN longitude DECIMAL(10,6) not null default 0");
        $this->addSql("TRUNCATE TABLE cities");
        $this->addSql("ALTER SEQUENCE cities_id_seq RESTART WITH 1");
        $sqlFile = file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'cities.sql');
        foreach (explode(';', $sqlFile) as $sql) {
            if(!empty($sql)) {
                $this->addSql($sql);
            }
        }
        $this->addSql('UPDATE cities SET "offset" = 0');

    }

    public function down(Schema $schema)
    {
        $this->addSql("ALTER TABLE cities DROP COLUMN latitude");
        $this->addSql("ALTER TABLE cities DROP COLUMN longitude");
    }
}
