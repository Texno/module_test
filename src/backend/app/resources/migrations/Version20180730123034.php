<?php

namespace Civitours\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180730123034 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE report_types
                            (
                                id serial PRIMARY KEY NOT NULL,
                                name varchar(255) NOT NULL
                            )");

        $this->addSql("COMMENT ON COLUMN report_types.name IS 'Report type name'");
        $this->addSql("COMMENT ON TABLE report_types IS 'List of all available report types'");
        $this->addSql("INSERT INTO report_types VALUES (1, 'Suppliers report'), (2, 'Activity report'), (3, 'City report'), (4, 'Country report')");

        $this->addSql("CREATE TABLE reports
                            (
                                id serial PRIMARY KEY NOT NULL,
                                report_type int NOT NULL,
                                name varchar(255) NOT NULL,
                                created_at timestamp DEFAULT current_timestamp NOT NULL,
                                generated_at timestamp DEFAULT null,
                                status varchar(10) DEFAULT 'created' NOT NULL,
                                params json DEFAULT '{}' NOT NULL,
                                file varchar(255)
                            )");
        $this->addSql("COMMENT ON COLUMN reports.report_type IS 'Type of report from table report_types'");
        $this->addSql("COMMENT ON COLUMN reports.name IS 'Name of the report'");
        $this->addSql("COMMENT ON COLUMN reports.created_at IS 'timestamp of report creation'");
        $this->addSql("COMMENT ON COLUMN reports.generated_at IS 'Timestamp of report generation'");
        $this->addSql("COMMENT ON COLUMN reports.status IS 'report generation status'");
        $this->addSql("COMMENT ON COLUMN reports.params IS 'report filtering parameters'");
        $this->addSql("COMMENT ON COLUMN reports.file IS 'filename for generate reports'");
        $this->addSql("COMMENT ON TABLE reports IS 'Contains all generated reports'");

        $this->addSql("ALTER TABLE activities ADD created_at timestamp DEFAULT current_timestamp NOT NULL");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE report_types");
        $this->addSql("DROP TABLE reports");

    }
}
