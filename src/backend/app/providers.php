<?php

use Civitours\Provider\ActivityServiceProvider;
use Civitours\Provider\ContactServiceProvider;
use Civitours\Provider\GeoDataServiceProvider;
use Civitours\Provider\GoogleCaptchaProvider;
use Civitours\Provider\LanguageServiceProvider;
use Civitours\Provider\MailServiceProvider;
use Civitours\Provider\OrderServiceProvider;
use Civitours\Provider\PaymentServiceProvider;
use Civitours\Provider\ReportServiceProvider;
use Civitours\Provider\ReviewServiceProvider;
use Civitours\Provider\SocialServiceProvider;
use Civitours\Provider\SupplierServiceProvider;
use Civitours\Provider\UploadServiceProvider;
use Civitours\Provider\UserServiceProvider;
use Knp\Provider\ConsoleServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\HttpCacheServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;

$now = new DateTime('now');

$app->register(new ServiceControllerServiceProvider());

$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => $app['driver'],
        'host' => $app['host'],
        'port' => $app['port'],
        'dbname' => $app['dbname'],
        'user' => $app['user'],
        'password' => $app['password'],
    ),
));

$app->register(new HttpCacheServiceProvider(), array('http_cache.cache_dir' => __DIR__.'/../app/cache'));
$app->register(new ConsoleServiceProvider());

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../app/logs/'.$app['environment'].'/'.$now->format('Y-m-d').'.log',
    'monolog.level' => $app['log.level'],
    'monolog.name' => 'application',
));

$app->register(
    new \Kurl\Silex\Provider\DoctrineMigrationsProvider(),
    array(
        'migrations.directory' => __DIR__ . '/../app/resources/migrations',
        'migrations.name' => 'Civitours Database Migrations',
        'migrations.namespace' => 'Civitours\Migrations',
    )
);

$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new SwiftmailerServiceProvider(), [
    'swiftmailer.options'           => $app['mailer.options'],
    'swiftmailer.use_spool'         => false
]);

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/resources/templates',
));

$app->register(new UserServiceProvider());
$app->register(new GeoDataServiceProvider());
$app->register(new MailServiceProvider());
$app->register(new GoogleCaptchaProvider());
$app->register(new SocialServiceProvider());
$app->register(new ActivityServiceProvider());
$app->register(new ReviewServiceProvider());
$app->register(new LanguageServiceProvider());
$app->register(new UploadServiceProvider());
$app->register(new OrderServiceProvider());
$app->register(new PaymentServiceProvider());
$app->register(new SupplierServiceProvider());
$app->register(new ContactServiceProvider());
$app->register(new ReportServiceProvider());
