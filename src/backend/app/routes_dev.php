<?php

$app->mount('{wildcard}' . $app['api'].'/books', new Civitours\Controller\Provider\Book());

// Admin endpoints
$app->get('{wildcard}' . $app['api'].'/admin/activities', "Civitours\Controller\Admin\ActivitiesController::getList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities', "Civitours\Controller\Admin\ActivitiesController::create")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::getById")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/destinations/country/list', "Civitours\Controller\Admin\DestinationController::countryList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities/{idActivity}/set/draft', "Civitours\Controller\Admin\ActivitiesController::setDraftStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->delete('{wildcard}' . $app['api'].'/admin/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/activities/{idActivity}/clear/draft', "Civitours\Controller\Admin\ActivitiesController::clearDraftStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/destinations/country/{countryCode}/cities', "Civitours\Controller\Admin\DestinationController::cityList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/countries/{id}', "Civitours\Controller\Admin\CountryController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/cities/{id}', "Civitours\Controller\Admin\CityController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/reviews/activity/{idActivity}', "Civitours\Controller\Admin\ReviewController::getForActivity")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/reviews/{idReview}/set/approve', "Civitours\Controller\Admin\ReviewController::setApprovedStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idReview', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/reviews/{idReview}/clear/approve', "Civitours\Controller\Admin\ReviewController::clearApprovedStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idReview', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/reviews/upload', "Civitours\Controller\Admin\ReviewController::uploadCsv")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/suppliers', "Civitours\Controller\Admin\SupplierController::getList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::getById")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/suppliers', "Civitours\Controller\Admin\SupplierController::create")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/admin/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->delete('{wildcard}' . $app['api'].'/admin/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/operations', "Civitours\Controller\Admin\OperationController::getList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/operations/{id}', "Civitours\Controller\Admin\OperationController::getByid")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->delete('{wildcard}' . $app['api'].'/admin/operations/{id}', "Civitours\Controller\Admin\OperationController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/order/{code}/status/{status}', "Civitours\Controller\Admin\OperationController::updateOrderStatus")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/operations/{id}/note', "Civitours\Controller\Admin\OperationController::updateAdminNote")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/operations/emails/unfinished', "Civitours\Controller\Admin\OperationController::getUnfinishedEmailsCsv")
    ->assert('wildcard', '.*');

// Report routes
$app->get('{wildcard}' . $app['api'].'/admin/reports', "Civitours\Controller\Admin\ReportController::getReports")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/reports/{id}/{type}', "Civitours\Controller\Admin\ReportController::getReportById")
    ->assert('wildcard', '.*');
$app->delete('{wildcard}' . $app['api'].'/admin/reports/{id}', "Civitours\Controller\Admin\ReportController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/admin/reports', "Civitours\Controller\Admin\ReportController::createReport")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/admin/reports/types', "Civitours\Controller\Admin\ReportController::getReportTypesList")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('wildcard', '.*');

// User auth endpoints
$app->post('{wildcard}' . $app['api'].'/register', "Civitours\Controller\RegistrationController::register")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/resend', "Civitours\Controller\RegistrationController::resend")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/confirm/{token}', "Civitours\Controller\RegistrationController::confirm")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/login', "Civitours\Controller\AuthenticateController::authenticate")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/login/social', "Civitours\Controller\AuthenticateController::authenticateSocial")
    ->assert('wildcard', '.*');

$app->match('{wildcard}' . $app['api'].'/restore', "Civitours\Controller\AuthenticateController::restore")
    ->assert('wildcard', '.*');
$app->match('{wildcard}' . $app['api'].'/reset_request', "Civitours\Controller\AuthenticateController::resetRequest")
    ->assert('wildcard', '.*');
$app->match('{wildcard}' . $app['api'].'/new_password', "Civitours\Controller\AuthenticateController::newPassword")
    ->assert('wildcard', '.*');

$app->post('{wildcard}' . $app['api'].'/update_password', "Civitours\Controller\AuthenticateController::updatePassword")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/update_account', "Civitours\Controller\AuthenticateController::updateAccount")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/renew_token', "Civitours\Controller\AuthenticateController::renewToken")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');

// Geo data routes
$app->get('{wildcard}' . $app['api'].'/countries', "Civitours\Controller\CountryController::getAll")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/countries/{id}', "Civitours\Controller\CountryController::get")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/cities/{id}', "Civitours\Controller\CityController::get")
    ->assert('id', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/cities/{country}/search/{name}', "Civitours\Controller\CityController::search")
    ->assert('country', '\d+')
    ->assert('wildcard', '.*');

//Destination routes
$app->get('{wildcard}' . $app['api'].'/destinations/country/list', "Civitours\Controller\DestinationController::countryList")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/main/list', "Civitours\Controller\DestinationController::mainList")
    ->assert('wildcard', '.*');

$app->get('{wildcard}' . $app['api'].'/destinations/country/{routeName}', "Civitours\Controller\DestinationController::getDestinations")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/country/{countryCode}/cities', "Civitours\Controller\DestinationController::cityList")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/destinations/city/{countryRoute}/{cityRoute}', "Civitours\Controller\DestinationController::getDestinationCity")
    ->assert('wildcard', '.*');


//Activities routes
$app->get('{wildcard}' . $app['api'].'/activities/featured', "Civitours\Controller\ActivitiesController::featured")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/{idActivity}', "Civitours\Controller\ActivitiesController::getById")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/{idActivity}/related', "Civitours\Controller\ActivitiesController::getRelated")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/categories', "Civitours\Controller\ActivitiesController::getCategories")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/city/{idCity}', "Civitours\Controller\ActivitiesController::getForCity")
    ->assert('idCity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/activities/{countryRoute}/{cityRoute}/{route}', "Civitours\Controller\ActivitiesController::getByRoute")
    ->assert('wildcard', '.*');

//Review routes
$app->get('{wildcard}' . $app['api'].'/reviews/traveller_types', "Civitours\Controller\ReviewController::getTravellerTypes")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::getById")
    ->assert('idReview', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/reviews/city/{idCity}', "Civitours\Controller\ReviewController::getForCity")
    ->assert('idCity', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/reviews/activity/{idActivity}', "Civitours\Controller\ReviewController::getForActivity")
    ->assert('idActivity', '\d+')
    ->assert('wildcard', '.*');
$app->delete('{wildcard}' . $app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::delete")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('idReview', '\d+')
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/reviews', "Civitours\Controller\ReviewController::create")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');
$app->patch('{wildcard}' . $app['api'].'/reviews/{idReview}', "Civitours\Controller\ReviewController::update")
    ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
    ->assert('idReview', '\d+')
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/reviews/user', "Civitours\Controller\ReviewController::getForUser")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');

// Upload routes
$app->post('{wildcard}' . $app['api'].'/upload/image', "Civitours\Controller\UploadController::uploadImage")
    ->assert('wildcard', '.*');

// Language routes
$app->get('{wildcard}' . $app['api'].'/languages',  "Civitours\Controller\LanguageController::getList")
    ->assert('wildcard', '.*');

// Order routes
$app->get('{wildcard}' . $app['api'].'/order/{orderCode}',  "Civitours\Controller\OrderController::get")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/order/create',  "Civitours\Controller\OrderController::create")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/order/update/{orderCode}',  "Civitours\Controller\OrderController::update")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/order/{orderCode}/redsys/pay/{amount}',  "Civitours\Controller\OrderController::payRedsys")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/order/{orderCode}/viewed',  "Civitours\Controller\OrderController::setViewedStatus")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/redsys_notify',  "Civitours\Controller\OrderController::redsysNotification")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/order/{orderCode}/stripe',  "Civitours\Controller\OrderController::payStripe")
    ->assert('wildcard', '.*');

// User routes
$app->get('{wildcard}' . $app['api'].'/user/order',  "Civitours\Controller\OrderController::getForUser")
    ->before("Civitours\Middleware\AuthMiddleware::checkAuthorized")
    ->assert('wildcard', '.*');


// Search routes
$app->get('{wildcard}' . $app['api'].'/search/countries',  "Civitours\Controller\SearchController::getCountries")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/search/cities',  "Civitours\Controller\SearchController::getCities")
    ->assert('wildcard', '.*');
$app->get('{wildcard}' . $app['api'].'/search/activities',  "Civitours\Controller\SearchController::getActivities")
    ->assert('wildcard', '.*');

// Contact routes
$app->post('{wildcard}' . $app['api'].'/contacts/request',  "Civitours\Controller\ContactController::request")
    ->assert('wildcard', '.*');
$app->post('{wildcard}' . $app['api'].'/faq/request',  "Civitours\Controller\ContactController::requestFaq")
    ->assert('wildcard', '.*');