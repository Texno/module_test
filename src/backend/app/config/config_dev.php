<?php

// DEV CONFIGURATION
$app['environment'] = "dev";
$app['debug'] = true;
$app['log.level'] = Monolog\Logger::DEBUG;
$app['server.name'] = 'localhost';
$app['api'] = '/api/v1';

// PARAMETERS
$app['driver'] = 'pdo_pgsql';
$app['host'] = 'localhost';
$app['port'] = '5432';
$app['dbname'] = 'civitours';
$app['user'] = 'local';
$app['password'] = 'local';

// Mailer
$app['mailer.options'] = array(
    'host' => 'localhost',
    'port' => '1025',
    'username' => 'e24c04cd8c2ec8',
    'password' => '39684691d38a6a',
    'encryption' => null,
    'auth_mode' => null
);

$app['base.url'] = 'https://localhost:4200';
$app['api.url'] = 'https://192.168.0.29/civitours/src/backend/api/v1';
$app['image.url'] = 'https://192.168.0.29/civitours/src/backend/public/img/';
$app['image.path'] = __DIR__ . '/../../public/img/';
$app['files.path'] = __DIR__ . '/../../public/files/';
$app['reports.path'] = __DIR__ . '/../reports/';
