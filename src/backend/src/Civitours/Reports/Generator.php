<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:05
 */

namespace Civitours\Reports;

use Civitours\Reports\Handler\AbstractHandler;
use Doctrine\DBAL\Connection;

/**
 * Class Generator
 * @package Civitours\Reports
 */
class Generator
{

    /**
     * @var Connection
     */
    private $db;
    private $reportPath;

    public function __construct(Connection $db, $reportPath) {
        $this->db = $db;
        $this->reportPath = $reportPath;
    }

    /**
     * Generate data and write CSV file
     *
     * @param $type
     * @param $id
     */
    public function generateReport($type, $id) {
        $handler = AbstractHandler::factory($type, $this->db);
        $reportData = $handler->getReportData();

        $filename = $this->reportPath . DIRECTORY_SEPARATOR . $id;

        if(file_exists($filename)) {
            // renew report
            unlink($filename);
        }

        // Write data to file
        $fileHandler = fopen($filename, 'w');
        if (false === $fileHandler) {
            throw new \RuntimeException('Cannot write file: ' . $filename);
        }
        foreach ($reportData as $row) {
            fputcsv($fileHandler, $row);
        }
        fclose($fileHandler);
    }
}
