<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:09
 */

namespace Civitours\Reports\Handler;

use Doctrine\DBAL\Connection;

/**
 * Abstract class for all handlers
 *
 * @package Civitours\Reports\Handler
 */
abstract class AbstractHandler implements HandlerInterface
{
    const SUPPLIERS_TYPE = 1;
    const ACTIVITY_TYPE = 2;
    const CITY_TYPE = 3;
    const COUNTRY_TYPE = 4;

    /**
     * @var Connection
     */
    protected $db = null;

    /**
     * @param Connection $db
     */
    public function __construct(Connection $db) {
        $this->db = $db;
    }

    /** @inheritdoc */
    abstract function getReportData();

    /**
     * Build handler by type
     *
     * @param $type
     * @param Connection $db
     * @return HandlerInterface
     */
    static function factory($type, Connection $db = null) {
        switch ($type) {
            case self::SUPPLIERS_TYPE:
                return new SuppliersHandler($db);
            case self::ACTIVITY_TYPE:
                return new ActivityHandler($db);
            case self::CITY_TYPE:
                return new CityHandler($db);
            case self::COUNTRY_TYPE:
                return new CountryHandler($db);
            default:
                throw new \InvalidArgumentException('Wrong handler type is provided: ' . $type);
        }

    }

    /**
     * Retrieve field names
     *
     * @param $type
     * @return mixed
     */
    static function getFieldsNamesForType($type) {
        switch ($type) {
            case self::SUPPLIERS_TYPE:
                return SuppliersHandler::getFieldNames();
            case self::ACTIVITY_TYPE:
                return ActivityHandler::getFieldNames();
            case self::CITY_TYPE:
                return CityHandler::getFieldNames();
            case self::COUNTRY_TYPE:
                return CountryHandler::getFieldNames();
            default:
                throw new \InvalidArgumentException('Wrong handler type is provided: ' . $type);
        }
    }
}
