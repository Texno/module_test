<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:23
 */

namespace Civitours\Reports\Handler;

/**
 * Class SuppliersHandler
 * @package Civitours\Reports\Handler
 */
class SuppliersHandler extends AbstractHandler
{
    /** @inheritdoc */
    public static function getFieldNames()
    {
        return [
            'id'            => 'Supplier ID',
            'name'          => 'Supplier name',
            'created_at'    => 'Date of creation',
            'activities'    => 'Activities of supplier',
            'amount'        => 'Income amount'
        ];
    }


    /** @inheritdoc */
    public function getReportData() {
        $data = $this->getStatsInfo();
        $info = $this->getSupplierInfo();

        foreach ($info as &$supplier) {
            if (array_key_exists($supplier['id'], $data)) {
                $supplier = array_merge($supplier, $data[$supplier['id']]);
            } else {
                $supplier = array_merge($supplier, [
                    'amount'        => '0.00',
                ]);
            }
        }

        return $info;
    }

    /**
     * Retrieve activity info
     *
     * @return array
     */
    private function getSupplierInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_suppliers.id',
                't_suppliers.name',
                'CAST(EXTRACT(epoch FROM t_suppliers.registered_at) AS integer) as registered_at',
                'COUNT(DISTINCT(t_activities.id)) AS activities'
            ])
            ->from('suppliers', 't_suppliers')
            ->innerJoin('t_suppliers', 'activity_suppliers', 't_activity_suppliers', 't_suppliers.id = t_activity_suppliers.supplier')
            ->innerJoin('t_activity_suppliers', 'activities', 't_activities', 't_activities.id = t_activity_suppliers.activity')
            ->groupBy([
                't_suppliers.id',
            ])
            ->orderBy('t_suppliers.id', 'ASC');

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get statistics
     *
     * @return array
     */
    private function getStatsInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_suppliers.id',
                'SUM(t_order_activities_prices.price) AS amount',
            ])
            ->from('order_activities', 't_order_activities')
            ->innerJoin('t_order_activities', 'activities', 't_activities', 't_activities.id = t_order_activities.activity')
            ->innerJoin('t_activities', 'activity_suppliers', 't_activity_suppliers', 't_activities.id = t_activity_suppliers.activity')
            ->innerJoin('t_activity_suppliers', 'suppliers', 't_suppliers', 't_suppliers.id = t_activity_suppliers.supplier')
            ->leftJoin('t_order_activities', 'orders', 't_orders', 't_orders.id = t_order_activities.order')
            ->leftJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities.id = t_order_activities_prices.order_activity')
            ->where('t_orders.is_payed = true')
            ->groupBy('t_suppliers.id');

        $data = $queryBuilder->execute()->fetchAll();
        $result = [];
        foreach ($data as $row) {
            $id = $row['id'];
            unset($row['id']);
            $result[$id] = $row;
        }
        return $result;
    }
}
