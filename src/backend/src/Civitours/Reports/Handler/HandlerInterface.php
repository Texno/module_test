<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:06
 */

namespace Civitours\Reports\Handler;

/**
 * Interface to all report handlers
 *
 * @package Civitours\Reports\Handler
 */
interface HandlerInterface
{
    /**
     * Get array of report data
     *
     * @return mixed
     */
    public function getReportData();

    /**
     * Get array of field names
     *
     * @return mixed
     */
    public static function getFieldNames();

}
