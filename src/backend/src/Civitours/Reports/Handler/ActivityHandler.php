<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:24
 */

namespace Civitours\Reports\Handler;

/**
 * Class ActivityHandler
 * @package Civitours\Reports\Handler
 */
class ActivityHandler extends AbstractHandler
{
    /** @inheritdoc */
    public static function getFieldNames()
    {
        return [
            'id'            => 'Activity ID',
            'name'          => 'Activity name',
            'created_at'    => 'Date of creation',
            'city'          => 'Activity city',
            'suppliers'     => 'Activity suppliers',
            'amount'        => 'Income amount',
            'visitors'      => 'Total visitors',
            'adults'        => 'Total adults',
            'children'      => 'Total children'
        ];
    }


    /** @inheritdoc */
    public function getReportData() {
        $data = $this->getStatsInfo();
        $activityInfo = $this->getActivityInfo();

        foreach ($activityInfo as &$activity) {
            if (array_key_exists($activity['id'], $data)) {
                $activity = array_merge($activity, $data[$activity['id']]);
            } else {
                $activity = array_merge($activity, [
                    'amount'    => '0.00',
                    'visitors'  => 0,
                    'adults'    => 0,
                    'children'  => 0
                ]);
            }
        }

        return $activityInfo;
    }

    /**
     * Retrieve activity info
     *
     * @return array
     */
    private function getActivityInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_activities.id',
                't_activities.name',
                'CAST(EXTRACT(epoch FROM t_activities.created_at) AS integer) as created_at',
                't_cities.name AS city_name',
                "array_to_string(array_agg(t_suppliers.name), ';') AS suppliers"
            ])
            ->from('activities', 't_activities')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_cities.id = t_activities.city')
            ->leftJoin('t_activities', 'activity_suppliers', 't_activity_suppliers', 't_activities.id = t_activity_suppliers.activity')
            ->leftJoin('t_activity_suppliers', 'suppliers', 't_suppliers', 't_suppliers.id = t_activity_suppliers.supplier')
            ->groupBy([
                't_activities.id',
                't_cities.name'
            ])
            ->orderBy('t_activities.id', 'ASC');

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get statistics
     *
     * @return array
     */
    private function getStatsInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_order_activities.activity',
                'SUM(t_order_activities_prices.price) AS amount',
                'SUM(t_order_activities_prices.amount) AS visitors',
                'SUM(CASE WHEN t_person_types.is_adult = true THEN t_order_activities_prices.amount ELSE 0 END) AS adults',
                'SUM(CASE WHEN t_person_types.is_adult = false THEN t_order_activities_prices.amount ELSE 0 END) AS children'
            ])
            ->from('order_activities', 't_order_activities')
            ->leftJoin('t_order_activities', 'orders', 't_orders', 't_orders.id = t_order_activities.order')
            ->leftJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities.id = t_order_activities_prices.order_activity')
            ->leftJoin('t_order_activities_prices', 'person_types', 't_person_types', 't_person_types.id = t_order_activities_prices.person_type')
            ->where('t_orders.is_payed = true')
            ->groupBy('t_order_activities.activity');

        $data = $queryBuilder->execute()->fetchAll();
        $result = [];
        foreach ($data as $row) {
            $id = $row['activity'];
            unset($row['activity']);
            $result[$id] = $row;
        }
        return $result;
    }
}
