<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 01.08.18
 * Time: 13:24
 */

namespace Civitours\Reports\Handler;


class CityHandler extends AbstractHandler
{
    /** @inheritdoc */
    public static function getFieldNames()
    {
        return [
            'id'            => 'City ID',
            'name'          => 'City name',
            'country'       => 'Country name',
            'activities'    => 'Activities in city',
            'amount'        => 'Income amount',
            'visitors'      => 'Total visitors'
        ];
    }


    /** @inheritdoc */
    public function getReportData() {
        $data = $this->getStatsInfo();
        $info = $this->getCityInfo();

        foreach ($info as &$city) {
            if (array_key_exists($city['id'], $data)) {
                $city = array_merge($city, $data[$city['id']]);
            } else {
                $city = array_merge($city, [
                    'amount'        => '0.00',
                    'visitors'      => 0,
                ]);
            }
        }

        return $info;
    }

    /**
     * Retrieve city info
     *
     * @return array
     */
    private function getCityInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_cities.id',
                't_cities.name',
                't_countries.name AS country',
                'COUNT(DISTINCT(t_activities.id)) AS activities'
            ])
            ->from('activities', 't_activities')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_cities.id = t_activities.city')
            ->innerJoin('t_activities', 'countries', 't_countries', 't_countries.id = t_activities.country')
            ->groupBy([
                't_cities.id',
                't_countries.name',
            ])
            ->orderBy('t_cities.id', 'ASC');

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get statistics
     *
     * @return array
     */
    private function getStatsInfo() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_activities.city',
                'SUM(t_order_activities_prices.price) AS amount',
                'SUM(t_order_activities_prices.amount) AS visitors'
            ])
            ->from('order_activities', 't_order_activities')
            ->innerJoin('t_order_activities', 'activities', 't_activities', 't_activities.id = t_order_activities.activity')
            ->leftJoin('t_order_activities', 'orders', 't_orders', 't_orders.id = t_order_activities.order')
            ->leftJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities.id = t_order_activities_prices.order_activity')
            ->where('t_orders.is_payed = true')
            ->groupBy('t_activities.city');

        $data = $queryBuilder->execute()->fetchAll();
        $result = [];
        foreach ($data as $row) {
            $id = $row['city'];
            unset($row['city']);
            $result[$id] = $row;
        }
        return $result;
    }
}
