<?php

namespace Civitours\Controller;

use Civitours\Service\GeoDataService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to operate with cities
 *
 * Class CityController
 * @package Civitours\Controller
 */
class CityController
{

    /**
     * Retrieve one country information by id
     *
     * @param Application $app
     * @param $id
     * @return JsonResponse
     */
    public function get(Application $app, $id) {
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        $country = $geoDataService->getCity($id);
        if (false === $country) {
            throw new NotFoundHttpException("City not found");
        }
        return new JsonResponse($country);
    }

    /**
     * Get cities by country and name search string
     *
     * @param Application $app
     * @param $country
     * @param $name
     * @return JsonResponse
     */
    public function search(Application $app, $country, $name) {
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        return new JsonResponse($geoDataService->searchCity($country, $name));
    }
}
