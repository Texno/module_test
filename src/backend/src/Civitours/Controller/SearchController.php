<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.06.18
 * Time: 12:25
 */

namespace Civitours\Controller;


use Civitours\Service\ActivityService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController
{

    /**
     * Perform activities search
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getActivities(Application $app, Request $request) {
        $queryString = $request->query->get('q');
        $dates = $request->query->get('d');
        $times = $request->query->get('t');
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $queryActivities = $activityService->getFilteredByQuery($queryString);
        if (empty($dates) && empty($times)) {
            return new JsonResponse($activityService->getFilteredActivities($queryActivities));
        } else {
            return new JsonResponse($activityService->searchByDatesAndTime($dates, $times, null, $queryActivities));
        }
    }

    /**
     * Perform cities search
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function getCities(Application $app, Request $request) {
        $queryString = $request->query->get('q');
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCityList(null, true, $queryString));
    }

    /**
     * Perform countries search
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function getCountries(Application $app, Request $request) {
        $queryString = $request->query->get('q');
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCountriesList(true, $queryString));
    }
}
