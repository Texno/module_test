<?php

namespace Civitours\Controller;

use Civitours\Service\ActivityService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DestinationController
{

    /**
     * Get all available countries with activities
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function countryList(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCountriesList());
    }

    /**
     * Get all available countries with activities
     *
     * @param Application $app
     * @param $countryCode
     * @return JsonResponse
     */
    public function cityList(Application $app, $countryCode) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCityList($countryCode));
    }

    /**
     * Get main destinations list
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function mainList(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getDestinations());
    }

    /**
     * Get destination info by country
     *
     * @param Application $app
     * @param $routeName
     * @return JsonResponse
     */
    public function getDestinations(Application $app, $routeName) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $result = $activityService->getCountryInfo($routeName);
        if(!$result) {
            throw new NotFoundHttpException('Country not found');
        }
        $result['destinations'] = $activityService->getDestinations($result['code']);
        return new JsonResponse($result);
    }

    /**
     * Get destination info by city
     *
     * @param Application $app
     * @param $countryRoute
     * @param $cityRoute
     * @return JsonResponse
     */
    public function getDestinationCity(Application $app, $countryRoute, $cityRoute) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getDestination($countryRoute, $cityRoute));
    }
}
