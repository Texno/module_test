<?php

namespace Civitours\Controller;

use Civitours\Service\ActivityService;
use Civitours\Service\SupplierService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ActivitiesController
{

    /**
     * Retrieve list of featured activities
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function featured(Application $app, Request $request) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $limit = 6;
        if($request->query->has('l')) {
            $limit = intval($request->query->get('l'));
        }
        $offset = 0;
        if($request->query->has('s')) {
            $offset = intval($request->query->get('s'));
        }
        $country = null;
        if($request->query->has('c')) {
            $country = $request->query->get('c');
        }
        return new JsonResponse($activityService->getFeaturedActivities($offset, $limit, $country));
    }

    /**
     * Retrieve full activity info
     *
     * @param Application $app
     * @param $idActivity
     * @return JsonResponse|NotFoundHttpException
     */
    public function getById(Application $app, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activity = $activityService->getActivity($idActivity);
        if(!$activity) {
            throw new NotFoundHttpException('Activity not found');
        }
        /** @var SupplierService $suppliersService */
        $suppliersService = $app['supplier.service'];
        $activity['schedule'] = $activityService->getActivitySchedule($idActivity);
        $activity['gallery'] = $activityService->getGallery($idActivity);
        $activity['types'] = $activityService->getTypes($idActivity);
        $activity['suppliers'] = $suppliersService->getForActivity($idActivity);
        return new JsonResponse($activity);
    }

    /**
     * Retrieve activity by id
     *
     * @param Application $app
     * @param $countryRoute
     * @param $cityRoute
     * @param $route
     * @return JsonResponse|NotFoundHttpException
     */
    public function getByRoute(Application $app, $countryRoute, $cityRoute, $route) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activity = $activityService->getFilteredByRoute($countryRoute, $cityRoute, $route);
        if(!$activity) {
            throw new NotFoundHttpException('Activity not found');
        }
        return $this->getById($app, $activity);
    }

    /**
     * Retrieve list of related activities
     *
     * @param Application $app
     * @param Request $request
     * @param $idActivity
     * @return JsonResponse
     */
    public function getRelated(Application $app, Request $request, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $limit = 3;
        if($request->query->has('l')) {
            $limit = intval($request->query->get('l'));
        }
        return new JsonResponse($activityService->getRelatedActivities($idActivity, $limit));
    }

    /**
     * Retrieve list of categories
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getCategories(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getCategoriesList());
    }

    /**
     * Get activities for city in short format
     *
     * @param Application $app
     * @param Request $request
     * @param $idCity
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getForCity(Application $app, Request $request, $idCity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $dates = $request->query->get('d');
        $times = $request->query->get('t');
        if (empty($dates) && empty($times)) {
            return new JsonResponse($activityService->getActivitiesForCity($idCity));
        } else {
            return new JsonResponse($activityService->searchByDatesAndTime($dates, $times, $idCity));
        }
    }

}
