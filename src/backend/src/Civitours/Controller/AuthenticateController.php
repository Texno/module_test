<?php

namespace Civitours\Controller;

use Civitours\Entity\SocialLoginData;
use Civitours\Form\AccountFormType;
use Civitours\Form\NewPasswordFormType;
use Civitours\Form\SocialLoginType;
use Civitours\Service\UserService;
use Rhumsaa\Uuid\Uuid;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthenticateController
{

    use FormProcessTrait;

    /**
     * login user
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Application $app, Request $request)
    {

        $rawHeader = $request->headers->get('Authorization');
        if (strpos($rawHeader, 'Basic ') === false) {
            throw new BadRequestHttpException('Credenciales erróneas');
        }
        $token = str_replace('Basic ', '', $rawHeader);
        $decodedToken = base64_decode($token);
        list ($username, $password) = explode(':', $decodedToken);

        if (!$username || !$password) {
            throw new BadRequestHttpException('Credenciales erróneas');
        }

        /** @var UserService $userService */
        $userService = $app['user.service'];
        $user = $userService->login($username, $password);

        if (!$user) {
            throw new AccessDeniedHttpException('Autenticación fallida');
        }

        if (!$user['email_confirmed']) {
            throw new AccessDeniedHttpException('Confirmar el correo electrónico');
        }

        return new JsonResponse(['token' => $this->generateToken($app, $user)]);
    }

    /**
     * Renew user token
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function renewToken(Application $app, Request $request) {
        /** @var UserService $userService */
        $userService = $app['user.service'];
        $token = $request->attributes->get('token');
        $idUser = $token->data->userId;
        $user = $userService->getUserById($idUser);
        return new JsonResponse(['token' => $this->generateToken($app, $user)]);
    }

    /**
     * login user by social
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticateSocial(Application $app, Request $request)
    {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(SocialLoginType::class, null, [
            'social_service'      => $app['social.service']
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var SocialLoginData $data */
            $data = $form->getViewData();
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $user = $userService->getUserBySocialId($data->provider, $data->id);
            if (false === $user) {
                $userService->createUserFromSocialData($data);
                $user = $userService->getUserBySocialId($data->provider, $data->id);
            } else {
                $userService->updateUserFromSocialData($data);
            }
            return new JsonResponse(['token' => $this->generateToken($app, $user)]);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Generate token for user
     *
     * @param Application $app
     * @param $user
     * @return string
     */
    private function generateToken(Application $app, $user) {
        $tokenId    = Uuid::uuid4();
        $issuedAt   = time();
        $notBefore  = $issuedAt;
        $expire     = $notBefore + 604800; // Adding 1 week
        $serverName = $app['server.name'];

        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'userId'            => $user['id'],             // userid from the users table
                'userName'          => $user['name'],           // User name
                'userSurname'       => $user['surname'],        // User surname
                'userCountry'       => $user['country'],        // User country
                'userCountryName'   => $user['country_name'],   // User country
                'userCity'          => $user['city'],           // User city
                'userCityName'      => $user['city_name'],      // User city
                'fullName'          => $user['name'] . ' ' . $user['surname'],
                'phone'             => $user['phone'],
                'email'             => $user['email'],
                'isAdmin'           => $user['is_admin']        // Has admin role
            ]
        ];
        // Get the secret key for signing the JWT from an environment variable
        $secretKey = base64_decode($app['secret']);

        $algorithm = $app['algorithm'];
        // Sign the JWT with the secret key
        return JWT::encode(
            $data,
            $secretKey,
            $algorithm
        );
    }

    /**
     * Request password reset
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     */
    public function resetRequest(Application $app, Request $request) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || empty($submittedData['email'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        /** @var UserService $userService */
        $userService = $app['user.service'];
        if (!$userService->getUserByEmail($submittedData['email'])) {
            throw new NotFoundHttpException('usuario no encontrado');
        }
        $userService->resetPassword($submittedData['email']);

        return new JsonResponse([
            'message' => 'Email successfully sent. Follow instructions to reset password'
        ]);
    }

    /**
     * Set new password
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function newPassword(Application $app, Request $request)
    {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(NewPasswordFormType::class, null, [
            'user_service'      => $app['user.service']
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $userService->setNewPassword($form->getViewData());
            return new Response(null, Response::HTTP_NO_CONTENT);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Update user password
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     */
    public function updatePassword(Application $app, Request $request) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $token = $request->attributes->get('token');
        $idUser = $token->data->userId;

        $oldPassword = $submittedData['oldPassword'];
        $newPassword = $submittedData['newPassword'];
        $passwordConfirm = $submittedData['passwordConfirm'];

        /** @var UserService $userService */
        $userService = $app['user.service'];

        if (!$userService->checkPassword($idUser, $oldPassword)) {
            throw new BadRequestHttpException("Antigua contraseña errónea");
        }

        if ($newPassword !== $passwordConfirm) {
            throw new BadRequestHttpException("contraseñas no coinciden");
        }
        $userService->updatePassword($idUser, $newPassword);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Update user account data
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function updateAccount(Application $app, Request $request) {

        $token = $request->attributes->get('token');
        $idUser = $token->data->userId;

        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(AccountFormType::class, null, [
            'user_service'      => $app['user.service'],
            'geo_data_service'  => $app['geo_data.service'],
            'user_id'           => $idUser
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $userService->updateAccountData($idUser, $form->getViewData());
            return new Response(null, Response::HTTP_CREATED);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

}
