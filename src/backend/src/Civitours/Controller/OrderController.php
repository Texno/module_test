<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.18
 * Time: 11:19
 */

namespace Civitours\Controller;

use Civitours\Form\OrderFormType;
use Civitours\Service\MailService;
use Civitours\Service\OrderService;
use Civitours\Service\PaymentService;
use Civitours\Service\SupplierService;
use Civitours\Service\UserService;
use Silex\Application;
use Stripe\Charge;
use Stripe\Stripe;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController
{
    use FormProcessTrait;

    /**
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(Application $app, Request $request) {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(OrderFormType::class);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var OrderService $orderService */
            $orderService = $app['order.service'];
            $result = $orderService->create($form->getViewData());
            return new JsonResponse($result, Response::HTTP_CREATED);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Mark order as viewed
     *
     * @param Application $app
     * @param $orderCode
     * @return Response
     */
    public function setViewedStatus(Application $app, $orderCode) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $orderService->setViewedStatus($orderCode);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Perform redsys check
     *
     * @param Application $app
     * @return Response
     * @throws \Exception
     */
    public function redsysNotification(Application $app) {
        $app['logger']->notice("Received notification from redsys");
        $paymentData = [
            'signatureVersion'      => $_POST['Ds_SignatureVersion'],
            'merchantParameters'    => $_POST['Ds_MerchantParameters'],
            'signature'             => $_POST['Ds_Signature']
        ];
        $app['logger']->notice(print_r($paymentData, true));

        /** @var PaymentService $paymentService */
        $paymentService = $app['payment.service'];
        $result = $paymentService->verifyRedsysPayment($paymentData);
        $check = !$result['error'] && intval($result['Ds_Response']) < intval('0100');
        if(!$check) {
            $app['logger']->warning('Failed to verify redsys payment');
            $app['logger']->warning(print_r($result, true));
            return new Response(null, Response::HTTP_OK);
        }

        $app['logger']->notice("Succesfully verified - updating order");

        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $orderService->updateRedsysStatus($result['Ds_Order']);
        $orderCode = $orderService->getOrderCodeByTransactionId($result['Ds_Order']);
        $this->notifyWithMail($app, $orderCode);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * Update order status
     *
     * @param Application $app
     * @param Request $request
     * @param $orderCode
     * @return Response
     * @throws \Exception
     */
    public function update(Application $app, Request $request, $orderCode) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || empty($submittedData['gateway'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $order = $orderService->get($orderCode);
        if (!$order) {
            throw new BadRequestHttpException("Order not found");
        }

        /** @var PaymentService $paymentService */
        $paymentService = $app['payment.service'];
        $result = $paymentService->verifyPayment($submittedData, $order);
        if (!$result) {
            throw new BadRequestHttpException('Cannot verify payment');
        }

        $result = $orderService->update($orderCode, $submittedData);
        if (!$result) {
            throw new BadRequestHttpException("Wrong order code data is provided");
        }
        $this->notifyWithMail($app, $orderCode);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Send all necessary mails
     *
     * @param Application $app
     * @param $orderCode
     */
    private function notifyWithMail(Application $app, $orderCode) {
        /** @var UserService $userService */
        $userService = $app['user.service'];
        /** @var MailService $mailService */
        $mailService = $app['mail.service'];
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];

        $admins = $userService->getAdminEmailsList();
        $orderData = $orderService->getMailConfirmData($orderCode);

        if(array_key_exists(0, $orderData)) {
            try {
                $mailService->sendAdminPaymentConfirmEmail($admins, $orderData);
                $mailService->sendUserPaymentConfirmEmail($orderData[0]['email'], $orderData);
                foreach ($orderData as $orderItem) {
                    $suppliers = $orderItem['suppliers'];
                    $supplierMails = [];
                    foreach ($suppliers as $supplier) {
                        array_push($supplierMails, $supplier['email']);
                    }
                    $mailService->sendSupplierPaymentConfirmEmail($supplierMails, [$orderItem]);
                }

            } catch (\Exception $e) {
                $app['logger']->warning("Cannot send email: " . $e->getMessage());
            }
        } else {
            $app['logger']->warning("No order data is found for order ${orderCode}");
        }
    }

    /**
     * Update order status
     *
     * @param Application $app
     * @param $orderCode
     * @return Response
     */
    public function get(Application $app, $orderCode) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $order = $orderService->get($orderCode);
        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }
        foreach ($order as &$item) {
            $item['prices'] = $orderService->getOrderActivityPrices($item['order_activity']);
            unset($item['order_activity']);
        }
        return new JsonResponse($order);
    }

    /**
     * Get order list for user
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function getForUser(Application $app, Request $request) {
        $token = $request->attributes->get('token');
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $orders = $orderService->getForUser($token->data->email);
        foreach ($orders as &$item) {
            $item['prices'] = $orderService->getOrderActivityPrices($item['order_activity']);
            unset($item['order_activity']);
        }
        return new JsonResponse($orders);
    }

    /**
     * @param Application $app
     * @param $orderCode
     * @param $amount
     * @return Response
     * @throws \Exception
     */
    public function payRedsys(Application $app, $orderCode, $amount) {
        /** @var PaymentService $paymentService */
        $paymentService = $app['payment.service'];
        $code = $paymentService->generateOrderCodeForRedSys();
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $order = $orderService->get($orderCode);
        if (empty($order[0]) || $order[0]['is_payed'] == 'true') {
            throw new BadRequestHttpException('Cannot generate button for order');
        }
        $orderService->updateTransactionId($orderCode, $code);
        return new JsonResponse(['button' => $paymentService->performRedsysWebPayment($orderCode, $code, $order[0]['total_amount'])], Response::HTTP_OK);
    }

    /**
     * Perform stripe payment
     *
     * @param Application $app
     * @param $orderCode
     * @param Request $request
     * @return Response
     */
    public function payStripe(Application $app, $orderCode, Request $request) {

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || empty($submittedData['token'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $order = $orderService->get($orderCode);
        if (empty($order[0]) || $order[0]['is_payed'] == 'true') {
            throw new BadRequestHttpException('Cannot find order');
        }

        $app['logger']->notice("Start stripe payment - ${submittedData['token']}");

        Stripe::setApiKey($app['stripe.config']['secretKey']);

        $charge = Charge::create(array(
            "amount"        => $order[0]['total_amount'] * 100,
            "currency"      => "eur",
            "source"        => $submittedData['token'],
            "receipt_email" => $order[0]['email']
        ));

        if ($charge['status'] !== 'succeeded') {
            $app['logger']->warn('Cannot charge payment!');
            $app['logger']->warn(print_r($charge, true));
            throw new BadRequestHttpException('Cannot process charge for the payment');
        }

        $app['logger']->notice("Successfully charged - ${submittedData['token']}");
        $orderService->update($orderCode, [
            'gateway'           => 'stripe',
            'transactionId'     => $charge['id']
        ]);
        $this->notifyWithMail($app, $orderCode);

        return new Response(null, Response::HTTP_ACCEPTED);

    }
}