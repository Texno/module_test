<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.07.18
 * Time: 13:35
 */

namespace Civitours\Controller;

use Civitours\Service\ContactService;
use Civitours\Service\MailService;
use Civitours\Service\UploadService;
use Civitours\Service\UserService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ContactController
{
    /**
     * Retrieve one country information by id
     *
     * @param Application $app
     * @return Response
     */
    public function request(Application $app) {
        if (!array_key_exists('data', $_POST)) {
            throw new BadRequestHttpException('Wrong data is provided');
        }
        $submittedData = json_decode($_POST['data'], true);
        if(empty($submittedData)) {
            throw new BadRequestHttpException('Wrong data is provided');
        }
        $attached = null;
        if(!empty($_FILES['file'])) {
            /** @var UploadService $uploadService */
            $uploadService = $app['upload.service'];
            $submittedData['file'] = $uploadService->uploadFile();
            $attached = $app['files.path'] . DIRECTORY_SEPARATOR . $submittedData['file'];
        }

        /** @var ContactService $contactService */
        $contactService = $app['contact.service'];
        $contactService->create($submittedData);

        /** @var UserService $userService */
        $userService = $app['user.service'];
        /** @var MailService $mailService */
        $mailService = $app['mail.service'];
        $admins = $userService->getAdminEmailsList();
        $mailService->sendAdminContactRequest($admins, $submittedData, $attached);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }


    /**
     * Retrieve one country information by id
     *
     * @param Application $app
     * @return Response
     */
    public function requestFaq(Application $app) {
        if (!array_key_exists('data', $_POST)) {
           throw new BadRequestHttpException('Wrong data is provided');
        }
        $submittedData = json_decode($_POST['data'], true);
        if(empty($submittedData)) {
            throw new BadRequestHttpException('Wrong data is provided');
        }
        if(!empty($_FILES['file'])) {
            /** @var UploadService $uploadService */
            $uploadService = $app['upload.service'];
            $submittedData['file'] = $uploadService->uploadFile();
        }

        /** @var ContactService $contactService */
        $contactService = $app['contact.service'];
        $contactService->createFaq($submittedData);

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
