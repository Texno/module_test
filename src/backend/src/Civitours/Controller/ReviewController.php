<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.05.18
 * Time: 11:32
 */

namespace Civitours\Controller;


use Civitours\Entity\ReviewData;
use Civitours\Form\ReviewFormType;
use Civitours\Service\OrderService;
use Civitours\Service\ReviewService;
use Civitours\Service\UserService;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ReviewController
{
    use FormProcessTrait;

    /**
     * Retrieve all available traveller types
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getTravellerTypes(Application $app) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->getTravellerTypes());
    }

    /**
     * Retrive one review by id
     *
     * @param Application $app
     * @param $idReview
     * @return JsonResponse
     */
    public function getById(Application $app, $idReview) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $review = $reviewService->getReviewById($idReview);
        if(!$review) {
            throw new NotFoundHttpException('Review not found');
        }
        return new JsonResponse($review);
    }

    /**
     * Retrieve reviews list for city
     *
     * @param Application $app
     * @param $idCity
     * @return JsonResponse
     */
    public function getForCity(Application $app, $idCity) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->getReviewsForCity($idCity));
    }

    /**
     * Retrieve reviews list for activity
     *
     * @param Application $app
     * @param Request $request
     * @param $idActivity
     * @return JsonResponse
     */
    public function getForActivity(Application $app, Request $request, $idActivity) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $limit = 3;
        if($request->query->has('all')) {
            $limit = null;
        }
        return new JsonResponse($reviewService->getReviewsForActivity($idActivity, $limit));
    }

    /**
     * Get review list for activity for admin
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getForUser(Application $app, Request $request) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $token = $request->attributes->get('token');
        $idUser = $token->data->userId;
        return new JsonResponse($reviewService->getReviewsForUser($idUser));
    }

    /**
     * Delete review
     *
     * @param Application $app
     * @param Request $request
     * @param $idReview
     * @return Response
     */
    public function delete(Application $app, Request $request, $idReview) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $review = $reviewService->getReviewById($idReview);
        if(!$review) {
            throw new NotFoundHttpException('Review not found');
        }
        $token = $request->attributes->get('token');
        if (!$token->data->isAdmin && !$reviewService->isUserHasOwnedReview($token->data->userId, $idReview)) {
            throw new UnauthorizedHttpException(null, 'Access denied');
        }
        $reviewService->deleteReview($idReview);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Create new review
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Application $app, Request $request) {
        return $this->processForm($app, $request);
    }

    /**
     * Update existing review
     *
     * @param Application $app
     * @param Request $request
     * @param $idReview
     * @return JsonResponse
     */
    public function update(Application $app, Request $request, $idReview) {
        return $this->processForm($app, $request, $idReview);
    }

    /**
     * Process review form
     *
     * @param Application $app
     * @param Request $request
     * @param null $idReview
     * @return Response
     */
    private function processForm(Application $app, Request $request, $idReview = null) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];

        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(ReviewFormType::class, null, [
            'review_service'      => $reviewService,
            'order_service'       => $orderService
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var ReviewData $formData */
            $formData = $form->getViewData();
            $token = $request->attributes->get('token');
            $idUser = $token->data->userId;
            if (!$token->data->isAdmin || !$formData->user_name) {
                // Fill review by user data

                /** @var UserService $userService */
                $userService = $app['user.service'];
                $userData = $userService->getUserById($idUser);
                $formData->country_id  = $userData['country'];
                $formData->city_id  = $userData['city'];
                $formData->user_name  = $userData['name'] . ' ' . $userData['surname'];
            }

            $formData->ip = $this->getUserIp();
            $formData->user = $idUser;

            if ($idReview) {
                $reviewService->update($idReview, $formData);
                return new Response(null, Response::HTTP_NO_CONTENT);
            } else {
                $reviewService->create($formData);
                return new Response(null, Response::HTTP_CREATED);
            }
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Retrieve user ip
     *
     * @return int
     */
    private function getUserIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return ip2long($ip);
    }

}