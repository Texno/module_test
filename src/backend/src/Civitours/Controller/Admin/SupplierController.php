<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.18
 * Time: 14:44
 */

namespace Civitours\Controller\Admin;

use Civitours\Controller\FormProcessTrait;
use Civitours\Entity\SupplierData;
use Civitours\Form\SupplierFormType;
use Civitours\Service\SupplierService;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller to manage suppliers
 *
 * Class SuppliersController
 * @package Civitours\Controller\Admin
 */
class SupplierController
{

    use FormProcessTrait;

    /**
     * Retrieve list of suppliers
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getList(Application $app) {
        /** @var SupplierService $supplierService */
        $supplierService = $app['supplier.service'];
        return new JsonResponse($supplierService->getList());
    }

    /**
     * Get one supplier by id
     *
     * @param Application $app
     * @param $id
     * @return JsonResponse
     */
    public function getById(Application $app, $id) {
        /** @var SupplierService $supplierService */
        $supplierService = $app['supplier.service'];
        $supplier = $supplierService->getById($id);
        if(!$supplier) {
            throw new NotFoundHttpException('Supplier not found');
        }
        return new JsonResponse($supplier);
    }

    /**
     * Create new supplier
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function create(Application $app, Request $request) {
        return $this->processForm($app, $request);
    }

    /**
     * Update existing supplier
     *
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return JsonResponse|Response
     */
    public function update(Application $app, Request $request, $id) {
        return $this->processForm($app, $request, $id);
    }

    /**
     * Process supplier form
     *
     * @param Application $app
     * @param Request $request
     * @param null $idSupplier
     * @return JsonResponse|Response
     */
    private function processForm(Application $app, Request $request, $idSupplier = null) {
        /** @var SupplierService $reviewService */
        $supplierService = $app['supplier.service'];

        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(SupplierFormType::class);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var SupplierData $formData */
            $formData = $form->getViewData();
            if ($idSupplier) {
                $supplierService->update($idSupplier, $formData);
                return new Response(null, Response::HTTP_NO_CONTENT);
            } else {
                $supplierService->create($formData);
                return new Response(null, Response::HTTP_CREATED);
            }
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete supplier
     *
     * @param Application $app
     * @param $id
     * @return Response
     */
    public function delete(Application $app, $id) {
        /** @var SupplierService $reviewService */
        $supplierService = $app['supplier.service'];
        $supplierService->delete($id);
        return new Response(null, Response::HTTP_CREATED);
    }

}
