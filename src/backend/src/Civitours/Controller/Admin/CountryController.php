<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.05.18
 * Time: 14:32
 */

namespace Civitours\Controller\Admin;

use Civitours\Service\GeoDataService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Admin country controller
 *
 * Class CountryController
 * @package Civitours\Controller\Admin
 */
class CountryController
{
    /**
     * Update country from request
     *
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return Response
     */
    public function update(Request $request, Application $app, $id) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        $country = $geoDataService->getCountry($id);
        if (false === $country) {
            throw new NotFoundHttpException("Country not found");
        }
        $error = $geoDataService->updateCountry($id, $submittedData, $country);
        if ($error) {
            throw new ServiceUnavailableHttpException(10, $error);
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}