<?php

namespace Civitours\Controller\Admin;

use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class Admin implements ControllerProviderInterface {

    public function connect(Application $app)
    {
        $admin = $app['controllers_factory'];

        $admin->get('/activities', "Civitours\Controller\Admin\ActivitiesController::getList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/activities', "Civitours\Controller\Admin\ActivitiesController::create")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::getById")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->patch('/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->get('/destinations/country/list', "Civitours\Controller\Admin\DestinationController::countryList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/destinations/country/{countryCode}/cities', "Civitours\Controller\Admin\DestinationController::cityList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/activities/{idActivity}/set/draft', "Civitours\Controller\Admin\ActivitiesController::setDraftStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->delete('/activities/{idActivity}', "Civitours\Controller\Admin\ActivitiesController::delete")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->post('/activities/{idActivity}/clear/draft', "Civitours\Controller\Admin\ActivitiesController::clearDraftStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->patch('/countries/{id}', "Civitours\Controller\Admin\CountryController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->patch('/cities/{id}', "Civitours\Controller\Admin\CityController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->get('/reviews/activity/{idActivity}', "Civitours\Controller\Admin\ReviewController::getForActivity")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idActivity', '\d+');

        $admin->post('/reviews/{idReview}/set/approve', "Civitours\Controller\Admin\ReviewController::setApprovedStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idReview', '\d+');

        $admin->post('/reviews/{idReview}/clear/approve', "Civitours\Controller\Admin\ReviewController::clearApprovedStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('idReview', '\d+');

        $admin->get('/suppliers', "Civitours\Controller\Admin\SupplierController::getList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::getById")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->post('/suppliers', "Civitours\Controller\Admin\SupplierController::create")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->patch('/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::update")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->delete('/suppliers/{id}', "Civitours\Controller\Admin\SupplierController::delete")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin")
            ->assert('id', '\d+');

        $admin->get('/operations', "Civitours\Controller\Admin\OperationController::getList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/operations/{id}', "Civitours\Controller\Admin\OperationController::getById")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->delete('/operations/{id}', "Civitours\Controller\Admin\OperationController::delete")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/order/{code}/status/{status}', "Civitours\Controller\Admin\OperationController::updateOrderStatus")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/operations/{id}/note', "Civitours\Controller\Admin\OperationController::updateAdminNote")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/reports', "Civitours\Controller\Admin\ReportController::getReports")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->post('/reports', "Civitours\Controller\Admin\ReportController::createReport")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/reports/types', "Civitours\Controller\Admin\ReportController::getReportTypesList")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/reports/{id}/{type}', "Civitours\Controller\Admin\ReportController::getReportById");

        $admin->delete('/reports/{id}', "Civitours\Controller\Admin\ReportController::delete");

        $admin->post('/reviews/upload', "Civitours\Controller\Admin\ReviewController::uploadCsv")
            ->before("Civitours\Middleware\AuthMiddleware::checkAdmin");

        $admin->get('/operations/emails/unfinished', "Civitours\Controller\Admin\OperationController::getUnfinishedEmailsCsv");

        return $admin;
    }

}