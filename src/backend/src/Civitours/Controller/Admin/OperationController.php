<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10.07.18
 * Time: 11:09
 */

namespace Civitours\Controller\Admin;


use Civitours\Service\OrderService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OperationController
{
    /**
     * Get list of operations
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getList(Application $app) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        return new JsonResponse($orderService->getOperationsList());
    }

    /**
     * Get one operation by id
     *
     * @param Application $app
     * @param $id
     * @return JsonResponse
     */
    public function getById(Application $app, $id) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $operation = $orderService->getOperation($id);
        if (!$operation) {
            throw new NotFoundHttpException('Reqired operation is not found');
        }
        $operation['invoice'] = $orderService->getOrderInvoice($operation['order']);
        unset($operation['order']);
        return new JsonResponse($operation);
    }

    /**
     * Set new order status
     *
     * @param Application $app
     * @param $code
     * @param $status
     * @return Response
     */
    public function updateOrderStatus(Application $app, $code, $status) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $orderService->updateOrderStatus($code, $status);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Update note for operation
     *
     * @param Application $app
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateAdminNote(Application $app, Request $request, $id) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || !array_key_exists('data', $submittedData)) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $orderService->updateOperationNote($id, $submittedData['data']);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Delete order by operation
     *
     * @param Application $app
     * @param $id
     * @return Response
     */
    public function delete(Application $app, $id) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $operation = $orderService->getOperation($id);
        if (!$operation) {
            throw new NotFoundHttpException('Required operation is not found');
        }
        $orderService->markAsDeleted($operation['order']);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Retrieve csv file for emails with unfinished order
     *
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function getUnfinishedEmailsCsv(Application $app) {
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $emailList = $orderService->getUnpaidOrderEmails();

        $stream = function() use ($emailList) {
            $output = fopen('php://output', 'w');
            foreach ($emailList as $email)
            {
                fputcsv($output, [$email]);
            }
            fclose($output);
        };

        return $app->stream($stream, 200, array(
            'Content-Type' => 'text/csv',
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="'.urlencode('unfinished_emails.csv').'',
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public',
        ));
    }
}