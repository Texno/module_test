<?php

namespace Civitours\Controller\Admin;

use Civitours\Controller\FormProcessTrait;
use Civitours\Form\ActivityFormType;
use Civitours\Service\ActivityService;
use Civitours\Service\SupplierService;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Admin activities controller
 *
 * Class ActivitiesController
 * @package Civitours\Controller\Admin
 */
class ActivitiesController
{
    use FormProcessTrait;

    /**
     * Retrieve activity
     *
     * @param Application $app
     * @param $idActivity
     * @return JsonResponse
     */
    public function getById(Application $app, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activity = $activityService->getActivity($idActivity, false);
        if(!$activity) {
            throw new NotFoundHttpException('Activity not found');
        }
        /** @var SupplierService $suppliersService */
        $suppliersService = $app['supplier.service'];
        $activity['schedule'] = $activityService->getActivitySchedule($idActivity);
        $activity['gallery'] = $activityService->getGallery($idActivity);
        $activity['types'] = $activityService->getTypes($idActivity);
        $activity['suppliers'] = $suppliersService->getForActivity($idActivity);
        $activity['voucher'] = $activityService->getVoucher($idActivity);
        return new JsonResponse($activity);
    }

    /**
     * Create activity
     *
     * @param Request $request
     * @param Application $app
     * @return Response
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(Request $request, Application $app) {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(ActivityFormType::class, null, [
            'geo_data_service'  => $app['geo_data.service'],
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var ActivityService $activityService */
            $activityService = $app['activity.service'];
            $activityService->createActivity($form->getViewData());
            return new Response(null, Response::HTTP_CREATED);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Update activity data
     *
     * @param Request $request
     * @param Application $app
     * @param $idActivity
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, Application $app, $idActivity) {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(ActivityFormType::class, null, [
            'geo_data_service'  => $app['geo_data.service'],
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var ActivityService $activityService */
            $activityService = $app['activity.service'];
            $activityService->updateActivity($idActivity, $form->getViewData());
            return new Response(null, Response::HTTP_NO_CONTENT);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Retrieve list of featured activities
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getList(Application $app) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        return new JsonResponse($activityService->getList(false));
    }

    /**
     * Set draft status for activity
     *
     * @param Application $app
     * @param $idActivity
     * @return Response
     */
    public function setDraftStatus(Application $app, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activityService->setDraftStatus($idActivity, true);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Clear draft status for activity
     *
     * @param Application $app
     * @param $idActivity
     * @return Response
     */
    public function clearDraftStatus(Application $app, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activityService->setDraftStatus($idActivity, false);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Delete activity
     *
     * @param Application $app
     * @param $idActivity
     * @return Response
     */
    public function delete(Application $app, $idActivity) {
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activityService->markAsDeleted($idActivity);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
