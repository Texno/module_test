<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.06.18
 * Time: 15:48
 */

namespace Civitours\Controller\Admin;


use Civitours\Service\ReviewService;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ReviewController
{
    /**
     * Get review list for activity for admin
     *
     * @param Application $app
     * @param $idActivity
     * @return JsonResponse
     */
    public function getForActivity(Application $app, $idActivity) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->getReviewsForActivity($idActivity, null, false));
    }

    /**
     * Set approved status
     *
     * @param Application $app
     * @param $idReview
     * @return Response
     */
    public function setApprovedStatus(Application $app, $idReview) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $reviewService->setApprovedStatus($idReview, true);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Clear draft status for activity
     *
     * @param Application $app
     * @param $idReview
     * @return Response
     */
    public function clearApprovedStatus(Application $app, $idReview) {
        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        $reviewService->setApprovedStatus($idReview, false);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Upload csv data with reviews
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function uploadCsv(Application $app, Request $request) {
        if(empty($_FILES['file'])) {
            throw new BadRequestHttpException('File not provided');
        }

        $token = $request->attributes->get('token');
        $idUser = $token->data->userId;

        /** @var ReviewService $reviewService */
        $reviewService = $app['review.service'];
        return new JsonResponse($reviewService->uploadCsv($idUser, $this->getUserIp()));
    }

    /**
     * Retrieve user ip
     *
     * @return int
     */
    private function getUserIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return ip2long($ip);
    }

}