<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.05.18
 * Time: 14:32
 */

namespace Civitours\Controller\Admin;
use Civitours\Service\GeoDataService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Admin city controller
 *
 * Class CityController
 * @package Civitours\Controller\Admin
 */
class CityController
{
    /**
     * Update city
     *
     * @param Request $request
     * @param Application $app
     * @param $id
     * @return Response
     */
    public function update(Request $request, Application $app, $id) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }
        /** @var GeoDataService $geoDataService */
        $geoDataService = $app['geo_data.service'];
        $city = $geoDataService->getCity($id);
        if (false === $city) {
            throw new NotFoundHttpException("City not found");
        }
        $error = $geoDataService->updateCity($id, $submittedData, $city);
        if ($error) {
            throw new ServiceUnavailableHttpException(10, $error);
        }

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}