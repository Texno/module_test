<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.07.18
 * Time: 13:34
 */

namespace Civitours\Controller\Admin;

use Civitours\Reports\Handler\AbstractHandler;
use Civitours\Service\ReportService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Controller for reports
 *
 * Class ReportController
 * @package Civitours\Controller\Admin
 */
class ReportController
{

    const TYPE_CSV = 'csv';
    const TYPE_XLS = 'xls';
    const TYPE_JSON = 'json';

    /**
     * Retrieve report types list for reports
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getReportTypesList(Application $app) {
        /** @var ReportService $reportService */
        $reportService = $app['report.service'];
        return new JsonResponse($reportService->getReportTypesList());
    }

    /**
     * Retrieve report types list for reports
     *
     * @param Application $app
     * @return JsonResponse
     */
    public function getReports(Application $app) {
        /** @var ReportService $reportService */
        $reportService = $app['report.service'];
        return new JsonResponse($reportService->getReportsList());
    }

    /**
     * Create new report
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createReport(Application $app, Request $request) {
        /** @var ReportService $reportService */
        $reportService = $app['report.service'];

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        if (empty($submittedData['name']) || empty($submittedData['report_type'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $reportService->createReport($submittedData);
        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param Application $app
     * @param $id
     * @param string $type
     * @return Response
     */
    public function getReportById(Application $app, $id, $type = self::TYPE_JSON) {
        /** @var ReportService $reportService */
        $reportService = $app['report.service'];
        $report = $reportService->getReportById($id);
        if(!$report) {
            throw new NotFoundHttpException('Report not found');
        }

        $fieldNames = AbstractHandler::getFieldsNamesForType($report['report_type']);
        $reportData = $reportService->getReportData($id);
        if(null === $reportData) {
            throw new NotFoundHttpException('Report not found');
        }

        switch ($type) {
            case self::TYPE_JSON:
                return $this->wrapJson($fieldNames, $reportData);
            case self::TYPE_CSV:
                return $this->wrapCsv($fieldNames, $reportData, $app, $id);
            case self::TYPE_XLS:
                return $this->wrapXls($fieldNames, $reportData, $app, $id);
            default:
                throw new BadRequestHttpException('Unknown type: ' . $type);
        }
    }

    /**
     * Delete report
     *
     * @param Application $app
     * @param $id
     * @return Response
     */
    public function delete(Application $app, $id) {
        /** @var ReportService $reportService */
        $reportService = $app['report.service'];
        $report = $reportService->getReportById($id);
        if(!$report) {
            throw new NotFoundHttpException('Report not found');
        }
        $reportService->markAsDeleted($id);
        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Get json wrapped data
     *
     * @param $header
     * @param $data
     * @return JsonResponse
     */
    private function wrapJson($header, $data) {
        $result = [];
        foreach ($data as $row) {
            array_push($result, array_combine(array_keys($header), $row));
        }
        return new JsonResponse([
            'header'    => $header,
            'data'      => $result
        ]);
    }

    /**
     * Get csv wrapped data
     *
     * @param $header
     * @param $data
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function wrapCsv($header, $data, Application $app, $id) {
        $stream = function() use ($header, $data) {
            $output = fopen('php://output', 'w');
            fputcsv($output, $header);
            $headerKeys = array_keys($header);
            $dateColumn = array_search('created_at', $headerKeys);

            foreach ($data as $row)
            {
                if($dateColumn) {
                    $row[$dateColumn] = date('d.m.o H:i:s', $row[$dateColumn]);
                }
                fputcsv($output, $row);
            }
            fclose($output);
        };

        return $app->stream($stream, 200, array(
            'Content-Type' => 'text/csv',
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="'.urlencode('report_'.$id.'.csv').'',
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public',
        ));

    }

    /**
     * Get xls wrapped data
     *
     * @param $header
     * @param $data
     * @param Application $app
     * @param $id
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function wrapXls($header, $data, Application $app, $id) {

        $stream = function() use ($header, $data) {
            $spreadsheet = new Spreadsheet();

            $headerKeys = array_keys($header);
            $dateColumn = array_search('created_at', $headerKeys);

            $worksheet = $spreadsheet->setActiveSheetIndex(0);

            $worksheet->fromArray($header, null, 'A1');
            $worksheet->fromArray($data, null, 'A2');

            // Replace data
            if($dateColumn) {
                $dateColumnName = 'A';
                $dateColumnCounter = $dateColumn;
                while($dateColumnCounter--) ++$dateColumnName;
                foreach ($data as $index => $row) {
                    $excelDateValue = Date::PHPToExcel((int)$row[$dateColumn]);
                    $worksheet->setCellValue($dateColumnName . ($index+2), $excelDateValue);
                    $worksheet->getStyle($dateColumnName . ($index+2))
                        ->getNumberFormat()
                        ->setFormatCode(NumberFormat::FORMAT_DATE_DATETIME);
                }
            }

            $writer = IOFactory::createWriter($spreadsheet, 'Xls');
            $writer->save('php://output');

            $output = fopen('php://output', 'w');
            fputcsv($output, $header);
            foreach ($data as $row)
            {
                fputcsv($output, $row);
            }
            fclose($output);
        };

        return $app->stream($stream, 200, array(
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename="'.urlencode('report_'.$id.'.xls').'',
            'Expires' => '0',
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public',
        ));
    }
}
