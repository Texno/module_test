<?php

namespace Civitours\Controller;

use Civitours\Form\RegistrationFormType;
use Civitours\Service\UserService;
use Silex\Application;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RegistrationController
 * @package Civitours\Controller
 */
class RegistrationController
{

    use FormProcessTrait;

    /**
     * Class to perform registration actions
     *
     * @param Application $app
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function register(Application $app, Request $request)
    {
        /**
         * @var $form FormInterface
         */
        $form = $app['form.factory']->create(RegistrationFormType::class, null, [
            'user_service'      => $app['user.service'],
            'geo_data_service'  => $app['geo_data.service'],
            'recaptcha_service' => $app['recaptcha.service']
        ]);

        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        $form->submit($submittedData);
        if($form->isValid()) {
            /** @var UserService $userService */
            $userService = $app['user.service'];
            $userService->registerUser($form->getViewData());
            return new Response(null, Response::HTTP_CREATED);
        }
        return new JsonResponse([
            'message'   => 'validation failed',
            'errors'    => $this->getErrorMessages($form)
        ],Response::HTTP_BAD_REQUEST);
    }

    /**
     * Perform confirm action
     *
     * @param Application $app
     * @param Request $request
     * @param $token
     * @return RedirectResponse
     */
    public function confirm(Application $app, Request $request, $token) {
        /** @var UserService $userService */
        $userService = $app['user.service'];
        $result = $userService->verifyMail($token);
        if ($result) {
            return new RedirectResponse($app['base.url'] . '/info/email-verified');
        } else {
            return new RedirectResponse($app['base.url'] . '/not-found');
        }
    }

    /**
     * Request resend
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     */
    public function resend(Application $app, Request $request) {
        $submittedData = json_decode($request->getContent(), true);
        if(!$submittedData || empty($submittedData['email'])) {
            throw new BadRequestHttpException("Wrong json data is provided");
        }

        /** @var UserService $userService */
        $userService = $app['user.service'];
        $result = $userService->resendVerificationMail($submittedData['email']);

        $statusCode = Response::HTTP_NO_CONTENT;
        if (!$result) {
            $statusCode = Response::HTTP_NOT_FOUND;
        }
        return new Response(null, $statusCode);
    }
}
