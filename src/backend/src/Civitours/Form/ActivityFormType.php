<?php

namespace Civitours\Form;

use Civitours\Entity\ActivityData;
use Civitours\Service\GeoDataService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Form type for create/update activity
 *
 * Class ActivityFormType
 * @package Civitours\Form
 */
class ActivityFormType extends AbstractType
{

    /**
     * @var GeoDataService
     */
    private $geoDataService = null;

    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->geoDataService = $options['geo_data_service'];

        $builder
            ->add('country', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateCountry'],
                        'payload'   => $options['geo_data_service']
                    ])
                ]
            ])
            ->add('city', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateCity'],
                        'payload'   => $options['geo_data_service']
                    ])
                ]
            ])
            ->add('category', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank()
                ]
            ])
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max'           => 255,
                        'maxMessage'    => 'Name should be less than 255 characters'
                    ])
                ]
            ])
            ->add('route_name', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max'           => 255,
                        'maxMessage'    => 'Route name should be less than 255 characters'
                    ])
                ]
            ])
            ->add('avatar', TextType::class)
            ->add('avatar_small', TextType::class)
            ->add('shortDescription', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max'           => 1024,
                        'maxMessage'    => 'Short description should be less than 1024 characters'
                    ])
                ]
            ])
            ->add('description', TextType::class)
            ->add('duration', TextType::class)
            ->add('language', TextType::class)
            ->add('included', TextType::class)
            ->add('notIncluded', TextType::class)
            ->add('whenToBook', TextType::class)
            ->add('accessibility', TextType::class)
            ->add('ticket', TextType::class)
            ->add('howToBook', TextType::class)
            ->add('meetingPointLatitude', TextType::class, [
                'constraints' => [
                    new Assert\Type("numeric"),
                ]
            ])
            ->add('meetingPointLongitude', TextType::class, [
                'constraints' => [
                    new Assert\Type("numeric"),
                ]
            ])
            ->add('meetingPointText', TextType::class)
            ->add('cancelationHoursBefore', TextType::class, [
                'constraints' => [
                    new Assert\Type("numeric"),
                ]
            ])
            ->add('cancelationDescription', TextType::class)
            ->add('types', TextType::class)
            ->add('gallery', TextType::class)
            ->add('schedule', TextType::class)
            ->add('suppliers', TextType::class)
            ->add('book_limit', TextType::class)
            ->add('blocks', TextType::class)
            ->add('travellers', TextType::class)
            ->add('voucher', TextType::class);
    }

    /**
     * Perform country check validation
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param GeoDataService $payload
     */
    public function validateCountry($data, ExecutionContextInterface $context, $payload)
    {
        if(false === $payload->getCountry($data)) {
            $context->buildViolation('Unknown country id is provided')
                ->atPath('country')
                ->addViolation();
        }
    }

    /**
     * Perform city check validation
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param GeoDataService $payload
     */
    public function validateCity($data, ExecutionContextInterface $context, $payload)
    {
        if(false === $payload->getCity($data)) {
            $context->buildViolation('Unknown city id is provided')
                ->atPath('city')
                ->addViolation();
        }
    }

    /**
     * Chack that city is in country
     *
     * @param ActivityData $data
     * @param ExecutionContextInterface $context
     */
    public function validateCityBelongsCountry($data, ExecutionContextInterface $context) {
        $city = $this->geoDataService->getCity($data->city);
        if (false !== $city && $data->country != $city['country']) {
            $context->buildViolation('City doesn`t belong to country')
                ->atPath('city')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => ActivityData::class,
            'constraints'       => [
                new Assert\Callback([$this, 'validateCityBelongsCountry']),
            ],
            'csrf_protection'   => false,
        ));

        $resolver->setRequired(['geo_data_service']);
    }
}