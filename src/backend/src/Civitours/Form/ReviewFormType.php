<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.06.18
 * Time: 13:42
 */

namespace Civitours\Form;

use Civitours\Entity\ReviewData;
use Civitours\Service\OrderService;
use Civitours\Service\ReviewService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ReviewFormType extends AbstractType
{
    /**
     * @var ReviewService
     */
    private $reviewService;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->reviewService = $options['review_service'];
        $this->orderService = $options['order_service'];

        $builder
            ->add('activity', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                ]
            ])
            ->add('traveller_type', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                ]
            ])
            ->add('rate', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Range([
                        'min' => 0,
                        'max' => 10
                    ])
                ]
            ])
            ->add('header', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 255
                    ])
                ]
            ])
            ->add('text', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 1024
                    ])
                ]
            ])
            ->add('user_name', TextType::class)
            ->add('order_code', TextType::class)
            ->add('country_id', TextType::class)
            ->add('city_id', TextType::class)
            ->add('created_at', TextType::class);
    }

    /**
     * Chack that city is in country
     *
     * @param ReviewData $data
     * @param ExecutionContextInterface $context
     */
    public function validateNewReview($data, ExecutionContextInterface $context) {
        if (!$data->created_at && $this->reviewService->isExist($data->activity, $data->order_code)) {
            $context->buildViolation('You are already rated this order')
                ->atPath('form')
                ->addViolation();
        }
    }

    /**
     * Check that review is written for existing order
     *
     * @param ReviewData $data
     * @param ExecutionContextInterface $context
     */
    public function validateOrderExists($data, ExecutionContextInterface $context) {
        $order = $this->orderService->get($data->order_code);
        if (!$data->created_at && empty($order)) {
            $context->buildViolation('Order not exist')
                ->atPath('form')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => ReviewData::class,
            'constraints'       => [
                new Assert\Callback([$this, 'validateNewReview']),
                new Assert\Callback([$this, 'validateOrderExists']),
            ],
            'csrf_protection'   => false
        ));
        $resolver->setRequired(['review_service', 'order_service']);
    }
}
