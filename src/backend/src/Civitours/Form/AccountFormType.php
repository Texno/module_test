<?php
namespace Civitours\Form;

use Civitours\Entity\AccountData;
use Civitours\Service\GeoDataService;
use Civitours\Service\UserService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Form type for user account information update
 *
 * Class RegistrationFormType
 * @package Civitours\Form
 */
class AccountFormType extends AbstractType
{

    /**
     * @var GeoDataService
     */
    private $geoDataService = null;

    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->geoDataService = $options['geo_data_service'];

        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max'           => 150,
                        'maxMessage'    => 'Name should be less than 150 characters'
                    ])
                ]
            ])
            ->add('surname', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max'           => 150,
                        'maxMessage'    => 'Surname should be less than 150 characters'
                    ])
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateUserEmailUnique'],
                        'payload'   => [
                            'service' => $options['user_service'],
                            'id'      => $options['user_id']
                        ]
                    ])
                ]
            ])
            ->add('country', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateCountry'],
                        'payload'   => $options['geo_data_service']
                    ])
                ]
            ])
            ->add('city', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Type("numeric"),
                    new Assert\Callback([
                        'callback'  => [$this, 'validateCity'],
                        'payload'   => $options['geo_data_service']
                    ])
                ]
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Regex([
                        'pattern' => '/^\+\d{5,49}$/',
                        'message' => 'Phone should be numeric starts from +'
                    ])
                ]
            ])
            ->add('news', TextType::class);
    }

    /**
     * Perform country check validation
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param GeoDataService $payload
     */
    public function validateCountry($data, ExecutionContextInterface $context, $payload)
    {
        if(false === $payload->getCountry($data)) {
            $context->buildViolation('Unknown country id is provided')
                ->atPath('country')
                ->addViolation();
        }
    }

    /**
     * Perform city check validation
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param GeoDataService $payload
     */
    public function validateCity($data, ExecutionContextInterface $context, $payload)
    {
        if(false === $payload->getCity($data)) {
            $context->buildViolation('Unknown city id is provided')
                ->atPath('city')
                ->addViolation();
        }
    }

    /**
     * Chack that city is in country
     *
     * @param AccountData $data
     * @param ExecutionContextInterface $context
     */
    public function validateCityBelongsCountry(AccountData $data, ExecutionContextInterface $context) {
        $city = $this->geoDataService->getCity($data->city);
        if (false !== $city && $data->country != $city['country']) {
            $context->buildViolation('City doesn`t belong to country')
                ->atPath('city')
                ->addViolation();
        }
    }


    /**
     * Validate that new created user hasn`t registered before
     *
     * @param $data
     * @param ExecutionContextInterface $context
     * @param UserService $payload
     */
    public function validateUserEmailUnique($data, ExecutionContextInterface $context, $payload) {
        $userService = $payload['service'];
        $userId = $payload['id'];
        $existingUser = $userService->getUserByEmail($data);
        if(false !== $existingUser && $userId !== $existingUser['id']) {
            $context->buildViolation('User with such email already exists')
                ->atPath('email')
                ->addViolation();
        }
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => AccountData::class,
            'constraints'       => [
                new Assert\Callback([$this, 'validateCityBelongsCountry']),
            ],
            'csrf_protection'   => false,
        ));

        $resolver->setRequired(['user_service', 'geo_data_service', 'user_id']);
    }
}
