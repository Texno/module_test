<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 09.07.18
 * Time: 11:29
 */

namespace Civitours\Form;

use Civitours\Entity\SupplierData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class SupplierFormType extends AbstractType
{
    /**
     * Construct the form with assertions
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 50
                    ])
                ]
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 20
                    ])
                ]
            ])
            ->add('email', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Email(),
                    new Assert\Length([
                        'max' => 50
                    ])
                ]
            ])
            ->add('contact', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 50
                    ])
                ]
            ])
            ->add('vat', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 20
                    ])
                ]
            ])
            ->add('post_code', TextType::class, [
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length([
                        'max' => 10
                    ])
                ]
            ])
            ->add('website', TextType::class)
            ->add('bank_details', TextType::class);

    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => SupplierData::class,
            'csrf_protection'   => false
        ));
    }
}
