<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.05.18
 * Time: 11:33
 */

namespace Civitours\Provider;


use Civitours\Service\ReviewService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ReviewServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['review.service'] = function () use ($app) {
            return new ReviewService($app['db'], $app['image.url']);
        };
    }
}