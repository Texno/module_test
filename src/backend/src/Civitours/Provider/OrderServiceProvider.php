<?php

namespace Civitours\Provider;

use Civitours\Service\OrderService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class OrderServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['order.service'] = function () use ($app) {
            return new OrderService($app['db'], $app['image.url'], $app['image.path'], $app['supplier.service']);
        };
    }
}