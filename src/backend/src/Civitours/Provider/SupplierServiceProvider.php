<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.18
 * Time: 15:26
 */

namespace Civitours\Provider;

use Civitours\Service\SupplierService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class SupplierServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['supplier.service'] = function () use ($app) {
            return new SupplierService($app['db']);
        };
    }
}
