<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.07.18
 * Time: 13:37
 */

namespace Civitours\Provider;


use Civitours\Service\ReportService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ReportServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['report.service'] = function () use ($app) {
            return new ReportService($app['db'], $app['reports.path'], $app['api.url']);
        };
    }
}