<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.05.18
 * Time: 15:55
 */

namespace Civitours\Provider;


use Civitours\Service\UploadService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class UploadServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['upload.service'] = function () use ($app) {
            return new UploadService($app['image.path'], $app['image.url'], $app['files.path'], $app['image.quality']);
        };
    }
}
