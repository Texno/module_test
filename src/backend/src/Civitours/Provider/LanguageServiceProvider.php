<?php

namespace Civitours\Provider;

use Civitours\Service\LanguageService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class LanguageServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['language.service'] = function () use ($app) {
            return new LanguageService($app['db']);
        };
    }
}
