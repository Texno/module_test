<?php

namespace Civitours\Provider;

use Civitours\Service\GoogleCaptchaService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class GoogleCaptchaProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['recaptcha.service'] = function () use ($app) {
            return new GoogleCaptchaService($app['recapthca.secret']);
        };
    }
}
