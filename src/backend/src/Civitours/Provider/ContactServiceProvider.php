<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.07.18
 * Time: 17:37
 */

namespace Civitours\Provider;


use Civitours\Service\ContactService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ContactServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['contact.service'] = function () use ($app) {
            return new ContactService(
                $app['db'],
                $app['upload.service']
            );
        };
    }
}