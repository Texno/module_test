<?php

namespace Civitours\Provider;

use Civitours\Service\MailService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class MailServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['mail.service'] = function () use ($app) {
            return new MailService($app['mailer'], $app['mailer_service.template_path'], $app['twig']);
        };
    }
}