<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.06.18
 * Time: 12:25
 */

namespace Civitours\Provider;


use Civitours\Service\PaymentService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PaymentServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['payment.service'] = function () use ($app) {
            return new PaymentService($app['paypal.config'], $app['redsys.config'], $app['base.url'], $app['api.url'], $app['logger']);
        };
    }
}
