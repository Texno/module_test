<?php

namespace Civitours\Provider;

use Civitours\Service\ActivityService;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ActivityServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['activity.service'] = function () use ($app) {
            return new ActivityService($app['db'], $app['image.url'], $app['upload.service']);
        };
    }
}
