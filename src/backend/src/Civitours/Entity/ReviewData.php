<?php

namespace Civitours\Entity;

class ReviewData
{
    public $user_name;
    public $traveller_type;
    public $rate;
    public $header;
    public $text;
    public $activity;
    public $country_id;
    public $city_id;
    public $created_at;
    public $ip;
    public $user;
    public $order_code;
}
