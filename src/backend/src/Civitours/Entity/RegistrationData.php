<?php

namespace Civitours\Entity;

class RegistrationData
{

    public $email;
    public $password;
    public $passwordConfirm;
    public $name;
    public $surname;
    public $country;
    public $city;
    public $phone;
    public $token;
}
