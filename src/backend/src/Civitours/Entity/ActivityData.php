<?php

namespace Civitours\Entity;

class ActivityData
{
    public $name;
    public $route_name;
    public $avatar;
    public $avatar_small;
    public $country;
    public $city;
    public $category;
    public $shortDescription;
    public $description;
    public $duration;
    public $language;
    public $included;
    public $notIncluded;
    public $whenToBook;
    public $accessibility;
    public $ticket;
    public $howToBook;
    public $meetingPointLatitude;
    public $meetingPointLongitude;
    public $meetingPointText;
    public $cancelationHoursBefore;
    public $cancelationDescription;
    public $types;
    public $gallery;
    public $schedule;
    public $suppliers;
    public $book_limit;
    public $blocks;
    public $travellers;
    public $voucher;
}