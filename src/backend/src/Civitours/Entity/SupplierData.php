<?php

namespace Civitours\Entity;

class SupplierData
{
    public $name;
    public $phone;
    public $email;
    public $contact;
    public $vat;
    public $post_code;
    public $website;
    public $bank_details;
}
