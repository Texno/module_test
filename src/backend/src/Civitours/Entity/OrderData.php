<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.18
 * Time: 11:45
 */

namespace Civitours\Entity;


class OrderData
{
    public $email;
    public $fullName;
    public $invoice;
    public $isVerified;
    public $items;
    public $phone;
    public $code;
    public $verifiedTotal;
}
