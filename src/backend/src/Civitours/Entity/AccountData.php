<?php

namespace Civitours\Entity;

class AccountData
{
    public $name;
    public $surname;
    public $phone;
    public $email;
    public $country;
    public $city;
    public $news;
}