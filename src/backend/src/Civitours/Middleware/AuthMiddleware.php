<?php

namespace Civitours\Middleware;

use Firebase\JWT\JWT;
use Psr\Log\LoggerInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Middleware to perform auth operations
 *
 * Class AuthMiddleware
 * @package Civitours\Middleware
 */
class AuthMiddleware
{

    /**
     * Check user is authorized
     *
     * @param Request $request
     * @param Application $app
     */
    public static function checkAuthorized(Request $request, Application $app) {
        self::getToken($request, $app);
    }

    /**
     * Check admin access
     *
     * @param Request $request
     * @param Application $app
     */
    public static function checkAdmin(Request $request, Application $app) {
        $token = self::getToken($request, $app);
        if (empty($token->data->isAdmin)) {
            throw new UnauthorizedHttpException(null, 'Access denied');
        }
    }

    /**
     * Parse token from request
     *
     * @param Request $request
     * @param Application $app
     * @return object
     */
    private static function getToken(Request $request, Application $app) {
        /** @var LoggerInterface $logger */
        $logger = $app['logger'];
        $rawHeader = $request->headers->get('Authorization');
        if ($rawHeader) {
            if (strpos($rawHeader, 'Bearer ') === false) {
                throw new UnauthorizedHttpException(null, 'Access denied');
            }

            $jwt = str_replace('Bearer ', '', $rawHeader);
            $secretKey = base64_decode($app['secret']);

            try {
                $token = JWT::decode($jwt, $secretKey, [$app['algorithm']]);
                $request->attributes->set('token', $token);
                return $token;
            } catch (\Exception $e) {
                $logger->warning('Cannot decode token: ' . $e->getMessage());
                throw new UnauthorizedHttpException(null, 'Access denied');
            }
        } else {
            throw new UnauthorizedHttpException(null, 'Access denied');
        }

    }
}