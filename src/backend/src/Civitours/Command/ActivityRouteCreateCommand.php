<?php

namespace Civitours\Command;

use Civitours\Service\ActivityService;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command to update activity routes
 *
 * Class ActivityRouteCreateCommand
 * @package Civitours\Command
 */
class ActivityRouteCreateCommand extends Command
{
    /**
     * Configure command
     */
    protected function configure()
    {
        $this
            ->setName('misc:routes')
            ->setDescription('Update activity routes according to their names')
            ->addOption('override', '-o', InputOption::VALUE_NONE, 'Force to override routes');
    }

    /**
     * Main command function
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting update activity routes.');
        $override = $input->getOption('override');
        $app = $this->getSilexApplication();
        /** @var ActivityService $activityService */
        $activityService = $app['activity.service'];
        $activities = $activityService->getActivityRoutesList();
        $output->writeln('Found ' . count($activities) . ' activities');
        $cnt = 0;
        $result = [];
        foreach ($activities as $activity) {
            if($override || empty($activity['route_name'])) {
                $routeName = preg_replace('/\s/', '-', strtolower(trim($activity['name'])));
                $routeName = preg_replace('/[^\w-]/', '', $routeName);
                if (empty($routeName)) {
                    $output->writeln('Cannot convert ' . $activity['name'] . ' to route name. Skipping');
                } else {
                    $result[$activity['id']] = $routeName;
                }
                $cnt++;
            }
        }
        try {
            $activityService->updateActivityRoutes($result);
        } catch (\Exception $e) {
            $output->writeln('Failed to update routes: ' . $e->getMessage());
        }
        $output->writeln("Updated routes for {$cnt} activities. Done!");
    }
}
