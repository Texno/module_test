<?php

namespace Civitours\Command;

use Civitours\Service\ActivityService;
use Civitours\Service\MailService;
use Civitours\Service\OrderService;
use Civitours\Service\SupplierService;
use Knp\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command to send test email
 *
 * Class ActivityRouteCreateCommand
 * @package Civitours\Command
 */
class EmailSendCommand extends Command
{

    const ADMIN_PAYMENT = 'AdminPayment';
    const USER_PAYMENT = 'UserPayment';
    const SUPPLIER_PAYMENT = 'SupplierPayment';

    const MAIL_TYPES = [
        self::ADMIN_PAYMENT,
        self::USER_PAYMENT,
        self::SUPPLIER_PAYMENT
    ];

    /**
     * Configure command
     */
    protected function configure()
    {
        $this
            ->setName('misc:email')
            ->setDescription('Send email to address (for testing purposes)')
            ->addArgument('type', InputOption::VALUE_REQUIRED, 'Type of email to send')
            ->addArgument('email', InputOption::VALUE_REQUIRED, 'Email to send');
        $this->setHelp('Available mail types: ' . implode(',', self::MAIL_TYPES) );
    }

    /**
     * Main command function
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting seding test email');
        $mail = $input->getArgument('email');
        $type = $input->getArgument('type');

        if (!in_array($type, self::MAIL_TYPES)) {
            throw new \Exception('Unknown type is specified: ' . $type);
        }
        $app = $this->getSilexApplication();
        /** @var OrderService $orderService */
        $orderService = $app['order.service'];
        $randomCode = $orderService->getRandomOrderCode();
        if(!$randomCode) {
            throw new \Exception('Cannot find order to send');
        }

        $orderData = $orderService->getMailConfirmData($randomCode);

        /** @var MailService $mailService */
        $mailService = $app['mail.service'];
        switch ($type) {
            case self::ADMIN_PAYMENT:
                $mailService->sendAdminPaymentConfirmEmail($mail, $orderData);
                break;

            case self::USER_PAYMENT:
                $mailService->sendUserPaymentConfirmEmail($mail, $orderData);
                break;

            case self::SUPPLIER_PAYMENT:
                $mailService->sendSupplierPaymentConfirmEmail($mail, [$orderData[0]]);
                break;
        }
        $output->writeln('Mail sent succesfully to ' . $mail);
    }
}
