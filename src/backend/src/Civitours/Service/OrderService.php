<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.18
 * Time: 11:20
 */

namespace Civitours\Service;

use Civitours\Entity\OrderData;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use function GuzzleHttp\Psr7\try_fopen;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Service to operate orders
 *
 * Class OrderService
 * @package Civitours\Service
 */
class OrderService
{
    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * Url to images directory
     *
     * @var string
     */
    private $urlToImages = '';

    /**
     * Path to image directory
     *
     * @var string
     */
    private $pathToImages = '';

    /**
     * @var SupplierService
     */
    private $suppliersService;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param $urlToImages
     * @param $pathToImages
     */
    public function __construct(Connection $db, $urlToImages, $pathToImages, $suppliersService)
    {
        $this->db = $db;
        $this->urlToImages = $urlToImages;
        $this->pathToImages = $pathToImages;
        $this->suppliersService = $suppliersService;
    }

    /**
     * Create order in db
     *
     * @param OrderData $data
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(OrderData $data) {
        $this->db->beginTransaction();
        try {
            $code = uniqid();
            list ($name, $surname) = explode(' ', $data->fullName);
            // First - create order row
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->insert('orders')
                ->setValue('email'              , ':email')
                ->setValue('name'               , ':name')
                ->setValue('surname'            , ':surname')
                ->setValue('phone'              , ':phone')
                ->setValue('is_need_invoice'    , ':invoice')
                ->setValue('total_amount'       , ':total')
                ->setValue('code'               , ':code')
                ->setParameters([
                    'email'     => $data->email,
                    'name'      => $name,
                    'surname'   => $surname,
                    'phone'     => $data->phone,
                    'total'     => 0, // We cannot insert total on initial stage
                    'code'      => $code,
                ]);
            $queryBuilder->setParameter('invoice', !empty($data->invoice), ParameterType::BOOLEAN);
            $queryBuilder->execute();

            $idOrder = $this->db->lastInsertId();
            $total = 0;
            foreach ($data->items as $item) {
                // Create order row items
                $total += $this->createOrderItem($idOrder, $item);
            }

            if(!empty($data->invoice)) {
                $this->createOrderInvoice($idOrder, $data->invoice);
            }

            // Update order total
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->update('orders')
                ->set('total_amount', ':total')
                ->set('code', ':code')
                ->where('id = :id')
                ->setParameters([
                    'total' => $total,
                    'code'  => $idOrder,
                    'id'    => $idOrder
                ]);
            $queryBuilder->execute();

            $this->db->commit();
            return [
                'code'  => $idOrder,
                'total' => $total
            ];
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Create order invoice record
     *
     * @param $orderId
     * @param $invoice
     */
    private function createOrderInvoice($orderId, $invoice) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('order_invoices')
            ->setValue('"order"',         ':order')
            ->setValue('tax_name',      ':taxName')
            ->setValue('tax_id',        ':taxId')
            ->setValue('document_type', ':docType')
            ->setValue('address',       ':address')
            ->setValue('city',          ':city')
            ->setValue('country',       ':country')
            ->setValue('postal',        ':postal')
            ->setParameters([
                'order'     => $orderId,
                'taxName'   => $invoice['taxName'],
                'taxId'     => $invoice['taxId'],
                'docType'   => $invoice['documentType'],
                'address'   => $invoice['address'],
                'city'      => $invoice['city'],
                'country'   => $invoice['country'],
                'postal'    => $invoice['postalCode'],
            ]);
        $queryBuilder->execute();
    }

    /**
     * Create order item
     *
     * @param $idOrder
     * @param $item
     * @return float|int
     */
    private function createOrderItem($idOrder, $item) {
        $total = 0;
        $pricesInfo = $this->getOrderActivityData($item['type']);
        if (empty($pricesInfo)) {
            throw new BadRequestHttpException('No prices for order');
        }
        $tzOffsetString = sprintf('%+03d00', $pricesInfo[0]['offset']);
        $tz = new \DateTimeZone($tzOffsetString);
        $date = new \DateTime('@' . $item['time']);
        $date->setTimezone($tz);
        $selectedTime = $date->format('H:i');
        $selectedDate = $date->format('Y-m-d');

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('order_activities')
            ->setValue('"order"'    , ':order')
            ->setValue('date'       , ':date')
            ->setValue('time'       , ':time')
            ->setValue('comment'    , ':comment')
            ->setValue('activity'   , ':activity')
            ->setParameters([
                'order'     => $idOrder,
                'date'      => $selectedDate,
                'time'      => $selectedTime,
                'comment'   => $item['comment'],
                'activity'  => (string)$pricesInfo[0]['activity'],
            ]);
        $queryBuilder->execute();
        $idOrderActivity = $this->db->lastInsertId();

        foreach ($item['tickets'] as $personType => $amount) {
            foreach ($pricesInfo as $priceInfo) {
                if($personType == $priceInfo['person_type']) {
                    $total += $this->createOrderPrice($idOrderActivity, $priceInfo, $amount);
                    continue 2;
                }
            }
            throw new BadRequestHttpException('Wrong ticket code is provided: ' . $personType);
        }

        return $total;
    }

    /**
     * Create order price
     *
     * @param $idOrderActivity
     * @param $priceInfo
     * @param $amount
     * @return float|int
     */
    private function createOrderPrice($idOrderActivity, $priceInfo, $amount) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('order_activities_prices')
            ->setValue('activity_type'  , ':activity_type')
            ->setValue('person_type'    , ':person_type')
            ->setValue('amount'         , ':amount')
            ->setValue('price'          , ':price')
            ->setValue('order_activity' , ':activity')
            ->setParameters([
                'activity_type'     => $priceInfo['activity_type'],
                'person_type'       => $priceInfo['person_type'],
                'amount'            => $amount,
                'price'             => $amount * $priceInfo['price'],
                'activity'          => $idOrderActivity
            ]);
        $queryBuilder->execute();
        return $amount * $priceInfo['price'];
    }

    /**
     * Retrieve info for item
     *
     * @param $activityTypeId
     * @return array
     */
    private function getOrderActivityData($activityTypeId) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_activity_prices.activity',
                't_activity_prices.price',
                't_activity_prices.person_type',
                't_activity_prices.activity_type',
                't_cities.offset'
            ])
            ->from('activity_prices', 't_activity_prices')
            ->innerJoin('t_activity_prices','activities', 't_activities', 't_activity_prices.activity = t_activities.id')
            ->innerJoin('t_activities','cities', 't_cities', 't_activities.city = t_cities.id')
            ->where('t_activity_prices.activity_type = :type')
            ->setParameter('type', $activityTypeId);
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Update order with payed data
     *
     * @param $orderCode
     * @param $orderData
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function update($orderCode, $orderData) {
        // Update order total
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('orders')

            ->set('gateway_name', ':gate')
            ->set('payed_at', 'now()')
            ->set('is_payed', ':payed')
            ->set('status', "'paid'")
            ->where('code = :code')
            ->setParameters([
                'gate'  => $orderData['gateway'],
                'code'  => $orderCode
            ])
            ->setParameter('payed', true, ParameterType::BOOLEAN);
        if(!empty($orderData['transactionId'])) {
            $queryBuilder
                ->set('transaction_id', ':id')
                ->setParameter('id', $orderData['transactionId']);
        }

        return $queryBuilder->execute();
    }

    /**
     * Update order for redsys
     *
     * @param $transactionId
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function updateRedsysStatus($transactionId) {
        // Update order total
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('orders')
            ->set('payed_at', 'now()')
            ->set('is_payed', ':payed')
            ->set('status', "'paid'")
            ->where('transaction_id = :id')
            ->setParameters([
                'id'    => $transactionId,
            ])
            ->setParameter('payed', true, ParameterType::BOOLEAN);
        return $queryBuilder->execute();
    }

    /**
     * Prepare query builder for order
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function prepareQueryBuilder() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_orders.code',
                't_orders.is_payed',
                't_orders.total_amount',
                't_orders.transaction_id',
                't_orders.email',
                't_orders.name',
                't_orders.surname',
                't_orders.phone',
                't_order_activities.id as order_activity',
                't_order_activities.date',
                't_order_activities.time',
                't_orders.viewed',
                't_activities.id AS activity',
                't_activities.name AS activity_name',
                't_activity_types.name AS activity_type_name',
                't_activities.avatar',
                't_activities.duration',
                't_activities.language',
                't_activities.meeting_point_text',
                't_activities.meeting_point_latitude',
                't_activities.meeting_point_longitude',
                't_countries.name AS country_name',
                't_cities.name AS city_name',
                't_countries.route_name AS country_route',
                't_cities.route_name AS city_route',
                't_activities.route_name AS activity_route',
                't_reviews.rate'
            ])
            ->from('orders', 't_orders')
            ->innerJoin('t_orders','order_activities', 't_order_activities', 't_order_activities.order = t_orders.id')
            ->innerJoin('t_order_activities','activities', 't_activities', 't_order_activities.activity = t_activities.id')
            ->leftJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->leftJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->innerJoin('t_order_activities','order_activities_prices', 't_order_activities_prices', 't_order_activities_prices.order_activity = t_order_activities.id')
            ->innerJoin('t_order_activities_prices', 'activity_types', 't_activity_types', 't_order_activities_prices.activity_type = t_activity_types.id')
            ->leftJoin('t_order_activities', 'reviews', 't_reviews', 't_order_activities.activity = t_reviews.activity AND t_orders.code = t_reviews.order_code')
            ->where('t_orders.is_deleted = false')
            ->groupBy([
                't_activities.id',
                't_activity_types.name',
                't_order_activities.id',
                't_orders.id',
                't_reviews.rate',
                't_countries.id',
                't_cities.id',
            ]);
        return $queryBuilder;
    }

    /**
     * get order by code
     *
     * @param $orderCode
     * @return array
     */
    public function get($orderCode) {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->where('t_orders.code = :code')
            ->setParameter('code', $orderCode);
        return array_map([$this, 'populateAvatar'] , $queryBuilder->execute()->fetchAll());
    }

    /**
     * Return all orders grouped by activities fro user
     *
     * @param $email
     * @return array
     */
    public function getForUser($email) {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->where('t_orders.email = :email')
            ->setParameter('email', $email);
        return array_map([$this, 'populateAvatar'] , $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get all prices for activity id
     *
     * @param $orderActivityId
     * @return array
     */
    public function getOrderActivityPrices($orderActivityId) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_person_types.name',
                't_order_activities_prices.amount',
                't_order_activities_prices.price',
            ])
            ->from('order_activities_prices', 't_order_activities_prices')
            ->innerJoin('t_order_activities_prices', 'activity_prices', 't_activity_prices', 't_order_activities_prices.activity_type = t_activity_prices.activity_type and t_order_activities_prices.person_type = t_activity_prices.person_type')
            ->innerJoin('t_activity_prices', 'person_types', 't_person_types', 't_activity_prices.person_type = t_person_types.id')
            ->where('t_order_activities_prices.order_activity = :activity')
            ->setParameter('activity', $orderActivityId);
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Update transaction id for order
     *
     * @param $orderCode
     * @param $transactionId
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function updateTransactionId($orderCode, $transactionId) {
        // Update order total
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('orders')
            ->set('transaction_id', ':id')
            ->where('code = :code')
            ->setParameters([
                'id'    => $transactionId,
                'code'  => $orderCode
            ]);
        return $queryBuilder->execute();
    }

    /**
     * Retrieve operations list
     *
     * @return array
     */
    public function getOperationsList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_order_activities.id',
                't_orders.code',
                't_activities.name AS activity',
                't_cities.name AS city',
                't_countries.name AS country',
                'CAST(EXTRACT(epoch FROM t_orders.created_at) AS integer) as created_at',
                't_order_activities.date || \' \' || t_order_activities.time AS date',
                'SUM(t_order_activities_prices.price) AS price',
                't_orders.status'
            ])
            ->from('order_activities', 't_order_activities')
            ->innerJoin('t_order_activities', 'orders', 't_orders', 't_order_activities.order = t_orders.id')
            ->innerJoin('t_order_activities', 'activities', 't_activities', 't_order_activities.activity = t_activities.id')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_activities.city = t_cities.id')
            ->innerJoin('t_activities', 'countries', 't_countries', 't_activities.country = t_countries.id')
            ->innerJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities_prices.order_activity = t_order_activities.id')
            ->where('t_orders.payed_at IS NOT NULL') //show all orders
            ->andWhere('t_orders.is_deleted = false')
            ->groupBy([
                't_order_activities.id',
                't_orders.id',
                't_activities.id',
                't_cities.id',
                't_countries.id'
            ])
            ->orderBy('t_orders.created_at', 'DESC');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get operation detailed information
     *
     * @param $id
     * @return mixed
     */
    public function getOperation($id) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_orders.id AS order',
                't_orders.code',
                't_orders.name || \' \' || t_orders.surname AS client',
                't_orders.phone',
                't_orders.email',
                't_activities.name AS activity',
                't_activities.id AS activity_id',
                't_cities.name AS city',
                't_countries.name AS country',
                't_order_activities.date',
                't_order_activities.time',
                't_order_activities.comment',
                't_order_activities.admin_comment',
                't_orders.is_need_invoice AS need_invoice',
                't_activity_types.name AS type',
                'SUM(t_order_activities_prices.price) AS amount',
                'SUM(CASE WHEN t_person_types.is_adult = true THEN t_order_activities_prices.amount ELSE 0 END) AS adults',
                'SUM(CASE WHEN t_person_types.is_adult = false THEN t_order_activities_prices.amount ELSE 0 END) AS children'
            ])
            ->from('order_activities', 't_order_activities')
            ->innerJoin('t_order_activities', 'orders', 't_orders', 't_order_activities.order = t_orders.id')
            ->innerJoin('t_order_activities', 'activities', 't_activities', 't_order_activities.activity = t_activities.id')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_activities.city = t_cities.id')
            ->innerJoin('t_activities', 'countries', 't_countries', 't_activities.country = t_countries.id')
            ->innerJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities_prices.order_activity = t_order_activities.id')
            ->innerJoin('t_order_activities_prices', 'person_types', 't_person_types', 't_order_activities_prices.person_type = t_person_types.id')
            ->innerJoin('t_order_activities_prices', 'activity_types', 't_activity_types', 't_order_activities_prices.activity_type = t_activity_types.id')
            ->groupBy([
                't_order_activities.id',
                't_orders.id',
                't_activities.id',
                't_cities.id',
                't_countries.id',
                't_activity_types.id'
            ])
            ->where('t_order_activities.id = :id')
            ->andWhere('t_orders.is_deleted = false')
            ->setParameter('id', $id)
            ->setMaxResults(1);
        return $queryBuilder->execute()->fetch();
    }

    /**
     * Retrieve invoice params fro order
     *
     * @param $id
     * @return mixed
     */
    public function getOrderInvoice($id) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_order_invoice.tax_name',
                't_order_invoice.tax_id',
                't_order_invoice.document_type',
                't_order_invoice.address',
                't_order_invoice.city',
                't_order_invoice.country',
                't_order_invoice.postal',
            ])
            ->from('order_invoices', 't_order_invoice')
            ->where('t_order_invoice.order = :order')
            ->setMaxResults(1)
            ->setParameter('order', $id);
        return $queryBuilder->execute()->fetch();
    }

    /**
     * Set new status for order
     *
     * @param $code
     * @param $newStatus
     */
    public function updateOrderStatus($code, $newStatus) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('orders')
            ->set('status', ':status')
            ->where('code = :code')
            ->setParameters([
                'status'    => $newStatus,
                'code'      => $code
            ]);
        $queryBuilder->execute();
    }

    /**
     * Set new status for order
     *
     * @param $code
     */
    public function setViewedStatus($code) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('orders')
            ->set('viewed', 'true')
            ->where('code = :code')
            ->setParameters([
                'code'      => $code
            ]);
        $queryBuilder->execute();
    }

    /**
     * Set new status for order
     *
     * @param $id
     * @param $note
     */
    public function updateOperationNote($id, $note) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('order_activities')
            ->set('admin_comment', ':note')
            ->where('id = :id')
            ->setParameters([
                'note'  => $note,
                'id'    => $id
            ]);
        $queryBuilder->execute();
    }

    /**
     * Get random order code (for testing nad debug)
     *
     * @return bool|string
     */
    public function getRandomOrderCode() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('code')
            ->from('orders')
            ->where('is_payed = true')
            ->setMaxResults(1)
            ->orderBy('random()');
        return $queryBuilder->execute()->fetchColumn();
    }

    /**
     * Retrieve operations by order
     *
     * @param $code
     * @return array
     */
    public function getOrderOperations($code) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_order_activities.id')
            ->from('order_activities', 't_order_activities')
            ->innerJoin('t_order_activities', 'orders', 't_orders', 't_orders.id = t_order_activities.order')
            ->where('t_orders.code = :code')
            ->andWhere('t_orders.is_deleted = false')
            ->setParameter('code', $code);
        $result = [];
        $stmt = $queryBuilder->execute();
        while($id = $stmt->fetchColumn()) {
            array_push($result, $this->getOperation($id));
        }
        return $result;
    }

    /**
     * Return order code for transaction id
     *
     * @param $transactionId
     * @return bool|string
     */
    public function getOrderCodeByTransactionId($transactionId) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('code')
            ->from('orders')
            ->where('transaction_id = :id')
            ->setParameter('id', $transactionId)
            ->setMaxResults(1);

        return $queryBuilder->execute()->fetchColumn();
    }

    /**
     * Get data for email confirmation
     *
     * @param $code
     * @return mixed
     */
    public function getMailConfirmData($code) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_orders.id AS order',
                't_orders.code',
                't_orders.name || \' \' || t_orders.surname AS client',
                't_orders.phone',
                't_orders.email',
                't_orders.is_need_invoice',
                't_activities.name AS activity',
                't_activities.id AS activity_id',
                't_activities.language',
                't_activities.duration',
                't_activities.avatar',
                't_cities.name AS city',
                't_countries.name AS country',
                't_order_activities.date',
                't_order_activities.time',
                't_order_activities.comment',
                'SUM(t_order_activities_prices.price) AS amount',
                'SUM(CASE WHEN t_person_types.is_adult = true THEN t_order_activities_prices.amount ELSE 0 END) AS adults',
                'SUM(CASE WHEN t_person_types.is_adult = false THEN t_order_activities_prices.amount ELSE 0 END) AS children'
            ])
            ->from('orders', 't_orders')
            ->innerJoin('t_orders', 'order_activities', 't_order_activities', 't_order_activities.order = t_orders.id')
            ->innerJoin('t_order_activities', 'activities', 't_activities', 't_order_activities.activity = t_activities.id')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_activities.city = t_cities.id')
            ->innerJoin('t_activities', 'countries', 't_countries', 't_activities.country = t_countries.id')
            ->innerJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities_prices.order_activity = t_order_activities.id')
            ->innerJoin('t_order_activities_prices', 'person_types', 't_person_types', 't_order_activities_prices.person_type = t_person_types.id')
            ->groupBy([
                't_order_activities.id',
                't_orders.id',
                't_activities.id',
                't_cities.id',
                't_countries.id'
            ])
            ->where('t_orders.code = :code')
            ->andWhere('t_orders.is_deleted = false')
            ->setParameter('code', $code);
        $result = $queryBuilder->execute()->fetchAll();

        //add supplier`s information
        $result = array_map(function ($item) {
            $item['suppliers'] = $this->suppliersService->getForActivity($item['activity_id']);
            unset($item['activity_id']);
            return $item;
        }, $result);

        //replace avatar with base64 image
//        $result = array_map([$this, 'convertImage'], $result); //Removed, no gmail support
        $result = array_map([$this, 'populateAvatar'], $result);

        //convert date
        $result = array_map(function($item) {
            setlocale(LC_TIME, 'es_ES.UTF8');
            $date = new \DateTime($item['date']);
            //Sabado, 3 de junio de 2017
            $item['date'] = strftime('%A, %e de %B de %Y', $date->getTimestamp());
            return $item;
        }, $result);

        //convert duration
        $result = array_map(function($item) {
            $duration = $item['duration'];
            if($duration >=1440) {
                $add = $duration % 1440;
                $addHours = (int)($add / 60);
                $item['duration'] = (int)($duration / 1440) . ' días' . ($add > 0 ? ', ' . $addHours . ' horas' : '');
            } else if ($duration >= 60) {
                $add = $duration % 60;
                $item['duration'] = (int)($duration / 60) . ' horas' . ($add > 0 ? ', ' . $add . ' minutos': '');
            } else {
                $item['duration'] = $duration . ' minutos';
            }

            return $item;
        }, $result);

        return $result;
    }

    /**
     * Set delete status
     *
     * @param $idReport
     * @return int
     */
    public function markAsDeleted($idOrder) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('orders')
            ->set('is_deleted', 'true')
            ->where('id = :id')
            ->setParameter('id', $idOrder);

        return $queryBuilder->execute();
    }

    /**
     * Retrieve list of email that started order but didn`t finished
     *
     *
     * @return array
     */
    public function getUnpaidOrderEmails() {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('DISTINCT(email)')
            ->from('orders')
            ->where('is_payed = false');

        $result = [];
        $stmt = $queryBuilder->execute();
        while ($email = $stmt->fetchColumn()) {
            array_push($result, $email);
        }

        return $result;
    }

    /**
     * Resize and convert image to base64
     *
     * @param $item
     * @return mixed
     */
    private function convertImage($item) {
        $w = 184;
        $h = 141;
        $file = $this->pathToImages . DIRECTORY_SEPARATOR . $item['avatar'];
        if (!file_exists($file)) {
            $item['image_src'] = '';
            unset($item['avatar']);
            return $item;
        }

        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        // copy image to string
        ob_start();
        imagejpeg($dst);
        $contents = ob_get_contents();
        ob_end_clean();

        $item['image_src'] = "data:image/jpeg;base64," . base64_encode($contents);
        unset($item['avatar']);
        return $item;
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populateAvatar($itemData) {
        if(!empty($itemData['avatar'])) {
            $itemData['avatar'] = $this->urlToImages . $itemData['avatar'];
        }
        return $itemData;
    }
}
