<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.05.18
 * Time: 11:32
 */

namespace Civitours\Service;


use Civitours\Entity\ReviewData;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

/**
 * Service to operate with reviews
 *
 * Class ReviewService
 * @package Civitours\Service
 */
class ReviewService
{
    /**
     * @var Connection
     */
    private $db;
    private $urlToImages;

    /**
     * ReviewService constructor.
     * @param Connection $db
     * @param $urlToImages
     */
    public function __construct(Connection $db, $urlToImages)
    {
        $this->db = $db;
        $this->urlToImages = $urlToImages;
    }

    /**
     * Prepare query builder for review requests
     *
     * @param bool $onlyApproved
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getPreparedQueryBuilder($onlyApproved = true) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_reviews.id',
                't_reviews.header',
                't_reviews.text',
                't_reviews.rate',
                't_reviews.activity',
                't_reviews.is_approved',
                'CAST(EXTRACT(epoch FROM t_reviews.created_at) AS integer) as created_at',
                't_activities.avatar',
                't_activities.language',
                't_activities.duration',
                't_countries.id AS country_id',
                't_countries.name AS country',
                't_cities.id AS city_id',
                't_cities.name AS city',
                't_traveller_types.id AS traveller_type',
                't_traveller_types.name AS traveller_type_name',
                't_reviews.author AS user_name',
                't_reviews.order_code AS order_code',
            ])
            ->from('reviews', 't_reviews')
            ->innerJoin('t_reviews','activities','t_activities', 't_reviews.activity = t_activities.id')
            ->innerJoin('t_reviews','countries','t_countries', 't_reviews.country = t_countries.id')
            ->innerJoin('t_reviews','cities','t_cities', 't_reviews.city = t_cities.id')
            ->innerJoin('t_reviews','traveller_types','t_traveller_types', 't_reviews.traveller_type = t_traveller_types.id');
        if($onlyApproved) {
            $queryBuilder
                ->where('is_approved = :approved')
                ->setParameter('approved', $onlyApproved, ParameterType::BOOLEAN);
        }
        return $queryBuilder;
    }

    /**
     * Check user has owned review
     *
     * @param $idUser
     * @param $idReview
     * @return bool
     */
    public function isUserHasOwnedReview($idUser, $idReview) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('reviews')
            ->where('id = :id')
            ->andWhere('author = :author')
            ->setParameters([
                'id'        => $idReview,
                'author'    => $idUser
            ]);
        return $queryBuilder->execute()->rowCount() > 0;
    }

    /**
     * Retrieve review by id
     *
     * @param $id
     * @return mixed
     */
    public function getReviewById($id) {
        $queryBuilder = $this->getPreparedQueryBuilder(false);
        $queryBuilder
            ->andWhere('t_reviews.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1);
        return $this->populateAvatar($queryBuilder->execute()->fetch());
    }

    /**
     * Retrieve reviews for city
     *
     * @param $idCity
     * @param int $limit
     * @return array
     */
    public function getReviewsForCity($idCity, $limit = 4) {
        $queryBuilder = $this->getPreparedQueryBuilder();
        $queryBuilder
            ->andWhere('t_activities.city = :city')
            ->orderBy('created_at', 'DESC')
            ->setParameter('city', $idCity)
            ->setMaxResults($limit);

        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get reviews for activity
     *
     * @param $idActivity
     * @param int $limit
     * @param bool $onlyApproved
     * @return array
     */
    public function getReviewsForActivity($idActivity, $limit = 3, $onlyApproved = true) {
        $queryBuilder = $this->getPreparedQueryBuilder($onlyApproved);
        $queryBuilder
            ->andWhere('t_reviews.activity = :activity')
            ->orderBy('created_at', 'DESC')
            ->setParameter('activity', $idActivity);
        if($limit) {
            $queryBuilder->setMaxResults($limit);
        }
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get reviews for activity
     *
     * @param $idUser
     * @return array
     */
    public function getReviewsForUser($idUser) {
        $queryBuilder = $this->getPreparedQueryBuilder(false);
        $queryBuilder
            ->andWhere('t_reviews.user = :user')
            ->orderBy('t_reviews.id', 'ASC')
            ->setParameter('user', $idUser);
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Delete review from database
     *
     * @param $idReview
     */
    public function deleteReview($idReview) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->delete('reviews')
            ->where('id = :id')
            ->setParameter('id', $idReview);
        $queryBuilder->execute();
    }

    /**
     * Retrieve all traveller types
     *
     * @return array
     */
    public function getTravellerTypes() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(['id', 'name'])
            ->from('traveller_types');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * @param ReviewData $reviewData
     * @throws \Doctrine\DBAL\DBALException
     */
    public function create(ReviewData $reviewData) {
        $this->db->query("SET TIME ZONE 'UTC'");

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('reviews')
            ->setValue('header'         , ':header')
            ->setValue('text'           , ':text')
            ->setValue('rate'           , ':rate')
            ->setValue('author'         , ':author')
            ->setValue('activity'       , ':activity')
            ->setValue('traveller_type' , ':traveller')
            ->setValue('country'        , ':country')
            ->setValue('city'           , ':city')
            ->setValue('ip'             , ':ip')
            ->setValue('"user"'         , ':user')
            ->setValue('order_code'     , ':order')
            ->setParameters([
                'header'    => $reviewData->header,
                'text'      => $reviewData->text,
                'rate'      => $reviewData->rate,
                'author'    => $reviewData->user_name,
                'activity'  => $reviewData->activity,
                'traveller' => $reviewData->traveller_type,
                'country'   => $reviewData->country_id,
                'city'      => $reviewData->city_id,
                'ip'        => $reviewData->ip,
                'user'      => $reviewData->user,
            'order'         => $reviewData->order_code,
            ]);

        if (!empty($reviewData->created_at)) {
            $queryBuilder
                ->setValue('created_at', 'to_timestamp(:time)')
                ->setParameter('time', $reviewData->created_at);
        }
        $queryBuilder->execute();
    }

    /**
     * Check is exist
     *
     * @param $idActivity
     * @param $orderCode
     * @return bool
     */
    public function isExist($idActivity, $orderCode) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('reviews')
            ->where('activity = :activity')
            ->andWhere('order_code = :order')
            ->setParameters([
                'activity'  => $idActivity,
                'order'     => $orderCode
            ]);
        return $queryBuilder->execute()->rowCount() > 0;
    }

    /**
     * Update review with data
     *
     * @param $idReview
     * @param ReviewData $reviewData
     * @throws \Doctrine\DBAL\DBALException
     */
    public function update($idReview, ReviewData $reviewData) {
        $this->db->query("SET TIME ZONE 'UTC'");

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('reviews')
            ->where('id = :id')
            ->set('header'         , ':header')
            ->set('text'           , ':text')
            ->set('rate'           , ':rate')
            ->set('author'         , ':author')
            ->set('activity'       , ':activity')
            ->set('traveller_type' , ':traveller')
            ->set('country'        , ':country')
            ->set('city'           , ':city')
            ->set('ip'             , ':ip')
            ->set('"user"'         , ':user')
            ->setParameters([
                'header'    => $reviewData->header,
                'text'      => $reviewData->text,
                'rate'      => $reviewData->rate,
                'author'    => $reviewData->user_name,
                'activity'  => $reviewData->activity,
                'traveller' => $reviewData->traveller_type,
                'country'   => $reviewData->country_id,
                'city'      => $reviewData->city_id,
                'ip'        => $reviewData->ip,
                'user'      => $reviewData->user,
                'id'        => $idReview
            ]);

        if (!empty($reviewData->created_at)) {
            $queryBuilder
                ->set('created_at', 'to_timestamp(:time)')
                ->setParameter('time', $reviewData->created_at);
        }
        $queryBuilder->execute();
    }

    /**
     * Set approved status
     *
     * @param $idReview
     * @param $approvedStatus
     * @return int
     */
    public function setApprovedStatus($idReview, $approvedStatus) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('reviews')
            ->set('is_approved', ':is_approved')
            ->where('id = :id')
            ->setParameter('is_approved', $approvedStatus, ParameterType::BOOLEAN)
            ->setParameter('id', $idReview);

        return $queryBuilder->execute();
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populateAvatar($itemData) {
        if(!empty($itemData['avatar'])) {
            $itemData['avatar'] = $this->urlToImages . $itemData['avatar'];
        }
        return $itemData;
    }

    /**
     * @param $idUser
     * @param $ip
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function uploadCsv($idUser, $ip) {

        $result = [];
        $writeResultLog = function ($data) use (&$result) {
            array_push($result, $data);
        };

        if ('text/plain' !== mime_content_type($_FILES['file']['tmp_name'])) {
            $writeResultLog('Cannot process non-text file');
            return $result;
        }

        $fileHandler = fopen($_FILES['file']['tmp_name'], 'r');


        //Skip header
        $line = fgets($fileHandler);

        // Process file
        $counter = 0;
        while ($line = fgets($fileHandler)) {
            $lineResult = $this->processCsvLine($line, $idUser, $ip);
            if(!empty($lineResult)) {
                $writeResultLog($lineResult);
            } else {
                ++$counter;
            }
        }
        $writeResultLog("Parsing complete. Added ${counter} reviews");
        return $result;
    }

    /**
     * Process line from csv file
     *
     * @param $line
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    private function processCsvLine($line, $idUser, $ip) {
        $dataArray = str_getcsv($line);
        if (count($dataArray) != 9) {
            return "Wrong line format (${line})";
        }

        list ($activity, $travellerType, $rate, $name, $country, $city, $date, $header, $text) = $dataArray;

        // Perform checks
        if(!$this->checkTravellerType($travellerType)) return "Wrong traveller type: {$travellerType} ({$line})";
        if(!$this->checkActivity($activity)) return "Wrong activity: {$activity} ({$line})";
        if($rate < 0 || $rate > 5) return "Wrong rate: {$rate} ({$line})";
        if(empty($name)) return "Name is empty ({$line})";
        if(!($countryId = $this->checkCountry($country))) return "Country not found: {$country} ({$line})";
        if(!($cityId = $this->checkCity($countryId, $city))) return "City not found in country: {$country} -> {$city} ({$line})";
        if(!($time = strtotime($date))) return "Unsupported time format: {$date} ({$line})";
        if(empty($header)) return "Header is empty ({$line})";
        if(empty($text)) return "Text is empty ({$line})";

        $data = new ReviewData();
        $data->header = $header;
        $data->text = $text;
        $data->rate = $rate * 2;
        $data->traveller_type = $travellerType;
        $data->created_at = $time;
        $data->activity = $activity;
        $data->city_id = $cityId;
        $data->country_id = $countryId;
        $data->user_name = $name;
        $data->user = $idUser;
        $data->ip = $ip;

        $this->create($data);

        return '';
    }

    /**
     * Check traveller type
     *
     * @param $type
     * @return bool
     */
    private function checkTravellerType($type) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('name')
            ->from('traveller_types')
            ->where('id = :id')
            ->setParameter('id', $type)
            ->setMaxResults(1);

        return $queryBuilder->execute()->rowCount() > 0;
    }

    /**
     * Check activity
     *
     * @param $activity
     * @return bool
     */
    private function checkActivity($activity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('name')
            ->from('activities')
            ->where('id = :id')
            ->setParameter('id', $activity)
            ->setMaxResults(1);

        return $queryBuilder->execute()->rowCount() > 0;
    }

    /**
     * Check country
     *
     * @param $country
     * @return bool|string
     */
    private function checkCountry($country) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('countries')
            ->where('name = :name')
            ->setParameter('name', $country)
            ->setMaxResults(1);

        return $queryBuilder->execute()->fetchColumn();
    }

    /**
     * Check city
     *
     * @param $countryId
     * @param $city
     * @return bool|string
     */
    private function checkCity($countryId, $city) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('cities')
            ->where('country = :country')
            ->andWhere('name = :name')
            ->setParameter('country', $countryId)
            ->setParameter('name', $city)
            ->setMaxResults(1);

        return $queryBuilder->execute()->fetchColumn();
    }
}