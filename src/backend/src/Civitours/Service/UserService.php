<?php

namespace Civitours\Service;

use Civitours\Entity\AccountData;
use Civitours\Entity\NewPasswordData;
use Civitours\Entity\RegistrationData;
use Civitours\Entity\SocialLoginData;
use Doctrine\DBAL\Connection;
use Rhumsaa\Uuid\Uuid;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;


/**
 * Service to perform operations with users
 *
 * Class UserService
 * @package Civitours\Service
 */
class UserService
{

    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * Service to send mails
     *
     * @var MailService
     */
    private $mailService = null;

    /**
     * Link to confirm mail
     *
     * @var string
     */
    private $confirmLink = null;

    /**
     * Link to reset mail
     *
     * @var string
     */
    private $resetLink;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param MailService $mailService
     * @param $confirmLink
     * @param $resetLink
     */
    public function __construct(Connection $db, MailService $mailService, $confirmLink, $resetLink)
    {
        $this->db = $db;
        $this->mailService = $mailService;
        $this->confirmLink = $confirmLink;
        $this->resetLink = $resetLink;
    }

    /**
     * Register user in system
     *
     * @param RegistrationData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function registerUser(RegistrationData $data) {
        $emailConfirmationCode = Uuid::uuid4();

        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();

            $queryBuilder
                ->insert('users')
                ->values([
                    'name'              => '?',
                    'surname'           => '?',
                    'email'             => '?',
                    'confirmation_code' => '?',
                    'password'          => '?',
                    'phone'             => '?',
                    'country'           => '?',
                    'city'              => '?'
                ])->setParameters([
                    $data->name,
                    $data->surname,
                    $data->email,
                    $emailConfirmationCode,
                    password_hash($data->password, PASSWORD_DEFAULT),
                    $data->phone,
                    $data->country,
                    $data->city
                ]);

            $queryBuilder->execute();
            $this->mailService->sendVerificationEmail($data->email, $this->buildConfirmationLink($emailConfirmationCode));
            $this->db->commit();
        } catch ( \Exception $e) {
            $this->db->rollBack();
            throw new ServiceUnavailableHttpException(1, $e->getMessage());
        }

    }

    /**
     * Resend confirmation email
     *
     * @param $email
     * @return bool
     */
    public function resendVerificationMail($email) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('confirmation_code')
            ->from('users')
            ->where( $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('email', '?'),
                $queryBuilder->expr()->isNotNull('confirmation_code')
            ))
            ->setMaxResults(1)
            ->setParameter(0, $email);

        $user = $queryBuilder->execute()->fetch();
        if (false === $user) {
            return false;
        }

        $this->mailService->sendVerificationEmail($email, $this->buildConfirmationLink($user['confirmation_code']));
        return true;
    }

    /**
     * Reset user password
     *
     * @param $email
     * @return bool
     */
    public function resetPassword($email) {
        $passwordReset = Uuid::uuid4();

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('recovery_code', '?')
            ->where('email = ?')
            ->setParameter(0, $passwordReset)
            ->setParameter(1, $email);

        $queryBuilder->execute();
        $this->mailService->sendPasswordResetEmail($email, $this->buildResetLink($passwordReset));
        return true;

    }

    /**
     * Perform verify process
     *
     * @param $code
     * @return bool
     */
    public function verifyMail($code) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('id')
            ->from('users')
            ->where('confirmation_code = ?')
            ->setMaxResults(1)
            ->setParameter(0, $code);

        $user = $queryBuilder->execute()->fetch();
        if (false === $user) {
            return false;
        }

        $queryBuilder
            ->update('users')
            ->where('confirmation_code = ?')
            ->setParameter(0, $code)
            ->set('confirmation_code', 'NULL')
            ->set('email_confirmed', 'true');

        $queryBuilder->execute();
        return true;
    }

    /**
     * Get query builder for user
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function prepareQueryBuilder() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_users.*',
                't_countries.name AS country_name',
                't_cities.name AS city_name',
            ])
            ->from('users', 't_users')
            ->innerJoin('t_users', 'countries', 't_countries', 't_users.country = t_countries.id')
            ->innerJoin('t_users', 'cities', 't_cities', 't_users.city = t_cities.id');

        return $queryBuilder;
    }

    /**
     * Find user by email
     *
     * @param $email
     * @return array|false
     */
    public function getUserByEmail($email) {
        $queryBuilder = $this->prepareQueryBuilder();

        $queryBuilder
            ->andWhere('t_users.email = ?')
            ->setMaxResults(1)
            ->setParameter(0, $email);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Get user by id
     *
     * @param $idUser
     * @return array|false
     */
    public function getUserById($idUser) {
        $queryBuilder = $this->prepareQueryBuilder();

        $queryBuilder
            ->andWhere('t_users.id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $idUser);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Find user by social id
     *
     * @param $provider
     * @param $id
     * @return array|false
     */
    public function getUserBySocialId($provider, $id) {
        $queryBuilder = $this->prepareQueryBuilder();

        $queryBuilder
            ->andWhere($provider . '_id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Create new user from social data
     *
     * @param SocialLoginData $data
     */
    public function createUserFromSocialData(SocialLoginData $data) {

        list ($name, $surname) = explode(' ', $data->name);
        $queryBuilder = $this->db->createQueryBuilder();

        $user = $this->getUserByEmail($data->email);
        if ($user) {
            // link accounts
            $queryBuilder
                ->update('users')
                ->set($data->provider . '_id', '?')
                ->where('email = ?')
                ->setParameter(0, $data->id)
                ->setParameter(1, $data->email);
        } else {
            //create new account
            $queryBuilder
                ->insert('users')
                ->values([
                    'name'                  => '?',
                    'surname'               => '?',
                    'email'                 => '?',
                    'email_confirmed'       => '?',
                    'password'              => '?',
                    $data->provider . '_id' => '?'
                ])->setParameters([
                    $name,
                    $surname,
                    $data->email,
                    true,
                    'social',
                    $data->id
                ]);
        }

        $queryBuilder->execute();
    }

    /**
     * Update existing user from social data
     *
     * @param SocialLoginData $data
     * @return int
     */
    public function updateUserFromSocialData(SocialLoginData $data) {
        list ($name, $surname) = explode(' ', $data->name);

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('name', '?')
            ->set('surname', '?')
            ->set('email', '?')
            ->where($data->provider . '_id = ?')
            ->setParameter(0, $name)
            ->setParameter(1, $surname)
            ->setParameter(2, $data->email)
            ->setParameter(3, $data->id);

        return $queryBuilder->execute();
    }

    /**
     * Find user by reset token
     *
     * @param $token
     * @return array|false
     */
    public function getUserByRecoveryToken($token) {
        $queryBuilder = $this->prepareQueryBuilder();

        $queryBuilder
            ->andWhere('t_users.recovery_code = ?')
            ->setMaxResults(1)
            ->setParameter(0, $token);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Set new password
     *
     * @param NewPasswordData $data
     */
    public function setNewPassword(NewPasswordData $data) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('users')
            ->set('password', '?')
            ->set('recovery_code', 'NULL')
            ->where('recovery_code = ?')
            ->setParameter(0, password_hash($data->password, PASSWORD_DEFAULT))
            ->setParameter(1, $data->token);

        $queryBuilder->execute();
    }

    /**
     * Build confirmation link
     *
     * @param $emailConfirmationCode
     * @return string
     */
    private function buildConfirmationLink($emailConfirmationCode) {
        return $this->confirmLink . DIRECTORY_SEPARATOR . $emailConfirmationCode;
    }

    /**
     * Build reset link
     *
     * @param $resetConfirmationCode
     * @return string
     */
    private function buildResetLink($resetConfirmationCode) {
        return $this->resetLink . $resetConfirmationCode;
    }

    /**
     * Login user
     *
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($email, $password) {

        $user = $this->getUserByEmail($email);
        if (!$user) {
            return false;
        }

        if (!password_verify($password, $user['password'])) {
            return false;
        }
        return $user;

    }

    /**
     * Check user password
     *
     * @param $id
     * @param $password
     * @return bool
     */
    public function checkPassword($id, $password) {
        $user = $this->getUserById($id);
        if (!$user) {
            return false;
        }
        return password_verify($password, $user['password']);
    }

    /**
     * Update user password
     *
     * @param $id
     * @param $password
     */
    public function updatePassword($id, $password) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('users')
            ->set('password', ':password')
            ->where('id = :id')
            ->setParameter('password', password_hash($password, PASSWORD_DEFAULT))
            ->setParameter('id', $id);
        $queryBuilder->execute();
    }

    /**
     * Update account data for user
     *
     * @param $id
     * @param AccountData $data
     */
    public function updateAccountData($id, AccountData $data) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('users')
            ->set('name', ':name')
            ->set('surname', ':surname')
            ->set('country', ':country')
            ->set('city', ':city')
            ->set('email', ':email')
            ->set('phone', ':phone')
            ->where('id = :id')
            ->setParameters([
               'name'       => $data->name,
               'surname'    => $data->surname,
               'country'    => $data->country,
               'city'       => $data->city,
               'email'      => $data->email,
               'phone'      => $data->phone,
               'id'         => $id
            ]);
        $queryBuilder->execute();
    }

    /**
     * Retrieve admin`s email list
     *
     * @return array
     */
    public function getAdminEmailsList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('email')
            ->from('users')
            ->where('is_admin = true');

        $result = [];
        $stmt = $queryBuilder->execute();
        while($email = $stmt->fetchColumn()) {
            array_push($result, $email);
        }
        return $result;
    }

}