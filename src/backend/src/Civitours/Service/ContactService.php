<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.07.18
 * Time: 17:37
 */

namespace Civitours\Service;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

class ContactService
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * ContactService constructor.
     *
     * @param Connection $db
     * @param UploadService $uploadService
     */
    public function __construct(Connection $db, UploadService $uploadService)
    {
        $this->db = $db;
        $this->uploadService = $uploadService;
    }

    /**
     * Create new request
     *
     * @param $data
     */
    public function create($data) {

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('contact_requests')
            ->setValue('has_reservation', ':has')
            ->setValue('number'         , ':number')
            ->setValue('name'           , ':name')
            ->setValue('email'          , ':email')
            ->setValue('subject'        , ':subject')
            ->setValue('destination'    , ':destination')
            ->setValue('message'        , ':message')
            ->setParameters([
                'number'        => $data['number'],
                'name'          => $data['name'],
                'email'         => $data['email'],
                'subject'       => $data['subject'],
                'destination'   => $data['destination'],
                'message'       => $data['message']
            ])
            ->setParameter('has', $data['hasReservation'], ParameterType::BOOLEAN);

        if(!empty($data['file'])){
            $queryBuilder
                ->setValue('file', ':file')
                ->setParameter('file', $data['file']);
        }
        $queryBuilder->execute();

    }


    /**
     * Create new request
     *
     * @param $data
     */
    public function createFaq($data) {

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('faq_requests')
            ->setValue('email'          , ':email')
            ->setValue('subject'        , ':subject')
            ->setValue('description'    , ':description')
            ->setParameters([
                'email'         => $data['email'],
                'subject'       => $data['subject'],
                'description'   => $data['description']
            ]);

       if(!empty($data['file'])){
            $queryBuilder
               ->setValue('file', ':file')
               ->setParameter('file', $data['file']);
       }
       $queryBuilder->execute();

    }
}