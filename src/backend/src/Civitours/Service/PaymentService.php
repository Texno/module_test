<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.06.18
 * Time: 12:23
 */

namespace Civitours\Service;

use Buuum\Redsys;
use PayPal\Api\Payment;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PaymentService
{
    /**
     * @var array
     */
    private $paypalConfig;
    /**
     * @var array
     */
    private $redsysConfig;
    private $baseUrl;

    /**
     * @var LoggerInterface
     */
    private $logger;
    private $apiUrl;

    /**
     * UserService constructor.
     * @param array $paypalConfig
     * @param array $redsysConfig
     * @param $baseUrl
     * @param $logger
     */
    public function __construct(array $paypalConfig, array $redsysConfig, $baseUrl, $apiUrl, $logger)
    {
        $this->paypalConfig = $paypalConfig;
        $this->redsysConfig = $redsysConfig;
        $this->baseUrl = $baseUrl;
        $this->logger = $logger;
        $this->apiUrl = $apiUrl;
    }

    /**
     * Check payment by gateway
     *
     * @param $paymentData
     * @param $orderData
     * @return bool
     * @throws \Exception
     */
    public function verifyPayment(&$paymentData, $orderData) {
        $this->logger->notice('Starting verify payment');
        $this->logger->notice(print_r($paymentData, true));
        if (empty($paymentData['gateway'])) {
            $this->logger->warning('Empty gateway is provided');
            throw new BadRequestHttpException('Wrong payment data is provided');
        }
        switch ($paymentData['gateway']) {
            case 'paypal':
                return $this->verifyPaypalPayment($paymentData);
            case 'redsys':
                return $orderData[0]['is_payed'] == 'true'; // In case of redsys order should be updated
            default:
                $this->logger->warning('Unknown gateway: ' . $paymentData['gateway']);
                throw new BadRequestHttpException('Unknown payment gateway is provided');
        }
    }

    /**
     * Verify payment for paypal
     *
     * @param $paymentData
     * @return bool
     */
    private function verifyPaypalPayment($paymentData) {
        if(empty($paymentData['transactionId'])) {
            $this->logger->warning('Empty transaction id is provided');
            $this->logger->warning(print_r($paymentData, true));
            throw new BadRequestHttpException('Wrong payment data is provided');
        }
        $apiContext = new ApiContext(new OAuthTokenCredential($this->paypalConfig['clientId'], $this->paypalConfig['clientSecret']));
        $apiContext->setConfig(['mode' => $this->paypalConfig['mode']]);
        $payment = Payment::get($paymentData['transactionId'], $apiContext);
        $result = $payment->getState() == 'approved';
        if(!$result) {
            $this->logger->warning('Wrong paypal payment state received: ', $payment->getState());
        }
        return $result;
    }

    /**
     * Verify payment for redsys
     *
     * @param $paymentData
     * @return array
     * @throws \Exception
     */
    public function verifyRedsysPayment($paymentData) {
        if(empty($paymentData['signatureVersion'] || empty($paymentData['merchantParameters']) || empty($paymentData['signature']))) {
            $this->logger->warning('Wrong redsys payment data is provided');
            $this->logger->warning(print_r($paymentData, true));
            throw new BadRequestHttpException('Wrong payment data is provided');
        }

        $redsys = new Redsys($this->redsysConfig['merchantKey']);
        $result = $redsys->checkPaymentResponse([
            'Ds_MerchantParameters' => $paymentData['merchantParameters'],
            'Ds_Signature'          => $paymentData['signature'],
        ]);
        return $result;
    }

    /**
     * Get redsys payment button
     * @param $orderCode
     * @param $code
     * @param $amount
     * @return string
     * @throws \Exception
     */
    public function performRedsysWebPayment($orderCode, $code, $amount) {
        $redsys = new Redsys($this->redsysConfig['merchantKey']);

        $testMode = $this->redsysConfig['testMode'];

        $redsys->setMerchantcode($this->redsysConfig['merchantCode']);
        $redsys->setAmount($amount);
        $redsys->setOrder($code);
        $redsys->setTerminal($this->redsysConfig['merchantTerminal']);
        $redsys->setCurrency($this->redsysConfig['merchantCurrency']);

        $redsys->setUrlOk($this->baseUrl . '/order/thank-you/' . $orderCode);
        $redsys->setUrlKo($this->baseUrl . '/checkout/payment?redsys=failed');
        $redsys->setNotification($this->apiUrl . '/redsys_notify');

        $redsys->setTransactiontype('0');
        $redsys->setMethod('C');

        $redsys->setTradeName($this->redsysConfig['tradeName']);
        $redsys->setTitular($this->redsysConfig['titular']);
        $redsys->setProductDescription($this->redsysConfig['description']);

        return $redsys->createForm($testMode ? 'test' : 'live', ['submit_value' => 'Pagar']);
    }

    /**
     * Redsys order code generation
     * @return string
     */
    public function generateOrderCodeForRedSys() {
        return rand(11, 99) . substr(uniqid(), 0, 10);
    }

}
