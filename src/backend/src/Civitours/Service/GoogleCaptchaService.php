<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.18
 * Time: 17:14
 */

namespace Civitours\Service;

use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Service to perform recaptcha operations
 *
 * Class GoogleCaptchaService
 * @package Civitours\Service
 */
class GoogleCaptchaService
{
    const API_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Secret server code
     *
     * @var string
     */
    private $secret = null;

    /**
     * GoogleCaptchaService constructor.
     * @param $secret
     */
    public function __construct($secret)
    {
        $this->secret = $secret;
    }

    /**
     * Validate user token from recaptcha
     *
     * @param $token
     * @return mixed
     */
    public function validateToken($token) {
        $result = $this->makeRequest($token);
        if (!$result || !isset($result['success'])) {
            throw new ServiceUnavailableHttpException(null, 'Wrong recaptcha response');
        }
        return $result['success'];
    }

    /**
     * Perform curl request to recaptcha service
     *
     * @param $token
     * @return mixed
     */
    private function makeRequest($token) {

        $fields = "secret={$this->secret}&response=$token";

        $post = curl_init();

        curl_setopt($post, CURLOPT_URL, self::API_URL);
        curl_setopt($post, CURLOPT_POST, 2);
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($post), true);

        curl_close($post);
        return $result;
    }

}
