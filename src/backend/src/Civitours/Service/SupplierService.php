<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.18
 * Time: 15:21
 */

namespace Civitours\Service;


use Civitours\Entity\SupplierData;
use Doctrine\DBAL\Connection;


/**
 * Service to operate with suppliers
 *
 * Class SupplierService
 * @package Civitours\Service
 */
class SupplierService
{
    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;


    /**
     * UserService constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db) {
        $this->db = $db;
    }

    /**
     * Prepare query builder with data fields
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function prepareQueryBuilder() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_suppliers.id',
                't_suppliers.name',
                't_suppliers.phone',
                't_suppliers.email',
                't_suppliers.contact',
                't_suppliers.vat',
                't_suppliers.post_code',
                't_suppliers.website',
                't_suppliers.bank_details',
                'CAST(EXTRACT(epoch FROM t_suppliers.registered_at) AS integer) AS registered_at'
            )
            ->from('suppliers', 't_suppliers');
        return $queryBuilder;
    }

    /**
     * Retrieve list of suppliers
     *
     * @return array
     */
    public function getList() {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->orderBy('id', 'ASC');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get supplier info by id
     *
     * @param $id
     * @return mixed
     */
    public function getById($id) {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->andWhere('t_suppliers.id = :id')
            ->setParameter('id', $id);
        return $queryBuilder->execute()->fetch();
    }

    /**
     * Create new supplier
     *
     * @param SupplierData $data
     * @throws \Doctrine\DBAL\DBALException
     */
    public function create(SupplierData $data) {
        $this->db->query("SET TIME ZONE 'UTC'");

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('suppliers')
            ->setValue('name'        , ':name')
            ->setValue('phone'       , ':phone')
            ->setValue('email'       , ':email')
            ->setValue('contact'     , ':contact')
            ->setValue('vat'         , ':vat')
            ->setValue('post_code'   , ':post_code')
            ->setValue('website'     , ':website')
            ->setValue('bank_details', ':bank_details')
            ->setParameters([
                'name'          => $data->name,
                'phone'         => $data->phone,
                'email'         => $data->email,
                'contact'       => $data->contact,
                'vat'           => $data->vat,
                'post_code'     => $data->post_code,
                'website'       => $data->website,
                'bank_details'  => $data->bank_details,
            ]);
        $queryBuilder->execute();
    }

    /**
     * Update existing supplier
     *
     * @param $idSupplier
     * @param SupplierData $data
     * @throws \Doctrine\DBAL\DBALException
     */
    public function update($idSupplier, SupplierData $data) {
        $this->db->query("SET TIME ZONE 'UTC'");

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->update('suppliers')
            ->where('id = :id')
            ->set('name'        , ':name')
            ->set('phone'       , ':phone')
            ->set('email'       , ':email')
            ->set('contact'     , ':contact')
            ->set('vat'         , ':vat')
            ->set('post_code'   , ':post_code')
            ->set('website'     , ':website')
            ->set('bank_details', ':bank_details')
            ->setParameters([
                'name'          => $data->name,
                'phone'         => $data->phone,
                'email'         => $data->email,
                'contact'       => $data->contact,
                'vat'           => $data->vat,
                'post_code'     => $data->post_code,
                'website'       => $data->website,
                'bank_details'  => $data->bank_details,
                'id'            => $idSupplier
            ]);
        $queryBuilder->execute();
    }

    /**
     * Delete supplier from database
     *
     * @param $idSupplier
     */
    public function delete($idSupplier) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->delete('suppliers')
            ->where('id = :id')
            ->setParameter('id', $idSupplier);
        $queryBuilder->execute();
    }

    /**
     * Get suppliers for activity
     *
     * @param $idActivity
     * @return array
     */
    public function getForActivity($idActivity) {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->innerJoin('t_suppliers', 'activity_suppliers', 't_activity_suppliers', 't_activity_suppliers.activity = :activity AND t_suppliers.id = t_activity_suppliers.supplier')
            ->setParameter('activity', $idActivity);
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get mail list for activity
     *
     * @param $idActivity
     * @return array
     */
    public function getMailListForActivity($idActivity) {
        $result = [];
        $suppliers = $this->getForActivity($idActivity);
        foreach ($suppliers as $supplier) {
            array_push($result, $supplier['email']);
        }
        return $result;
    }
}
