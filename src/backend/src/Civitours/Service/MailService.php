<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.18
 * Time: 13:58
 */

namespace Civitours\Service;

use Pug\Facade as PugFacade;
use Pug\Pug;
use Swift_Attachment;

/**
 * Service to send mails
 *
 * Class MailService
 * @package Civitours\Service
 */
class MailService
{

    const EMAIL_VERIFICATION_TEMPLATE_NAME = 'emailVerification';
    const PASSWORD_RESET_TEMPLATE_NAME = 'passwordReset';
    const ADMIN_PAYMENT_CONFIRM = 'adminPaymentConfirm';
    const USER_PAYMENT_CONFIRM = 'userPaymentConfirm';
    const SUPPLIER_PAYMENT_CONFIRM = 'supplierPaymentConfirm';
    const ADMIN_CONTACT_REQUEST = 'adminContactRequest';

    const MAIL_SENDER = 'no-reply@civitours.com';

    /**
     * Swift mailer
     *
     * @var \Swift_Mailer
     */
    private $mailer = null;

    /**
     *  Path to email templates
     *
     * @var string
     */
    private $pathToTemplates = null;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * MailService constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param $pathToTemplates
     */
    public function __construct(\Swift_Mailer $mailer, $pathToTemplates, $twig)
    {
        $this->mailer = $mailer;
        $this->pathToTemplates = $pathToTemplates;
        $this->twig = $twig;
    }

    /**
     * Send verification mail to user
     *
     * @param $mailto
     * @param $link
     */
    public function sendVerificationEmail($mailto, $link)
    {
        $this->sendMail(
            $mailto,
            'Verify your mail',
            self::EMAIL_VERIFICATION_TEMPLATE_NAME,
            [
                'link' => $link,
                'mail' => $mailto
            ]
        );
    }

    /**
     * Send password reset mail to user
     *
     * @param $mailto
     * @param $link
     */
    public function sendPasswordResetEmail($mailto, $link)
    {
        $this->sendMail(
            $mailto,
            'Reset your password',
            self::PASSWORD_RESET_TEMPLATE_NAME,
            [
                'link' => $link,
                'mail' => $mailto
            ]
        );
    }

    /**
     * Send confirmation email to admin
     *
     * @param $mailto
     * @param $orderData
     */
    public function sendAdminPaymentConfirmEmail($mailto, $orderData) {
        if (empty($orderData)) return; //No operations - no mail
        $this->sendMail(
            $mailto,
            'Tu reserva ha quedado confirmada',
            self::ADMIN_PAYMENT_CONFIRM,
            [
                'user_name' => 'Admin',
                'data'      => $orderData
            ]
        );
    }

    /**
     * Send copy of request to admin
     *
     * @param $mailto
     * @param $data
     * @param null $file
     */
    public function sendAdminContactRequest($mailto, $data, $file = null) {
        $this->sendMail(
            $mailto,
            'Civitours submitted form: ' . $data['type'],
            self::ADMIN_CONTACT_REQUEST,
            $data,
            $file
        );
    }

    /**
     * Send confirmation email to user
     *
     * @param $mailto
     * @param $orderData
     */
    public function sendUserPaymentConfirmEmail($mailto, $orderData) {
        if (empty($orderData)) return; //No operations - no mail
        $this->sendMail(
            $mailto,
            'Tu reserva ha quedado confirmada',
            self::USER_PAYMENT_CONFIRM,
            [
                'user_name' => $orderData[0]['client'],
                'data'      => $orderData
            ]
        );
    }

    /**
     * Send confirmation email to supplier
     *
     * @param $mailto
     * @param $orderData
     */
    public function sendSupplierPaymentConfirmEmail($mailto, $orderData) {
        if (empty($orderData)) return; //No operations - no mail
        $this->sendMail(
            $mailto,
            'Booking Request/Petición de Reserva',
            self::SUPPLIER_PAYMENT_CONFIRM,
            [
                'user_name' => $orderData[0]['client'],
                'data'      => $orderData
            ]
        );
    }

    /**
     * Send mail via mail transport
     *
     * @param $mailto
     * @param $subject
     * @param $templateName
     * @param array $params
     * @param null $file
     */
    protected function sendMail($mailto, $subject, $templateName, $params = [], $file = null)
    {
        if(!is_array($mailto)) {
            $mailto[0] = $mailto;
        }

        $html = '';
        if(file_exists($this->pathToTemplates . DIRECTORY_SEPARATOR. $templateName . '.html.twig')) {
            try {
                $html = $this->twig->render('mails' . DIRECTORY_SEPARATOR . $templateName . '.html.twig', $params);
            } catch (\Exception $e) { print_r($e->getMessage()); }
        }
        if(empty($html)) {
            $html = PugFacade::renderFile($this->pathToTemplates . DIRECTORY_SEPARATOR. $templateName . '.pug', $params);
        }

        $message = \Swift_Message::newInstance(null)
            ->setSubject($subject)
            ->setFrom(self::MAIL_SENDER)
            ->setTo($mailto)
            ->setBody(
                $this->parsePlainMessage(
                    $this->pathToTemplates . DIRECTORY_SEPARATOR. $templateName . '.plain', $params
                )
            )
            ->addPart(
                $html,
                'text/html'
            );

        if ($file) {
           $message->attach(Swift_Attachment::fromPath($file));
        }

        $this->mailer->send($message);
    }

    /**
     * Convert plain template to string with extended variables
     *
     * @param $template
     * @param array $params
     * @return bool|mixed|string
     */
    private function parsePlainMessage($template, $params = []) {
        $data = file_get_contents($template);
        foreach ($params as $name => $value) {
//            $data = str_replace("%{$name}%", $value, $data);
        }
        return $data;
    }
}