<?php

namespace Civitours\Service;

use Civitours\Entity\ActivityData;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ActivityService
{

    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * Url to images directory
     *
     * @var string
     */
    private $urlToImages = '';

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param $urlToImages
     * @param UploadService $uploadService
     */
    public function __construct(Connection $db, $urlToImages, UploadService $uploadService)
    {
        $this->db = $db;
        $this->urlToImages = $urlToImages;
        $this->uploadService = $uploadService;
    }

    /**
     * Get list of countries with activities available
     *
     * @param bool $excludeDraft
     * @param string $query
     * @return array
     */
    public function getCountriesList($excludeDraft = true, $query = '') {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.languages',
                't_countries.locale_name',
                't_countries.avatar',
                't_countries.avatar_small',
                't_countries.route_name',
                'COUNT(t_activities.id) AS activities'
            )
            ->from('activities', 't_activities')
            ->innerJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->orderBy('name', 'ASC')
            ->where('t_activities.is_deleted = false')
            ->groupBy(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.languages',
                't_countries.locale_name',
                't_countries.avatar',
                't_countries.avatar_small',
                't_countries.route_name'
            );

        if(!empty($query)) {
            $queryBuilder
                ->andWhere('t_countries.name ILIKE :query')
                ->setParameter('query', "%{$query}%");
        }

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll()));
    }

    /**
     * Get list of countries with activities available
     *
     * @param $countryCode
     * @param bool $excludeDraft
     * @param string $query
     * @return array
     */
    public function getCityList($countryCode, $excludeDraft = true, $query = '') {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.avatar_small',
                't_cities.offset',
                't_cities.locale_name',
                't_cities.latitude',
                't_cities.longitude',
                't_cities.route_name',
                't_cities.travellers',
                't_countries.route_name AS country_route',
                'COUNT(t_activities.id) AS activities'
            )
            ->from('activities', 't_activities')
            ->innerJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->innerJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->where('t_activities.is_deleted = false')
            ->orderBy('name', 'ASC')
            ->groupBy(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.avatar_small',
                't_cities.offset',
                't_cities.latitude',
                't_cities.longitude',
                't_cities.locale_name',
                't_cities.route_name',
                't_cities.travellers',
                't_countries.route_name'
            );

        if(!empty($query)) {
            $queryBuilder
                ->andWhere('t_cities.name ILIKE :query')
                ->setParameter('query', "%{$query}%");
        }

        if(!empty($countryCode)) {
            if (intval($countryCode)) {
                $queryBuilder
                    ->andWhere('t_countries.id = ?')
                    ->setParameter(0, intval($countryCode));
            } else {
                $queryBuilder
                    ->andWhere('t_countries.code = ?')
                    ->setParameter(0, $countryCode);
            }
        }

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll()));
    }

    /**
     * Get country info with activities
     *
     * @param $routeName
     * @return mixed
     */
    public function getCountryInfo($routeName) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.locale_name',
                't_countries.languages',
                't_countries.avatar',
                't_countries.avatar_small',
                't_countries.route_name',
                'count(DISTINCT(a.id)) as excursions',
                'count(DISTINCT(r.id)) as reviews',
                'coalesce(round(avg(r.rate), 1), 0) as rate'
            )
            ->from('countries', 't_countries')
            ->leftJoin('t_countries','activities','a', 't_countries.id = a.country')
            ->leftJoin('a','reviews','r', 'a.id = r.activity AND r.is_approved = true')
            ->where('t_countries.route_name = :route')
            ->andWhere('a.is_deleted = false')
            ->setParameter('route', $routeName)
            ->groupBy(
                't_countries.id',
                't_countries.code',
                't_countries.name',
                't_countries.currency',
                't_countries.calling_code',
                't_countries.locale_name',
                't_countries.languages',
                't_countries.avatar',
                't_countries.avatar_small'
            );

        $result = $queryBuilder->execute()->fetch();
        $result['travelers'] = $this->getTravellersForCountry($result['id']);

        return $this->extractLocaleName($this->populateAvatar($result));
    }

    /**
     * Get list of country destinations
     *
     * @param $countryCode
     * @param null $sort
     * @return array
     */
    public function getDestinations($countryCode = null, $sort = null) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.avatar_small',
                't_cities.locale_name',
                't_cities.offset',
                't_cities.latitude',
                't_cities.longitude',
                't_cities.route_name',
                't_cities.travellers AS travelers',
                't_countries.route_name as country_route',
                't_countries.name as country_name',
                't_countries.code as country_code',
                'count(DISTINCT(t_reviews.id)) as reviews',
                'coalesce(round(avg(t_reviews.rate), 1), 0) as rate',
                'count (DISTINCT(t_activities.id)) as excursions'
            )
            ->from('countries', 't_countries')
            ->innerJoin('t_countries','activities','t_activities', 't_countries.id = t_activities.country AND t_activities.is_draft = false AND t_activities.is_deleted = false')
            ->innerJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->leftJoin('t_activities','reviews','t_reviews', 't_activities.id = t_reviews.activity AND t_reviews.is_approved = true')
            ->groupBy(
                't_cities.id',
                't_cities.country',
                't_cities.name',
                't_cities.avatar',
                't_cities.avatar_small',
                't_cities.locale_name',
                't_cities.offset',
                't_cities.latitude',
                't_cities.longitude',
                't_cities.route_name',
                't_countries.name',
                't_countries.code',
                't_countries.route_name'
            );

        if ($countryCode) {
            $queryBuilder
                ->andWhere('t_countries.code = ?')
                ->setParameter(0, $countryCode);
        }
        if ($sort) {
            $queryBuilder
                ->orderBy($sort, 'DESC');
        }
        $result = $queryBuilder->execute()->fetchAll();
        foreach ($result as &$item) {
            if (empty($item['travelers'])) {
                $item['travelers'] = $this->getTravellersForCity($item['id']);
            }
        }

        return array_map([$this, 'extractLocaleName'], array_map([$this, 'populateAvatar'], $result));
    }

    /**
     * Retrieve destination info
     *
     * @param $countryRoute
     * @param $cityRoute
     * @return mixed
     */
    public function getDestination($countryRoute, $cityRoute) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'c.id',
                'c.country',
                'c.name',
                'c.avatar',
                'c.avatar_small',
                'c.locale_name',
                'c.offset',
                'c.latitude',
                'c.longitude',
                'c.route_name',
                'c.travellers AS travelers',
                'co.route_name AS country_route',
                'co.name as country_name',
                'count(DISTINCT(r.id)) as reviews',
                'coalesce(round(avg(r.rate), 1), 0) as rate',
                'count (DISTINCT(a.id)) as excursions'
            )
            ->from('cities', 'c')
            ->leftJoin('c','activities','a', 'c.id = a.city AND a.is_draft = false AND a.is_deleted = false')
            ->innerJoin('c','countries','co', 'co.id = c.country AND co.route_name = :country_route')
            ->leftJoin('a','reviews','r', 'a.id = r.activity AND r.is_approved = true')
            ->where('c.route_name = :city_route')
            ->setParameters([
                'city_route'    => $cityRoute,
                'country_route' => $countryRoute
            ])
            ->setMaxResults(1)
            ->groupBy(
                'c.id',
                'c.country',
                'c.name',
                'c.avatar',
                'c.avatar_small',
                'c.locale_name',
                'c.offset',
                'c.latitude',
                'c.longitude',
                'c.route_name',
                'c.travellers',
                'co.route_name',
                'co.name'
            );

        $stmt = $queryBuilder->execute();
        if (!$stmt->rowCount()) {
            throw new NotFoundHttpException('Wrong city code is provided');
        }
        $result = $stmt->fetch();
        if (empty($result['travelers'])) {
            $result['travelers'] = $this->getTravellersForCity($result['id']);
        }
        return $this->extractLocaleName($this->populateAvatar($result));
    }

    /**
     * Get base query builder for activities
     *
     * @param bool $excludeDraft
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function getPreparedActivityQueryBuilder($excludeDraft = true) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_activities.id',
                't_activities.name',
                't_activities.avatar',
                't_activities.avatar_small',
                't_activities.short_description',
                't_activities.language',
                't_activities.duration',
                't_activities.category',
                't_activities.is_draft',
                't_activities.city',
                't_cities.name AS city_name',
                't_countries.name AS country_name',
                't_activities.route_name',
                't_cities.route_name AS city_route',
                't_countries.route_name AS country_route',
                'coalesce(min(CASE WHEN t_person_types.id is null THEN null ELSE t_activity_prices.price END), 0) AS min_price',
                'count(DISTINCT(t_reviews.id)) AS reviews',
                'coalesce(avg(t_reviews.rate), 0) AS rate'
            )
            ->from('activities', 't_activities')
            ->leftJoin('t_activities','activity_prices','t_activity_prices', 't_activity_prices.activity = t_activities.id')
            ->leftJoin('t_activities','reviews','t_reviews', 't_activities.id = t_reviews.activity AND t_reviews.is_approved = true')
            ->leftJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->leftJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->leftJoin('t_activity_prices', 'person_types', 't_person_types', 't_person_types.id = t_activity_prices.person_type AND t_person_types.is_adult IS TRUE')
            ->where('t_activities.is_deleted = false')
            ->groupBy(
                't_activities.id',
                't_activities.name',
                't_activities.avatar',
                't_activities.avatar_small',
                't_activities.short_description',
                't_activities.language',
                't_activities.duration',
                't_activities.category',
                't_activities.is_draft',
                't_activities.city',
                't_cities.name',
                't_countries.name',
                't_activities.route_name',
                't_cities.route_name',
                't_countries.route_name'
            );

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return $queryBuilder;
    }

    /**
     * Get full activity list
     *
     * @param bool $excludeDraft
     * @return array
     */
    public function getList($excludeDraft = true) {
        $queryBuilder = $this->getPreparedActivityQueryBuilder($excludeDraft);
        $queryBuilder->orderBy('t_activities.id', 'asc');
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get featured activities list
     *
     * @param int $offset
     * @param int $limit
     * @param null $country
     * @return array
     */
    public function getFeaturedActivities($offset = 0, $limit = 6, $country = null) {
        $queryBuilder = $this->getPreparedActivityQueryBuilder()
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        if($country) {
            $queryBuilder
                ->andWhere('t_countries.code = ?')
                ->setParameter(0, $country);
        }
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get activity short list for city
     *
     * @param $cityId
     * @return array
     */
    public function getActivitiesForCity($cityId) {
        $activities = $this->getFilteredByCity($cityId);
        return $this->getFilteredActivities($activities);
    }

    /**
     * Get activity ids filtered by query string
     *
     * @param $query
     * @return array
     */
    public function getFilteredByQuery($query = '') {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_activities.id')
            ->from('activities', 't_activities')
            ->where('t_activities.is_draft = false')
            ->andWhere('t_activities.is_deleted = false');
        if(!empty($query)) {
            $queryBuilder
                ->leftJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
                ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    't_activities.name ILIKE :query',
                    't_cities.name ILIKE :query']))
                ->setParameter('query', "%{$query}%");
        }
        $stmt = $queryBuilder->execute();
        $activities = [];
        while ($idActivity = $stmt->fetchColumn()) {
            array_push($activities, $idActivity);
        };
        return $activities;
    }

    /**
     * Retrieve list of activities
     *
     * @param $countryRoute
     * @param $cityRoute
     * @param $route
     * @return bool|string
     */
    public function getFilteredByRoute($countryRoute, $cityRoute, $route) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_activities.id')
            ->from('activities', 't_activities')
            ->leftJoin('t_activities','cities','t_cities', 't_cities.id = t_activities.city')
            ->leftJoin('t_activities','countries','t_countries', 't_countries.id = t_activities.country')
            ->where('t_activities.route_name = :route')
            ->andWhere('t_activities.is_deleted = false')
            ->andWhere('t_countries.route_name = :country_route')
            ->andWhere('t_cities.route_name = :city_route')
            ->setParameters([
                'route'         => $route,
                'country_route' => $countryRoute,
                'city_route'    => $cityRoute
            ])
            ->setMaxResults(1);
        return $queryBuilder->execute()->fetchColumn();
    }

    /**
     * Retrieve activities list that filtered before
     *
     * @param array $activities
     * @return array
     */
    public function getFilteredActivities($activities = []) {
        if(empty($activities)) {
            return []; //No need to search
        }
        $queryBuilder = $this->getPreparedActivityQueryBuilder();
        $queryBuilder
            ->andWhere($queryBuilder->expr()->in('t_activities.id', $activities));
        return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get activities filtered by city
     *
     * @param $idCity
     * @return array
     */
    private function getFilteredByCity($idCity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('activities')
            ->where('city = :city')
            ->andWhere('is_deleted = false')
            ->setParameter('city', $idCity);
        $stmt = $queryBuilder->execute();
        $activities = [];
        while ($idActivity = $stmt->fetchColumn()) {
            array_push($activities, $idActivity);
        };

        return $activities;
    }

    /**
     * Get activities filtered by country
     *
     * @param $idCountry
     * @return array
     */
    private function getFilteredByCountry($idCountry) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id')
            ->from('activities')
            ->where('country = :country')
            ->andWhere('is_deleted = false')
            ->setParameter('country', $idCountry);
        $stmt = $queryBuilder->execute();
        $activities = [];
        while ($idActivity = $stmt->fetchColumn()) {
            array_push($activities, $idActivity);
        };

        return $activities;
    }

    /**
     * Get names for activity gallery
     *
     * @param $cityId
     * @return mixed
     *
     */
    private function getCountryCityName($cityId) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_cities.name AS city_name',
                't_countries.name AS country_name',
            ])
            ->from('cities', 't_cities')
            ->innerJoin('t_cities', 'countries', 't_countries', 't_cities.country = t_countries.id')
            ->where('t_cities.id = :id')
            ->setParameter('id', $cityId)
            ->setMaxResults(1);
        return $queryBuilder->execute()->fetch();
    }

    /**
     * Retrieve activity by id
     *
     * @param $id
     * @param bool $excludeDraft
     * @return array|false
     */
    public function getActivity($id, $excludeDraft = true) {

        $queryBuilder = $this->getPreparedActivityQueryBuilder($excludeDraft)
            ->addSelect(
                't_activities.country',
                't_activities.description',
                't_activities.included',
                't_activities.travellers',
                't_activities.not_included',
                't_activities.when_to_book',
                't_activities.acessibility',
                't_activities.ticket',
                't_activities.how_to_book',
                't_activities.meeting_point_latitude',
                't_activities.meeting_point_longitude',
                't_activities.meeting_point_text',
                't_activities.cancelation_hours_before',
                't_activities.cancelation_descriprion',
                't_activities.book_limit',
                't_activities.blocks'
            )
            ->andWhere('t_activities.id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id)
            ->addGroupBy(
                't_activities.country',
                't_activities.city',
                't_cities.name',
                't_activities.description',
                't_activities.included',
                't_activities.travellers',
                't_activities.not_included',
                't_activities.when_to_book',
                't_activities.acessibility',
                't_activities.ticket',
                't_activities.how_to_book',
                't_activities.meeting_point_latitude',
                't_activities.meeting_point_longitude',
                't_activities.meeting_point_text',
                't_activities.cancelation_hours_before',
                't_activities.cancelation_descriprion'
            );

        if ($excludeDraft) {
            $queryBuilder->andWhere('t_activities.is_draft = false');
        }

        return $this->decodeBlocks($this->populateAvatar($queryBuilder->execute()->fetch()));
    }

    /**
     * Retrieve related activities
     *
     * @param $idActivity
     * @param $limit
     * @return array
     */
    public function getRelatedActivities($idActivity, $limit = 3) {
        $activityInfo = $this->getActivity($idActivity);
        if ($activityInfo) {
            $city = $activityInfo['city'];
            $queryBuilder = $this->getPreparedActivityQueryBuilder();
            $queryBuilder
                ->leftJoin('t_activities', 'order_activities', 't_order_activities', 't_activities.id = t_order_activities.activity')
                ->setMaxResults($limit)
                ->andWhere($queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('t_activities.city', '?'),
                    $queryBuilder->expr()->neq('t_activities.id', '?')
                ))
                ->setParameter(0, $city)
                ->setParameter(1, $idActivity)
                ->orderBy('count(t_order_activities.id)', 'DESC');
            return array_map([$this, 'populateAvatar'], $queryBuilder->execute()->fetchAll());
        } else {
            return [];
        }
    }

    /**
     * Get activity schedule
     *
     * @param $idActivity
     * @return mixed|null
     */
    public function getActivitySchedule($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('t_activity_schedule.time', 't_cities.offset')
            ->from('activity_schedule', 't_activity_schedule')
            ->innerJoin('t_activity_schedule', 'activities', 't_activities', 't_activities.id=t_activity_schedule.activity')
            ->innerJoin('t_activities', 'cities', 't_cities', 't_cities.id=t_activities.city')
            ->where('activity = ?')
            ->setParameter(0, $idActivity)
            ->setMaxResults(1);

        $result = $queryBuilder->execute()->fetch();
        if (!$result) {
            return null;
        }
        return $this->extractSchedule($result);
    }

    /**
     * Retrieve all available prices and types for activity
     *
     * @param $idActivity
     * @return array
     */
    public function getTypes($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                't_activity_prices.price',
                't_activity_types.id as activity_type_id',
                't_activity_types.name as activity_type',
                't_person_types.id as person_type_id',
                't_person_types.name as person_type_name',
                't_person_types.is_adult as is_adult'
            )
            ->from('activity_prices', 't_activity_prices')
            ->innerJoin('t_activity_prices','activity_types','t_activity_types', 't_activity_types.id = t_activity_prices.activity_type')
            ->innerJoin('t_activity_prices','person_types','t_person_types', 't_person_types.id = t_activity_prices.person_type')
            ->where('t_activity_prices.activity = ?')
            ->orderBy('t_person_types.id', 'ASC')
            ->setParameter(0, $idActivity);

        $dbResult = $queryBuilder->execute()->fetchAll();
        $activityTypes = [];

        //Build tree from prices
        // activityType -> prices
        foreach ($dbResult as $row) {
            $activityTypeId = $row['activity_type_id'];
            $personTypeId = $row['person_type_id'];

            if (empty($activityTypes[$activityTypeId])) {
                $activityTypes[$activityTypeId] = [
                    'id'        => $activityTypeId,
                    'name'      => $row['activity_type'],
                    'tickets'   => []
                ];
            }
            $activityTypes[$row['activity_type_id']]['tickets'][$personTypeId] = [
                'id'        => $personTypeId,
                'name'      => $row['person_type_name'],
                'price'     => $row['price'],
                'is_adult'  => $row['is_adult']
            ];
        }

        // Convert to plain array
        $activityTypes = array_values($activityTypes);
        foreach ($activityTypes as &$activityType) {
            $activityType['tickets'] = array_values($activityType['tickets']);
        }
        return $activityTypes;
    }

    /**
     * Get list of activity categories
     *
     * @return array
     */
    public function getCategoriesList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from('categories');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Activity search function
     *
     * @param $dates
     * @param $times
     * @param $idCity
     * @param array $filtered
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function searchByDatesAndTime($dates, $times, $idCity = null, $filtered = []) {

        $this->db->query("SET TIME ZONE 'UTC'");

        $explodeItem = function ($item) {
            return array_map('intval', explode(':', $item));
        };

        $dates = array_map($explodeItem, $dates);
        $times = array_map($explodeItem, $times);

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('activity')
            ->from('activity_schedule');

        $dateTimesExpStr = [];
        if (!empty($dates) && !empty($times)) {
            // In this case we should filter simultaneously
            foreach ($dates as $date) {
                list ($fromDate, $toDate) = $date;
                foreach ($times as $time) {
                    list ($fromTime, $toTime) = $time;
                    if ($toTime == 0) {
                        $toTime = 24;
                    }
                    $dateTimesExpStr[] = "'[${fromTime},${toTime}]'::int4range @> ANY(extract_hour_for_day(time, '[${fromDate},${toDate}]'::int8range))";
                }
            }
            $queryBuilder->where($queryBuilder->expr()->orX()->addMultiple($dateTimesExpStr));
        } else {
            // Otherwise we filter on date or on time
            $datesExpStr = [];
            foreach ($dates as $date) {
                list ($from, $to) = $date;
                $from = intval($from);
                $to = intval($to);
                $datesExpStr[] = "int8range(${from}, ${to}) @> ANY(time)";
            }

            $timesExpStr = [];
            foreach ($times as $time) {
                list ($from, $to) = $time;
                $from = intval($from);
                $to = intval($to);
                if ($to == 0) {
                    $to = 24;
                }
                $timesExpStr[] = "int4range(${from}, ${to}) @> ANY(extract_hour(time))";
            }
            $dateExpr = $queryBuilder->expr()->orX()->addMultiple($datesExpStr);
            $timeExpr = $queryBuilder->expr()->orX()->addMultiple($timesExpStr);
            $queryBuilder->where($queryBuilder->expr()->andX($dateExpr, $timeExpr));
        }
        $stmt = $queryBuilder->execute();
        $activities = [];
        while ($idActivity = $stmt->fetchColumn()) {
            array_push($activities, $idActivity);
        };
        if(!empty($idCity)) {
            $filteredByCity = $this->getFilteredByCity($idCity);
            $activities = array_intersect($activities, $filteredByCity);
        }
        if(!empty($filtered)) {
            $activities = array_intersect($activities, $filtered);
        }
        return $this->getFilteredActivities($activities);
    }

    /**
     * Get activity gallery items
     *
     * @param $idActivity
     * @return array
     */
    public function getGallery($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'path',
                'description',
                'is_main'
            )
            ->from('gallery', 'a')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);

        return array_map([$this, 'populatePath'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Get activity voucher
     *
     * @param $idActivity
     * @return mixed
     */
    public function getVoucher($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'image',
                'text'
            )
            ->from('activity_voucher')
            ->where('activity = :activity')
            ->setParameter('activity', $idActivity)
            ->setMaxResults(1);

        return $this->populatePath($queryBuilder->execute()->fetch());
    }

    /**
     * Set draft status for activity
     *
     * @param $idActivity
     * @param $draftStatus
     * @return int
     */
    public function setDraftStatus($idActivity, $draftStatus) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('activities')
            ->set('is_draft', ':is_draft')
            ->where('id = :id')
            ->setParameter('is_draft', $draftStatus, ParameterType::BOOLEAN)
            ->setParameter('id', $idActivity);

        return $queryBuilder->execute();
    }

    /**
     * Set delete status
     *
     * @param $idActivity
     * @return int
     */
    public function markAsDeleted($idActivity) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('activities')
            ->set('is_deleted', 'true')
            ->where('id = :id')
            ->setParameter('id', $idActivity);

        return $queryBuilder->execute();
    }

    /**
     * Create new activity
     *
     * @param ActivityData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createActivity(ActivityData $data) {

        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->insert('activities')
                ->setValue('country'                   , ':country')
                ->setValue('city'                      , ':city')
                ->setValue('category'                  , ':category')
                ->setValue('name'                      , ':name')
                ->setValue('route_name'                , ':route_name')
                ->setValue('short_description'         , ':short')
                ->setValue('description'               , ':description')
                ->setValue('duration'                  , ':duration')
                ->setValue('language'                  , ':language')
                ->setValue('included'                  , ':included')
                ->setValue('not_included'              , ':not_included')
                ->setValue('when_to_book'              , ':when_to_book')
                ->setValue('acessibility'              , ':access')
                ->setValue('ticket'                    , ':ticket')
                ->setValue('how_to_book'               , ':how')
                ->setValue('meeting_point_latitude'    , ':lat')
                ->setValue('meeting_point_longitude'   , ':lon')
                ->setValue('meeting_point_text'        , ':text')
                ->setValue('cancelation_hours_before'  , ':hours')
                ->setValue('cancelation_descriprion'   , ':cancel')
                ->setValue('book_limit'                , ':limit')
                ->setValue('blocks'                    , ':blocks')
                ->setValue('travellers'                , ':travellers')
                ->setParameters([
                    'country'      => $data->country,
                    'city'         => $data->city,
                    'category'     => $data->category,
                    'name'         => $data->name,
                    'route_name'   => $data->route_name,
                    'short'        => $data->shortDescription,
                    'description'  => $data->description,
                    'duration'     => $data->duration,
                    'language'     => $data->language,
                    'included'     => $data->included,
                    'not_included' => $data->notIncluded,
                    'when_to_book' => $data->whenToBook,
                    'access'       => $data->accessibility,
                    'ticket'       => $data->ticket,
                    'how'          => $data->howToBook,
                    'lat'          => $data->meetingPointLatitude,
                    'lon'          => $data->meetingPointLongitude,
                    'text'         => $data->meetingPointText,
                    'hours'        => $data->cancelationHoursBefore,
                    'cancel'       => $data->cancelationDescription,
                    'limit'        => $data->book_limit,
                    'blocks'       => json_encode($data->blocks),
                    'travellers'   => $data->travellers
                ]);
            $queryBuilder->execute();

            $idActivity = $this->db->lastInsertId();
            $this->saveActivityAvatars($idActivity, $data);
            $this->saveActivityTypes($idActivity, $data->types);
            $this->saveActivityGallery($idActivity, $data->gallery, $data);
            $this->saveActivitySchedule($idActivity, $data->schedule);
            $this->saveActivitySuppliers($idActivity, $data->suppliers);
            $this->saveActivityVoucher($idActivity, $data->voucher, $data);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Create voucher for activity
     *
     * @param $idActivity
     * @param $voucherData
     * @param $data
     * @throws \ImagickException
     */
    private function saveActivityVoucher($idActivity, $voucherData, $data) {
        // Clear shcedule for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('activity_voucher')
            ->where('activity = :activity')
            ->setParameter('activity', $idActivity);
        $qb->execute();


        $activityInfo = $this->getCountryCityName($data->city);
        if (!empty($voucherData['image'])) {
            $voucherData['image'] = $this->uploadService->copyActivityImage(
                $voucherData['image'],
                $activityInfo['country_name'],
                $activityInfo['city_name'],
                $idActivity
            );
        } else {
            $voucherData['image'] = str_replace($this->urlToImages, '', $voucherData['image']);
        }

        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->insert('activity_voucher')
            ->setValue('image',     ':image')
            ->setValue('text',      ':text')
            ->setValue('activity',  ':activity')
            ->setParameters([
                'activity'  => $idActivity,
                'image'     => $voucherData['image'],
                'text'      => $voucherData['text']
            ]);
        $queryBuilder->execute();
    }

    /**
     * Retrieve activity list with names and routes
     *
     * @return array
     */
    public function getActivityRoutesList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('id', 'name', 'route_name')
            ->from('activities');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Update activity routes
     *
     * @param $routes
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateActivityRoutes($routes) {
        if (empty($routes)) {
            return;
        }
        $this->db->beginTransaction();
        try {
            foreach ($routes as $id => $name) {
                $queryBuilder = $this->db->createQueryBuilder();
                $queryBuilder
                    ->update('activities')
                    ->set('route_name', ':route')
                    ->where('id = :id')
                    ->setParameters([
                        'id'    => $id,
                        'route' => $name
                    ]);
                $queryBuilder->execute();
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Update activity with provided data
     *
     * @param $idActivity
     * @param ActivityData $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateActivity($idActivity, ActivityData $data) {

        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->update('activities')
                ->where('id = :id')
                ->set('country'                   , ':country')
                ->set('city'                      , ':city')
                ->set('category'                  , ':category')
                ->set('name'                      , ':name')
                ->set('route_name'                , ':route_name')
                ->set('short_description'         , ':short')
                ->set('description'               , ':description')
                ->set('duration'                  , ':duration')
                ->set('language'                  , ':language')
                ->set('included'                  , ':included')
                ->set('not_included'              , ':not_included')
                ->set('when_to_book'              , ':when_to_book')
                ->set('acessibility'              , ':access')
                ->set('ticket'                    , ':ticket')
                ->set('how_to_book'               , ':how')
                ->set('meeting_point_latitude'    , ':lat')
                ->set('meeting_point_longitude'   , ':lon')
                ->set('meeting_point_text'        , ':text')
                ->set('cancelation_hours_before'  , ':hours')
                ->set('cancelation_descriprion'   , ':cancel')
                ->set('book_limit'                , ':limit')
                ->set('blocks'                    , ':blocks')
                ->set('travellers'                , ':travellers')
                ->setParameters([
                    'country'      => $data->country,
                    'city'         => $data->city,
                    'category'     => $data->category,
                    'name'         => $data->name,
                    'route_name'   => $data->route_name,
                    'short'        => $data->shortDescription,
                    'description'  => $data->description,
                    'duration'     => $data->duration,
                    'language'     => $data->language,
                    'included'     => $data->included,
                    'not_included' => $data->notIncluded,
                    'when_to_book' => $data->whenToBook,
                    'access'       => $data->accessibility,
                    'ticket'       => $data->ticket,
                    'how'          => $data->howToBook,
                    'lat'          => $data->meetingPointLatitude,
                    'lon'          => $data->meetingPointLongitude,
                    'text'         => $data->meetingPointText,
                    'hours'        => $data->cancelationHoursBefore,
                    'cancel'       => $data->cancelationDescription,
                    'limit'        => $data->book_limit,
                    'blocks'       => json_encode($data->blocks),
                    'travellers'   => $data->travellers,
                    'id'           => $idActivity
                ]);

            if(!empty($data->avatar)) {
                $queryBuilder->set('avatar', ':avatar')->setParameter(':avatar', $data->avatar);
            }
            $queryBuilder->execute();
            $this->saveActivityAvatars($idActivity, $data);
            $this->saveActivityTypes($idActivity, $data->types);
            $this->saveActivityGallery($idActivity, $data->gallery, $data);
            $this->saveActivitySchedule($idActivity, $data->schedule);
            $this->saveActivitySuppliers($idActivity, $data->suppliers);
            $this->saveActivityVoucher($idActivity, $data->voucher, $data);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }

    }

    /**
     * Save avatars for activity
     *
     * @param $idActivity
     * @param ActivityData $data
     * @throws \ImagickException
     */
    private function saveActivityAvatars($idActivity, ActivityData $data) {
        if (!empty($data->avatar) || !empty($data->avatar_small)) {
            $activityInfo = $this->getCountryCityName($data->city);

            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->update('activities')
                ->where('id = :id')
                ->setParameter('id', $idActivity);

            if(!empty($data->avatar)) {
                $data->avatar = $this->uploadService->copyActivityImage(
                    $data->avatar,
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
                $queryBuilder
                    ->set('avatar', ':avatar')
                    ->setParameter('avatar', $data->avatar);
            }
            if (!empty($data->avatar_small)) {
                $data->avatar_small = $this->uploadService->copyActivityImage(
                    $data->avatar_small,
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
                $queryBuilder
                    ->set('avatar_small', ':avatar_small')
                    ->setParameter('avatar_small', $data->avatar_small);
            }
            $queryBuilder->execute();
        }
    }

    /**
     * Update activity suppliers
     *
     * @param $idActivity
     * @param $suppliers
     */
    private function saveActivitySuppliers($idActivity, $suppliers) {
        //Clear database
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->delete('activity_suppliers')
            ->where('activity = :activity')
            ->setParameter('activity', $idActivity);
        $queryBuilder->execute();

        //Update with new values
        foreach ($suppliers as $supplier) {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->insert('activity_suppliers')
                ->setValue('activity', ':activity')
                ->setValue('supplier', ':supplier')
                ->setParameters([
                    'activity'  => $idActivity,
                    'supplier'  => $supplier
                ]);
            $queryBuilder->execute();
        }
    }

    /**
     * Save types for activity
     *
     * @param $idActivity
     * @param $types
     */
    private function saveActivityTypes($idActivity, $types) {
        // Clear prices for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('activity_prices')
           ->where('activity = ?')
           ->setParameter(0, $idActivity);
        $qb->execute();

        $ticketRef = [];
        foreach ($types as $type) {
            if (empty($type['id'])) {
                $type['id'] = $this->createNewActivityType($idActivity, $type['name']);
            } else {
                $this->updateActivityType($type['id'], $type['name']);
            }
            foreach ($type['tickets'] as $index => $ticket) {
                if (empty($ticketRef[$index])) {
                    if (empty($ticket['id'])) {
                        $ticket['id'] = $this->createNewPersonType($idActivity, $ticket['name'], $ticket['is_adult']);
                    } else {
                        $this->updatePersonType($ticket['id'], $ticket['name'], $ticket['is_adult']);
                    }
                    $ticketRef[$index] = $ticket;
                }
                $qb = $this->db->createQueryBuilder();
                $qb->insert('activity_prices')
                    ->setValue('activity', ':activity')
                    ->setValue('person_type', ':person')
                    ->setValue('activity_type', ':type')
                    ->setValue('price', ':price')
                    ->setParameters([
                        'activity'  => $idActivity,
                        'person'    => $ticketRef[$index]['id'],
                        'type'      => $type['id'],
                        'price'     => $ticket['price']
                    ]);
                $qb->execute();
            }
        }
    }

    /**
     * Create new activity type and return it`s id
     *
     * @param $idActivity
     * @param $name
     * @return string
     */
    private function createNewActivityType($idActivity, $name) {
        $qb = $this->db->createQueryBuilder();
        $qb->insert('activity_types')
            ->setValue('activity', ':activity')
            ->setValue('name', ':name')
            ->setParameters([
                'activity' => $idActivity,
                'name'     => $name
            ]);
        $qb->execute();
        return $this->db->lastInsertId();
    }

    /**
     * Create new activity type and return it`s id
     *
     * @param $id
     * @param $name
     * @return string
     */
    private function updateActivityType($id, $name) {
        $qb = $this->db->createQueryBuilder();
        $qb->update('activity_types')
            ->set('name', ':name')
            ->where('id = :id')
            ->setParameters([
                'id'    => $id,
                'name'  => $name
            ]);
        return $qb->execute();
    }

    /**
     * Create new person type and return it`s id
     *
     * @param $idActivity
     * @param $name
     * @param $isAdult
     * @return string
     */
    private function createNewPersonType($idActivity, $name, $isAdult) {
        $qb = $this->db->createQueryBuilder();
        $qb->insert('person_types')
            ->setValue('activity', ':activity')
            ->setValue('name', ':name')
            ->setValue('is_adult', ':adult')
            ->setParameters([
                'activity' => $idActivity,
                'name'     => $name,
            ])
            ->setParameter('adult', $isAdult, ParameterType::BOOLEAN);
        $qb->execute();
        return $this->db->lastInsertId();
    }

    /**
     * Update person type name
     *
     * @param $id
     * @param $name
     * @param $isAdult
     * @return string
     */
    private function updatePersonType($id, $name, $isAdult) {
        $qb = $this->db->createQueryBuilder();
        $qb->update('person_types')
            ->set('name', ':name')
            ->set('is_adult', ':adult')
            ->where('id = :id')
            ->setParameters([
                'id'    => $id,
                'name'  => $name
            ])
            ->setParameter('adult', $isAdult, ParameterType::BOOLEAN);
        return $qb->execute();
    }

    /**
     * Save item gallery
     *
     * @param $idActivity
     * @param $gallery
     * @param $data
     * @throws \ImagickException
     */
    private function saveActivityGallery($idActivity, $gallery, $data) {
        // Clear gallery for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('gallery')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);
        $qb->execute();

        foreach ($gallery as $item) {
            $activityInfo = $this->getCountryCityName($data->city);
            if (!empty($item['upload'])) {
                $item['path'] = $this->uploadService->copyActivityImage(
                    $item['upload'],
                    $activityInfo['country_name'],
                    $activityInfo['city_name'],
                    $idActivity
                );
            } else {
                $item['path'] = str_replace($this->urlToImages, '', $item['path']);
            }
            $qb = $this->db->createQueryBuilder();
            $qb->insert('gallery')
                ->setValue('activity', ':activity')
                ->setValue('path', ':path')
                ->setValue('description', ':description')
                ->setValue('is_main', ':is_main')
                ->setParameter('activity', $idActivity)
                ->setParameter('path', $item['path'])
                ->setParameter('description', $item['description'])
                ->setParameter('is_main', $item['is_main'], ParameterType::BOOLEAN);
            $qb->execute();
        }
    }

    /**
     * Save activity schedule
     *
     * @param $idActivity
     * @param $schedule
     */
    private function saveActivitySchedule($idActivity, $schedule) {
        // Clear shcedule for activity
        $qb = $this->db->createQueryBuilder();
        $qb->delete('activity_schedule')
            ->where('activity = ?')
            ->setParameter(0, $idActivity);
        $qb->execute();

        $qb = $this->db->createQueryBuilder();
        $times = $schedule['time'];
        $qb->insert('activity_schedule')
           ->setValue('activity', ':activity')
           ->setValue('time', ':time')
            ->setParameter('activity', $idActivity)
            ->setParameter('time', '{' . implode(',', $times) . '}');
        $qb->execute();
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populateAvatar($itemData) {
        if(!empty($itemData['avatar'])) {
            $itemData['avatar'] = $this->urlToImages . $itemData['avatar'];
        }
        if(!empty($itemData['avatar_small'])) {
            $itemData['avatar_small'] = $this->urlToImages . $itemData['avatar_small'];
        }
        return $itemData;
    }

    /**
     * Callback to decode block
     *
     * @param $itemData
     * @return mixed
     */
    private function decodeBlocks($itemData) {
        if(!empty($itemData['blocks'])) {
            $itemData['blocks'] = json_decode($itemData['blocks']);
        }
        return $itemData;
    }

    /**
     * Callback to fulfill image url
     *
     * @param $itemData
     * @return mixed
     */
    private function populatePath($itemData) {
        if (!empty($itemData['path'])) {
            $itemData['path'] = $this->urlToImages . $itemData['path'];
        }
        if (!empty($itemData['image'])) {
            $itemData['image'] = $this->urlToImages . $itemData['image'];
        }
        return $itemData;
    }

    /**
     * Convert stored schedule to array
     *
     * @param $itemData
     * @return mixed
     */
    private function extractSchedule($itemData) {
        if(!empty($itemData['time'])) {
            $itemData['time'] = array_filter(explode(',', substr($itemData['time'], 1, -1)));
        }
        return $itemData;
    }

    /**
     * Convert locale name to db format
     *
     * @param $itemData
     * @return mixed
     */
    private function extractLocaleName($itemData) {
        if(!empty($itemData['locale_name'])) {
            $locales = explode(',', substr($itemData['locale_name'], 1, -1));
            $itemData['locale_name'] = []; // Convert from string to array
            foreach ($locales as $locale) {
                $locale = str_replace('"', '', $locale);
                list ($id, $name) = explode(':', $locale);
                $itemData['locale_name'][$id] = $name;
            }
        }
        return $itemData;
    }

    /**
     * Calculate travellers for country
     *
     * @param $idCountry
     * @return bool|string
     */
    private function getTravellersForCountry($idCountry) {
        $cities = $this->getCityList($idCountry);
        $result = 0;
        foreach ($cities as $city) {
            if(empty($city['travellers'])) {
                $result += $this->getTravellersForCity($city['id']);
            } else {
                $result += $city['travellers'];
            }
        }
        return $result;
    }

    /**
     * Calculate travellers for city
     *
     * @param $idCity
     * @return bool|string
     */
    private function getTravellersForCity($idCity) {
        $activities = $this->getFilteredByCity($idCity);
        return $this->getTravellersForActivities($activities);
    }

    /**
     * Calculate travellers for activities
     *
     * @param $activities
     * @return bool|string
     */
    private function getTravellersForActivities($activities) {
        if (!is_array($activities)) {
            $activities = [$activities];
        }
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('CASE WHEN t_activities.travellers > 0 THEN t_activities.travellers ELSE SUM(t_order_activities_prices.amount) END')
            ->from('activities', 't_activities')
            ->leftJoin('t_activities', 'order_activities', 't_order_activities', 't_activities.id = t_order_activities.activity')
            ->leftJoin('t_order_activities', 'order_activities_prices', 't_order_activities_prices', 't_order_activities.id = t_order_activities_prices.order_activity')
            ->where($queryBuilder->expr()->in('t_activities.id', $activities))
            ->groupBy('t_activities.id')
            ->setParameter('activities', $activities);
        $result = 0;
        $stmt = $queryBuilder->execute();
        while(false !== $sum = $stmt->fetchColumn()) {
            $result += intval($sum);
        }
        return $result;
    }
}
