<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.04.18
 * Time: 13:06
 */

namespace Civitours\Service;

use Doctrine\DBAL\Connection;
use function GuzzleHttp\Psr7\str;

/**
 * Service provides access to geo database
 *
 * Class GeoDataService
 * @package Civitours\Service
 */
class GeoDataService
{
    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     * @param UploadService $uploadService
     */
    public function __construct(Connection $db, UploadService $uploadService)
    {
        $this->db = $db;
        $this->uploadService = $uploadService;
    }

    /**
     * Retrieve city by id
     *
     * @param $id
     * @return array|false
     */
    public function getCity($id) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('cities')
            ->where('id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Search cities by country and search string
     *
     * @param $country
     * @param $name
     * @return array
     */
    public function searchCity($country, $name) {
        $queryBuilder = $this->db->createQueryBuilder();
        $name = strtolower($name);
        $queryBuilder
            ->select('*')
            ->from('cities')
            ->where($queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('country', '?'),
                $queryBuilder->expr()->like('LOWER(name)', $queryBuilder->expr()->literal("%{$name}%"))
            ))
            ->setParameter(0, $country);

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Retrieve country by id
     *
     * @param $id
     * @return array|false
     */
    public function getCountry($id) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('countries')
            ->where('id = ?')
            ->setMaxResults(1)
            ->setParameter(0, $id);

        return $queryBuilder->execute()->fetch();
    }

    /**
     * Retrieve all countries
     *
     * @return array|false
     */
    public function getCountries() {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->select('*')
            ->from('countries');

        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Update country and load image
     *
     * @param $id
     * @param $data
     * @param null $countryData
     * @return string
     */
    public function updateCountry($id, $data, $countryData = null) {

        // Copy image if uploaded
        $imagePath = '';
        if (!empty($data['imageFile'])) {
            if (null === $countryData) {
                $countryData = $this->getCountry($id);
            }
            $imagePath = $this->uploadService->copyCountryImage($data['imageFile'], $countryData['name']);
        }

        // Copy image if uploaded
        $imagePathSmall = '';
        if (!empty($data['imageFileSmall'])) {
            if (null === $countryData) {
                $countryData = $this->getCountry($id);
            }
            $imagePathSmall = $this->uploadService->copyCountryImage($data['imageFileSmall'], $countryData['name']);
        }

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('countries')
            ->set('name', ':name')
            ->set('locale_name', ':locale_name')
            ->set('route_name', ':route_name')
            ->where('id = :id')
            ->setParameter('name', $data['name'])
            ->setParameter('locale_name', $this->implodeLocaleNames($data['localeNames']))
            ->setParameter('route_name', $data['route_name'])
            ->setParameter('id', $id);

        if ($imagePath) {
            $queryBuilder
                ->set('avatar', ':avatar')
                ->setParameter('avatar', $imagePath);
        }

        if ($imagePathSmall) {
            $queryBuilder
                ->set('avatar_small', ':avatar_small')
                ->setParameter('avatar_small', $imagePathSmall);
        }

        $queryBuilder->execute();
        return '';
    }

    /**
     * Update country and load image
     *
     * @param $id
     * @param $data
     * @param null $cityData
     * @return string
     */
    public function updateCity($id, $data, $cityData = null) {

        // Copy image if uploaded
        $imagePath = '';
        if (!empty($data['imageFile'])) {
            if (null === $cityData) {
                $cityData = $this->getCity($id);
            }
            $countryData = $this->getCountry($cityData['country']);
            $imagePath = $this->uploadService->copyCityImage($data['imageFile'], $countryData['name'], $cityData['name']);
        }

        $imagePathSmall = '';
        if (!empty($data['imageFileSmall'])) {
            if (null === $cityData) {
                $cityData = $this->getCity($id);
            }
            $countryData = $this->getCountry($cityData['country']);
            $imagePathSmall = $this->uploadService->copyCityImage($data['imageFileSmall'], $countryData['name'], $cityData['name']);
        }

        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('cities')
            ->set('name', ':name')
            ->set('locale_name', ':locale_name')
            ->set('route_name', ':route_name')
            ->set('travellers', ':travellers')
            ->where('id = :id')
            ->setParameter('name', $data['name'])
            ->setParameter('locale_name', $this->implodeLocaleNames($data['localeNames']))
            ->setParameter('route_name', $data['route_name'])
            ->setParameter('travellers', $data['travellers'])
            ->setParameter('id', $id);

        if ($imagePath) {
            $queryBuilder
                ->set('avatar', ':avatar')
                ->setParameter('avatar', $imagePath);
        }

        if ($imagePathSmall) {
            $queryBuilder
                ->set('avatar_small', ':avatar_small')
                ->setParameter('avatar_small', $imagePathSmall);
        }

        $queryBuilder->execute();
        return '';
    }

    /**
     * Convert locales to db format
     *
     * @param $localeNames
     * @return null|string
     */
    private function implodeLocaleNames($localeNames) {
        $resultArr = [];
        foreach ($localeNames as $locale => $name) {
            $locale = str_replace([',', ':', '"'], '', $locale);
            $name = str_replace([',', ':', '"'], '', $name);
            $resultArr[] = "\"$locale:$name\"";
        }
        if ($resultArr) {
            return '{' . implode(',', $resultArr) . '}';
        } else {
            return null;
        }
    }

}