<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.05.18
 * Time: 15:55
 */

namespace Civitours\Service;


use BulletProof\Image;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

class UploadService
{
    private $tmpPath;
    private $galleryPath;
    private $imageUrl;
    private $dbPath;
    private $filesPath;
    private $jpegQuality;

    public function __construct($imagePath, $imageUrl, $filesPath, $jpegQuality = 75)
    {
        $this->tmpPath = $imagePath . 'tmp' . '/';
        $this->galleryPath = $imagePath . 'gallery' . '/';
        $this->imageUrl = $imageUrl;
        $this->dbPath = 'gallery' . '/';
        $this->filesPath = $filesPath;
        $this->jpegQuality = $jpegQuality;
    }

    /**
     * Upload image
     * @param $files
     * @param $params
     * @return array
     */
    public function uploadImage($files, $params) {
        $image = new Image($files);

        $image->setSize($params['min'], $params['max']);
        $image->setMime($params['mimes']);
        $image->setLocation($this->tmpPath);

        //generate filename
        $hash = uniqid();
        while(file_exists($this->tmpPath . $hash)) {
            $hash = uniqid();
        }

        $image->setName($hash);

        if($image['fileItem']){
            $upload = $image->upload();
            if($upload){
                return [
                    'imageName' => $image->getName() . '.' . $image->getMime(),
                    'imageUrl'  => $this->imageUrl . 'tmp/' . $image->getName() . '.' . $image->getMime()
                ];
            }else{
                throw new BadRequestHttpException($image['error']);
            }
        }
        throw new BadRequestHttpException('Wrong image is provided');
    }

    /**
     * upload file to server
     *
     * @return string
     */
    public function uploadFile() {
        $originalName = $_FILES['file']['name'];
        $originalNameArray = explode('.', $originalName);
        $originalNameExt = end($originalNameArray);
        $filename = uniqid() . '.' . $originalNameExt;
        $uploadFile = $this->filesPath . DIRECTORY_SEPARATOR . $filename;
        if (!move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)) {
            throw new ServiceUnavailableHttpException(0, 'Cannot upload file');
        }
        return $filename;
    }

    /**
     * Copy image for country and return db path
     *
     * @param $imageFile
     * @param $countryName
     * @return string
     * @throws \ImagickException
     */
    public function copyCountryImage($imageFile, $countryName) {
        $entityDir = $this->toFolderName($countryName);
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . '/' . $imageFile;
    }

    /**
     * Copy image for city and return db path
     *
     * @param $imageFile
     * @param $countryName
     * @param $cityName
     * @return string
     * @throws \ImagickException
     */
    public function copyCityImage($imageFile, $countryName, $cityName) {
        $entityDir = $this->toFolderName($countryName) . '/' . $this->toFolderName($cityName);
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . '/' . $imageFile;
    }

    /**
     * Copy image file for activity
     *
     * @param $imageFile
     * @param $countryName
     * @param $cityName
     * @param $idActivity
     * @return string
     * @throws \ImagickException
     */
    public function copyActivityImage($imageFile, $countryName, $cityName, $idActivity) {
        $entityDir = $this->toFolderName($countryName) . '/' . $this->toFolderName($cityName) . '/' . $idActivity;
        if ($res = $this->copyImage($imageFile, $entityDir)) {
            throw new ServiceUnavailableHttpException(10, $res);
        }
        return $this->dbPath . $entityDir . '/' . $imageFile;
    }

    /**
     * Copy image - filesystem operations
     *
     * @param $imageFile
     * @param $entityDir
     * @return string
     * @throws \ImagickException
     */
    private function copyImage($imageFile, $entityDir) {
        $tmpFile = $this->tmpPath . $imageFile;
        if(!file_exists($tmpFile)) {
            return 'Image not found: ' . $tmpFile;
        }
        $targetDir = $this->galleryPath . $entityDir;
        if(!file_exists($targetDir)) {
            mkdir($targetDir, 0777,true);
        }
        $result = imagejpeg(imagecreatefromstring(file_get_contents($tmpFile)), $targetDir . '/' . $imageFile, $this->jpegQuality);
        if (!$result) {
            return 'Cannot copy file: ' . $targetDir . '/' . $imageFile;
        }
        return '';
    }

    /**
     * Convert object name to folder name
     *
     * @param $string
     * @return string
     */
    private function toFolderName($string) {
        return strtolower(preg_replace('/\W/','', $string));
    }

}