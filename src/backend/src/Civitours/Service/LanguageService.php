<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 29.05.18
 * Time: 11:31
 */

namespace Civitours\Service;

use Doctrine\DBAL\Connection;

/**
 * Service to operate with languages
 *
 * Class LanguageService
 * @package Civitours\Service
 */
class LanguageService
{
    /**
     *  Database connection
     *
     * @var Connection
     */
    private $db = null;

    /**
     * UserService constructor.
     *
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * Get list of countries with activities available
     *
     * @return array
     */
    public function getLanguagesList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select(
                'code',
                'name',
                'native_name'
            )
            ->from('languages');

        return $queryBuilder->execute()->fetchAll();
    }
}
