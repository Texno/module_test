<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 31.07.18
 * Time: 13:35
 */

namespace Civitours\Service;


use Civitours\Reports\Generator;
use Doctrine\DBAL\Connection;

/**
 * Service to operate reports
 *
 * Class ReportService
 * @package Civitours\Service
 */
class ReportService
{

    const READY_STATUS = 'ready';

    /**
     * Url to reports
     *
     * @var string
     */
    private $urlToReports;
    /**
     * @var Connection
     */
    private $db;

    /**
     * Path to reports storage
     *
     * @var
     */
    private $pathToReports;

    /**
     * ReportService constructor.
     * @param Connection $db
     * @param $pathToReports
     * @param $apiUrl
     */
    public function __construct(Connection $db, $pathToReports, $apiUrl)
    {
        $this->urlToReports = $apiUrl . '/admin/reports/' ;
        $this->db = $db;
        $this->pathToReports = $pathToReports;
    }

    /**
     * Get all available report types
     *
     * @return array
     */
    public function getReportTypesList() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                'id',
                'name'
            ])
            ->from('report_types')
            ->orderBy('id', 'ASC');
        return $queryBuilder->execute()->fetchAll();
    }

    /**
     * Get query duilder for reports
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    private function prepareQueryBuilder() {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select([
                't_reports.id',
                't_reports.report_type',
                't_report_types.name AS report_type_name',
                't_reports.name',
                'CAST(EXTRACT(epoch FROM t_reports.created_at) AS integer) as created_at',
                't_reports.status',
                't_reports.params',
                't_reports.file'
            ])
            ->from('reports', 't_reports')
            ->join('t_reports', 'report_types', 't_report_types', 't_reports.report_type = t_report_types.id')
            ->where('t_reports.is_deleted = false');
        return $queryBuilder;
    }

    /**
     * Return list of reports
     *
     * @return array
     */
    public function getReportsList() {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->orderBy('t_reports.id', 'ASC');
        return array_map([$this, 'populateLinks'], $queryBuilder->execute()->fetchAll());
    }

    /**
     * Return list of reports
     *
     * @param $id
     * @return array
     */
    public function getReportById($id) {
        $queryBuilder = $this->prepareQueryBuilder();
        $queryBuilder
            ->andWhere('t_reports.id = :id')
            ->setParameter('id', $id)
            ->setMaxResults(1);
        return $this->populateLinks($queryBuilder->execute()->fetch());
    }

    /**
     * Get generated report data
     *
     * @param $id
     * @return array
     */
    public function getReportData($id) {
        $file = $this->pathToReports . DIRECTORY_SEPARATOR . $id;
        if (!$file) {
            return null;
        }
        $fileHandler = fopen($file, 'r');
        $result = [];
        while ($row = fgetcsv($fileHandler)) {
            array_push($result, $row);
        }
        fclose($fileHandler);
        return $result;
    }

    /**
     * Create new report
     *
     * @param $data
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createReport($data) {
        $this->db->beginTransaction();
        try {
            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->insert('reports')
                ->setValue('name'        , ':name')
                ->setValue('report_type' , ':type')
                ->setParameters([
                    'name'  => $data['name'],
                    'type'  => $data['report_type'],
                ]);
            $queryBuilder->execute();
            $idReport = $this->db->lastInsertId();

            $reportGenerator = new Generator($this->db, $this->pathToReports);
            $reportGenerator->generateReport($data['report_type'], $idReport);

            $queryBuilder = $this->db->createQueryBuilder();
            $queryBuilder
                ->update('reports')
                ->set('status', ':status')
                ->where('id = :id')
                ->setParameter('status', self::READY_STATUS)
                ->setParameter('id', $idReport);
            $queryBuilder->execute();

            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    /**
     * Set delete status
     *
     * @param $idReport
     * @return int
     */
    public function markAsDeleted($idReport) {
        $queryBuilder = $this->db->createQueryBuilder();

        $queryBuilder
            ->update('reports')
            ->set('is_deleted', 'true')
            ->where('id = :id')
            ->setParameter('id', $idReport);

        return $queryBuilder->execute();
    }

    /**
     * Callback to fulfill report download links
     *
     * @param $item
     * @return mixed
     */
    private function populateLinks($item) {
        unset($item['file']);
        if ($item['status'] == self::READY_STATUS) {
            $item['csv'] = $this->urlToReports . $item['id'] . '/csv';
            $item['xls'] = $this->urlToReports . $item['id'] . '/xls';
        } else {
            $item['csv'] = null;
            $item['xls'] = null;
        }
        return $item;
    }
}
