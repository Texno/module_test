import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search.component';

const routes: Routes = [
  {
    path: '', component: SearchComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Search page'
      }
    },
  },
];

export const SearchRoutes = RouterModule.forChild(routes);
