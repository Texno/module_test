import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { SearchRoutes } from './search.routing';
import { ComponentsModule } from '@shared/components/components.module';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';

@NgModule({
  imports: [
    CommonModule,
    SearchRoutes,
    ComponentsModule,
    BookingCalendarModule
  ],
  declarations: [SearchComponent]
})
export class SearchModule { }
