import { Component, OnInit } from '@angular/core';
import { AjaxActivityFilter, FastActivityFilter } from '@shared/components/data-types/filter.types';
import { Activity } from '@shared/backend/data-types/activity.types';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '@shared/backend/rest-api.service';
import { CityFilter } from '@shared/backend/data-types/geo.types';
import { isArray } from 'util';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.styl']
})
export class SearchComponent implements OnInit {

  activitiesPromise: Promise<Activity[]>;
  activities: Activity[] = [];
  fastFilter = new FastActivityFilter();
  citiesFilterMap = new Map<number, CityFilter>();
  citiesFilter: CityFilter[] = [];
  queryString: string = '';
  openSidebar = false;
  isLoading = false;
  isActivityLoading = false;

  private searchString = '';

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router, private ga: GoogleTracking) { }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      this.queryString = params.get('q') || '';
      this.ga.fireEvent(GoogleTracking.SEARCH_CATEGORY, 'Other', this.queryString);
      const navigatedData = this.router.getNavigatedData();
      if(isArray(navigatedData) && navigatedData.length) {
        this.activities = navigatedData;
        this.activitiesPromise = Promise.resolve(navigatedData);
      } else {
        this.isLoading = true;
        this.activitiesPromise = this.rest.searchActivities(this.queryString).then(this.activityLoadedHandler);
      }
    })
  }

  /**
   * Handler after activities are loaded
   *
   * @param {Activity[]} activities
   * @returns {Activity[]}
   */
  private activityLoadedHandler = (activities: Activity[]) => {
    this.isLoading = false;
    this.isActivityLoading = false;
    this.citiesFilterMap.clear();
    activities.forEach(activity => {
      const citiesFilter = CityFilter.fromActivity(activity);
      this.citiesFilterMap.set(citiesFilter.id, citiesFilter);
    });
    this.citiesFilter = Array.from(this.citiesFilterMap.values());
    this.citiesFilter.sort((a, b) => a.name.localeCompare(b.name));
    return this.activities = activities;
  };

  /**
   * Search filtering function
   *
   * @returns {Promise<void>}
   * @param search
   */
  async searchInput(search: string) {
    this.searchString = search;
    this.fastFilterChanged(this.fastFilter);
  }

  /**
   * Fast filtering function
   * @param {FastActivityFilter} filter
   */
  async fastFilterChanged(filter: FastActivityFilter) {
    const activitiesSource = await this.activitiesPromise;
    this.fastFilter = filter;
    this.activities = activitiesSource.filter(item => {
      const searchPassed = !this.searchString.length
                          || (-1 !== item.name.toLowerCase().indexOf(this.searchString.toLowerCase()))
                          || (-1 !== item.short_description.toLowerCase().indexOf(this.searchString.toLowerCase()));
      const cityPassed = !this.fastFilter.cities.length || this.fastFilter.cities.includes(item.city);
      const pricePassed = (item.min_price >= this.fastFilter.minPrice) && (item.min_price <= this.fastFilter.maxPrice);
      const duration = parseInt(item.duration) || 0;
      const durationPassed = (duration >= this.fastFilter.minDuration) && (duration <= this.fastFilter.maxDuration || FastActivityFilter.MAX_DURATION == this.fastFilter.maxDuration);
      const ratePassed = item.rate >= this.fastFilter.rate * 2;
      return cityPassed && pricePassed && durationPassed && ratePassed && searchPassed;
    });
  }

  /**
   * Perform async filtering
   *
   * @param {AjaxActivityFilter} filter
   * @returns {Promise<void>}
   */
  async ajaxFilterChanged(filter: AjaxActivityFilter) {
    this.activities = [];
    this.isActivityLoading = true;
    this.activitiesPromise = this.rest.searchActivities(this.queryString, filter).then(this.activityLoadedHandler);
    await this.fastFilterChanged(this.fastFilter);
    this.isActivityLoading = false;
  }
  closeSideBar(){
    this.openSidebar = false;
  }
}
