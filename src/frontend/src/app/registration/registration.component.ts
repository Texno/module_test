import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Observable } from 'rxjs/Observable';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { RegistrationForm } from '@shared/backend/data-types/registration-form';
import { RecaptchaComponent } from 'ng-recaptcha';
import { City, Country } from '@shared/backend/data-types/geo.types';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { combineLatest } from 'rxjs/observable/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { of } from 'rxjs/observable/of';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { AppConfig } from '@shared/config/app-config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.styl']
})
export class RegistrationComponent implements OnInit, AfterViewInit {

  serverErrors = {
    email: [],
    password: [],
    passwordConfirm: [],
    name: [],
    surname: [],
    phone: [],
    country: [],
    city: [],
    token: []
  };

  registerError = '';

  registerForm: FormGroup;
  formData = new RegistrationForm();
  @ViewChild('captchaRef') recaptchaRef: RecaptchaComponent;

  passwordType = "password";
  passwordConfirmType = "password";
  checkFields = false;
  inProgress = false;

  termsStatus = true;
  
  countryError = false;
  cityError = false;

  countryFocus = false;
  cityFocus = false;

  reCapthcaPublicKey = '';

  private countries$: Promise<Country[]>;

  constructor(private rest: RestApiService, private router: Router) {
    this.createForm();
    this.reCapthcaPublicKey = AppConfig.getConfiguration().googleReCaptcha.publicKey;
  }

  ngOnInit() {

    this.countries$ = this.rest.getCountryList();
  }

  ngAfterViewInit() {
    this.recaptchaRef.resolved.subscribe((captcha) => {
      if (null === captcha) {
        // Captcha is reloaded
        return;
      }
      this.formData.fillFromObject(this.registerForm.value);
      this.formData.token = captcha;
      this.rest.registerUser(this.formData)
        .then(res => {
          this.recaptchaRef.reset();
          this.inProgress = false;
          this.router.navigate(['/info/registration-completed']);
        })
        .catch(err => {
          this.recaptchaRef.reset();
          if (err.error.hasOwnProperty('errors')) {
            Object.assign(this.serverErrors, err.error.errors);
          } else {
            this.registerError = err.error.message;
          }
          this.inProgress = false;
        });
    });
  }

  /**
   * Create registerForm class
   */
  private createForm() {
    this.registerForm = new FormGroup({
      name:       new FormControl('',    [
        Validators.required,
        Validators.maxLength(150)
      ]),
      surname:       new FormControl('',    [
        Validators.required,
        Validators.maxLength(150)
      ]),
      email:      new FormControl('',   [
        Validators.required,
        Validators.email
      ]),
      password:   new FormControl('',   [
        Validators.required,
        Validators.pattern(/^(?=.*[A-Z]).{8,}$/)
      ]),
      phone:      new FormControl('',   [
        Validators.required,
        Validators.pattern(/^\+\d{11,15}$/)
      ]),
      terms:  new FormControl('', {
        validators: [Validators.requiredTrue],
        updateOn: 'change'  // updateon blur doesn`t work with checkboxes
      }),
    }, { updateOn: 'change'});

    const passwordConfirm = new FormControl('',   [
      Validators.required,
      this.sameValueAs(this.registerForm, 'password')
    ]);

    this.registerForm.addControl('passwordConfirm', passwordConfirm);
    for(let control in this.registerForm.controls) {
      this.registerForm.controls[control].valueChanges.subscribe(() => {
        this.serverErrors[control] = []; // Reset server error for this control
      })
    }

  }

  /**
   * Custom validation function
   *
   * @param {FormGroup} group
   * @param {string} controlName
   * @returns {ValidatorFn}
   */
  sameValueAs(group: FormGroup, controlName: string): ValidatorFn {
    return (control: AbstractControl) => {
      const myValue = control.value;
      const compareValue = group.controls[controlName].value;
      return (myValue === compareValue) ? null : {valueDifferent: true};
    };
  }

  /**
   * Show password field
   */
  showPassword() {
    this.passwordType="text";
  }
  /**
   * Hide password field
   */
  hidePassword() {
    this.passwordType="password";
  }

  /**
   * Show password confirm field
   */
  showPasswordConfirm() {
    this.passwordConfirmType="text";
  }
  /**
   * Hide password confirm field
   */
  hidePasswordConfirm() {
    this.passwordConfirmType="password";
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    let country: Country = event.item;
    this.formData.country = country.id;
    this.registerForm.get('phone').setValue('+' + country.calling_code);
    this.countryError = false;
  };

  /**
   * Callback to filter cities
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any[]>}
   */
  searchCity = (text$: Observable<string>) => {
    return text$.debounceTime(200).distinctUntilChanged()
      .flatMap(searchText => {
        if (!this.formData.country || !searchText.length) {
          return of([]);
        }
        return this.rest.searchCitiesByCountry(this.formData.country, searchText);
      });

  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  cityFormatter = (result: City) => {
    return result.name;
  };

  /**
   * City select handler
   * @param event
   */
  onSelectCity = (event) => {
    let city: City = event.item;
    this.formData.city = city.id;
    this.cityError = false;
  };

  /**
   * Check & submit form
   */
  onSubmit() {
    this.checkFields = true;
    this.countryError = <boolean>!this.formData.country;
    this.cityError = <boolean>!this.formData.city;
    if (this.registerForm.invalid || this.countryError || this.cityError) {
      return;
    }
    this.inProgress = true;
    this.recaptchaRef.execute();
  }

  /**
   * Check entered city
   */
  onCityBlur() {
    this.cityError = <boolean>!this.formData.city;
    this.cityFocus = false;
  }


  /**
   * Check entered country
   */
  onCountryBlur() {
    this.countryError = <boolean>!this.formData.country;
    this.countryFocus = false;
  }

  /**
   * Set focus to element
   * @param {string} id
   */
  setFocus(id: string) {
    document.getElementById(id).focus();
  }

  /**
   * Form field getters
   */
  get name() { return this.registerForm.get('name'); }
  get surname() { return this.registerForm.get('surname'); }
  get email() { return this.registerForm.get('email'); }
  get phone() { return this.registerForm.get('phone'); }
  get password() { return this.registerForm.get('password'); }
  get passwordConfirm() { return this.registerForm.get('passwordConfirm'); }
  get terms() { return this.registerForm.get('terms'); }
  showPasswordMain(){
    var showPassword = document.getElementById("password");
    if(showPassword.getAttribute("type")=="password"){
        showPassword.setAttribute("type","text");
    } else {
        showPassword.setAttribute("type","password");
    }
  }
  PasswordConfirm(){
    var showPasswordMain = document.getElementById("passwordConfirm");
    if(showPasswordMain.getAttribute("type")=="password"){
        showPasswordMain.setAttribute("type","text");
    } else {
        showPasswordMain.setAttribute("type","password");
    }
  }
}
