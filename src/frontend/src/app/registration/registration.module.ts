import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutes } from './registration.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecaptchaModule } from 'ng-recaptcha';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '@shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    RegistrationRoutes,
    NgbModule,
    RecaptchaModule,
    ReactiveFormsModule,
    ComponentsModule
  ],
  declarations: [RegistrationComponent]
})
export class RegistrationModule { }
