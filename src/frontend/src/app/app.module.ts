// angular
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// libs
import { CookieService } from 'ngx-cookie-service';
import { PrebootModule } from 'preboot';
// shared
import { SharedModule } from '@shared/shared.module';
// components
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecaptchaModule } from 'ng-recaptcha';
import { AppConfig } from '@shared/config/app-config.service';
import "angular2-navigate-with-data";

export function ConfigLoader(appConfig: AppConfig) {
  return () => appConfig.load()
}

import es from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

registerLocaleData(es);

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    PrebootModule.withConfig({ appRoot: 'app-root' }),
    HttpClientModule,
    RouterModule,
    AppRoutes,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    SharedModule.forRoot(),
    RecaptchaModule.forRoot(),
  ],
  declarations: [AppComponent],
  providers: [
    CookieService,
    AppConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [AppConfig],
      multi: true
    },
    { provide: LOCALE_ID, useValue: "es" },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
