import { Component } from '@angular/core';

import { MetaService } from '@ngx-meta/core';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/fromPromise';
import { NavigationEnd, Router } from '@angular/router';

@Component({
	selector: 'app-root',
	template: '<router-outlet></router-outlet>'
})
export class AppComponent {
	constructor(private readonly meta: MetaService, router: Router) {

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).dataLayer.push({'event': 'pageview'});
      }
    });

		this.meta.setTag('og:title', 'home ctor');
		if (!Array.prototype.includes) {
			Object.defineProperty(Array.prototype, 'includes', {
				value: function(searchElement, fromIndex) {
					if (this == null) {
						throw new TypeError('"this" is null or not defined');
					}
					var o = Object(this);
					var len = o.length >>> 0;
					if (len === 0) {
						return false;
					}
					var n = fromIndex | 0;
					var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
					function sameValueZero(x, y) {
						return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
					}
					while (k < len) {
						if (sameValueZero(o[k], searchElement)) {
							return true;
						}
						k++;
					}
					return false;
				}
			});
		}
	}
}
