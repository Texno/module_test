import { Routes, RouterModule } from '@angular/router';
import { GeneralComponent } from './general/general.component';
import { LegalComponent } from './legal/legal.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CookiesComponent } from './cookies/cookies.component';

const routes: Routes = [
  {
    path: 'general',
    component: GeneralComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Home description'
      }
    },
  },
  {
    path: 'legal',
    component: LegalComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Home description'
      }
    },
  },
  {
    path: 'privacy',
    component: PrivacyComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Home description'
      }
    },
  },
  {
    path: 'cookies',
    component: CookiesComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Home description'
      }
    },
  },
  { path: '',   redirectTo: 'general', pathMatch: 'full' },
];

export const PolicyRoutes = RouterModule.forChild(routes);