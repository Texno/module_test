import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GeneralComponent } from './general/general.component';
import { LegalComponent } from './legal/legal.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CookiesComponent } from './cookies/cookies.component';
import { PolicyRoutes } from 'app/policy/policy.routing';

@NgModule({
  imports: [
    CommonModule,
    PolicyRoutes
  ],
  declarations: [GeneralComponent, LegalComponent, PrivacyComponent, CookiesComponent]
})
export class PolicyModule { }
