import { Routes, RouterModule } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

import { WrapperComponent } from '@shared/layouts/wrapper/wrapper.component';
import { WrapperComponent as CheckoutWrapperComponent } from '@shared/layouts/checkout/wrapper/wrapper.component';
import { AdminGuard, AuthGuard } from '@shared/auth/auth-guard';

const routes: Routes = [
  {
    path: 'checkout', component: CheckoutWrapperComponent, canActivateChild: [MetaGuard], children: [
      { path: '', loadChildren: './checkout/checkout.module#CheckoutModule' },
    ]
  },
  {
    path: '', component: WrapperComponent, canActivateChild: [MetaGuard], children: [
      { path: '', loadChildren: './home/home.module#HomeModule' },
      { path: 'destinations', loadChildren: './destinations/destinations.module#DestinationsModule' },
      { path: 'admin', canActivate: [AdminGuard], loadChildren: './admin/admin.module#AdminModule' },
      { path: 'dashboard', canActivate: [AuthGuard], loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'order', loadChildren: './checkout/checkout.module#CheckoutModule' },
      { path: 'signup', loadChildren: './registration/registration.module#RegistrationModule' },
      { path: 'search', loadChildren: './search/search.module#SearchModule' },
      { path: 'info/:id', loadChildren: './info/info.module#InfoModule' },
      { path: 'policy', loadChildren: './policy/policy.module#PolicyModule' },
      { path: 'contacts', loadChildren: './contacts/contacts.module#ContactsModule' },
      { path: 'faq', loadChildren: './faq/faq.module#FaqModule' },
      { path: 'es/:country', loadChildren: './country/country.module#CountryModule' },
      { path: 'es/:country/:city', loadChildren: './city/city.module#CityModule' },
      { path: 'es/:country/:city/:activity', loadChildren: './activity-page/activity-page.module#ActivityPageModule' },
      { path: '**', loadChildren: './not-found/not-found.module#NotFoundModule' },
    ]
  }
];
// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes, { initialNavigation: 'enabled' });
