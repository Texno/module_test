import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestinationsComponent } from './destinations.component';
import { DestinationsRoutes } from './destinations.routing';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { ComponentsModule } from '@shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    DestinationsRoutes,
    AmChartsModule,
    ComponentsModule
  ],
  declarations: [DestinationsComponent]
})
export class DestinationsModule { }
