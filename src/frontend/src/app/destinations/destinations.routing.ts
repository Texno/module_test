import { Routes, RouterModule } from '@angular/router';
import { DestinationsComponent } from './destinations.component';

const routes: Routes = [
  {
    path: '', component: DestinationsComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Destinations'
      }
    },
  },
];

export const DestinationsRoutes = RouterModule.forChild(routes);