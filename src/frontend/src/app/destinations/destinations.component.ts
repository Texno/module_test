import { AfterViewInit, Component, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { Router } from '@angular/router';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.styl']
})
export class DestinationsComponent implements OnInit, AfterViewInit {

  destinations: DestinationCity[] = [];
  countries: CountryGrouped[] = [];

  routeHelper = RouteHelper;

  private chart: AmChart;

  constructor(private chartService: AmChartsService, private rest: RestApiService, private router: Router) { }

  ngOnInit() {
    this.rest.getMainDestinationsList().then(destinations => {
      this.destinations = destinations;
      let countryMap = new Map<string, CountryGrouped>();

      // prepare country list
      this.destinations.forEach(destination => {
        if (countryMap.has(destination.country_code)) {
          countryMap.get(destination.country_code).cities.push(destination);
        } else {
          countryMap.set(destination.country_code, {
            country:    destination.country_name,
            route_name: destination.country_route,
            code:       destination.country_code,
            cities:     [destination]
          })
        }
      });
      this.countries = Array.from(countryMap.values());

      // Update chart with countries
      this.chartService.updateChart(this.chart, () => {

        this.chart.dataProvider.areas = [];
        this.chart.dataProviderlines = [
          {
            "latitudes": [51.5002, 50.4422],
            "longitudes": [-0.1262, 30.5367]
          }
        ];

        this.countries.forEach(country => {
          let area = {
            id: country.code,
            balloonText: `${country.country}, ${country.cities.length} destinos`,
            title: ' ',
            images:[]
          };
          country.cities.forEach(city => {
            area.images.push({
              svgPath: 'M8 20S0 14.747 0 8c0-4.253 3.543-8 8-8s8 3.747 8 8c0 6.747-8 12-8 12zm.24-10c1.05 0 1.9-.84 1.9-1.875 0-1.036-.85-1.875-1.9-1.875-1.05 0-1.902.84-1.902 1.875C6.338 9.161 7.189 10 8.24 10z',
              color: '#0086e6',
              balloonText: city.name + ', ' + city.excursions + ' Actividades',
              latitude: city.latitude,
              longitude: city.longitude,
              url: '?',
              customData: city
            });
          });

          this.chart.dataProvider.areas.push(area);
        });
      });
    });
  }

  getActivitiesForCountry = (country: CountryGrouped): number => {
      let result = 0;
      country.cities.forEach(city => result+= parseInt(city.excursions.toString()));
      return result;
  };

  ngAfterViewInit() {
    this.chart = this.chartService.makeChart("chartdiv", {
      type: "map",
      theme: "light",
      projection: "miller",
      dataProvider: {
        map: "worldLow",
      },
      areasSettings: {
        autoZoom: true,
        selectedColor: "#CC0000"
      },
      smallMap: {
        enabled: false
      },
      zoomControl: {
        enabled: true,
        bottom: 150
      }
    });

    this.chart.addListener("mouseDownMapObject", (event) => {
      if('MapImage' == event.mapObject.cname) {
        // User clicked city object
        this.router.navigate([RouteHelper.buildCityRoute(event.mapObject.customData)]);
      }
    });
  }

  onWheel(event: WheelEvent) {
    if(event.deltaY > 0) {
      this.chart.zoomOut();
    } else {
      this.chart.zoomIn();
    }
    return false;
  }

}

interface CountryGrouped {
  country:    string;
  route_name: string;
  code:       string;
  cities:     DestinationCity[];
}
