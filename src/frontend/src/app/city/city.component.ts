import { Component, HostListener, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '@shared/backend/data-types/activity.types';
import { Review } from '@shared/backend/data-types/review.types';
import { AjaxActivityFilter, FastActivityFilter } from '@shared/components/data-types/filter.types';
import { AuthService } from '@shared/auth/auth.service';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.styl']
})
export class CityComponent implements OnInit {

  destinationInfo: Promise<DestinationCity>;
  activitiesPromise: Promise<Activity[]> = new Promise<Activity[]>(resolve => {});
  activities: Activity[] = [];
  reviews: Review[] = [];
  isLoading: boolean;
  isActivityLoading = false;

  categoriesFilter = [];
  activityCategoriesFilter = [];

  fastFilter = new FastActivityFilter();

  openSidebar = false;

  private idCity: number;
  private searchString = '';
  private oldClass = ''; 

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router, private ga: GoogleTracking) {
    this.isLoading = true;
  }

  ngOnInit() {
    this.oldClass = document.body.className;
    document.body.className = this.oldClass;
    this.route.paramMap.subscribe(params => {
      if(params.has('country') && params.has('city')) {
        this.destinationInfo = this.rest.getDestinationCity(params.get('country'), params.get('city'));
        this.destinationInfo.then(info => {
          (<any>window).dataLayer.push({'content_grouping_1' : 'city_page'});
          this.idCity = info.id;
          this.activitiesPromise = this.rest.getActivitiesForCity(info.id).then(activities => {
            this.ga.fireProductImpression(activities);
            this.isActivityLoading = false;
            return this.activities = activities;
          });
          let reviewsInfo = this.rest.getReviewsForCity(info.id).then(reviews => this.reviews = reviews);
          Promise.all([
            this.activitiesPromise,
            reviewsInfo
          ]).then(() => this.isLoading = false);
          return info;
        }).catch(err => {
          if (err.hasOwnProperty('status') && err.status == 404) {
            this.router.navigate([AuthService.FALLBACK_URL]);
          }
          console.log(err);
        });
      }
    });
  }

  fireActivityClick(activity: Activity) {
    this.ga.fireProductClick(activity);
  }

  /**
   * Callback for categories filter change
   * @param {number[]} categories
   */
  changedCategoriesFilter(categories: number[]) {
    this.categoriesFilter = categories;
    this.fastFilter.categories = categories;
    this.fastFilterChanged(this.fastFilter);
  }

  /**
   * Search filtering function
   *
   * @returns {Promise<void>}
   * @param search
   */
  async searchInput(search: string) {
    this.searchString = search;
    this.fastFilterChanged(this.fastFilter);
  }

  /**
   * Fast filtering function
   * @param {FastActivityFilter} filter
   */
  async fastFilterChanged(filter: FastActivityFilter) {
    const activitiesSource = await this.activitiesPromise;
    this.activityCategoriesFilter = filter.categories;
    this.fastFilter = filter;
    this.activities = activitiesSource.filter(item => {
      const searchPassed = !this.searchString.length
                            || (-1 !== item.name.toLowerCase().indexOf(this.searchString.toLowerCase()))
                            || (-1 !== item.short_description.toLowerCase().indexOf(this.searchString.toLowerCase()));
      const categoryPassed = !this.fastFilter.categories.length || this.fastFilter.categories.includes(item.category);
      const pricePassed = (item.min_price >= this.fastFilter.minPrice) && (item.min_price <= this.fastFilter.maxPrice);
      const duration = parseInt(item.duration) || 0;
      const durationPassed = (duration >= this.fastFilter.minDuration) && (duration <= this.fastFilter.maxDuration || FastActivityFilter.MAX_DURATION == this.fastFilter.maxDuration);
      const ratePassed = item.rate >= this.fastFilter.rate * 2;
      return categoryPassed && pricePassed && durationPassed && ratePassed && searchPassed;
    });
  }

  /**
   * Perform async filtering
   *
   * @param {AjaxActivityFilter} filter
   * @returns {Promise<void>}
   */
  async ajaxFilterChanged(filter: AjaxActivityFilter) {
    this.activities = [];
    this.isActivityLoading = true;
    this.activitiesPromise = this.rest.getActivitiesForCity(this.idCity, filter);
    await this.fastFilterChanged(this.fastFilter);
    this.isActivityLoading = false;
  }

  openSideBar(){

    this.openSidebar = true;
    this.oldClass = document.body.className;
    if(!document.body.className.split(' ').includes('fixed')){
      document.body.className += ' ' + 'fixed';
    }
  }
  closeSideBar(){
    this.openSidebar = false;
    document.body.className = this.oldClass;
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }
}
