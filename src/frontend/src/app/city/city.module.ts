import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityComponent } from './city.component';
import { CityRoutes } from './city.routing';
import { HeroComponent } from './components/hero/hero.component';
import { ComponentsModule } from '@shared/components/components.module';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { ReviewComponent } from './components/review/review.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    CityRoutes,
    ComponentsModule,
    NgbModule,
    BookingCalendarModule
  ],
  declarations: [CityComponent, HeroComponent, ReviewComponent]
})
export class CityModule { }
