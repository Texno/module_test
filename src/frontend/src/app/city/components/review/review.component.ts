import { Component, Input, OnInit } from '@angular/core';
import { Review } from '@shared/backend/data-types/review.types';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.styl']
})
export class ReviewComponent implements OnInit {

  @Input() review: Review;

  constructor() { }

  ngOnInit() {
  }

}
