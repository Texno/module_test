import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './components/request/request.component';
import { InfoComponent } from './components/info/info.component';
import { FaqRoutes } from './faq.routing';
import { FormsModule } from '@angular/forms';
import { ComponentsModule } from '@shared/components/components.module';
import { FaqComponent } from './faq.component';

  @NgModule({
      imports: [
      CommonModule,
      FaqRoutes,
      FormsModule,
      ComponentsModule
    ],
    declarations: [FaqComponent, RequestComponent, InfoComponent]
})
export class FaqModule { }