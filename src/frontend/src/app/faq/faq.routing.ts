import { Routes, RouterModule } from '@angular/router';
import { FaqComponent } from './faq.component';
import { InfoComponent } from './components/info/info.component';
import { RequestComponent } from './components/request/request.component';

  const routes: Routes = [
    {
      path: '', component: FaqComponent,
      data: {
        meta: {
            title: 'Civitours',
              override: true,
              description: 'Faq'
          }
      },
    children: [
        { path: 'request', component: RequestComponent},
        { path: '', component: InfoComponent}
      ]
  }
];

export const FaqRoutes = RouterModule.forChild(routes);