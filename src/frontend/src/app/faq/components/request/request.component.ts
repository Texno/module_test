import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.styl']
})
export class RequestComponent implements OnInit {

  selectedFile: File;
  tryToSubmit = false;

  form = {
    email: '',
    subject: '',
    message: '',
    description: ''
  };

  constructor(private rest: RestApiService) { }

  ngOnInit() {
  }

  submitData(form: NgForm) {
      this.tryToSubmit = true;
      if(!form.valid) {
          return false;
        }
      const formData = new FormData();
      formData.append('data', JSON.stringify(this.form));
      if(this.selectedFile) {
          formData.append('file', this.selectedFile);
        }
      this.rest.requestFaq(formData)
        .then(() => {
            this.tryToSubmit = false;
            alert('Solicitud aceptada.Nos pondremos en contacto con usted pronto.');
            this.form = {
                email: '',
                subject: '',
                message: '',
                description: ''
            };
            form.reset();
            this.selectedFile = null;
          })
        .catch(() => {
            alert('La petición falló. Por favor, inténtelo de nuevo más tarde.');
          })
    }
}