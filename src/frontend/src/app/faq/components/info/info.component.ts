import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.styl']
})
export class InfoComponent implements OnInit {

  selectedTab = 0;

  information = [
    {
      header: 'actividades y excursiones de un día',
      blocks: [
        {
          header: 'Preguntas Generales',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false}
          ]
        },
        {
          header: 'Proceso De Reserva',
          opened: false,
          questions: [
            {q: '¿Puedo reservar una actividad para otra persona?', a: 'Si es posible. Es importante que cuando reserves para otra persona indiques los datos personales de la persona que realizará la actividad. La información de pago sí debe ser tus datos para finalizar la transacción.', o:false},
            {q: '¿Cómo realizo el pago de mi reserva?', a: 'Realizar el pago con nosotros es muy fácil y rápido. Puedes realizarlo a través de PayPal o bien una tarjeta Visa o Mastercard. Todos los pagos de las actividades deben ser realizados a través de la página web.', o:false},
            {q: '¿El pago es seguro?', a: 'Contamos con un seguro de pago online 100% seguro y libre de fraudes o transacciones no autorizadas. Como empresa, no tenemos acceso a tus datos bancarios ni datos de tarjeta.', o:false},
            {q: 'He realizado el pago, pero no he recibido el correo de información', a: 'Si no recibes el correo electrónico de confirmación de forma inmediata, es muy probable que el pago no se realizara correctamente. Inténtalo nuevamente o contáctanos al siguiente correo: administracion@civitours.com', o:false}
          ]
        },
        {
          header: 'Opiniones Y Evaluaciones',
          opened: false,
          questions: [
            {q: '¿Son las opiniones reales?', a: 'Todas nuestras opiniones y puntuaciones son de clientes que han reservado con Civitours. Por protocolo de la empresa, al finalizar la actividad, recibirás un correo electrónico para que puedas evaluarnos y ayudarnos a mejorar.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'Al recibir tu opinión, esta pasa por un proceso de verificación y en breve es publicada. No obstante, Civitours se reserva el derecho a eliminar cualquier comentario o valoración obscena, discriminativa, que incluya anuncios de otros sitios web, que no se refiera a la actividad valorada y realizada, que el cliente mismo retire su valoración.', o:false},
            {q: '¿Cómo puedo dejar mi opinión de la actividad que he realizado?', a: 'Al finalizar la actividad, recibirás un correo electrónico para que puedas evaluarnos y ayudarnos a mejorar.', o:false}
          ]
        }
      ]
    },
    {
      header: 'Servicio de traslado al aeropuerto',
      blocks: [
        {
          header: 'Preguntas Generales',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        },
        {
          header: 'Proceso De Reserva',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        },
        {
          header: 'Opiniones Y Evaluaciones',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        }
      ]
    },
    {
      header: 'Otros temas',
      blocks: [
        {
          header: 'Preguntas Generales',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        },
        {
          header: 'Proceso De Reserva',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        },
        {
          header: 'Opiniones Y Evaluaciones',
          opened: false,
          questions: [
            {q: '¿Dónde inicia la actividad?', a: 'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.', o:false},
            {q: '¿Cuentan con otros tours o actividades a parte de las publicadas?', a: 'En algunos destinos, Civitours te da la opción de cambiar tu tour a uno privado. Puedes solicitar el tour privado escribiendo a administracion@civitours.com', o:false},
            {q: '¿Es necesario imprimir un bono o justificante?', a: 'Los detalles se encuentran en cada actividad, algunas requieren de un bono impreso, otras bastan con presentar en el móvil y otras no requieren de nada.', o:false},
            {q: '¿Se accede a todos los lugares mencionados en el itinerario?', a: 'Siempre intentamos dejar muy en claro los lugares en los cuales se accederá o si solo pasaremos por él. En la sección de “Incluye y No Incluye”, puedes observar de manera más especifica el acceso a cada lugar. En caso de no mencionarse nada, es muy probable que no se acceda al lugar.', o:false},
            {q: '¿Si llego tarde al tour me esperarán?', a: 'Si has reservado un tour privado, puedes consultar un cambio de hora y siempre y cuando sea posible se te concederá. En cuanto a los tours regulares, estos suelen ser muy puntuales y no se puede realizar modificación en la hora de inicio.', o:false},
          ]
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
