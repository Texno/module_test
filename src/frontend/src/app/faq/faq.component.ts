import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCity } from '@shared/backend/data-types/destination.types';

@Component({
      selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.styl']
})
export class FaqComponent implements OnInit {

  mainDestinationList: Promise<DestinationCity[]>;

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.mainDestinationList = this.rest.getMainDestinationsList();
  }

}
