import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { OrderInfo } from '@shared/backend/data-types/order.types';

/**
 * Date operations helper class
 */
export class DateHelper {

  /**
   * format date to correct string
   * @param {NgbDateStruct} date
   * @param offset
   * @returns {string}
   */
  static formatDate(date: NgbDateStruct, offset: number = null): string {
    const str = date.year + "-" + this.pad(date.month, 2) + "-" + this.pad(date.day, 2);
    if (null === offset) {
      return str;
    }
    const offsetStr = (offset < 0 ? '-' : '+') + this.pad(offset, 2) + ':00';
    return str + ' ' + offsetStr;
  }

  /**
   * Time format function
   *
   * @param {number} time
   * @returns {string}
   */
  static formatTime(time: number): string {
    return DateHelper.pad(time, 2) + ':00';
  }

  /**
   * Add zero num padding
   * @param {number} num
   * @param {number} size
   * @returns {string}
   */
  private static pad(num: number, size: number): string {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }

  /**
   * Convert ngb date to moment format
   *
   * @param {NgbDateStruct} date
   * @param offset
   * @returns {moment.Moment}
   */
  static parseNgbDateStruct(date: NgbDateStruct, offset = 0): moment.Moment {
    return moment.parseZone(DateHelper.formatDate(date, offset), 'YYYY-MM-DD ZZ');
  }

  /**
   * Get seconds from time struct
   *
   * @param {NgbTimeStruct} time
   * @returns {number}
   */
  static convertNgbTimeStructToSeconds(time: NgbTimeStruct): number {
    return time.hour * 3600 + time.minute * 60 + time.second;
  }

  /**
   * Convert unix timestamp to date struct
   *
   * @param {number} time
   * @param {number} offset
   * @returns {NgbDateStruct}
   */
  static getNgbDateStructFromUnix(time: number, offset = 0): NgbDateStruct {
    let momentDate = moment.unix(time).utcOffset(offset);
    return {
      year: momentDate.get('year'),
      month: momentDate.get('month') + 1,
      day: momentDate.get('date')
    }
  }

  /**
   * Convert unix timestamp to time struct
   *
   * @param {number} time
   * @param {number} offset
   * @returns {NgbTimeStruct}
   */
  static getNgbTimeStructFromUnix(time: number, offset = 0): NgbTimeStruct {
    let momentDate = moment.unix(time).utcOffset(offset);
    return {
      hour: momentDate.get('hour'),
      minute: momentDate.get('minute'),
      second: momentDate.get('second')
    }
  }

  /**
   * Get moment object from order
   *
   * @returns {moment.Moment}
   * @param info
   */
  static getMomentFromOrder(info: OrderInfo) {
    return moment(`${info.date}T${info.time}:00Z`);
  }
}
