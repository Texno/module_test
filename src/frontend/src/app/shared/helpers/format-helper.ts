
/**
 * Formatting helper
 */
export class FormatHelper {

  /**
   * Convert duration to text format
   *
   * @param value
   * @returns {string}
   */
  public static formatDuration(value): string {
    let numberValue = parseInt(value);
    if (numberValue >= 1440) {
      let add = numberValue % 1440;
      let addHours = (add / 60).toFixed(0);
      return (numberValue / 1440).toFixed(0) + ' días' + (add > 0 ? ', ' + addHours + ' horas' : '');
    } else if (numberValue >= 60) {
      let add = numberValue % 60;
      return (numberValue / 60).toFixed(0) + ' horas' + (add > 0 ? ', ' + add.toString() + ' minutos' : '');
    } else {
      return numberValue.toString() + ' minutos';
    }
  }
}
