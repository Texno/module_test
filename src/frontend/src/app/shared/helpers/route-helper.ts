import { City, Country } from '@shared/backend/data-types/geo.types';
import { Activity } from '@shared/backend/data-types/activity.types';
import { OrderInfo } from '@shared/backend/data-types/order.types';

/**
 * Route build helper class
 */
export class RouteHelper {

  /**
   * Create route string for city
   *
   * @param {City} city
   * @returns {string}
   */
  public static buildCityRoute(city: City): string {
    return '/es/' + city.country_route + '/' + city.route_name;
  }

  /**
   * Create route string for city
   *
   * @returns {string}
   * @param activity
   */
  public static buildCityRouteFromActivity(activity: Activity): string {
    return '/es/' + activity.country_route + '/' + activity.city_route;
  }

  /**
   * Create route string for country
   *
   * @param {Country} country
   * @returns {string}
   */
  public static buildCountryRoute(country: Country): string {
    return '/es/' + country.route_name;
  }

  /**
   * Create route string for country
   *
   * @returns {string}
   * @param city
   */
  public static buildCountryRouteFromCity(city: City): string {
    return '/es/' + city.country_route;
  }

  /**
   * Create route string for activity
   *
   * @param {Activity} activity
   * @returns {string}
   */
  public static buildActivityRoute(activity: Activity): string {
    return '/es/' + activity.country_route + '/' + activity.city_route + '/' + activity.route_name;
  }

  /**
   * Create route string for activity
   *
   * @returns {string}
   * @param info
   */
  public static buildActivityRouteFromOrderInfo(info: OrderInfo): string {
    return '/es/' + info.country_route + '/' + info.city_route + '/' + info.activity_route;
  }
}
