/**
 * Route build helper class
 */
import { Activity, ActivityFull } from '@shared/backend/data-types/activity.types';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import { OrderInfo } from '@shared/backend/data-types/order.types';
import { Injectable } from '@angular/core';

@Injectable()
export class GoogleTracking {

  static readonly SEARCH_CATEGORY = 'Search';
  static readonly DESTINATION_CAROUSEL_CATEGORY = 'Principales Destinos';
  static readonly TOP_NAVBAR_CATEGORY = 'TopNavBar';
  static readonly BOTTOM_NAVBAR_CATEGORY = 'BottomNavBar';
  static readonly FEATURED_ACTIVITIES_CATEGORY = 'Actividades más populares';
  static readonly FILTERS_CATEGORY = 'Filters';
  static readonly SECOND_NAV_MENU_CATEGORY = 'SecondNavMenu';
  static readonly ACTIVITY_BOOKING_CATEGORY = 'ActivityBooking';
  static readonly RELATED_ACTIVITY_CATEGORY = 'También te puede interesar';
  static readonly PAYMENT_OPTIONS_CATEGORY = 'Payment option';

  constructor() {}

  /**
   * Send event to google analytics
   *
   * @param {string} category
   * @param {string} action
   * @param {string} label
   */
  fireEvent(category: string, action: string, label: string) {
    if (window && window.hasOwnProperty('dataLayer')) {
      (<any>window).dataLayer.push({
        'event': 'analyticsEventWithInteraction',
        'event.category': category,
        'event.action': action,
        'event.label': label
      });
    }
  }

  /**
   * Fire impression for the city
   *
   * @param activities
   */
  fireProductImpression(activities: Activity[]) {
    if (window && window.hasOwnProperty('dataLayer') && activities.length) {

      const data = {
        'event' : 'product-impressions',
        'ecommerce': {
          'currencyCode': 'EUR',
          'impressions': []
        }
      };
      let index = 1;
      activities.forEach(activity => {
        data.ecommerce.impressions.push({
          name: activity.name,
          id: activity.id,
          price: activity.min_price,
          brand: activity.country_name,
          category: activity.city_name,
          list: `${activity.country_name} - ${activity.city_name}`,
          position: index++
        });
      });

      (<any>window).dataLayer.push(data);
    }
  }

  /**
   * Fire click for the activity
   *
   * @param {ActivityFull} activity
   */
  fireProductClick(activity: Activity) {
    if (window && window.hasOwnProperty('dataLayer')) {
      const data = {
        'event': 'productClick',
        'ecommerce': {
          'click': {
            'actionField': {'list': `${activity.country_name} - ${activity.city_name}`},
            'products': [{
              'name': activity.name,
              'id': activity.id,
              'brand': activity.country_name,
              'category': activity.city_name,
              'price': activity.min_price,
              'quantity': 1
            }]
          }
        }
      };

      (<any>window).dataLayer.push(data);
    }
  }

  /**
   * Product details page
   *
   * @param {Activity} activity
   */
  fireProductDetails(activity: ActivityFull) {
    if (window && window.hasOwnProperty('dataLayer')) {

      const data = {
        'event': 'detail',
        'ecommerce': {
          'detail': {
            'actionField': {'list': `${activity.country_name} - ${activity.city_name}`},
            'products': [{
              'name': activity.name,
              'id': activity.id,
              'brand': activity.country_name,
              'category': activity.city_name,
              'price': activity.min_price,
              'quantity': 1
            }]
          }
        }
      };

      (<any>window).dataLayer.push(data);
    }
  }

  /**
   * Product details page
   *
   * @param item
   */
  fireAddToCard(item: BasketItem) {
    if (window && window.hasOwnProperty('dataLayer')) {
      const data = {
        'event': 'addToCart',
        'ecommerce': {
          'currencyCode': 'EUR',
          'add': {
            'products': [{
              'name': item.activity.name,
              'id': item.activity.id,
              'price': item.getTicketData().total,
              'brand': item.activity.country_name,
              'category': item.activity.city_name,
              'variant': item.getTicketData().activityType,
              'quantity': 1
            }]
          }
        }
      };

      (<any>window).dataLayer.push(data);
    }
  }


  /**
   * Track checkout pages
   *
   * @param {number} step
   * @param {BasketItem[]} items
   */
  fireCheckoutStep(step: number, items: BasketItem[]) {
    if (window && window.hasOwnProperty('dataLayer') && items.length) {
      const data = {
        'event': 'checkout',
        'ecommerce': {
          'checkout': {
            'actionField': {'step': step},
            'products': []
          }
        }
      };

      items.forEach(item => {
        data.ecommerce.checkout.products.push({
          'name': item.activity.name,
          'id': item.activity.id,
          'price': item.getTicketData().total,
          'brand': item.activity.country_name,
          'category': item.activity.city_name,
          'variant': item.getTicketData().activityType,
          'quantity': 1
        });
      });

      (<any>window).dataLayer.push(data);
    }
  }

  /**
   * Track purchase event
   *
   * @param {OrderInfo} order
   */
  firePurchase(order: OrderInfo[]) {
    if (window && window.hasOwnProperty('dataLayer') && order.length) {
      let totalPrice = 0;
      const data = {
        'event': 'purchases',
        'ecommerce': {
          'purchase': {
            'actionField': {
              'id': order[0].code,
              'revenue': 0
            },
            'products': []
          }
        }
      };

      order.forEach(item => {
        let total = 0;
        item.prices.forEach(price => total += +price.price);
        totalPrice += +total;
        data.ecommerce.purchase.products.push({
          'name': item.activity_name,
          'id': item.activity,
          'price': total,
          'brand': item.country_name,
          'category': item.city_name,
          'variant': item.activity_type_name,
          'quantity': 1
        });
      });

      data.ecommerce.purchase.actionField.revenue = totalPrice;

      (<any>window).dataLayer.push(data);
    }
  }

}
