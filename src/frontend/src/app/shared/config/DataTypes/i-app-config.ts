export interface IAppConfig {
  env: string;
  backendUrl: string;
  googleReCaptcha: {
    publicKey: string
  }
  socialLogin: {
    facebook: string,
    google: string
  },
  maps: {
    google: string
  },
  stripe: {
    publicKey: string
  }

}