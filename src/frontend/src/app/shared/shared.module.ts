import { ModuleWithProviders, NgModule } from '@angular/core';

import { LayoutsModule } from './layouts/layouts.module';
import { SharedMetaModule } from './shared-meta';
import { TransferHttpModule } from './transfer-http';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { RestApiService } from '@shared/backend/rest-api.service';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@NgModule({
  exports: [
    LayoutsModule,
    BookingCalendarModule,
    SharedMetaModule,
    TransferHttpModule
  ],
  providers: [RestApiService],
  declarations: []
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [RestApiService, GoogleTracking]
    };
  }
}
