import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivityFull, ActivityOrder } from '@shared/backend/data-types/activity.types';
import { BasketService } from '@shared/basket/basket.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RelatedActivitiesComponent } from '@shared/booking-calendar/related-activities/related-activities.component';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';
import { PlatformLocation } from '@angular/common'

@Component({
  selector: 'app-calendar-slider',
  templateUrl: './calendar-slider.component.html',
  styleUrls: ['./calendar-slider.component.styl']
})
export class CalendarSliderComponent implements OnInit {

  isLoaded = false;
  isActive = false;

  private oldClass = '';  

  relatedModal: NgbModalRef;

  activityInfo: ActivityFull;

  constructor(private restService: RestApiService,
              private basket: BasketService,
              private modalService: NgbModal,
              private router: Router,
              private calendarService: BookingCalendarService,
              private ga: GoogleTracking,
              location: PlatformLocation) {
                location.onPopState(() => {
                  document.body.className = this.oldClass;
                  if(document.body.className.split(' ').includes('fixed')){
                    document.body.className = this.oldClass;
                  }
                });
               }

  ngOnInit() {
    this.oldClass = document.body.className;
    document.body.className = this.oldClass;
    this.calendarService.getActivityObservable().subscribe(activityId => {
      this.isLoaded = false;
      this.isActive = true;
      if(!document.body.className.split(' ').includes('fixed')){
          document.body.className += ' ' + 'fixed';
      }

      
      this.restService.getActivityFull(activityId)
        .then(activityFull => {
          this.activityInfo = activityFull;
          this.isLoaded = true;
        })
        .catch(err => {
          alert(err);
        });
    });
  }

  @ViewChild('hostTarget') hostTarget;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.hostTarget.nativeElement.contains(targetElement) && this.isActive) {
      // Clicked outside of slider
      if(!targetElement.hasAttribute('open-slider')) {
        // this is not opener
        this.isActive = false;
        document.body.className = this.oldClass;
        if(document.body.className.split(' ').includes('fixed')){
          document.body.className = this.oldClass;
        }
      }
    }
  }

  close() {
    this.isActive = false;
    document.body.className = this.oldClass;
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }

  /**
   * Perform add order operation
   * @param {ActivityOrder} order
   */
  book(order: ActivityOrder) {
    const item = this.basket.addItem(this.activityInfo, order);
    this.calendarService.placeOrder(order);
    this.isActive = false;
    document.body.className = this.oldClass;
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
    this.router.navigate(['/checkout']);
    this.ga.fireAddToCard(item);
  }


}
