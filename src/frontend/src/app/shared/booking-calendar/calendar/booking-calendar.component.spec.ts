import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingCalendarComponent } from './booking-calendar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';
import * as BookingEvent from '@shared/booking-calendar/booking-event-types';

class MockBookingCalendarService {
  public getDataForEvent(idEvent: number): Promise<BookingEvent.Data> {
    return new Promise<BookingEvent.Data>(resolve => resolve({
      tickets: [
        {
          code: "EV1-AD",
          name: "Adult with name",
          price: 38
        },
        {
          code: "EV2-AD",
          name: "Adult2 with name",
          price: 28
        },
      ],
      types: [
        {
          code: "1",
          name: "With guide"
        },
        {
          code: "2",
          name: "Without guide"
        }
      ],
      dates: {
        "all": [
          "10:30",
          "11:20",
          "13:45",
          "22:20",
          "23:00"
        ],
      }
    }));
  }
}

describe('BookingCalendarComponent', () => {
  let component: BookingCalendarComponent;
  let fixture: ComponentFixture<BookingCalendarComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ BookingCalendarComponent ],
      imports: [NgbModule.forRoot()],
      providers: [
        { provide: BookingCalendarService, useClass: MockBookingCalendarService }
      ]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init after event id specified', () => {
    expect(component.types).toEqual([]);
    component.status.subscribe(status => {
      expect(status).toBe(true);
      expect(component.types.length).toEqual(2);
    });
    component.eventId = 1;
  });

  it('should check order correctness', () => {
    expect(component).toBeTruthy();
    component.status.subscribe(() => {
      component.submit();
      expect(component.dateTooltip.isOpen()).toBe(true);
      expect(component.timeTooltip.isOpen()).toBe(true);
      expect(component.ticketsTooltip.isOpen()).toBe(true);

      component.selectDate({year: 2018, month: 4, day: 1});
      expect(component.dateTooltip.isOpen()).toBe(false);

      component.selectTime("10:30");
      expect(component.timeTooltip.isOpen()).toBe(false);

      component.addTicket("EV1-AD");
      expect(component.ticketsTooltip.isOpen()).toBe(false);
    });
    component.eventId = 1;
  });

  it('should correct fill the order dates and type', () => {
    expect(component).toBeTruthy();
    component.status.subscribe(() => {
      expect(component.order.date).toBe("");
      component.selectDate({year:2018, month: 4, day: 1});
      expect(component.order.date).toBe("2018-04-01");

      expect(component.order.time).toBe("");
      component.selectTime("20:10");
      expect(component.order.time).toBe("20:10");

      expect(component.order.type).toBe("1"); // type is preloaded
      component.selectType("2");
      expect(component.order.type).toBe("2");
    });
    component.eventId = 1;
  });

  it('should correct fill order tickets', () => {
    expect(component).toBeTruthy();
    component.status.subscribe(() => {
      expect(component.order.ticketCodes).toEqual({});

      component.addTicket("EV1-AD");
      expect(component.order.ticketCodes).toEqual({"EV1-AD": 1});

      component.addTicket("EV1-AD");
      expect(component.order.ticketCodes).toEqual({"EV1-AD": 2});

      component.addTicket("EV2-AD");
      expect(component.order.ticketCodes).toEqual({"EV1-AD": 2, "EV2-AD": 1});

      component.removeTicket("EV2-AD");
      expect(component.order.ticketCodes).toEqual({"EV1-AD": 2});

      expect(function() {
        component.removeTicket("EV2-AD");
      }).not.toThrow();
      expect(component.order.ticketCodes).toEqual({"EV1-AD": 2});

      expect(function() {
        component.addTicket("EV3-AD");
      }).toThrow();

      expect(function() {
        component.removeTicket("EV3-AD");
      }).toThrow();
    });
    component.eventId = 1;
  });

});
