import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgbAccordion, NgbDatepicker, NgbDateStruct, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs/Subject';
import {
  ActivityOrder,
  ActivitySchedule,
  ActivityTicket,
  ActivityType
} from '@shared/backend/data-types/activity.types';
import * as moment from 'moment';
import { DateHelper } from '@shared/helpers/date-helper';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-booking-calendar',
  templateUrl: './booking-calendar.component.html',
  styleUrls: ['./booking-calendar.component.styl']
})
export class BookingCalendarComponent implements OnInit {

  @Input() disableBook = false;
  @Input() schedule: ActivitySchedule;
  @Input() book_limit = 0;
  @Input() types: ActivityType[];
  @Output() onBooked = new EventEmitter<ActivityOrder>();
  bookCompleted = true;

  // Inner storage to speedup ticket data searching
  ticketHash:{[id: number]: ActivityTicket} = {};
  typesHash:{[id: number]: ActivityType} = {};
  objectKeys = Object.keys;

  tickets: ActivityTicket[] = [];

  selectedDate = '';
  order = new ActivityOrder();
  times: {time: number, name: string}[] = [];
  peopleHeader = 'Personas';
  isLoaded = false;
  inSummit = false;
  isOk = false;
  status = new Subject<boolean>();

  private oldClass = '';  

  @ViewChild('datePicker') datePicker: NgbDatepicker;
  @ViewChild('typesAcc') typesAcc: NgbAccordion;
  @ViewChild('dateAcc') dateAcc: NgbAccordion;

  @ViewChild('dateTooltip') dateTooltip: NgbTooltip;
  @ViewChild('timeTooltip') timeTooltip: NgbTooltip;
  @ViewChild('ticketsTooltip') ticketsTooltip: NgbTooltip;


  /**
   * Constructor
   */
  constructor(private ga: GoogleTracking) {}

  /**
   * Before init hook
   */
  ngOnInit() {
    if(this.types.length > 0) {
      this.order.type = this.types[0].id;
      this.tickets = this.types[0].tickets;
      this.types.forEach(type => this.typesHash[type.id] = type);
      let selectedTicket = null;
      this.tickets.forEach(ticket => {
        if (!selectedTicket) { selectedTicket = ticket; }
        if ('Adulto' === ticket.name) {
          selectedTicket = ticket;
        }
        this.ticketHash[ticket.id] = ticket;
      });
      if (selectedTicket) { this.addTicket(selectedTicket); }
    }
    this.bookCompleted = this.order.isValid();
  }

  /**
   * Add time to order
   * @param time
   */
  selectTime(time) {
    this.order.time = time.time;
    this.timeTooltip.close();
    this.bookCompleted = this.order.isValid();
    this.ga.fireEvent(GoogleTracking.ACTIVITY_BOOKING_CATEGORY, 'Hora', time.name);
  }

  /**
   * Select type of order
   * @param {string} type
   */
  selectType(type: ActivityType) {
    this.order.type = type.id;
    this.tickets = type.tickets;
    this.tickets.forEach(ticket => this.ticketHash[ticket.id] = ticket);
    this.typesAcc.toggle('types-panel');
    this.bookCompleted = this.order.isValid();
    this.ga.fireEvent(GoogleTracking.ACTIVITY_BOOKING_CATEGORY, 'Tipo de actividad', type.name);
  }

  /**
   * Add selected ticket to order
   * @param ticket
   */
  addTicket(ticket: ActivityTicket) {
    if (!(ticket.id in this.ticketHash)) {
      throw new Error('Código de comprobante incorrecto: ' + ticket.id);
    }
    if (!(ticket.id in this.order.tickets)) {
      this.order.tickets[ticket.id] = 0;
    }
    ++this.order.tickets[ticket.id];
    this.buildPeopleHeader();
    let message = ticket.name + '-' + this.order.tickets[ticket.id];
    this.bookCompleted = this.order.isValid();
    this.ga.fireEvent(GoogleTracking.ACTIVITY_BOOKING_CATEGORY, 'People', message);
  }

  /**
   * remove ticket from order
   * @param ticket
   */
  removeTicket(ticket: ActivityTicket) {
    if (!(ticket.id in this.ticketHash)) {
      throw new Error('Código de comprobante incorrecto: ' + ticket.id);
    }
    if (ticket.id in this.order.tickets) {
      if (this.order.tickets[ticket.id] <= 1) {
        delete this.order.tickets[ticket.id];
      } else {
        --this.order.tickets[ticket.id];
      }
    }
    this.buildPeopleHeader();
    let message = ticket.name + '-' + this.order.tickets[ticket.id];
    this.bookCompleted = this.order.isValid();
    this.ga.fireEvent(GoogleTracking.ACTIVITY_BOOKING_CATEGORY, 'People', message);
  }

  /**
   * Add date to order
   * @param {NgbDateStruct} date
   */
  selectDate(date: NgbDateStruct) {
    this.times = [];
    this.order.time = 0;
    this.selectedDate = DateHelper.formatDate(date);
    const calendar = DateHelper.parseNgbDateStruct(date, this.schedule.offset);
    let now = moment().utcOffset(this.schedule.offset);
    this.schedule.time.forEach(time => {
      const schedule = moment.unix(time).utcOffset(this.schedule.offset);
      let bookLimit = schedule.clone().subtract(this.book_limit, 'minute');
      if (calendar.isSame(schedule, 'day') && now.isBefore(bookLimit)) {
        this.times.push({
          time: time,
          name: schedule.format('HH:mm')
        });
      }
    });
    if(this.times.length == 1) {
      this.order.time = this.times[0].time;
    }
    this.dateAcc.toggle('date-panel');
    this.dateTooltip.close();
    this.bookCompleted = this.order.isValid();
    this.ga.fireEvent(GoogleTracking.ACTIVITY_BOOKING_CATEGORY, 'Fecha', this.selectedDate);
  }

  /**
   * Calculate total of order
   * @returns {number}
   */
  getTotal():number {
    let total = 0;
    for (let id in this.order.tickets) {
      total += this.ticketHash[id].price * this.order.tickets[id];
    }
    return total;
  }

  /**
   * check all data and submit
   */
  submit() {
    if(!this.order.time) {
      this.timeTooltip.open();
    }
    if(Object.keys(this.order.tickets).length == 0) {
      this.ticketsTooltip.open();
    }
    if (!this.order.isValid()) {
      return;
    }
    this.onBooked.emit(this.order);
    document.body.className = this.oldClass;
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }

  /**
   * Call back for date check
   * @param {NgbDateStruct} date
   * @returns {boolean}
   */
  isDisabled = (date: NgbDateStruct) => {
    if (null === this.schedule.time) {
      return true; // No available dates
    }
    let calendar = DateHelper.parseNgbDateStruct(date, this.schedule.offset);
    let now = moment().utcOffset(this.schedule.offset);
    if (calendar.isBefore(now, 'day')) {
      return true;
    }

    for (let index = 0; index < this.schedule.time.length; index++) {
      let schedule = moment.unix(this.schedule.time[index]).utcOffset(this.schedule.offset);
      let bookLimit = schedule.clone().subtract(this.book_limit, 'minute');
      if(calendar.isSame(schedule, 'day') && now.isBefore(bookLimit)) {
        return false; // Switch on included day
      }
    }
    return true;
  };

  /**
   * Rebuild header after ticket selection
   */
  private buildPeopleHeader() {
    if (!Object.keys(this.order.tickets).length) {
      this.peopleHeader = 'Personas';
      return;
    }
    let headerArray = [];
    for (let id in this.order.tickets) {
      headerArray.push(this.order.tickets[id] + ' ' + this.ticketHash[id].name);
    }
    this.peopleHeader = headerArray.join(', ');
    this.ticketsTooltip.close();
  }
}
