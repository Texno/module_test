import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedActivitiesComponent } from './related-activities.component';

describe('RelatedActivitiesComponent', () => {
  let component: RelatedActivitiesComponent;
  let fixture: ComponentFixture<RelatedActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
