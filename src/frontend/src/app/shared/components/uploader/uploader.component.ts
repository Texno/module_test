import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpEventType } from '@angular/common/http';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.styl']
})
export class UploaderComponent implements OnInit {

  public static readonly CITY_AVATAR_RATE = { width: 265, height: 350 };
  public static readonly ACTIVITY_AVATAR_RATE = { width: 265, height: 250 };

  private static MAX_DIMENSION_ERROR = 30; //Percent

  error: string = null;
  isUploading = false;
  uploadProgress = 0;
  uploadError = '';

  @Input() recomended: Dimension;
  @Output() imageUploaded = new EventEmitter<UploadResult>();

  constructor(private rest: RestApiService) { }

  ngOnInit() {
  }

  /**
   * Handle upload image input
   * @param {FileList} files
   */
  async handleFileInput(files: FileList) {
    let fileItem = files.item(0);
    const dimensionError = await this.checkImageDimensions(fileItem);
    if(!dimensionError) {
      this.uploadError = 'Please upload image with similar dimensions';
      return;
    }
    this.isUploading = true;
    this.uploadError = '';
    this.rest.uploadImage(fileItem).subscribe(
      event => this.handleProgress(event),
      err => {
        this.isUploading = false;
        this.uploadError = err.error.message;
      }
    );
  }

  /**
   * Check parameter
   *
   * @param {File} file
   * @returns {Promise<boolean>}
   */
  private checkImageDimensions(file: File): Promise<boolean> {
    return new Promise<boolean>(resolve => {
      if(!this.recomended) {
        resolve(true);
      }
      const fr = new FileReader;
      fr.onload = () => {
        let img = new Image;
        img.onload = () => {
          if (this.recomended) {
            const requiredRate = this.recomended.width / this.recomended.height;
            const targetRate = img.width / img.height;
            const diff = Math.abs(requiredRate - targetRate);
            const proportion_check = diff / requiredRate * 100 <= UploaderComponent.MAX_DIMENSION_ERROR;
            const width_check = Math.abs(img.width - this.recomended.width) / this.recomended.width * 100 <= UploaderComponent.MAX_DIMENSION_ERROR;
            const height_check = Math.abs(img.height - this.recomended.height) / this.recomended.height * 100 <= UploaderComponent.MAX_DIMENSION_ERROR;
            resolve( proportion_check && width_check && height_check );
          } else {
            resolve(true);
          }
        };
        img.src = fr.result;
      };
      fr.readAsDataURL(file);
    });
  }

  /**
   * Handle progress of image loading
   * @param event
   */
  handleProgress(event: any) {
    if (event.type === HttpEventType.UploadProgress) {
      this.uploadProgress = Math.round(100 * event.loaded / event.total);
    }

    if (event.type === HttpEventType.Response) {
      this.isUploading = false;
      this.imageUploaded.next(event.body);
    }
  }
}

interface Dimension {
  width:  number;
  height: number;
}
