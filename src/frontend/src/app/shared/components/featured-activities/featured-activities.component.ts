import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Activity } from '@shared/backend/data-types/activity.types';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';

@Component({
  selector: 'app-featured-activities',
  templateUrl: './featured-activities.component.html',
  styleUrls: ['./featured-activities.component.styl']
})
export class FeaturedActivitiesComponent implements OnInit {

  private readonly INITIAL_LIMIT = 6;   //How much to show in first
  private readonly ADD_LIMIT = 3;       //How much to add on show more

  private offset = 0;                   // start from beginning
  thatsAll = false;                     // Indicate that server hasn`t more activities to show
  private country = null;

  featuredActivities: Activity[] = [];
  @Input() set countryCode(code: string) {
    this.country = code;
    this.ngOnInit();
  }

  @Output() onFeatureCalendarClicked = new  EventEmitter<number>();

  constructor(private rest: RestApiService, private router: Router, private ga: GoogleTracking) { }

  /**
   * Initialize activities list
   */
  ngOnInit() {
    this.offset = 0;
    this.thatsAll = false;
    this.rest.getFeaturedActivities(this.INITIAL_LIMIT, this.offset, this.country)
      .then(activities => {
        this.featuredActivities = activities;
        this.thatsAll = (activities.length < this.INITIAL_LIMIT);
      });
    this.offset += this.INITIAL_LIMIT;
  }

  /**
   * Get more activities
   */
  async showMore() {
    if (this.thatsAll) {
      return;  // Nothing to show more
    }
    const moreActivities = this.rest.getFeaturedActivities(this.ADD_LIMIT, this.offset, this.country)
      .then(activities => {
        this.featuredActivities = this.featuredActivities.concat(activities);
        this.thatsAll = (activities.length < this.ADD_LIMIT);
      });
    this.rest.getFeaturedActivities(this.ADD_LIMIT, this.offset += this.ADD_LIMIT, this.country)
      .then(async activities => {
        await moreActivities; // Wait while more activities is being loaded to remove race condition
        this.thatsAll = !activities.length;
      });
    this.offset += this.ADD_LIMIT;
  }

  fireEvent(name: string) {
    this.ga.fireEvent(GoogleTracking.FEATURED_ACTIVITIES_CATEGORY, name, this.router.url);
  }
}
