import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-iframe-modal',
  templateUrl: './iframe-modal.component.html'
})
export class IframeModalComponent {

  @Input() set url(unsafeUrl: string) {
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(unsafeUrl);
  }

  urlSafe: SafeResourceUrl = null;

  constructor(private sanitizer: DomSanitizer) { }

}
