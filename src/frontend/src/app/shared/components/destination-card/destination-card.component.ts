import { Component, Input, OnInit } from '@angular/core';
import { DestinationCity } from '@shared/backend/data-types/destination.types';

@Component({
  selector: 'app-destination-card',
  templateUrl: './destination-card.component.html',
  styleUrls: ['./destination-card.component.styl']
})
export class DestinationCardComponent implements OnInit {

  @Input() destination: DestinationCity;
  @Input() isWide = false;

  constructor() {
  }

  ngOnInit() {
  }
}
