import { Component, forwardRef, HostListener, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Country } from '@shared/backend/data-types/geo.types';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-phone-selector-new',
  templateUrl: './phone-selector-new.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneSelectorNewComponent),
      multi: true
    }
  ],
  exportAs: 'ngModel',
  styleUrls: ['./phone-selector-new.component.styl']
})
export class PhoneSelectorNewComponent implements ControlValueAccessor, OnInit {

  private static readonly DEFAULT_COUNTRY_CODE = 68;

  countriesSource: Country[] = [];
  countriesDisplay: Country[] = [];
  openSelect = false;

  selectSearch = new Subject<string>();
  phone = '';
  selectedCountry: Country = null;
  number: string = '';

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!targetElement.hasAttribute('select-open')) {
      this.openSelect = false;
    }
  }

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.selectSearch.asObservable().debounceTime(200).distinctUntilChanged().subscribe(searchText => {
        this.countriesDisplay = this.countriesSource.filter(value => {
          const callingCode = '+' + value.calling_code;
          return value.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1
            || callingCode.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
        });
    });
  }

  // Function to call when the phone changes.
  onChange = (phone: string) => {};

  // Function to call when the input is touched
  onTouched = () => {};

  /**
   * Initialize control with data
   *
   * @param {string} value
   */
  writeValue(value: string) {
    this.phone = value;
    this.rest.getCountryList().then(countries => {
      this.countriesSource = countries;
      this.countriesDisplay = countries;

      // prefill selected code
      const pureValue = this.phone.replace(/\+/g, '');
      countries.sort((a, b) =>  b.calling_code.length - a.calling_code.length);
      for (let index = 0; index < countries.length; index ++) {
        if (null !== this.phone && this.phone.length) {
          if (0 === pureValue.indexOf(countries[index].calling_code)) {
            this.selectedCountry = countries[index];
            this.number = pureValue.slice(countries[index].calling_code.length);
            break;
          }
        } else {
          if (PhoneSelectorNewComponent.DEFAULT_COUNTRY_CODE === countries[index].id) {
            this.selectedCountry = countries[index];
            break;
          }
        }
      }
    });
  }

  /**
   * Handler to update parent value
   */
  updateValue = () => {
    if (null == this.number) this.number = '';
    this.phone = '+' + this.selectedCountry.calling_code + this.number;
    this.onChange(this.phone);
  };

  /**
   * Register change function
   * @param fn
   */
  registerOnChange(fn) {
    this.onChange = fn;
  }

  /**
   * Register blur function
   * @param fn
   */
  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  /**
   * Select country from list
   * @param country
   */
  selectCountry(country: Country) {
    this.selectedCountry = country;
    this.openSelect = false;
  }

}
