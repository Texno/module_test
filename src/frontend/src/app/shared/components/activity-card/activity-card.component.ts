import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Activity } from '@shared/backend/data-types/activity.types';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import * as moment from 'moment';
import { FormatHelper } from '@shared/helpers/format-helper';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-activity-card',
  templateUrl: './activity-card.component.html',
  styleUrls: ['./activity-card.component.styl']
})
export class ActivityCardComponent implements OnInit {

  @Input() activity: Activity;
  @Input() isHorizontal = false;
  @Input() item: BasketItem = null;
  @Input() showDelete = false;

  @Output() onDeleteClicked = new EventEmitter<boolean>();

  orderDate = null;

  formatHelper = FormatHelper;
  routeHelper = RouteHelper;

  constructor(private calendarService: BookingCalendarService) { }

  ngOnInit() {
    if(this.item) {
      this.orderDate = moment.unix(this.item.order.time).utcOffset(this.item.activity.schedule.offset);
    }
  }

  /**
   * Emit new clicked event
   */
  click() {
    this.calendarService.openActivity(this.activity.id);
  }

}
