import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { City } from '@shared/backend/data-types/geo.types';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-city-search-bar',
  templateUrl: './city-search-bar.component.html',
  styleUrls: ['./city-search-bar.component.styl']
})
export class CitySearchBarComponent implements OnInit {

  @Output() onCitySelected = new  EventEmitter<City>();
  @Input() selectedCity: number;

  selectedName: string;
  isOpened = false;
  citiesSource: City[] = [];
  cities: City[] = [];

  constructor(private rest: RestApiService) { }

  ngOnInit() {

  }

  @Input() set selectedCountry(idCountry: any) {
    if (idCountry) {
      this.selectedName = '';
      this.rest.getDestinationCityList(idCountry).then(cities => {
        this.cities = cities.slice(0, 10);
        this.citiesSource = cities;
        for (let i=0; i < cities.length; i++) {
          if (this.selectedCity === cities[i].id) {
            this.selectedName = cities[i].name;
            break;
          }
        }
      });
    }
  }

  public selectCity(city: City) {
    this.isOpened = false;
    this.selectedCity = city.id;
    this.selectedName = city.name;
    this.onCitySelected.next(city);
  }

  public searchCity(event: any) {
    const searchText = event.target.value;
    if (searchText) {
      this.cities = this.citiesSource.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ).slice(0, 10);
    } else {
      this.cities = this.citiesSource.slice(0, 10)
    }
  }

}
