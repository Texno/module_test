import { NouiFormatter } from 'ng2-nouislider';
import { ActivityFilter } from '@shared/components/data-types/filter.types';

export class PriceFormatter implements NouiFormatter {
  to(value: number): string {
    if (0 == value) {
      return 'gratis';
    }
    return '€ ' +  value.toFixed();
  }

  from(value: string): number {
    if (-1 !== value.indexOf('gratis')) {
      return 0;
    }
    return Number(value.split(" ")[1]);
  }
}

export class NumberFormatter implements NouiFormatter {
  to(value: number): string {
    return value.toFixed();
  }

  from(value: string): number {
    return Number(value);
  }
}

export class DurationFormatter implements NouiFormatter {

  static readonly ALL_NAME = 'todo';

  to(value: number): string {
    if (value == ActivityFilter.MAX_DURATION) {
      return DurationFormatter.ALL_NAME;
    }
    return value.toFixed() + ' horas';
  }

  from(value: string): number {
    if (value == DurationFormatter.ALL_NAME) {
      return ActivityFilter.MAX_DURATION;
    }
    return Number(value.split(" ")[0]);
  }
}