import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Category } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivityFilter, AjaxActivityFilter, FastActivityFilter, timeTypes } from '../data-types/filter.types';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { DurationFormatter, PriceFormatter } from '@shared/components/activity-filter/tooltip.formatter';
import { NgbDatepicker, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateHelper } from '@shared/helpers/date-helper';
import { CityFilter } from '@shared/backend/data-types/geo.types';
import { ActivatedRoute, Router } from '@angular/router';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-activity-filter',
  templateUrl: './activity-filter.component.html',
  styleUrls: ['./activity-filter.component.styl']
})
export class ActivityFilterComponent implements OnInit {

  categories: Promise<Category[]>;

  clearFlag = false;
  timeTypes = timeTypes;
  filter = new ActivityFilter();
  prices = [this.filter.minPrice, this.filter.maxPrice];
  durations = [this.filter.minDuration, this.filter.maxDuration];

  dateHelper = DateHelper;
  type: string;

  @ViewChild('datePicker') datePicker: NgbDatepicker;

  @Input() offset = 0;
  @Input() hideType = false;
  @Input() citiesFilter: CityFilter[] = [];

  @Input() set selectedCategories(categories: number[]) {
    this.filter.categories = categories;
  }

  priceSliderConfig = {
    connect:  true,
    range: {
      min:      this.filter.minPrice,
      max:      this.filter.maxPrice,
    },
    step:     1,
    tooltips: [
      new PriceFormatter(),
      new PriceFormatter()
    ]
  };

  durationSliderConfig = {
    connect:  true,
    range: {
      min:      this.filter.minDuration,
      max:      this.filter.maxDuration,
    },
    step:     1,
    tooltips: [
      new DurationFormatter(),
      new DurationFormatter()
    ]
  };

  private fastFilter = new Subject<FastActivityFilter>();
  private ajaxFilter = new Subject<AjaxActivityFilter>();

  @Output() onFastFilter: Observable<FastActivityFilter>;
  @Output() onAjaxFilter: Observable<AjaxActivityFilter>;
  @Output() onViewActivitiesClick = new EventEmitter<boolean>();

  constructor(private rest: RestApiService, private router: Router, private route: ActivatedRoute, private ga: GoogleTracking) {
    // Don`t react until 300ms are passed
    this.onFastFilter = this.fastFilter.asObservable().debounceTime(300);
    this.onAjaxFilter = this.ajaxFilter.asObservable().debounceTime(500);
  }

  ngOnInit() {
    this.categories = this.rest.getActivityCategoriesList().then(categories => {
      const type = this.route.snapshot.queryParamMap.get('type');
      for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        if (category.name === type) {
          this.filter.categories = [category.id];
          this.updateFilter();
          break;
        }
      }
      return categories;
    });
    this.updateFilter();
  }

  /**
   * callback for category select
   * @param {number} id
   */
  selectCategory(id: number) {
    if (this.filter.categories.includes(id)) {
      this.filter.categories = this.filter.categories.filter(item => item != id)
    } else {
      this.filter.categories.push(id);
    }
    this.fireCategoryEvent(id);
    this.updateFilter();
  }

  async fireCategoryEvent(id: number) {
    let categories = await this.categories;
    for (let index = 0; index < categories.length; index ++) {
      if (id == categories[index].id) {
        this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, 'Tipo de Actividad', categories[index].name);
        break;
      }
    }
  }

  /**
   * callback for city select
   * @param {number} id
   */
  selectCity(id: number) {
    if (this.filter.cities.includes(id)) {
      const index = this.filter.cities.indexOf(id);
      this.filter.cities.splice(index, 1);
    } else {
      this.filter.cities.push(id);
    }
    this.updateFilter();
  }

  /**
   * callback for time type select
   * @param {number} id
   */
  selectTimeType(id: number) {
    if (this.filter.timeTypes.includes(id)) {
      this.filter.timeTypes = this.filter.timeTypes.filter(item => item != id)
    } else {
      this.filter.timeTypes.push(id);
    }
    this.updateFilter(true);
  }

  /**
   * callback for time select
   * @param date
   */
  selectDate(date) {
    this.filter.dates = [DateHelper.formatDate(date)];
    this.clearFlag = false;
    this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, this.filter.dates.join(', '), this.router.url);
    this.updateFilter(true);
  }

  /**
   * callback for time filter reset
   */
  resetTimeFilter() {
    this.filter.timeTypes = [];
    this.filter.dates = [];
    this.clearFlag = true;
    this.updateFilter(true)
  }

  /**
   * Callback for rate change
   * @param {number} value
   */
  changeRate(value: number) {
    this.filter.rate = value;
    this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, 'Mejores valoradas', value.toString());
    this.updateFilter();
  }

  /**
   * callback for rate reset
   */
  resetRateFilter() {
    this.filter.rate = 0;
    this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, 'Mejores valoradas', '0');
    this.updateFilter();
  }

  /**
   * callback for duration change
   */
  changeDuration() {
    this.filter.minDuration = this.durations[0];
    this.filter.maxDuration = this.durations[1];
    this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, 'Duración', `${this.durations[0]} - ${this.durations[1]}`);
    this.updateFilter();
  }

  /**
   * callback for price change
   */
  changePrice() {
    this.filter.minPrice = this.prices[0];
    this.filter.maxPrice = this.prices[1];
    this.ga.fireEvent(GoogleTracking.FILTERS_CATEGORY, 'Precio', `${this.prices[0]} - ${this.prices[1]}`);
    this.updateFilter();
  }

  /**
   * Update corresponding filter and emit event
   * @param {boolean} isAjax
   */
  updateFilter(isAjax = false) {
    if (isAjax) {
      this.ajaxFilter.next(AjaxActivityFilter.fromActivityFilter(this.filter, this.offset));
    } else {
      this.fastFilter.next(FastActivityFilter.fromActivityFilter(this.filter));
    }
  }

  /**
   * Call back for date check
   * @param {NgbDateStruct} date
   * @returns {boolean}
   */
  isDisabled = (date: NgbDateStruct) => {
    const now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    now.setMilliseconds(0);
    const cal = new Date(date.year, date.month - 1, date.day);
    return now > cal;
  };




}
