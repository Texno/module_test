import * as moment from 'moment';

export class ActivityFilter {

  static readonly MAX_DURATION = 48;

  constructor(
    public dates: string[] = [],
    public timeTypes: number[] = [],
    public categories: number[] = [],
    public cities: number[] = [],
    public rate = 0,
    public minPrice = 0,
    public maxPrice = 1000,
    public minDuration = 0,
    public maxDuration = ActivityFilter.MAX_DURATION
  ) {}
}

export class FastActivityFilter {
  static readonly MAX_DURATION = -1;

  constructor(
    public categories: number[] = [],
    public cities: number[] = [],
    public rate = 0,
    public minPrice = 0,
    public maxPrice = 200,
    public minDuration = 0,
    public maxDuration = 12
  ) {}

  static fromActivityFilter(filter: ActivityFilter): FastActivityFilter {
    return new FastActivityFilter(
      filter.categories,
      filter.cities,
      filter.rate,
      filter.minPrice,
      filter.maxPrice,
      filter.minDuration * 60,
      (filter.maxDuration == ActivityFilter.MAX_DURATION) ? FastActivityFilter.MAX_DURATION : filter.maxDuration * 60
    );
  }
}

export class AjaxActivityFilter {
  constructor(
    public dates: {start: number, end: number}[] = [],
    public times: {start: number, end: number}[] = []
  ) {}

  /**
   * Build ajax interval filter from full activity filter
   *
   * @param {ActivityFilter} filter
   * @param {number} offset
   * @returns {AjaxActivityFilter}
   */
  static fromActivityFilter(filter: ActivityFilter, offset: number = 0): AjaxActivityFilter {
    let dates = [];
    let times = [];
    filter.dates.forEach(date => {
      const offsetStr = (offset < 0 ? '-' : '+') + this.pad(offset, 2) + ':00';
      let momentDate = moment.parseZone(date + ' ' + offsetStr, 'YYYY-MM-DD ZZ').utc();
      dates.push({
        start: momentDate.unix(),
        end:   momentDate.add(1, 'd').unix()
      });
    });
    filter.timeTypes.forEach(timeTypeId => {
      let timeType = timeTypes[timeTypeId];
      times.push({
        start:  this.addHours(timeType.from, -offset),
        end:    this.addHours(timeType.to, -offset)
      })
    });
    return new AjaxActivityFilter(dates, times);
  }

  /**
   * Performs hours operations in range 0-24
   *
   * @param {number} from
   * @param {number} add
   * @returns {number}
   */
  private static addHours(from: number, add: number): number {
    let result = from + add;
    if (result >= 24) result -= 24;
    if (result < 0) result += 24;
    return result;
  }

  /**
   * Add zero num padding
   * @param {number} num
   * @param {number} size
   * @returns {string}
   */
  private static pad(num: number, size: number):string {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
  }
}

export interface TimeType {
  id:   number;
  name: string;
  from: number;
  to:   number;
}

export const timeTypes: TimeType[] = [
  {
    id:   0,
    name: "Noche",
    from: 0,
    to:   6,
  },
  {
    id:   1,
    name: "Mañana",
    from: 6,
    to:   12,
  },
  {
    id:   2,
    name: "Tarde",
    from: 12,
    to:   18,
  },
  {
    id:   3,
    name: "Noche",
    from: 18,
    to:   0,
  },
];