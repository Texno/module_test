import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivityCardComponent } from '@shared/components/activity-card/activity-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FeaturedActivitiesComponent } from '@shared/components/featured-activities/featured-activities.component';
import { DestinationCardComponent } from '@shared/components/destination-card/destination-card.component';
import { DestinationCarouselComponent } from '@shared/components/destination-carousel/destination-carousel.component';
import { RouterModule } from '@angular/router';
import { CategoryFilterComponent } from './category-filter/category-filter.component';
import { NouisliderModule } from 'ng2-nouislider';
import { FormsModule } from '@angular/forms';
import { ActivityFilterComponent } from './activity-filter/activity-filter.component';
import { CountrySearchBarComponent } from '@shared/components/country-search-bar/country-search-bar.component';
import { CitySearchBarComponent } from './city-search-bar/city-search-bar.component';
import { KeepHtmlPipe } from './pipes/keep-html.pipe';
import { UploaderComponent } from './uploader/uploader.component';
import { ReviewCardComponent } from './review-card/review-card.component';
import { OverlaySpinnerComponent } from './overlay-spinner/overlay-spinner.component';
import { EditReviewComponent } from './edit-review/edit-review.component';
import { SearchFilterComponent } from '@shared/components/search-filter/search-filter.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { PhoneSelectorComponent } from './phone-selector/phone-selector.component';
import { CountryCityNavigationComponent } from './country-city-navigation/country-city-navigation.component';
import { FileUploaderComponent } from '@shared/components/file-uploader/file-uploader.component';
import { FileDropModule } from 'ngx-file-drop';
import { PhoneSelectorNewComponent } from './phone-selector-new/phone-selector-new.component';
import { IframeModalComponent } from './iframe-modal/iframe-modal.component';
import { KeepUrlPipe } from '@shared/components/pipes/keep-url.pipe';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    FormsModule,
    NouisliderModule,
    FileDropModule
  ],
  declarations: [
    ActivityCardComponent,
    ActivityFilterComponent,
    FeaturedActivitiesComponent,
    DestinationCardComponent,
    DestinationCarouselComponent,
    CategoryFilterComponent,
    CountrySearchBarComponent,
    CitySearchBarComponent,
    KeepHtmlPipe,
    KeepUrlPipe,
    UploaderComponent,
    ReviewCardComponent,
    OverlaySpinnerComponent,
    EditReviewComponent,
    SearchFilterComponent,
    SearchInputComponent,
    PhoneSelectorComponent,
    CountryCityNavigationComponent,
    FileUploaderComponent,
    PhoneSelectorNewComponent,
    IframeModalComponent,
  ],
  entryComponents: [IframeModalComponent],
  exports: [
    ActivityCardComponent,
    ActivityFilterComponent,
    FeaturedActivitiesComponent,
    DestinationCardComponent,
    DestinationCarouselComponent,
    CategoryFilterComponent,
    CountrySearchBarComponent,
    CitySearchBarComponent,
    KeepHtmlPipe,
    KeepUrlPipe,
    UploaderComponent,
    ReviewCardComponent,
    OverlaySpinnerComponent,
    EditReviewComponent,
    SearchFilterComponent,
    SearchInputComponent,
    PhoneSelectorComponent,
    CountryCityNavigationComponent,
    FileUploaderComponent,
    PhoneSelectorNewComponent,
  ]
})
export class ComponentsModule { }
