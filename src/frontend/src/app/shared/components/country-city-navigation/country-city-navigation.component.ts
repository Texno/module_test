import { AfterViewInit, Component, HostListener, Input, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { Router } from '@angular/router';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-country-city-navigation',
  templateUrl: './country-city-navigation.component.html',
  styleUrls: ['./country-city-navigation.component.styl'],
  host: {'class': 'container container--country-hero'}
})
export class CountryCityNavigationComponent implements AfterViewInit {

  @Input() selectedCountryId: number;
  @Input() selectedCityId: number;

  selectedCountryName = 'Seleccionar país';
  isCountryOpened = false;
  countriesSource: Country[] = [];
  countries: Country[] = [];

  selectedCityName = 'Seleccionar ciudad';
  isCityOpened = false;
  citiesSource: City[] = [];
  cities: City[] = [];

  constructor(private rest: RestApiService, private router: Router) { }

  @ViewChild('countryHost') countryHost;
  @ViewChild('cityHost') cityHost;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.countryHost.nativeElement.contains(targetElement)) {
      this.isCountryOpened = false;
    }
    if(!this.cityHost.nativeElement.contains(targetElement)) {
      this.isCityOpened = false;
    }
  }

  ngAfterViewInit() {
    this.rest.getDestinationCountryList().then(countries => {
      this.countries = countries.slice(0, 10);
      this.countriesSource = countries;
      for (let i=0; i < countries.length; i++) {
        if ((this.selectedCountryId === countries[i].id)) {
          this.selectedCountryName = countries[i].name;
          break;
        }
      }
    });


    if (this.selectedCountryId) {
      this.rest.getDestinationCityList(this.selectedCountryId).then(cities => {
        this.cities = cities.slice(0, 10);
        this.citiesSource = cities;
        for (let i=0; i < cities.length; i++) {
          if (this.selectedCityId === cities[i].id) {
            this.selectedCityName = cities[i].name;
            break;
          }
        }
      });
    }

  }

  /**
   * Search country by entered text
   * @param event
   */
  public searchCountry(event: any) {
    const searchText = event.target.value;
    if (searchText) {
      this.countries = this.countriesSource.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ).slice(0, 10);
    } else {
      this.countries = this.countriesSource.slice(0, 10)
    }
  }

  /**
   * Search city by text
   *
   * @param event
   */
  public searchCity(event: any) {
    const searchText = event.target.value;
    if (searchText) {
      this.cities = this.citiesSource.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ).slice(0, 10);
    } else {
      this.cities = this.citiesSource.slice(0, 10)
    }
  }


  /**
   * Redirect to country page
   * @param {Country} country
   */
  selectCountry(country: Country) {
    this.router.navigate([RouteHelper.buildCountryRoute(country)]);
  }

  /**
   * Redirect to city page
   * @param {City} city
   */
  selectCity(city: City) {
    this.router.navigate([RouteHelper.buildCityRoute(city)]);
  }

}
