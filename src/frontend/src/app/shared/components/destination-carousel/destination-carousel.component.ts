import { Component, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-destination-carousel',
  templateUrl: './destination-carousel.component.html',
  styleUrls: ['./destination-carousel.component.styl']
})
export class DestinationCarouselComponent implements OnInit {

  private static readonly CARD_WIDTH = 285;
  private static readonly CARD_OVERRIDE = 2;
  private static readonly DRAG_MIN_LIMIT = 5; // px

  @Input() showAllButton = true;
  @Input() header = 'Principales destinos';
  @Input() headerClass = 'destination__header';
  @Input() wideCards = false;
  @Input() set destinations(cities: DestinationCity[]) {
    if (cities) {
      this._destinations = cities;
      this.destinationClicked = new Array(cities.length).fill(false);
      window.setTimeout(this.rebuildScroller.bind(this), 500);
    }
  }

  _destinations = [];
  destinationClicked = [];

  private isAnimating = false;
  private isMoved = false;

  constructor(private router: Router, private ga: GoogleTracking) { }

  @ViewChild('scroll') scroll;

  private dragX = 0;
  private dragCardObject: DestinationCity = null;
  private dragged = false;

  @HostListener('document:mouseup') onMouseUp () {
    this.dragged = false;
    this.rebuildScroller();
  }

  ngOnInit() {

  }

  moveLeft() {
    this.ga.fireEvent(GoogleTracking.DESTINATION_CAROUSEL_CATEGORY, 'Carousel', 'left-button');
    const start = this.scroll.nativeElement.scrollLeft;
    let change = start % DestinationCarouselComponent.CARD_WIDTH;
    if (0 === change) {
      change = DestinationCarouselComponent.CARD_WIDTH;
    }
    this.scrollTo(-change);
  }

  moveRight() {
    this.ga.fireEvent(GoogleTracking.DESTINATION_CAROUSEL_CATEGORY, 'Carousel', 'right-button');
    const start = this.scroll.nativeElement.scrollLeft + this.scroll.nativeElement.clientWidth;
    const unvisible = this.scroll.nativeElement.scrollWidth - start;
    let change = unvisible % DestinationCarouselComponent.CARD_WIDTH;
    if (0 === change) {
      change = DestinationCarouselComponent.CARD_WIDTH;
    }
    this.scrollTo(change);
  }

  startDrag(event: MouseEvent, city: DestinationCity) {
    this.dragX = event.clientX;
    this.dragCardObject = city;
    this.dragged = true;
    return false;
  }

  stopDrag(event: MouseEvent) {
    if (!this.isMoved) {
      this.clickCard();
    }
    this.isMoved = false;
    this.dragged = false;
    this.rebuildScroller();
    return false;
  }

  move(event: MouseEvent) {
    if (this.dragged && Math.abs(this.dragX - event.clientX) > DestinationCarouselComponent.DRAG_MIN_LIMIT) {
      this.isMoved = true;
      this.scroll.nativeElement.scrollLeft = this.scroll.nativeElement.scrollLeft - event.clientX + this.dragX;
      this.dragX = event.clientX;
      this.rebuildScroller();
    }
    return false;
  }

  clickCard() {
    if(this.dragCardObject) {
      this.ga.fireEvent(GoogleTracking.DESTINATION_CAROUSEL_CATEGORY, this.dragCardObject.name, this.router.url);
      this.router.navigate([RouteHelper.buildCityRoute(this.dragCardObject)]);
      this.dragCardObject = null;
    }
  }

  /**
   * Move slider left or rigth
   * @param change
   */
  private scrollTo(change: number) {
    if (this.isAnimating ) {
      return;
    }
    this.isAnimating = true;
    const start = this.scroll.nativeElement.scrollLeft;
    const increment = 20;
    let currentTime = 0;
    const duration = 500;

    // t = current time
    // b = start value
    // c = change in value
    // d = duration
    const easeInOutQuad = function (t: number, b: number, c: number, d: number) {
      t /= d / 2;
      if (t < 1) {
        return c / 2 * t * t + b;
      }
      t--;
      return -c / 2 * (t * (t - 2) - 1) + b;
    };

    const animateScroll = () => {
      currentTime += increment;
      this.scroll.nativeElement.scrollLeft = easeInOutQuad(currentTime, start, change, duration);
      if (currentTime < duration) {
        window.setTimeout(animateScroll, increment);
      } else {
        this.isAnimating = false;
      }
    };
    animateScroll();
  }

  /**
   * Rebuild scroller after scroll
   */
  private rebuildScroller() {
    const cardWidth = DestinationCarouselComponent.CARD_WIDTH;
    const cardOverride = DestinationCarouselComponent.CARD_OVERRIDE;
    const element = this.scroll.nativeElement;

    // If we have less than CARD_OVERRIDE hidden card at left - move CARD_OVERRIDE from end to begin
    if (element.scrollLeft < cardWidth * cardOverride) {
      const moved = this._destinations.splice(-cardOverride);
      this._destinations = moved.concat(this._destinations);
      element.scrollLeft += cardWidth * cardOverride;
    }

    // If we have less than CARD_OVERRIDE hidden card at right - move CARD_OVERRIDE from begin to end
    if (element.scrollLeft > (cardWidth * (this._destinations.length - cardOverride)) - element.clientWidth) {
      const moved = this._destinations.splice(0, cardOverride);
      this._destinations = this._destinations.concat(moved);
      this.scroll.nativeElement.scrollLeft -= cardWidth * cardOverride;
    }
  }
}
