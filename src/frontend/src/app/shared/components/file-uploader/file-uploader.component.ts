import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileSystemFileEntry, UploadEvent } from 'ngx-file-drop';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.styl']
})
export class FileUploaderComponent implements OnInit {

  @Input()  file: File = null;
  @Output() fileChange = new EventEmitter<File>();

  constructor() { }

  ngOnInit() {
  }

  public dropped(event: UploadEvent) {
    if (event.files.length != 1) {
      alert('Please add only one file');
      return false;
    }
    const fileEntry = event.files[0].fileEntry as FileSystemFileEntry;
    fileEntry.file(file => this.processFile(file));
  }

  openFileDialog(){
    document.getElementById('file-upload').click();
  }

  handleFileInput(files: FileList) {
    if (files.length != 1) {
      alert('Please add only one file');
      return false;
    }
    this.processFile(files[0]);
  }

  /**
   * Perform selected file processing
   *
   * @param {File} file
   */
  private processFile(file: File) {
    //Perform file checks here
    this.file = file;
    this.fileChange.next(file);
  }
}
