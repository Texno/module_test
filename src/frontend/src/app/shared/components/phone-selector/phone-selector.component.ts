import { Component, forwardRef, HostListener, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Country } from '@shared/backend/data-types/geo.types';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-phone-selector',
  templateUrl: './phone-selector.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneSelectorComponent),
      multi: true
    }
  ],
  exportAs: 'ngModel',
  styleUrls: ['./phone-selector.component.styl']
})
export class PhoneSelectorComponent implements ControlValueAccessor {

  onShow = false;

  phone = '';

  countries$: Promise<Country[]>;
  code: string = '';
  number: string = '';

  @ViewChild('hostTarget') hostTarget;
  @ViewChild('codeInput') codeInput;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if ('NGB-HIGHLIGHT' !== targetElement.tagName) {
      this.codeInput.nativeElement.value = this.code;
    }
  }

  constructor(private rest: RestApiService) {
    this.countries$ = this.rest.getCountryList();
  }

  // Function to call when the phone changes.
  onChange = (phone: string) => {};

  // Function to call when the input is touched
  onTouched = () => {};

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCodes = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList
          .filter(v => {
            let callingCode = '+' + v.calling_code;
            return v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1
                  || callingCode.toLowerCase().indexOf(searchText.toLowerCase()) > -1
          })
          .slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  resultFormatter = (result: Country) => {
    return `${result.name} (+${result.calling_code})`;
  };

  /**
   * Formatter for input
   * @param {Country} result
   * @returns {string}
   */
  inputFormatter  = (result: Country) => {
    return '+' + result.calling_code;
  };

  /**
   * Country select handler
   * @param country
   */
  onSelectCountry = (country: Country) => {
    this.code = '+' + country.calling_code;
    this.updateValue();
  };

  /**
   * Handler to update parent value
   */
  updateValue = () => {
    if(null == this.number) this.number = '';
    this.phone = this.code + this.number;
    this.onChange(this.phone);
  };

  /**
   * Initialize control with data
   *
   * @param {string} value
   */
  writeValue(value: string) {
    this.phone = value;
    if (null !== this.phone && this.phone.length) {
      this.countries$.then(countries => {
        let value = this.phone.replace(/\+/g, '');
        countries.sort((a, b) =>  b.calling_code.length - a.calling_code.length);
        for(let index = 0; index < countries.length; index ++) {
          if(0 == value.indexOf(countries[index].calling_code)) {
            this.code = '+' + countries[index].calling_code;
            this.number = value.slice(countries[index].calling_code.length);
            break;
          }
        }
      });
    }
  }

  /**
   * Register change function
   * @param fn
   */
  registerOnChange(fn) {
    this.onChange = fn;
  }

  /**
   * Register blur function
   * @param fn
   */
  registerOnTouched(fn) {
    this.onTouched = fn;
  }
}
