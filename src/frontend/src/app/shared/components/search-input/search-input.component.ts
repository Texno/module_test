import { Component, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { Router } from '@angular/router';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Activity } from '@shared/backend/data-types/activity.types';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.styl']
})
export class SearchInputComponent implements OnInit {

  @Input() destinations: DestinationCity[] = [];

  showDestinations = false;
  showSearch = false;

  typed = new Subject<string>();
  queryString = '';

  searchActivities: Activity[] = [];
  searchCountries: Country[] = [];
  searchCities: City[] = [];
  inSearch = false;

  routeHelper = RouteHelper;

  @ViewChild('hostTarget') hostTarget;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.hostTarget.nativeElement.contains(targetElement)) {
      // Clicked outside of search
      this.showDestinations = false;
      this.showSearch = false;
    }
  }

  constructor(private router: Router, private rest: RestApiService) { }

  ngOnInit() {
    this.typed.debounceTime(300).filter(value => value.length >= 3).subscribe(this.launchSearch); // Launch search if more 3 characters is typed
    this.typed.debounceTime(300).filter(value => value.length < 3).subscribe(() => this.showSearch = false); // Show top destinations if less tnah 3 characters is typed
  }

  /**
   * Search callback
   *
   * @param {string} value
   */
  private launchSearch = (value: string) => {
    this.showSearch = true;
    this.inSearch = true;
    const promises = [];
    promises.push(this.rest.searchActivities(value));
    promises.push(this.rest.searchCountries(value));
    promises.push(this.rest.searchCities(value));

    Promise.all(promises).then(result => {
      this.searchActivities = result[0];
      this.searchCountries = result[1];
      this.searchCities = result[2];
      this.inSearch = false;
    });
  };

  /**
   * Redirect to search page and save searched data
   */
  redirectToSearchPage() {
    this.router.navigateByData({
      url: ["/search"],
      data: this.searchActivities,
      extras: {queryParams: {q: this.queryString}}
    });
  }

}
