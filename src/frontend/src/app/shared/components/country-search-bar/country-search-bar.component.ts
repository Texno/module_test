import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Country } from '@shared/backend/data-types/geo.types';

@Component({
  selector: 'app-country-search-bar',
  templateUrl: './country-search-bar.component.html',
  styleUrls: ['./country-search-bar.component.styl']
})
export class CountrySearchBarComponent implements OnInit {

  @Output() onCountrySelected = new  EventEmitter<Country>();
  @Input() selectedCountry: string;
  selectedName: string;

  isOpened = false;
  countriesSource: Country[] = [];
  countries: Country[] = [];

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.rest.getDestinationCountryList().then(countries => {
      this.countries = countries.slice(0, 10);
      this.countriesSource = countries;
      for (let i=0; i < countries.length; i++) {
        if ((this.selectedCountry === countries[i].route_name)) {
          this.selectedName = countries[i].name;
          this.selectedCountry = countries[i].route_name;
          break;
        }
      }
    });
  }

  public selectCountry(country: Country) {
    this.isOpened = false;
    this.selectedCountry = country.route_name;
    this.selectedName = country.name;
    this.onCountrySelected.next(country);
  }

  public searchCountry(event: any) {
    const searchText = event.target.value;
    if (searchText) {
      this.countries = this.countriesSource.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 ).slice(0, 10);
    } else {
      this.countries = this.countriesSource.slice(0, 10)
    }
  }

}
