import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Category } from '@shared/backend/data-types/activity.types';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.styl']
})
export class CategoryFilterComponent implements OnInit {

  categories: Promise<Category[]>;

  @Input() selectedCategories: number[] = [];
  @Output() onCategoryClicked = new  EventEmitter<number[]>();

  fixed = false;

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.categories = this.rest.getActivityCategoriesList();
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = window.scrollY;
    if (number >= 530) {
      this.fixed = true;
    } else {
      this.fixed = false;
    }
  }

  /**
   * Category click callback
   *
   * @param {number} id
   */
  clickCategory(id: number) {
    this.selectedCategories = [id];
    this.onCategoryClicked.next(this.selectedCategories);
  }

  clear() {
    this.selectedCategories = [];
    this.onCategoryClicked.next(this.selectedCategories);
  }
}
