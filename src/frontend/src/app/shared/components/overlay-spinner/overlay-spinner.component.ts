import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-overlay-spinner',
  templateUrl: './overlay-spinner.component.html',
  styleUrls: ['./overlay-spinner.component.styl']
})
export class OverlaySpinnerComponent implements OnInit {

  @Input() loaderText = '';

  constructor() { }

  ngOnInit() {
  }

}
