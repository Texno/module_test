import { Component, Input, OnInit } from '@angular/core';
import { Review } from '@shared/backend/data-types/review.types';

@Component({
  selector: 'app-review-card',
  templateUrl: './review-card.component.html',
  styleUrls: ['./review-card.component.styl']
})
export class ReviewCardComponent implements OnInit {

  @Input() review: Review;

  constructor() { }

  ngOnInit() {
  }

}
