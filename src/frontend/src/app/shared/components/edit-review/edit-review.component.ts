import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Review } from '@shared/backend/data-types/review.types';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { ReviewForm } from '@shared/backend/data-types/review-form';
import { AuthService } from '@shared/auth/auth.service';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateHelper } from '@shared/helpers/date-helper';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-edit-review',
  templateUrl: './edit-review.component.html',
  styleUrls: ['./edit-review.component.styl']
})
export class EditReviewComponent implements OnInit, AfterViewInit {

  @Input() review: Review;
  @Input() activity: ActivityFull;
  @Input() orderCode: string = '';
  @Input() forAdmin = false;

  @Output() onComplete = new EventEmitter<boolean>();

  private countries$: Promise<Country[]>;

  form = new ReviewForm();

  tryToSubmit = false;
  inSubmit = false;

  selectedTime: NgbTimeStruct = {hour: 0, minute: 0, second: 0};
  selectedDate: NgbDateStruct;
  serverError = '';

  rate: number = 0;

  routeHelper = RouteHelper;

  constructor(public auth: AuthService, private rest: RestApiService) { }

  ngOnInit() {
    this.countries$ = this.rest.getCountryList();
    if(this.review) {
      this.form.fillFromReview(this.review);
      this.selectedDate = DateHelper.getNgbDateStructFromUnix(this.review.created_at, this.activity.schedule.offset);
      this.selectedTime = DateHelper.getNgbTimeStructFromUnix(this.review.created_at, this.activity.schedule.offset);
      this.rate = this.form.rate;
    } else {
      this.form.activity = this.activity.id;
    }
  }

  ngAfterViewInit() {
    if(this.orderCode.length) {
      this.form.order_code = this.orderCode;
    }
  }

  /**
   * Handler on form submit
   */
  submit() {
    this.tryToSubmit = true;
    this.form.rate = this.rate * 2; // Double rate
    if (!this.form.isValid(this.forAdmin)) return;
    this.serverError = '';
    this.inSubmit = true;
    const successHandler = () => this.onComplete.emit(true);
    const errorHandler = err => {
      this.form.rate /= 2;
      if(err.error.errors.hasOwnProperty('review_form')) {
        this.serverError = err.error.errors.review_form;
      } else {
        this.serverError = err.error.message;
      }
      this.inSubmit = false;
    };

    if(this.review) {
      this.rest.updateReview(this.review.id, this.form).then(successHandler).catch(errorHandler);
    } else {
      this.rest.createReview(this.form).then(successHandler).catch(errorHandler);
    }

  }

  /**
   * Handler on cancel
   */
  cancel() {
    this.onComplete.emit(false);
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    let country: Country = event.item;
    this.form.country_id = country.id;
  };

  /**
   * Callback to filter cities
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any[]>}
   */
  searchCity = (text$: Observable<string>) => {
    return text$.debounceTime(200).distinctUntilChanged()
      .flatMap(searchText => {
        if (!this.form.country_id || !searchText.length) {
          return of([]);
        }
        return this.rest.searchCitiesByCountry(this.form.country_id, searchText);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  cityFormatter = (result: City) => {
    return result.name;
  };

  /**
   * City select handler
   * @param event
   */
  onSelectCity = (event) => {
    let city: City = event.item;
    this.form.city_id = city.id;
  };

  /**
   * Handler for time selector
   */
  onTimeChange() {
    if (!this.selectedDate) return;
    this.updateFormTimestamp();
  }

  /**
   * Convert date and time to timestamp
   */
  updateFormTimestamp() {
    const momentDate = DateHelper.parseNgbDateStruct(this.selectedDate, this.activity.schedule.offset);
    this.form.created_at = momentDate.unix() + DateHelper.convertNgbTimeStructToSeconds(this.selectedTime);
  }

}
