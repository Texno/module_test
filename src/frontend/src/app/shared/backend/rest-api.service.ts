import { Injectable } from '@angular/core';
import { DestinationCity, DestinationCountry } from '@shared/backend/data-types/destination.types';
import {
  Activity,
  ActivityFull,
  Category
} from '@shared/backend/data-types/activity.types';
import { City, Country, Language } from '@shared/backend/data-types/geo.types';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { RegistrationForm } from '@shared/backend/data-types/registration-form';
import { AppConfig } from '@shared/config/app-config.service';
import { Review, TravellerType } from '@shared/backend/data-types/review.types';
import { AjaxActivityFilter} from '@shared/components/data-types/filter.types';
import { CountrySaveForm } from '../../admin/data-types/country-save-form';
import { CitySaveForm } from '../../admin/data-types/city-save-form';
import { ActivityForm } from '../../admin/data-types/activity-form';
import { BasketOrder, OrderUpdate } from '@shared/basket/data-types/basket-order';
import { OrderAdminFull, OrderAdminShort, OrderInfo, RedsysButton } from '@shared/backend/data-types/order.types';
import { ReviewForm } from '@shared/backend/data-types/review-form';
import { Supplier, SupplierForm } from '@shared/backend/data-types/supplier.types';
import { Report, ReportType } from '@shared/backend/data-types/report.types';


@Injectable()
export class RestApiService {

  private readonly apiUrl = null;
  private categoriesCache: Promise<Category[]>;

  private requestCache = new Map<string, Promise<any>>();

  static readonly COUNTRIES_CACHE_KEY = 'countries';
  static readonly REPORT_TYPES_CACHE_KEY = 'report_types';

  constructor(private http: HttpClient) {
    this.apiUrl = AppConfig.getConfiguration().backendUrl;
  }

  /**
   * Get cached value
   *
   * @param {string} key
   * @param {Promise<any>} value
   * @returns {any}
   */
  private getCached(key: string, value: () => Promise<any>) {
    if (!this.requestCache.has(key)) {
      this.requestCache.set(key, value());
    }
    return this.requestCache.get(key);
  }

  /**
   * Api request for retrieving destination data
   *
   * @returns {Promise<DestinationCity[]>}
   */
  public getMainDestinationsList(): Promise<DestinationCity[]> {
    return this.http.get<DestinationCity[]>(this.apiUrl + 'destinations/main/list').toPromise();
  }

  /**
   * Retrieve full list of activities
   *
   * @param forAdmin
   */
  public getActivities(forAdmin = false): Promise<Activity[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<Activity[]>(this.apiUrl + adminAdd + 'activities').toPromise();
  }

  /**
   * Create new activity
   *
   * @param {ActivityForm} form
   * @returns {Promise<Object>}
   */
  public createActivity(form: ActivityForm) {
    return this.http.post(this.apiUrl + 'admin/activities', form).toPromise();
  }

  /**
   * Edit existent activity
   * @param {number} id
   * @param {ActivityForm} form
   * @returns {Promise<Object>}
   */
  public updateActivity(id: number, form: ActivityForm) {
    return this.http.patch(this.apiUrl + 'admin/activities/' + id, form).toPromise();
  }

  /**
   * Set activity draft status
   *
   * @param {number} idActivity
   * @param {boolean} draftStatus
   * @returns {Promise<Object>}
   */
  public setActivityDraftStatus(idActivity: number, draftStatus: boolean) {
    const actionString = draftStatus ? 'set' : 'clear';
    return this.http.post(this.apiUrl + `admin/activities/${idActivity}/${actionString}/draft`, {}).toPromise();
  }

  /**
   * Delete activity
   * @param idActivity
   */
  public deleteActivity(idActivity: number) {
    return this.http.delete(this.apiUrl + `admin/activities/${idActivity}`).toPromise();
  }

  /**
   * Retrieve list of featured activities
   *
   * @param {number} limit
   * @param {number} offset
   * @param country
   */
  public getFeaturedActivities(limit = 6, offset = 0, country: string = null): Promise<Activity[]> {
    const queryString = `?s=${offset}&l=${limit}` + (country ? `&c=${country}` : '' );
    return this.http.get<Activity[]>(this.apiUrl + `activities/featured${queryString}`).toPromise();
  }

  /**
   * Retrieve all available categories for activities
   *
   * @returns {Promise<Category>}
   * @constructor
   */
  public getActivityCategoriesList(): Promise<Category[]> {
    if(!this.categoriesCache) {
      this.categoriesCache = this.http.get<Category[]>(this.apiUrl + 'activities/categories').toPromise();
    }
    return this.categoriesCache;
  }

  /**
   * Retrieve all activities for city in short format
   *
   * @param {number} idCity
   * @param filter
   * @returns {Promise<Activity[]>}
   */
  public getActivitiesForCity(idCity: number, filter?: AjaxActivityFilter): Promise<Activity[]> {
    let filterString = '';
    if (filter) {
      filterString = '?';
      filter.dates.forEach(interval => filterString += `d[]=${interval.start}:${interval.end}&`);
      filter.times.forEach(interval => filterString += `t[]=${interval.start}:${interval.end}&`);
    }
    return this.http.get<Activity[]>(this.apiUrl + `activities/city/${idCity}${filterString}`).toPromise();
  }

  /**
   * Retrieve all activities for city in short format
   *
   * @param query
   * @param filter
   * @returns {Promise<Activity[]>}
   */
  public searchActivities(query: string, filter?: AjaxActivityFilter): Promise<Activity[]> {
    let filterString = '';
    if (filter) {
      filter.dates.forEach(interval => filterString += `&d[]=${interval.start}:${interval.end}`);
      filter.times.forEach(interval => filterString += `&t[]=${interval.start}:${interval.end}`);
    }
    return this.http.get<Activity[]>(this.apiUrl + `search/activities?q=${query}${filterString}`).toPromise();
  }

  /**
   * Search countries
   *
   * @param {string} query
   * @returns {Promise<Country[]>}
   */
  public searchCountries(query: string): Promise<Country[]> {
    return this.http.get<Country[]>(this.apiUrl + `search/countries?q=${query}`).toPromise();
  }

  /**
   * Search cities
   *
   * @param {string} query
   * @returns {Promise<City[]>}
   */
  public searchCities(query: string): Promise<City[]> {
    return this.http.get<City[]>(this.apiUrl + `search/cities?q=${query}`).toPromise();
  }

  /**
   * Retrieve list of related activities
   *
   * @param {number} id
   * @returns {Promise<Activity[]>}
   */
  public getRelatedActivities(id: number): Promise<Activity[]> {
    return this.http.get<Activity[]>(this.apiUrl + `activities/${id}/related`).toPromise();
  }

  /**
   * Get full destination info for country
   * @param {string} routeName
   * @returns {Promise<DestinationCountry>}
   */
  public getDestinationCountry(routeName: string): Promise<DestinationCountry> {
    return this.http.get<DestinationCountry>(this.apiUrl + 'destinations/country/' + routeName).toPromise();
  }

  /**
   * Get full destination info for city
   * @returns {Promise<DestinationCity>}
   * @param countryRoute
   * @param cityRoute
   */
  public getDestinationCity(countryRoute: string, cityRoute: string): Promise<DestinationCity> {
    return this.http.get<DestinationCity>(this.apiUrl + `destinations/city/${countryRoute}/${cityRoute}`).toPromise();
  }

  /**
   * Get list of available destinations for country
   * @returns {Promise<Country[]>}
   */
  public getDestinationCountryList(forAdmin = false): Promise<Country[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<Country[]>(this.apiUrl + adminAdd + 'destinations/country/list').toPromise();
  }

  /**
   * Get destination cities for country
   *
   * @param countryCode
   * @param forAdmin
   * @returns {Promise<City[]>}
   */
  public getDestinationCityList(countryCode: any, forAdmin = false): Promise<City[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<City[]>(this.apiUrl + `${adminAdd}destinations/country/${countryCode}/cities`).toPromise();
  }

  /**
   * Retrieve full information about activity including
   *
   * @returns {Promise<ActivityFull>}
   */
  public getActivityFull(id: number, forAdmin = false): Promise<ActivityFull> {
    const adminAdd = forAdmin ? 'admin/' : '';
    return this.http.get<ActivityFull>(this.apiUrl + adminAdd  + 'activities/' + id).toPromise();
  }

  /**
   * Get activity info with route
   *
   * @returns {Promise<ActivityFull>}
   * @param countryRoute
   * @param cityRoute
   * @param route
   */
  public getActivityFullRoute(countryRoute: string, cityRoute: string, route: string): Promise<ActivityFull> {
    return this.http.get<ActivityFull>(this.apiUrl + `activities/${countryRoute}/${cityRoute}/${route}`).toPromise();
  }

  /**
   * Retrieve all countries list
   * @returns {Promise<Country[]>}
   */
  public getCountryList(): Promise<Country[]> {
    return this.getCached(
      RestApiService.COUNTRIES_CACHE_KEY,
      () =>  this.http.get<Country[]>(this.apiUrl + 'countries').toPromise()
    );
  }

  /**
   * Retrieve cities list by country and search string
   *
   * @param {number} country
   * @param {string} search
   * @returns {Promise<City[]>}
   */
  public searchCitiesByCountry(country: number, search: string): Promise<City[]> {
    return this.http.get<City[]>(this.apiUrl + 'cities/' + country + '/search/' + search).toPromise();
  }

  /**
   * Make user registration
   *
   * @param {RegistrationForm} form
   * @returns {Promise<any>}
   */
  public registerUser(form: RegistrationForm): Promise<any> {
    return this.http.post(this.apiUrl + 'register', form).toPromise();
  }

  /**
   * Retrieve review list for city
   * @param {number} idCity
   * @returns {Promise<Review[]>}
   */
  public getReviewsForCity(idCity: number): Promise<Review[]> {
    return this.http.get<Review[]>(this.apiUrl + `reviews/city/${idCity}`).toPromise();
  }

  /**
   * Retrieve review list for activity
   * @param {number} idActivity
   * @param forAdmin
   * @param all
   * @returns {Promise<Review[]>}
   */
  public getReviewsForActivity(idActivity: number, forAdmin = false, all = false): Promise<Review[]> {
    const adminAdd = forAdmin ? 'admin/' : '';
    const allAdd = all ? '?all' : '';
    return this.http.get<Review[]>(this.apiUrl + `${adminAdd}reviews/activity/${idActivity}${allAdd}`).toPromise();
  }

  /**
   * Retrieve list of languages
   * @returns {Promise<Language[]>}
   */
  public getLanguages(): Promise<Language[]> {
    return this.http.get<Language[]>(this.apiUrl + 'languages').toPromise();
  }

  /**
   * Update country in database
   * @returns {Promise<any>}
   */
  public saveCountryData(idCountry: number, form: CountrySaveForm): Promise<any> {
    return this.http.patch(this.apiUrl + `admin/countries/${idCountry}`, form).toPromise();
  }

  /**
   * Update country in database
   * @returns {Promise<any>}
   */
  public saveCityData(idCity: number, form: CitySaveForm): Promise<any> {
    return this.http.patch(this.apiUrl + `admin/cities/${idCity}`, form).toPromise();
  }

  /**
   * Place new order new order
   *
   * @param {BasketOrder} order
   * @returns {Promise<Object>}
   */
  public createOrder(order: BasketOrder) {
    return this.http.post(this.apiUrl + 'order/create', order).toPromise();
  }

  /**
   * Update order status
   *
   * @param {string} orderCode
   * @param {OrderUpdate} form
   * @returns {Promise<Object>}
   */
  public updateOrder(orderCode: string, form: OrderUpdate) {
    return this.http.post(this.apiUrl + 'order/update/' + orderCode, form).toPromise();
  }

  /**
   * Perform stripe payment
   *
   * @param orderCode
   * @param token
   */
  public payStripe(orderCode: string, token: string) {
    return this.http.post(`${this.apiUrl}order/${orderCode}/stripe`, {'token': token}).toPromise();
  }

  /**
   * Retrieve order information
   *
   * @param {string} orderCode
   * @returns {Promise<OrderInfo[]>}
   */
  public getOrderInfo(orderCode: string): Promise<OrderInfo[]> {
    return this.http.get<OrderInfo[]>(this.apiUrl + 'order/' + orderCode).toPromise();
  }

  /**
   * Retrieve order information
   *
   * @param {string} orderCode
   * @returns {Promise<OrderInfo[]>}
   */
  public makeOrderViewed(orderCode: string) {
    return this.http.post(this.apiUrl + 'order/' + orderCode + '/viewed', null).toPromise();
  }

  /**
   * Retrieve all orders by user
   * @returns {Promise<OrderInfo[]>}
   */
  public getUserOrders(): Promise<OrderInfo[]> {
    return this.http.get<OrderInfo[]>(this.apiUrl + 'user/order').toPromise();
  }

  /**
   * Get redsys pay button
   *
   * @param orderCode
   * @param {number} amount
   * @returns {Promise<string>}
   */
  public getRedSysButton(orderCode: string, amount: number): Promise<RedsysButton> {
    return this.http.get<RedsysButton>(this.apiUrl + `order/${orderCode}/redsys/pay/${amount}`).toPromise();
  }

  /**
   * get review from database
   *
   * @param {number} idReview
   * @returns {Promise<Object>}
   */
  public getReview(idReview: number): Promise<Review> {
    return this.http.get<Review>(this.apiUrl + 'reviews/' + idReview).toPromise();
  }

  /**
   * Delete review from database
   *
   * @param {number} idReview
   * @returns {Promise<Object>}
   */
  public deleteReview(idReview: number) {
    return this.http.delete(this.apiUrl + 'reviews/' + idReview).toPromise();
  }

  /**
   * Create new review
   *
   * @param {ReviewForm} form
   * @returns {Promise<Object>}
   */
  public createReview(form: ReviewForm) {
    return this.http.post(this.apiUrl + 'reviews', form).toPromise();
  }

  /**
   * Update review
   *
   * @param {number} idReview
   * @param {ReviewForm} form
   * @returns {Promise<Object>}
   */
  public updateReview(idReview: number, form: ReviewForm) {
    return this.http.patch(this.apiUrl + 'reviews/' + idReview, form).toPromise();
  }

  /**
   * Get review list for user
   */
  public getUserReviews(): Promise<Review[]> {
    return this.http.get<Review[]>(this.apiUrl + 'reviews/user').toPromise();
  }

  /**
   * Set activity draft status
   *
   * @param {number} idReview
   * @param approveStatus
   * @returns {Promise<Object>}
   */
  public setReviewApproveStatus(idReview: number, approveStatus: boolean) {
    const actionString = approveStatus ? 'set' : 'clear';
    return this.http.post(this.apiUrl + `admin/reviews/${idReview}/${actionString}/approve`, {}).toPromise();
  }

  /**
   * Retrieve list of traveller types
   *
   * @returns {Promise<TravellerType[]>}
   */
  public getTravellerTypes(): Promise<TravellerType[]> {
    return this.http.get<TravellerType[]>(this.apiUrl + 'reviews/traveller_types').toPromise();
  }

  /**
   * Get list of all available suppliers
   *
   * @returns {Promise<Supplier[]>}
   */
  public getSuppliers(): Promise<Supplier[]> {
    return this.http.get<Supplier[]>(this.apiUrl + 'admin/suppliers').toPromise();
  }

  /**
   * Create new supplier
   *
   * @param {SupplierForm} form
   * @returns {Promise<Object>}
   */
  public createSupplier(form: SupplierForm) {
    return this.http.post(this.apiUrl + 'admin/suppliers', form).toPromise();
  }

  /**
   * Update supplier
   *
   * @param {number} id
   * @param {SupplierForm} form
   * @returns {Promise<Object>}
   */
  public updateSupplier(id:number, form: SupplierForm) {
    return this.http.patch(this.apiUrl + `admin/suppliers/${id}`, form).toPromise();
  }

  /**
   * Get supplier by id
   *
   * @param {number} id
   * @returns {Promise<Supplier>}
   */
  public getSupplier(id: number): Promise<Supplier> {
    return this.http.get<Supplier>(this.apiUrl + `admin/suppliers/${id}`).toPromise();
  }

  /**
   * Delete review from database
   *
   * @param {number} idSupplier
   * @returns {Promise<Object>}
   */
  public deleteSupplier(idSupplier: number) {
    return this.http.delete(this.apiUrl + 'admin/suppliers/' + idSupplier).toPromise();
  }

  /**
   * Retrieve list of operations
   * @returns {Promise<OrderAdminShort[]>}
   */
  public getOperations(): Promise<OrderAdminShort[]> {
    return this.http.get<OrderAdminShort[]>(this.apiUrl + 'admin/operations').toPromise();
  }

  /**
   * Retrieve one operation info
   *
   * @param {number} id
   * @returns {Promise<OrderAdminFull>}
   */
  public getOperation(id: number): Promise<OrderAdminFull> {
    return this.http.get<OrderAdminFull>(this.apiUrl + `admin/operations/${id}`).toPromise();
  }

  /**
   * Retrieve one operation info
   *
   * @param {number} id
   * @returns {Promise<OrderAdminFull>}
   */
  public deleteOperation(id: number){
    return this.http.delete(this.apiUrl + `admin/operations/${id}`).toPromise();
  }

  /**
   * Update order status
   *
   * @param {string} code
   * @param {string} newStatus
   * @returns {Promise<Object>}
   */
  public updateOrderStatus(code: string, newStatus: string) {
    return this.http.post(this.apiUrl + `admin/order/${code}/status/${newStatus}`, {}).toPromise();
  }

  /**
   * Save admin note
   *
   * @param {number} id
   * @param {string} note
   * @returns {Promise<Object>}
   */
  public saveAdminNote(id: number, note: string) {
    return this.http.post(this.apiUrl + `admin/operations/${id}/note`, {data: note}).toPromise();
  }

  /**
   * Endpoint to upload image
   *
   * @param {File} fileItem
   * @returns {any}
   */
  public uploadImage (fileItem:File):any {
    const formData: FormData = new FormData();

    formData.append('fileItem', fileItem, fileItem.name);
    const req = new HttpRequest('POST', this.apiUrl + 'upload/image', formData, {
      reportProgress: true // for progress data
    });
    return this.http.request(req)
  }

  /**
   * Perform contact request
   *
   * @param {FormData} form
   * @returns {Promise<Object>}
   */
  public requestContact(form: FormData) {
    return this.http.post(this.apiUrl + 'contacts/request', form).toPromise();
  }

  /**
   * Perform faq` request
   *
   * @param {FormData} form
   * @returns {Promise<Object>}
   */
  public requestFaq(form: FormData) {
    return this.http.post(this.apiUrl + 'faq/request', form).toPromise();
  }

  /**
   * Get all available report types
   *
   * @returns {Promise<ReportType[]>}
   */
  public getReportTypes(): Promise<ReportType[]> {
    return this.getCached(
      RestApiService.REPORT_TYPES_CACHE_KEY,
      () => this.http.get<ReportType[]>(this.apiUrl + 'admin/reports/types').toPromise()
    );
  }

  /**
   * Get all available reports
   *
   * @returns {Promise<Report[]>}
   */
  public getReports(): Promise<Report[]> {
    return this.http.get<Report[]>(this.apiUrl + 'admin/reports').toPromise();
  }

  /**
   * Create new report
   *
   * @param form
   * @returns {Promise<Object>}
   */
  public createNewReport(form) {
    return this.http.post(this.apiUrl + 'admin/reports', form).toPromise();
  }

  /**
   * Get report data
   * @param id
   */
  public getReportData(id) {
    return this.http.get<any>(this.apiUrl + `admin/reports/${id}/json`).toPromise();
  }

  /**
   * Delete report
   * @param id
   */
  public deleteReport(id) {
    return this.http.delete(this.apiUrl + `admin/reports/${id}`).toPromise();
  }

  /**
   * Upload
   * @param file
   */
  public uploadReviews(file: File) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<string[]>(this.apiUrl + 'admin/reviews/upload', formData).toPromise();
  }

}
