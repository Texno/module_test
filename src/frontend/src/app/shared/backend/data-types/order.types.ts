export interface OrderInfo {
  activity: number;
  viewed: boolean;
  avatar: string;
  activity_name: string;
  activity_type_name: string;
  country_name: string;
  city_name: string;
  code: string;
  date: string;
  duration: string;
  email: string;
  language: string;
  meeting_point_latitude: number;
  meeting_point_longitude: number;
  meeting_point_text: string;
  name: string;
  phone: string;
  prices: OrderInfoPrices[];
  surname: string;
  time: string;
  rate: number;
  country_route: string;
  city_route: string;
  activity_route: string;
}

export interface OrderInfoPrices {
  name: string;
  amount: number;
  price: number;
}

export interface RedsysButton {
  button: string;
}

export interface OrderAdminShort {
  id:         number;
  code:       string;
  activity:   string;
  city:       string;
  country:    string;
  created_at: number;
  date:       string;
  price:      number;
  status:     string;
}

export interface OrderAdminFull {
  code:           string;
  client:         string;
  phone:          string;
  email:          string;
  country:        string;
  city:           string;
  activity:       string;
  adults:         number;
  children:       number;
  date:           string;
  time:           string;
  comment:        string;
  admin_comment:  string;
  need_invoice:   boolean;
  amount:         number;
  type:           string;
  invoice:        OrderInvoice;
}

interface OrderInvoice {
  tax_name:       string;
  tax_id:         string;
  document_type:  string;
  address:        string;
  city:           string;
  country:        string;
  postal:         string;
}