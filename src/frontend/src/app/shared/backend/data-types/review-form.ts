import { Review } from '@shared/backend/data-types/review.types';

export class ReviewForm {

  constructor(
    public user_name = '',
    public traveller_type = 0,
    public rate = 0,
    public header = '',
    public text = '',
    public activity = 0,
    public country_id = 0,
    public city_id = 0,
    public created_at = 0,
    public order_code = ''
  ) {}

  public fillFromReview(review: Review) {
    this.user_name = review.user_name;
    this.traveller_type = review.traveller_type;
    this.rate = review.rate / 2;
    this.header = review.header;
    this.text = review.text;
    this.activity = review.activity;
    this.country_id = review.country_id;
    this.city_id = review.city_id;
    this.created_at = review.created_at;
    this.order_code = review.order_code;
  }

  public isValid(forAdmin = false): boolean {
    let adminPassed = true;
    if (forAdmin) {
      adminPassed = this.user_name.length !== 0
                 && this.country_id !== 0
                 && this.city_id !== 0
                 && this.created_at !== 0;
    }

    return adminPassed
      && this.traveller_type !== 0
      && this.activity !== 0
      && this.rate !== 0
      && this.header.length !== 0
      && this.text.length !== 0
  }
}
