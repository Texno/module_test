import { Activity } from '@shared/backend/data-types/activity.types';

export interface Country {
  id:             number;
  code:           string;
  name:           string;
  currency:       string;
  calling_code:   string;
  languages:      string;
  avatar:         string;
  avatar_small:   string;
  locale_name:    LocaleNames;
  activities:     number;
  route_name:     string;
}

export interface City {
  id:             number;
  country:        number;
  name:           string;
  avatar:         string;
  avatar_small:   string;
  offset:         number;
  locale_name:    LocaleNames;
  activities:     number;
  country_route:  string;
  route_name:     string;
  travellers:     number;
  latitude:       number;
  longitude:      number;
}

export class CityFilter {

  constructor(public id: number, public name: string) {}

  static fromCity(city: City) {
    return new CityFilter(city.id, city.name);
  }

  static fromActivity(activity: Activity) {
    return new CityFilter(activity.city, activity.city_name);
  }

}

export interface Language {
  code:         string;
  name:         string;
  native_name:  string;
}

export interface LocaleNames {
  [locale: string]: string
}
