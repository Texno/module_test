export interface Review {
  id:             number;
  header:         string;
  text:           string;
  rate:           number;
  activity:       number;
  avatar:         string;
  language:       string;
  duration:       string;
  created_at:     number;
  country_id:     number;
  country:        string;
  city_id:        number;
  city:           string;
  traveller_type: number;
  traveller_type_name: number;
  user_name:      string;
  is_approved:    string;
  order_code:     string;
}

export interface TravellerType {
  id:   number;
  name: string;
}
