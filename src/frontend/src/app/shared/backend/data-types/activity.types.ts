import { Supplier } from '@shared/backend/data-types/supplier.types';

export const ALL_DATE = 'todo';

export interface ActivityFull extends Activity {
  country: number;
  description: string;
  included: string;
  not_included: string;
  when_to_book: string;
  acessibility: string;
  ticket: string;
  travellers: number;
  how_to_book: string;
  meeting_point_latitude: number;
  meeting_point_longitude: number
  meeting_point_text: string;
  cancelation_hours_before: number;
  cancelation_descriprion: string;
  book_limit: string;
  schedule: ActivitySchedule;
  gallery: GalleryItem[];
  types: ActivityType[];
  suppliers: Supplier[];
  blocks: any;
  voucher: ActivityVoucher;
}

export interface GalleryItem {
  path: string;
  description: string;
  is_main: boolean;
}

export interface Activity {
  id:                 number;
  skipLine:           boolean;
  name:               string;
  avatar:             string;
  avatar_small:       string;
  short_description:  string;
  duration:           string;
  language:           string;
  min_price:          number;
  reviews:            number;
  rate:               number;
  category:           number;
  is_draft:           boolean
  country_name:       string;
  city:               number;
  city_name:          string;
  country_route:      string;
  city_route:         string;
  route_name:         string;
}

export interface ActivitySchedule {
  time:   number[];
  offset: number;
}

export interface ActivityType {
  id:   number;
  name: string;
  tickets: ActivityTicket[];
}

export interface ActivityVoucher {
  image: string;
  text: string;
}

export interface ActivityTicket {
  id:       number;
  name:     string;
  is_adult: boolean;
  price:    number;
}

interface TicketOrder {
  [code: number]: number;
}

/**
 * Class to perform an order
 */
export class ActivityOrder {
  constructor(
    public time = 0,
    public type = 0,
    public tickets: TicketOrder = {},
    public comment = '',
  ) {};

  /**
   * Check order is valid
   */
  public isValid() {
    return (0 !== this.time) && (0 !== Object.keys(this.tickets).length);
  }
}

/**
 * Interface to activity category
 */
export interface Category {
  id:   number;
  name: string;
}
