export interface Supplier {
  id:             number;
  name:           string;
  phone:          string;
  email:          string;
  contact:        string;
  vat:            string;
  post_code:      string;
  registered_at:  string;
  website:        string;
  bank_details:   string;
}

export class SupplierForm {

  constructor(
    public name = '',
    public phone = '',
    public email = '',
    public contact = '',
    public vat = '',
    public post_code = '',
    public website = '',
    public bank_details = ''
  ) {}

  public fillFromSupplier(supplier: Supplier) {
    this.name = supplier.name;
    this.phone = supplier.phone;
    this.email = supplier.email;
    this.contact = supplier.contact;
    this.vat = supplier.vat;
    this.post_code = supplier.post_code;
    this.website = supplier.website;
    this.bank_details = supplier.bank_details;
  }
}
