export interface ReportType {
  id:   number;
  name: string;
}

export interface Report {
  id:               number;
  report_type:      number;
  report_type_name: string;
  name:             string;
  created_at:       number;
  status:           string;
  params:           string;
  csv:              string;
  xls:              string;
}
