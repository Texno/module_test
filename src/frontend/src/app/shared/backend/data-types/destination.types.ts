import { City, Country } from '@shared/backend/data-types/geo.types';

export interface DestinationCountry extends Country {
  avatar:       string;
  excursions:   number;
  reviews:      number;
  rate:         number;
  travelers:    number;
  destinations: DestinationCity[];
}

export interface DestinationCity extends City {
  reviews:      number;
  rate:         number;
  travelers:    number;
  excursions:   number;
  country_name: string;
  country_code: string;
}
