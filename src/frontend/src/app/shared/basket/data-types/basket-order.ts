import { ActivityOrder } from '@shared/backend/data-types/activity.types';
import { TokenUserData } from '@shared/auth/data-types/token.data';

/**
 * Order class that will be published to server
 */
export class BasketOrder {

  constructor(
    public fullName = '',
    public phone = '',
    public email = '',
    public isVerified = false,
    public items: ActivityOrder[] = [],
    public invoice: InvoiceRequest = null,
    public code = '',
    public verifiedTotal = 0
  ) { }

  public fillFromUserData(data: TokenUserData) {
    this.fullName = data.fullName;
    this.phone = data.phone;
    this.email = data.email;
  }
}

export class InvoiceRequest {
  constructor(
    public taxName = '',
    public taxId:number = null,
    public documentType = '',
    public address = '',
    public city = '',
    public country = '',
    public postalCode=  ''
  ) {}
}

export interface OrderUpdate {
  gateway: string,
  transactionId?: string,
  signatureVersion?: string;
  merchantParameters?: string;
  signature?: string;
}