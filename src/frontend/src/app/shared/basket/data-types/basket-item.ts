import { ActivityFull, ActivityOrder, ActivityTicket } from '@shared/backend/data-types/activity.types';

/**
 * Item of bascket. Immutable.
 */
export class BasketItem {

  /**
   * Cached result of ticket data
   *
   * @type BasketTicketData
   */
  private ticketDataCache: BasketTicketData = null;

  constructor(public id, public activity: ActivityFull, public order: ActivityOrder) {}

  /**
   * Retrieve info data for item
   *
   * @returns {BasketTicketData}
   */
  public getTicketData(): BasketTicketData {
    if (!this.ticketDataCache) {
      this.ticketDataCache = {ticketNames: [], total: 0, people: 0, activityType: ''};
      for(const code in this.order.tickets) {
        const info = this.getTicketInfo(parseInt(code), this.order.type);
        if (info) {
          this.ticketDataCache.total += this.order.tickets[code] * info.price;
          this.ticketDataCache.people += this.order.tickets[code];
          this.ticketDataCache.ticketNames.push(this.order.tickets[code] + " " + info.name);
        }
        this.ticketDataCache.activityType = this.getActivityTypeName(this.order.type);
      }
    }
    return this.ticketDataCache;
  }

  /**
   * Get ticket information by code
   *
   * @param {string} ticketCode
   * @param ticketType
   */
  private getTicketInfo(ticketCode: number, ticketType: number): ActivityTicket {
    for(let index = 0; index < this.activity.types.length; index++) {
      if (this.activity.types[index].id === ticketType) {
        for (let index2 = 0; index2 < this.activity.types[index].tickets.length; index2++) {
          if (this.activity.types[index].tickets[index2].id === ticketCode) {
            return this.activity.types[index].tickets[index2]
          }
        }
      }
    }
    return null;
  }

  /**
   * Retrieve activity type name
   * @returns {string}
   */
  private getActivityTypeName(ticketType: number): string {
    // console.log(ticketType, this.activity);
    for(let index = 0; index < this.activity.types.length; index++) {
      if (this.activity.types[index].id === ticketType) {
        return this.activity.types[index].name;
      }
    }
    return '';
  }

  /**
   * Convert data for storage
   */
  public toStorage(): BaskedStoredItem {
    return {
      activityId: this.activity.id,
      order: this.order
    }
  }

}

export interface BasketTicketData {
  activityType: string;
  ticketNames: string[];
  people: number;
  total: number;
}

export interface BaskedStoredItem {
  activityId: number,
  order: ActivityOrder
}
