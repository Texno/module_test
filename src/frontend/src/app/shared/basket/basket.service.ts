import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BaskedStoredItem, BasketItem } from '@shared/basket/data-types/basket-item';
import { Observable } from 'rxjs/Observable';
import { ActivityFull, ActivityOrder } from '@shared/backend/data-types/activity.types';
import { AuthService } from '@shared/auth/auth.service';
import { RestApiService } from '@shared/backend/rest-api.service';
import { StorageService } from '@shared/basket/storage.service';
import { BasketOrder } from '@shared/basket/data-types/basket-order';


@Injectable()
export class BasketService {

  private static readonly MAX_BASKET_SIZE = 100;

  private basketSubject: BehaviorSubject<BasketItem[]>;
  private orderSubject: BehaviorSubject<BasketOrder>;
  private basket = new Map<number, BasketItem>();
  private order: BasketOrder = null;

  private beforeCheckoutUrl: string = null;

  constructor(private auth: AuthService, private rest: RestApiService, private storage: StorageService) {
    this.basketSubject= new BehaviorSubject<BasketItem[]>([]);
    this.orderSubject= new BehaviorSubject<BasketOrder>(null);
    this.auth.authStatusChange().subscribe(() => {
      this.loadFromStorage();
    });
    this.getBasketObservable().subscribe(items => this.fullFillOrder(items));
  }

  /**
   * Load data from storage if available
   */
  private loadFromStorage() {
    let userId = 0;
    if (this.auth.isAuthenticated()) {
      userId = this.auth.getTokenData().userId;
    }
    let items = this.storage.loadItems(userId);
    let restPromises = [];
    this.basket.clear();
    let index = 0;
    items.forEach(item => restPromises.push(this.fullFillActivity.bind(this, index++)(item)));
    Promise.all(restPromises).then((result) => {
      result.forEach(item => this.basket.set(item.id, item));
      this.basketSubject.next(result);
    });
    this.order = this.storage.loadOrder(userId);
    this.orderSubject.next(this.order);
  }

  /**
   * Convert stored basket item to normal
   *
   * @param index
   * @param {BaskedStoredItem} item
   * @returns {Promise<BasketItem>}
   */
  private async fullFillActivity(index: number, item: BaskedStoredItem): Promise<BasketItem> {
    let activity = await this.rest.getActivityFull(item.activityId);
    return new BasketItem(index, activity, item.order);
  }

  /**
   * Save data to storage
   */
  private saveToStorage() {
    let userId = 0;
    if (this.auth.isAuthenticated()) {
      userId = this.auth.getTokenData().userId;
    }
    const items = this.getBasketCurrent();
    let itemsToStore = [];
    items.forEach(item => {
      itemsToStore.push(item.toStorage())
    });
    this.storage.saveItems(userId, itemsToStore);
  }

  /**
   * Update subject value
   */
  private updateSubject() {
    const iterator = this.basket.values();
    const items = [];
    while(true) {
      const result = iterator.next();
      if (result.done) break;
      items.push(result.value);
    }
    this.basketSubject.next(items);
  }

  /**
   * Get current basket items
   * @returns {BasketItem[]}
   */
  public getBasketCurrent(): BasketItem[] {
    return this.basketSubject.getValue();
  }

  /**
   * Get observable to subscribe basket changes
   * @returns {Observable<BasketItem[]>}
   */
  public getBasketObservable(): Observable<BasketItem[]> {
    return this.basketSubject.asObservable();
  }

  /**
   * Add item to basket
   *
   * @param {ActivityFull} activity
   * @param {ActivityOrder} order
   * @returns {boolean}
   */
  public addItem(activity: ActivityFull, order: ActivityOrder): BasketItem {
    let index = 0;
    const basketItem = new BasketItem(index, activity, order);
    while (true) {
      if (!this.basket.has(index)) {

        this.basket.set(index, basketItem);
        this.updateSubject();
        this.saveToStorage();
        break;
      }
      if (index++ > BasketService.MAX_BASKET_SIZE) {
        throw new RangeError("Maximum basket size is exceeded");
      }
      basketItem.id = index;
    }
    return basketItem;
  }

  /**
   * Remove item from basket
   *
   * @param {number} id
   * @returns {boolean}
   */
  public removeItem(id: number): boolean {
    const result = this.basket.delete(id);
    if (result) {
      this.updateSubject();
      this.saveToStorage();
    }
    return result;
  }

  /**
   * Update order by new values
   */
  private fullFillOrder(items: BasketItem[]) {
    if (this.order) {
      let orderItems:ActivityOrder[] = [];
      items.forEach(item => {
        orderItems.push(item.order);
      });
      this.order.items = orderItems;
    }
  }

  /**
   * Retrieve order
   * @returns {BasketOrder}
   */
  public getOrder(): BasketOrder {
    return this.order
  }

  /**
   * Get observable with order changes
   *
   * @returns {Observable<BasketOrder>}
   */
  public getOrderObservable(): Observable<BasketOrder> {
    return this.orderSubject.asObservable();
  }

  /**
   * Get order or create new if unavailable
   *
   * @returns {BasketOrder}
   */
  public getOrCreateOrder(): BasketOrder {
    let order = this.getOrder();
    if (order) {
      return order;
    }
    this.order = new BasketOrder();
    this.fullFillOrder(this.getBasketCurrent());
    if (this.auth.isAuthenticated()) {
      this.order.fillFromUserData(this.auth.getTokenData());
    }
    return this.order;
  }

  /**
   * Save order
   */
  public saveOrder() {
    if(this.order) {
      let userId = 0;
      if (this.auth.isAuthenticated()) {
        userId = this.auth.getTokenData().userId;
      }
      this.storage.saveOrder(userId, this.order);
      this.orderSubject.next(this.order);
      this.saveToStorage();
    }
  }

  /**
   * Clear data after order complete
   */
  public clear() {
    let userId = 0;
    if (this.auth.isAuthenticated()) {
      userId = this.auth.getTokenData().userId;
    }
    this.storage.clearItems(userId);
    this.storage.clearOrder(userId);
    this.order = null;
    this.basket.clear();
    this.updateSubject();
  }

  /**
   * Save route to go back
   * @param {string} url
   */
  public saveBeforeCheckoutRoute(url: string) {
    this.beforeCheckoutUrl = url;
  }

  /**
   * Get 'back' route
   * @returns {string}
   */
  public getBeforeCheckoutRoute(): string {
    return this.beforeCheckoutUrl;
  }
}
