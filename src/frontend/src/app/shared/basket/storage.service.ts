import { Injectable } from '@angular/core';
import { BaskedStoredItem } from '@shared/basket/data-types/basket-item';
import { BasketOrder } from '@shared/basket/data-types/basket-order';

@Injectable()
export abstract class StorageService {

  /**
   * Save items to storage
   *
   * @param {number} userId
   * @param {BaskedStoredItem[]} items
   * @returns {boolean}
   */
  public abstract saveItems(userId: number, items: BaskedStoredItem[]): boolean;

  /**
   * Save current order to storage
   *
   * @param {number} userId
   * @param {BasketOrder} order
   * @returns {boolean}
   */
  public abstract saveOrder(userId: number, order: BasketOrder): boolean;

  /**
   * Load items from storage
   *
   * @param {number} userId
   * @returns {BaskedStoredItem[]}
   */
  public abstract loadItems(userId: number): BaskedStoredItem[];

  /**
   * Load current order from storage
   *
   * @param {number} userId
   * @returns {BasketOrder}
   */
  public abstract loadOrder(userId: number): BasketOrder;

  /**
   * Clear items data
   * @param {number} userId
   * @returns {boolean}
   */
  public abstract clearItems(userId: number): boolean;

  /**
   * Clear order data
   * @param {number} userId
   * @returns {boolean}
   */
  public abstract clearOrder(userId: number): boolean;

}
