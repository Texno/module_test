import { Injectable } from '@angular/core';
import { BaskedStoredItem } from '@shared/basket/data-types/basket-item';
import { StorageService } from '@shared/basket/storage.service';
import { environment } from '../../../environments/environment';
import { BasketOrder } from '@shared/basket/data-types/basket-order';

@Injectable()
export class LocalStorageService extends StorageService {

  private static readonly STORAGE_BASKET_KEY = 'basket';
  private static readonly STORAGE_ORDER_KEY = 'order';

  /**
   * Build storage basket key
   *
   * @param {number} userId
   * @returns {string}
   */
  private static buildStorageBasketKey(userId: number) {
    return LocalStorageService.STORAGE_BASKET_KEY + userId;
  }

  /**
   * Build storage order key
   *
   * @param {number} userId
   * @returns {string}
   */
  private static buildStorageOrderKey(userId: number) {
    return LocalStorageService.STORAGE_ORDER_KEY + userId;
  }

  /**
   * @inheritDoc
   */
  public saveItems(userId: number, items: BaskedStoredItem[]): boolean {
    if (environment.isServer) {
      return false;
    }
    localStorage.setItem(LocalStorageService.buildStorageBasketKey(userId), JSON.stringify(items));
  };

  /**
   * @inheritDoc
   */
  public loadItems(userId: number): BaskedStoredItem[] {
    if (environment.isServer) {
      return [];
    }
    const data = localStorage.getItem(LocalStorageService.buildStorageBasketKey(userId));
    if (!data) {
      return [];
    }
    return JSON.parse(data);
  };

  /**
   * @inheritDoc
   */
  public saveOrder(userId: number, order: BasketOrder): boolean {
    if (environment.isServer) {
      return false;
    }
    localStorage.setItem(LocalStorageService.buildStorageOrderKey(userId), JSON.stringify(order));
  }

  /**
   * @inheritDoc
   */
  public loadOrder(userId: number): BasketOrder {
    if (environment.isServer) {
      return null;
    }
    const data = localStorage.getItem(LocalStorageService.buildStorageOrderKey(userId));
    if (!data) {
      return null;
    }
    return JSON.parse(data);
  }

  /**
   * @inheritDoc
   */
  public clearItems(userId: number): boolean {
    if (environment.isServer) {
      return false;
    }
    localStorage.removeItem(LocalStorageService.buildStorageBasketKey(userId));
    return true;
  }

  /**
   * @inheritDoc
   */
  public clearOrder(userId: number): boolean {
    if (environment.isServer) {
      return false;
    }
    localStorage.removeItem(LocalStorageService.buildStorageOrderKey(userId));
    return true;
  }
}
