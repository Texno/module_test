import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { BasketService } from '@shared/basket/basket.service';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.styl']
})
export class BasketComponent implements OnInit {

  isOpened = false;
  items: BasketItem[];
  private oldClass = '';

  constructor(private basket: BasketService, private router: Router, private location: Location, private ga: GoogleTracking) { }

  @ViewChild('hostTarget') hostTarget;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.hostTarget.nativeElement.contains(targetElement)) {
      // Clicked outside of basket
      if(!targetElement.hasAttribute('close-basket-item')) {
        // this is not delete item element
        this.isOpened = false;
      }

    }
  }

  ngOnInit() {
    this.basket.getBasketObservable().subscribe(items => this.items = items);
  }

  getTotal() {
    let total = 0;
    this.items.forEach(item => total += item.getTicketData().total);
    return total;
  }

  deleteItem(id: number) {
    this.basket.removeItem(id);
  }

  fireEvent() {
    this.ga.fireEvent(GoogleTracking.TOP_NAVBAR_CATEGORY, 'Basket', '/');
  }

  redirectToCheckout(){
    this.basket.saveBeforeCheckoutRoute(this.location.path());
    this.router.navigate(['/checkout/review']);
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }
  isOpenedDr(){
    this.isOpened = !this.isOpened; 
    this.fireEvent();
    if(!document.body.className.split(' ').includes('fixed')){
      document.body.className += ' ' + 'fixed';;
    }
    else if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }
  
}
