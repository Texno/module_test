import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BasketItem } from '@shared/basket/data-types/basket-item';

@Component({
  selector: 'app-basket-item',
  templateUrl: './basket-item.component.html',
  styleUrls: ['./basket-item.component.styl']
})
export class BasketItemComponent implements OnInit {

  @Input() item: BasketItem;
  @Output() onDeleted = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  delete() {
    this.onDeleted.next(this.item.id);
  }

}
