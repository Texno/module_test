import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketService } from '@shared/basket/basket.service';
import { BasketComponent } from './component/basket/basket.component';
import { BasketItemComponent } from './component/basket-item/basket-item.component';
import { StorageService } from '@shared/basket/storage.service';
import { LocalStorageService } from '@shared/basket/local-storage.service';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [BasketComponent, BasketItemComponent],
  exports: [BasketComponent],
  providers: [
    BasketService,
    {provide: StorageService, useClass: LocalStorageService}
  ]
})
export class BasketModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: BasketModule,
      providers: [BasketService]
    };
  }
}
