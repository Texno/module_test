import { Component, HostListener, Input, OnInit, AfterViewChecked, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity, ActivityFull, ActivityOrder } from '@shared/backend/data-types/activity.types';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RelatedActivitiesComponent } from '@shared/booking-calendar/related-activities/related-activities.component';
import { BasketService } from '@shared/basket/basket.service';
import { BookingCalendarService } from '@shared/booking-calendar/booking-calendar.service';
import { Review } from '@shared/backend/data-types/review.types';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { RouteHelper } from '@shared/helpers/route-helper';
import { AuthService } from '@shared/auth/auth.service';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Subject } from 'rxjs/Subject';
import { PlatformLocation } from '@angular/common'

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.styl']
})
export class ActivityComponent implements OnInit, AfterViewChecked {

  private static readonly CALENDAR_TOP_POSITION = 525;
  private static CALENDAR_BOT_POSITION;

  @Input() forAdmin = false;
  activityInfo: ActivityFull;
  relatedActivities: Activity[] = [];
  reviews: Review[] = [];
  reviewsFull: Review[] = [];
  isLoading: boolean;
  openSidebar = false;

  relatedModal: NgbModalRef;
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];

  routeHelper = RouteHelper;

  // mobile block statuses
  open1status = false;
  open2status = false;
  open3status = false;
  open4status = false;
  open5status = false;
  open6status = false;

  fixed = false;
  absolute = false;

  private oldClass = ''; 

  private scrollPosition = new Subject<number>();

  constructor(private rest: RestApiService,
              private route: ActivatedRoute,
              private router: Router,
              private sanitizer: DomSanitizer,
              private basket: BasketService,
              private modalService: NgbModal,
              private calendarService: BookingCalendarService,
              private ga: GoogleTracking,
              location: PlatformLocation) {
    this.isLoading = true;
    location.onPopState(() => {

        console.log('pressed back!');
        if(document.body.className.split(' ').includes('fixed')){
          document.body.className = this.oldClass;
        }
        
    });
  }


  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.scrollPosition.next(window.scrollY);
  }

  @ViewChild('sideBar') sideBar;

  @HostListener('document:click', ['$event.target']) onClick(targetElement) {
    if(!this.sideBar.nativeElement.contains(targetElement) && !targetElement.hasAttribute('open-sidebar')) {
      this.openSidebar = false;
      document.body.className = this.oldClass;
    }
  }

  ngAfterViewChecked() {
    const sidebar = document.getElementById('js-sidebar');
    const sidebarWrap = document.getElementById('js-scroll-wrapper');

    if (sidebarWrap && sidebar) {
      const sidebarWrapTop = sidebarWrap.getBoundingClientRect().top + window.pageYOffset;
      const fixEnd = sidebarWrapTop + sidebarWrap.clientHeight - sidebar.clientHeight - 137;

      ActivityComponent.CALENDAR_BOT_POSITION = fixEnd;
    }
  }

  ngOnInit() {
    this.oldClass = document.body.className;
    document.body.className = this.oldClass;
    
    this.scrollPosition.asObservable().debounceTime(2).subscribe(number => {
      this.fixed = number > ActivityComponent.CALENDAR_TOP_POSITION;
      this.absolute = number > ActivityComponent.CALENDAR_BOT_POSITION;
    });

    this.galleryOptions = [
      {
        width: '100%',
        height: '100%',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        imageInfinityMove: true
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20,
        imageInfinityMove: true
      },
      {
        breakpoint: 400,
        preview: false,
        imageInfinityMove: true
      }
    ];

    this.route.fragment.subscribe(fragment => {
      
    });

    this.route.paramMap.subscribe(params => {
      if(( params.has('country') && params.has('city') && params.has('activity'))
        || params.has('id'))
      {
        this.isLoading = true;
        let activityInfo: Promise<ActivityFull>;
        if (params.has('id')) {
          activityInfo = this.rest.getActivityFull(parseInt(params.get('id')), this.forAdmin);
        } else {
          activityInfo = this.rest.getActivityFullRoute(params.get('country'), params.get('city'), params.get('activity'));
        }
        activityInfo.then(activity => {
          (<any>window).dataLayer.push({'content_grouping_1' : 'activity_page'});
          this.ga.fireProductDetails(activity);
          this.activityInfo = activity;
          this.activityInfo.meeting_point_longitude = parseFloat(this.activityInfo.meeting_point_longitude.toString());
          this.activityInfo.meeting_point_latitude = parseFloat(this.activityInfo.meeting_point_latitude.toString());
          this.galleryImages = [];
          activity.gallery.forEach(galleryItem => {
            this.galleryImages.push({
              small: galleryItem.path,
              medium: galleryItem.path,
              big: galleryItem.path,
              description: galleryItem.description
            })
          });
          const relatedActivities = this.rest.getRelatedActivities(activity.id).then(activities => this.relatedActivities = activities);
          const reviewsInfo = this.rest.getReviewsForActivity(activity.id, false, true).then(reviews => {
            this.reviewsFull = reviews;
            this.reviews = reviews.slice(0, 3);
          });

          //Wait until all data has been loaded
          Promise.all([
            relatedActivities,
            reviewsInfo
          ]).then(() => this.isLoading = false);
        }).catch(err => {
          if (err.hasOwnProperty('status') && err.status == 404) {
            this.router.navigate([AuthService.FALLBACK_URL]);
          }
          console.log(err);
        });
      }
    });
  }

  /**
   * Perform add order operation
   * @param {ActivityOrder} order
   */
  book(order: ActivityOrder) {
    const item = this.basket.addItem(this.activityInfo, order);
    this.calendarService.placeOrder(order);
    this.router.navigate(['/checkout']);
    this.ga.fireAddToCard(item);
  }

  navigateToFragment(fragment) {
    this.ga.fireEvent(GoogleTracking.SECOND_NAV_MENU_CATEGORY, fragment.name , this.router.url);
    this.router.navigate([], {fragment: fragment.id});
    let el = document.getElementById(fragment.id);
    if(null !== el) {
      const headerHeight = (window.scrollY < 525) ? 211 : 140;
      const top = el.getBoundingClientRect().top + window.pageYOffset - headerHeight;
      window.scroll({top: top, behavior: "smooth"})
    }
  }

  fireEvent(name: string) {
    this.ga.fireEvent(GoogleTracking.RELATED_ACTIVITY_CATEGORY, name, this.router.url);
  }

  scrollToCalendar() {
    window.scroll({top: ActivityComponent.CALENDAR_TOP_POSITION, behavior: "smooth"})
  }

  openSideBar(){

    this.openSidebar = true;
    this.oldClass = document.body.className;
    if(!document.body.className.split(' ').includes('fixed')){
      document.body.className += ' ' + 'fixed';
    }
  }
  closeSideBar(){
    this.openSidebar = false;
    document.body.className = this.oldClass;
    if(document.body.className.split(' ').includes('fixed')){
      document.body.className = this.oldClass;
    }
  }
   
}
