import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { ComponentsModule } from '@shared/components/components.module';
import { AppConfig } from '@shared/config/app-config.service';
import { NgxGalleryModule } from 'ngx-gallery';
import { ActivityComponent } from '@shared/activity/activity.component';
import { HeroComponent } from '@shared/activity/components/hero/hero.component';
import { BannerComponent } from '@shared/activity/components/banner/banner.component';
import { NavigationComponent } from '@shared/activity/components/navigation/navigation.component';
import { ReviewsComponent } from '@shared/activity/components/reviews/reviews.component';
import { DetailsComponent } from '@shared/activity/components/details/details.component';
import { RouterModule } from '@angular/router';
import { ScrollbarModule } from 'ngx-scrollbar';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    ComponentsModule,
    BookingCalendarModule,
    NgxGalleryModule,
    AgmCoreModule.forRoot({
      apiKey: AppConfig.getConfiguration().maps.google
    }),
    ScrollbarModule
  ],
  declarations: [ActivityComponent, HeroComponent, BannerComponent, NavigationComponent, ReviewsComponent, DetailsComponent],
  exports: [ActivityComponent]
})
export class ActivityModule { }
