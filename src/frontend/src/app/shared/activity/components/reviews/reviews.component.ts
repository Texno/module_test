import { Component, Input } from '@angular/core';
import { Review } from '@shared/backend/data-types/review.types';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.styl']
})
export class ReviewsComponent {

  static readonly PREVIEW_LIMIT = 1;

  _reviews: Review[] = [];
  reviewsToShow: Review[] = [];
  rate = 0;
  buttonText = 'Ver Más';

  @Input() set reviews(reviews: Review[]) {
    this._reviews = reviews;
    this.reviewsToShow = this._reviews.slice(0, ReviewsComponent.PREVIEW_LIMIT);
    this._reviews.forEach(review => this.rate += +review.rate);
    this.rate /= this._reviews.length;
  }

  constructor() { }

  toggleShow() {
    if (this.reviewsToShow.length == ReviewsComponent.PREVIEW_LIMIT) {
      this.reviewsToShow = this._reviews;
      this.buttonText = 'Esconder';
    } else {
      this.reviewsToShow = this._reviews.slice(0, ReviewsComponent.PREVIEW_LIMIT);
      this.buttonText = 'Ver Más';
    }

  }

}
