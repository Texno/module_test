import { Component, Input, OnInit } from '@angular/core';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { FormatHelper } from '@shared/helpers/format-helper';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.styl']
})
export class HeroComponent implements OnInit {

  @Input() activity: ActivityFull;

  formatHelper = FormatHelper;

  constructor() { }

  ngOnInit() {
  }

}
