import { Component, Input, OnInit } from '@angular/core';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { FormatHelper } from '@shared/helpers/format-helper';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.styl']
})
export class DetailsComponent implements OnInit {

  @Input() activityInfo: ActivityFull;

  formatHelper = FormatHelper;

  constructor() { }

  ngOnInit() {
  }

}
