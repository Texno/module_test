import { AfterViewInit, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.styl']
})
export class NavigationComponent implements AfterViewInit {

  readonly items = [
    {id: 'desc',    name: 'Descripción'},
    {id: 'prices',  name: 'Precios'},
    {id: 'details', name: 'Detalles'},
    {id: 'meeting', name: 'Punto de Encuentro'},
    {id: 'cancel',  name: 'Cancelaciones'},
    {id: 'review',  name: 'Opiniones'}
  ];

  selectedItem = this.items[0];

  @Output() onMenuChanged = new EventEmitter<string>();
  @Output() onBookClick = new EventEmitter<boolean>();

  constructor() { }

  ngAfterViewInit() {
  }

  clickMenu(item) {
    this.selectedItem = item;
    this.onMenuChanged.next(item);
  }

}
