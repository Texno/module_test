import { Component, OnInit } from '@angular/core';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private ga: GoogleTracking) { }

  ngOnInit() {
  }

  fireEvent() {
    this.ga.fireEvent(GoogleTracking.TOP_NAVBAR_CATEGORY, 'CIVITOURS', this.router.url);
  }

}
