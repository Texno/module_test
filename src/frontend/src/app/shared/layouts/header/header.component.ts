import { Component, HostListener } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.styl'],
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  classAddition = '';

  private static readonly listUnscrolled = [];


  constructor(private router: Router, private ga: GoogleTracking) {
    router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        if (-1 == route.url.indexOf('#')) {
          window.scroll(0,0);
        }
        const url = route.url.split('?')[0];
        if (url === '/') {
          this.classAddition = '';
          return;
        }
        for (let index = 0; index < HeaderComponent.listUnscrolled.length; index ++) {
          if (-1 !== url.indexOf(HeaderComponent.listUnscrolled[index])) {
            this.classAddition = '';
            return;
          }
        }
        this.classAddition = 'head-top--scroll';
      }
    });
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = window.scrollY;
    if (number > window.innerHeight ) {
      this.classAddition = 'head-top--scroll';
    } else if (this.router.url == '/') {
      this.classAddition = '';
    }
  }


  fireEvent(name: string) {
    this.ga.fireEvent(GoogleTracking.TOP_NAVBAR_CATEGORY, name, this.router.url);
  }
}
