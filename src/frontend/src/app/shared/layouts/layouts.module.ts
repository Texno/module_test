import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FooterComponent } from './footer/footer.component';
import { FooterComponent as CheckoutFooterComponent } from './checkout/footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HeaderComponent as CheckoutHeaderComponent } from './checkout/header/header.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { WrapperComponent as CheckoutWrapperComponent } from './checkout/wrapper/wrapper.component';
import { AuthModule } from '@shared/auth/auth.module';
import { BasketModule } from '@shared/basket/basket.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AuthModule,
    BasketModule,
    TranslateModule.forChild()
  ],
  declarations: [
    FooterComponent,
    CheckoutFooterComponent,
    HeaderComponent,
    CheckoutHeaderComponent,
    WrapperComponent,
    CheckoutWrapperComponent
  ]
})
export class LayoutsModule {
}
