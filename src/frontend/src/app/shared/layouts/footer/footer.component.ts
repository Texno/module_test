import { Component } from '@angular/core';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  styleUrls: ['./footer.component.styl'],
  templateUrl: './footer.component.html'
})
export class FooterComponent {

  openNewsletter = false;
  open1menu= false;
  open2menu= false;
  open3menu= false;
  open4menu= false;

  constructor(private router: Router, private ga: GoogleTracking) {}

  fireEvent(category: string, name: string) {
    if (!name) {
      name = this.router.url;
    }
    this.ga.fireEvent(GoogleTracking.BOTTOM_NAVBAR_CATEGORY, category, name);
  }

}
