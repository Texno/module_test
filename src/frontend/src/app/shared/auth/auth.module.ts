import { Inject, ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './components/signin/signin.component';
import { MenuComponent } from './components/menu/menu.component';
import { AuthService } from '@shared/auth/auth.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { AdminGuard, AuthGuard } from '@shared/auth/auth-guard';
import { ResetComponent } from './components/reset/reset.component';
import { NewPasswordComponent } from './components/new-password/new-password.component';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular5-social-login";
import { AppConfig } from '@shared/config/app-config.service';
import { AppStorage } from '@shared/for-storage/universal.inject';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from '@shared/auth/auth.interceptor';

// Configs
export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider(AppConfig.getConfiguration().socialLogin.facebook)
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider(AppConfig.getConfiguration().socialLogin.google)
      },
    ]);
}

export function jwtOptionsFactory(appStorage) {
  return {
    tokenGetter: () => {
      return appStorage.getItem(AuthService.TOKEN_KEY);
    }
  }
}

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule,
    SocialLoginModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [AppStorage]
      }
    })
  ],
  declarations: [SigninComponent, MenuComponent, ResetComponent, NewPasswordComponent],
  entryComponents: [SigninComponent, ResetComponent, NewPasswordComponent],
  providers: [
    AuthService,
    AuthGuard,
    AdminGuard,
    { provide: AuthServiceConfig, useFactory: getAuthServiceConfigs },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  exports: [MenuComponent]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [AuthService, AuthGuard, AdminGuard]
    };
  }
}


