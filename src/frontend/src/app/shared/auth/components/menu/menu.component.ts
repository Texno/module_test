import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/auth/auth.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SigninComponent } from '../signin/signin.component';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ResetComponent } from '@shared/auth/components/reset/reset.component';
import { NewPasswordComponent } from '@shared/auth/components/new-password/new-password.component';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-login-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.styl']
})
export class MenuComponent implements OnInit {

  private openedDialog: NgbModalRef;

  constructor(public auth: AuthService,
              private modalService: NgbModal,
              private router: Router,
              private route: ActivatedRoute,
              private ga: GoogleTracking,
  ) { }

  ngOnInit() {

    // Operate on query parameters to open modal
    this.route.queryParams.subscribe(data => {
      window.setTimeout(() => { // set timeout is used to fix `expressionChangedAfterItHasBeenCheckedError`
        if (data.hasOwnProperty('modal')) {
          if(this.openedDialog) {
            this.openedDialog.close();
          }
          switch (data.modal) {
            case 'login':
              if (this.auth.isAuthenticated()) {
                this.removeModalQuery();
              } else {
                this.openedDialog = this.modalService.open(SigninComponent);
                this.openedDialog.result.catch(() => this.removeModalQuery());
              }
              break;

            case 'reset':
              if (this.auth.isAuthenticated()) {
                this.removeModalQuery();
              } else {
                this.openedDialog = this.modalService.open(ResetComponent, {
                  windowClass: 'modal--reset'
                });
                this.openedDialog.result.catch(() => this.removeModalQuery());
              }
              break;

            case 'newPassword':
              if (this.auth.isAuthenticated()) {
                this.removeModalQuery();
              } else {
                this.openedDialog = this.modalService.open(NewPasswordComponent, {
                  windowClass: 'modal--reset'
                });
                this.openedDialog.result.catch(() => this.removeModalQuery());
                if (data.hasOwnProperty('code')) {
                  this.openedDialog.componentInstance.code = data.code;
                }
              }
              break;
          }
        } else {
          if(this.openedDialog) {
            this.openedDialog.close();
            this.removeModalQuery();
          }
        }
      });
    });
  }

  /**
   * Remove query parameters from browser url string
   */
  private removeModalQuery() {
    this.router.navigate([], {queryParams: {}});
  }

  fireEvent(name: string, path: string) {
    this.ga.fireEvent(GoogleTracking.TOP_NAVBAR_CATEGORY, name, path);
  }
}
