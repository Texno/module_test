import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '@shared/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.styl']
})
export class ResetComponent implements OnInit {

  resetForm: FormGroup;

  checkFields = false;
  inProgress = false;
  resetError = '';

  constructor(public activeModal: NgbActiveModal, private auth: AuthService, private router: Router) {
    this.createForm();
  }

  /**
   * Create form class
   */
  private createForm() {
    this.resetForm = new FormGroup({
      email:    new FormControl('',   [Validators.required, Validators.email]),
    }, { updateOn: 'blur'});
  }

  ngOnInit() {
  }

  onSubmit() {
    this.checkFields = true;
    if (this.resetForm.invalid) {
      return;
    }
    this.inProgress = true;
    this.auth.resetRequest(this.resetForm.getRawValue().email)
      .then((res: any) => {
        this.inProgress = false;
        this.router.navigate(['/info/reset-requested']);
      })
      .catch(err => {
        this.inProgress = false;
        if (err.status == 404) {
          this.resetError = 'User not found';
        } else {
          this.resetError = err.error.message;
        }
      })
  }

  /**
   * Set focus to element
   * @param {string} id
   */
  setFocus(id: string) {
    document.getElementById(id).focus();
  }

  get email() { return this.resetForm.get('email'); }

}
