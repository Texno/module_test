import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@shared/auth/auth.service';
import {
  AuthService as SocialAuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  SocialUser
} from 'angular5-social-login';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.styl']
})
export class SigninComponent implements OnInit {

  loginForm: FormGroup;

  passwordType = 'password';

  checkFields = false;
  inProgress = false;
  loginError = '';

  openRightModalFlag = false;
  hideRightModalFlag = false;
  constructor(public activeModal: NgbActiveModal, private auth: AuthService, private socialAuthService: SocialAuthService) {
    this.createForm();
  }

  /**
   * Create form class
   */
  private createForm() {
    this.loginForm = new FormGroup({
      email:    new FormControl('',   [Validators.required, Validators.email]),
      password: new FormControl('',   [Validators.required]),
    }, { updateOn: 'blur'});
  }

  ngOnInit() {
  }

  /**
   * Submit data to server
   */
  onSubmit() {
    this.checkFields = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.inProgress = true;
    this.auth.login(this.loginForm.getRawValue().email, this.loginForm.getRawValue().password)
      .then(() => {
        this.inProgress = false;
        this.activeModal.dismiss();
      })
      .catch(err => {
        this.inProgress = false;
        this.loginError = err.error.message;
      })

  }

  /**
   * Set focus to element
   * @param {string} id
   */
  setFocus(id: string) {
    document.getElementById(id).focus();
  }

  get email()     { return this.loginForm.get('email'); }
  get password()  { return this.loginForm.get('password'); }

  /**
   * Sign in with social network account
   *
   * @param {string} socialPlatform
   */
  public socialSignIn(socialPlatform : string) {
    if(this.inProgress) return;
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData: SocialUser) => {
          this.inProgress = true;
          this.auth.socialLogin(userData)
            .then(() => {
              this.inProgress = false;
              this.activeModal.dismiss();
            })
            .catch(err => {
              this.inProgress = false;
              this.loginError = err.error.message;
            });
      });
  }
  openRightModal(){
    this.openRightModalFlag = true;
    this.hideRightModalFlag = false;
  }
  openLeftModal(){
    this.hideRightModalFlag = true;
    this.openRightModalFlag = false;
  }
  showPassword(){
    var showPassword = document.getElementById("showPassword");
    if(showPassword.getAttribute("type")=="password"){
        showPassword.setAttribute("type","text");
    } else {
        showPassword.setAttribute("type","password");
    }
  }
}
