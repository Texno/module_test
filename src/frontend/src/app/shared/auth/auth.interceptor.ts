import { Inject, Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';
import { AppStorage } from '@shared/for-storage/universal.inject';
import { AuthService } from '@shared/auth/auth.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(@Inject(AppStorage) private appStorage: Storage) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.appStorage.getItem(AuthService.TOKEN_KEY);
    if (token) {
      const authReq = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token)});
      return next.handle(authReq)
        .catch((error, caught) => {
          return Observable.throw(error);
        }) as any;
    } else {
      return next.handle(req)
        .catch((error, caught) => {
          return Observable.throw(error);
        }) as any;
    }

  }
}