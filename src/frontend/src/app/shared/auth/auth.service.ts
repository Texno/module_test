import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppStorage } from '@shared/for-storage/universal.inject';
import { AppConfig } from '@shared/config/app-config.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenData, TokenUserData } from '@shared/auth/data-types/token.data';
import { newPasswordForm } from '@shared/auth/data-types/new-password-form';
import { SocialUser } from 'angular5-social-login';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  static readonly TOKEN_KEY = 'token';
  static readonly FALLBACK_URL = '/';
  static readonly UNAUTHORIZED_URL = '/info/unauthorized';

  private apiUrl = null;

  private authStatus: BehaviorSubject<boolean>;

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService,
              private router: Router,
              @Inject(AppStorage) private appStorage: Storage) {
    this.apiUrl = AppConfig.getConfiguration().backendUrl;
    let token = appStorage.getItem(AuthService.TOKEN_KEY);
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      this.authStatus = new BehaviorSubject<boolean>(true);
    } else {
      this.authStatus = new BehaviorSubject<boolean>(false);
      appStorage.removeItem(AuthService.TOKEN_KEY);
    }
  }

  /**
   * Get observable for auth status change events
   *
   * @returns {Observable<boolean>}
   */
  public authStatusChange(): Observable<boolean> {
    return this.authStatus.asObservable();
  }

  /**
   * Get current login status
   *
   * @returns {boolean}
   */
  public isAuthenticated(): boolean {
    const token = this.appStorage.getItem(AuthService.TOKEN_KEY);
    return !this.jwtHelper.isTokenExpired(token);
  }

  /**
   * Check if user has admin role
   *
   * @returns {boolean}
   */
  public isAdmin(): boolean {
    const data = this.getTokenData();
    if(!data || !this.isAuthenticated()) {
      return false;
    }
    return data.isAdmin;
  }

  /**
   * Retrieve user data from token
   *
   * @returns {TokenUserData}
   */
  public getTokenData(): TokenUserData {
    const token = this.appStorage.getItem(AuthService.TOKEN_KEY);
    if(!token) {
      return null;
    }
    return this.jwtHelper.decodeToken(token).data;
  }

  /**
   * Log out user
   */
  public logOut() {
    this.appStorage.removeItem(AuthService.TOKEN_KEY);
    this.authStatus.next(false);
    this.router.navigate([AuthService.FALLBACK_URL])
  }


  /**
   * Login user to server
   *
   * @param {string} username
   * @param {string} password
   * @returns {Promise<void>}
   */
  public login(username: string, password: string) {
    let hash = btoa(username + ':' + password);
    const headers = new HttpHeaders('Authorization: Basic ' + hash);
    return this.http.get(this.apiUrl + 'login', {headers: headers})
      .toPromise()
      .then((res: any) => {
        this.appStorage.setItem(AuthService.TOKEN_KEY, res.token);
        this.authStatus.next(true);
      });
  }

  /**
   * Login user to server with social network
   *
   * @param {SocialLoginForm} form
   * @returns {Promise<void>}
   */
  public socialLogin(form: SocialUser) {
    return this.http.post(this.apiUrl + 'login/social', form)
      .toPromise()
      .then((res: any) => {
        this.appStorage.setItem(AuthService.TOKEN_KEY, res.token);
        this.authStatus.next(true);
      });
  }

  /**
   * Request resetting email
   *
   * @param {string} email
   * @returns {Promise<Object>}
   */
  public resetRequest(email: string) {
    return this.http.post(this.apiUrl + 'reset_request', {email: email}).toPromise();
  }

  /**
   * Update user password
   *
   * @param passwordForm
   * @returns {Promise<Object>}
   */
  public updatePassword(passwordForm) {
    return this.http.post(this.apiUrl + 'update_password', passwordForm).toPromise();
  }

  /**
   * Update user account
   *
   * @param accountForm
   * @returns {Promise<Object>}
   */
  public updateAccount(accountForm) {
    return this.http.post(this.apiUrl + 'update_account', accountForm).toPromise();
  }

  /**
   * Renew user token
   * @returns {Promise<void>}
   */
  public renewToken() {
    return this.http.get(this.apiUrl + 'renew_token')
      .toPromise()
      .then((res: any) => {
        this.appStorage.setItem(AuthService.TOKEN_KEY, res.token);
        this.authStatus.next(true);
      });
  }

  /**
   * Set new password
   *
   * @param {newPasswordForm} form
   * @returns {Promise<Object>}
   */
  public setNewPassword(form: newPasswordForm) {
    return this.http.post(this.apiUrl + 'new_password', form).toPromise();
  }

}
