export interface TokenUserData {
  userId: number;
  userName: string;
  userSurname: string;
  userCountry: number;
  userCountryName: string;
  userCity: number;
  userCityName: string;
  fullName: string;
  email: string;
  phone: string;
  isAdmin: boolean;
}

export interface TokenData {
  data: TokenUserData,
  exp: number,
  iat: number,
  iss: string,
  nbf: number
}


