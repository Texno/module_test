import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeRoutes } from './home.routing';
import { HomeComponent } from './home.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { ComponentsModule } from '@shared/components/components.module';
import { BannerComponent } from './components/banner/banner.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutes,
    RouterModule,
    FormsModule,
    TranslateModule.forChild(),
    BookingCalendarModule,
    ComponentsModule,
    NgbModule
  ],
  declarations: [
    HomeComponent,
    TestimonialsComponent,
    BannerComponent
  ]
})
export class HomeModule { }
