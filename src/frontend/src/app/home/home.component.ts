import { Component, OnInit, Inject } from '@angular/core';

import { AppStorage } from '@shared/for-storage/universal.inject';
import { TransferHttpService } from '@shared/transfer-http';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-home',
  styleUrls: ['./home.component.styl'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  mainDestinationList: Promise<DestinationCity[]>;

  constructor(private http: TransferHttpService,
              private rest: RestApiService,
              @Inject(AppStorage) private appStorage: Storage) {
  }

  ngOnInit(): void {
    this.mainDestinationList = this.rest.getMainDestinationsList();
    (<any>window).dataLayer.push({'content_grouping_1' : 'homepage'});
  }
}
