import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutes } from './admin.routing';
import { GeoEditComponent } from './pages/geo-edit/geo-edit.component';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { EditCountryComponent } from './components/edit-country/edit-country.component';
import { EditCityComponent } from './components/edit-city/edit-city.component';
import { EditLocaleNameComponent } from './components/edit-locale-name/edit-locale-name.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActivityEditComponent } from './pages/activity-edit/activity-edit.component';
import { ActivityViewComponent } from './pages/activity-view/activity-view.component';
import { ComponentsModule } from '@shared/components/components.module';
import { EditPricesComponent } from './components/edit-prices/edit-prices.component';
import { EditGalleryComponent } from './components/edit-gallery/edit-gallery.component';
import { AgmCoreModule } from '@agm/core';
import { AppConfig } from '@shared/config/app-config.service';
import { EditCalendarComponent } from './components/edit-calendar/edit-calendar.component';
import { ActivityModule } from '@shared/activity/activity.module';
import { ActivityReviewsComponent } from './pages/activity-reviews/activity-reviews.component';
import { ReviewEditComponent } from './pages/review-edit/review-edit.component';
import { NgxEditorModule } from 'ngx-editor';
import { SuppliersComponent } from './pages/suppliers/suppliers.component';
import { SupplierEditComponent } from './pages/supplier-edit/supplier-edit.component';
import { EditSuppliersComponent } from './components/edit-suppliers/edit-suppliers.component';
import { OperationsComponent } from './pages/operations/operations.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { OperationViewComponent } from './pages/operation-view/operation-view.component';
import { CustomCurrencyPipe, CustomDatePipe } from './pages/operations/custom.pipes';
import { ReportsComponent } from './pages/reports/reports.component';
import { NewReportComponent } from './components/new-report/new-report.component';
import { ReportViewComponent } from './pages/report-view/report-view.component';
import { ReportFilterComponent } from './components/report-filter/report-filter.component';
import { NouisliderModule } from 'ng2-nouislider';
import { UploadReviewsComponent } from './components/upload-reviews/upload-reviews.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutes,
    FormsModule,
    NgbModule,
    ActivityModule,
    ComponentsModule,
    NouisliderModule,
    AgmCoreModule.forRoot({
      apiKey: AppConfig.getConfiguration().maps.google
    }),
    NgxEditorModule,
    NgxDatatableModule
  ],
  declarations: [
    AdminComponent,
    GeoEditComponent,
    ActivitiesComponent,
    EditCountryComponent,
    EditCityComponent,
    EditLocaleNameComponent,
    ActivityEditComponent,
    ActivityViewComponent,
    EditPricesComponent,
    EditGalleryComponent,
    EditCalendarComponent,
    ActivityReviewsComponent,
    ReviewEditComponent,
    SuppliersComponent,
    SupplierEditComponent,
    EditSuppliersComponent,
    OperationsComponent,
    OperationViewComponent,
    CustomDatePipe,
    CustomCurrencyPipe,
    ReportsComponent,
    NewReportComponent,
    ReportViewComponent,
    ReportFilterComponent,
    UploadReviewsComponent
  ],
  entryComponents: [
    NewReportComponent,
    UploadReviewsComponent
  ]
})
export class AdminModule { }
