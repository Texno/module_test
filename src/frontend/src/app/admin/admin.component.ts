import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.styl']
})
export class AdminComponent implements OnInit {

  breadcrumbs: string[] = [];

  constructor(router: Router) {
    router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        this.breadcrumbs = route.url.split('/').slice(1).map(v => v.charAt(0).toUpperCase() + v.slice(1));
      }
    });
  }

  ngOnInit() {
  }

}
