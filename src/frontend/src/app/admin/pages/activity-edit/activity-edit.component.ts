import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityFull, Category } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { of } from 'rxjs/observable/of';
import { ActivityForm } from '../../data-types/activity-form';
import { AgmMap } from '@agm/core';
import { UploaderComponent } from '@shared/components/uploader/uploader.component';

@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.styl']
})
export class ActivityEditComponent implements OnInit {

  isLoading = false;
  isSaving = false;
  activityInfo: ActivityFull;
  countries$: Promise<Country[]>;
  categories$: Promise<Category[]>;

  selectedCountryName = '';
  selectedCityName = '';

  heroUploadData: UploadResult;
  avatarUploadData: UploadResult;
  voucherUploadData: UploadResult;

  form = new ActivityForm();
  serverErrors = new ActivityForm();
  activityError = '';

  duration = 0;
  durationCoeff = 1;

  limit = 0;
  limitCoeff = 1;

  uploader = UploaderComponent;

  editorTabs = [
    { name: 'Included', prop: 'included'},
    { name: 'Not Included', prop: 'notIncluded'},
    { name: 'When To Book', prop: 'whenToBook'},
    { name: 'Ticket', prop: 'ticket'},
    { name: 'Accessibility', prop: 'accessibility'},
    { name: 'How To Book', prop: 'howToBook'},
    { name: 'Cancellation Description', prop: 'cancelationDescription'},
  ];

  selectedTab = 'included';

  toolConfig = [
    ["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
    ["fontSize", "color"],
    ["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
    ["cut", "copy", "delete", "removeFormat", "undo", "redo"],
    ["paragraph", "blockquote", "removeBlockquote", "horizontalLine", "orderedList", "unorderedList"],
    ["link", "unlink", "image"]
  ];

  @ViewChild('map') public map: AgmMap;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.countries$ = this.rest.getCountryList();
    this.categories$ = this.rest.getActivityCategoriesList();

    this.route.params.subscribe(params => {
      if(params.hasOwnProperty('id')) {
        this.isLoading = true;
        const activityInfo = this.rest.getActivityFull(params.id, true).then(activity => {
          this.activityInfo = activity;
          this.form.fillFromActivity(activity);
          this.selectedCountryName = activity.country_name;
          this.selectedCityName = activity.city_name;
          let data = this.parseMinuteToForm(this.form.duration);
          this.duration = data.value;
          this.durationCoeff = data.coeff;
          data = this.parseMinuteToForm(this.form.book_limit);
          this.limit = data.value;
          this.limitCoeff = data.coeff;
        });
        //Wait until all data has been loaded
        Promise.all([
          activityInfo,
        ]).then(() => this.isLoading = false);
      }
    });
  }

  changeDuration() {
    this.form.duration = (this.duration * this.durationCoeff).toString();
  }

  changeLimit() {
    this.form.book_limit = (this.limit * this.limitCoeff).toString();
  }

  /**
   * Coinvert minute value to hours/day + selector coeff
   * @param {string} source
   * @returns {{value: number; coeff: number}}
   */
  private parseMinuteToForm(source: string): {value: number, coeff: number} {
    let value: number;
    let coeff: number;
    if (parseInt(source) >= 1440) {
      let add = parseInt(source) % 1440;
      if (add > 0) {
        coeff=60;
        value = parseInt((parseInt(source) / 60).toFixed(0));
      } else {
        coeff=1440;
        value = parseInt((parseInt(source) / 1440).toFixed(0));
      }
    } else if (parseInt(source) >= 60) {
      let add = parseInt(source) % 60;
      if (add > 0) {
        coeff=1;
        value = parseInt(source);
      } else {
        coeff=60;
        value = parseInt((parseInt(source) / 60).toFixed(0));
      }
    } else {
      coeff=1;
      value = parseInt(source);
    }

    return {value: value, coeff: coeff}
  }

  /**
   * Save activity callback
   */
  saveActivity() {
    this.isSaving = true;
    const errorHandler = err => {
      this.isSaving = false;
      if (err.error.hasOwnProperty('errors')) {
        Object.assign(this.serverErrors, err.error.errors);
        this.activityError = 'Fo';
      }
      this.activityError = err.error.message;
    };
    const successHandler = () => {
      this.isSaving = false;
      this.activityError = '';
      this.serverErrors = new ActivityForm();
      this.router.navigate(['/admin/activities']);
    };
    if (this.activityInfo) {
      this.rest.updateActivity(this.activityInfo.id, this.form).then(successHandler).catch(errorHandler);
    } else {
      this.rest.createActivity(this.form).then(successHandler).catch(errorHandler);
    }
  }

  /**
   * Google map click handler
   * @param event
   */
  onMapClicked(event) {
    this.form.meetingPointLatitude = event.coords.lat;
    this.form.meetingPointLongitude = event.coords.lng;
  }

  /**
   * Callback on hero upload update
   * @param {UploadResult} uploadData
   */
  onHeroAvatarUpdate(uploadData: UploadResult) {
    this.heroUploadData = uploadData;
    this.form.avatar = uploadData.imageName;
  }

  /**
   * Callback on hero upload update
   * @param {UploadResult} uploadData
   */
  onVoucherImageUpdate(uploadData: UploadResult) {
    this.voucherUploadData = uploadData;
    this.form.voucher.image = uploadData.imageName;
  }

  /**
   * Callback on hero upload update
   * @param {UploadResult} uploadData
   */
  onAvatarUpdate(uploadData: UploadResult) {
    this.avatarUploadData = uploadData;
    this.form.avatar_small = uploadData.imageName;
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    const country: Country = event.item;
    this.form.country = country.id;
  };

  /**
   * Callback to filter cities
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any[]>}
   */
  searchCity = (text$: Observable<string>) => {
    return text$.debounceTime(200).distinctUntilChanged()
      .flatMap(searchText => {
        if (!this.form.country || !searchText.length) {
          return of([]);
        }
        return this.rest.searchCitiesByCountry(this.form.country, searchText);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  cityFormatter = (result: City) => {
    return result.name;
  };

  /**
   * City select handler
   * @param event
   */
  onSelectCity = (event) => {
    const city: City = event.item;
    this.form.city = city.id;
    this.form.meetingPointLatitude = parseFloat(city.latitude.toString());
    this.form.meetingPointLongitude = parseFloat(city.longitude.toString());
  };

  onNameChange (newName: string) {
    this.form.route_name = newName.toLowerCase().replace(/\s/g, '-').replace(/[^\w-]/g, '')
  }
}
