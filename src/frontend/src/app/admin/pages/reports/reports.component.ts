import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Report, ReportType } from '@shared/backend/data-types/report.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { CustomDatePipe } from '../operations/custom.pipes';
import { Subject } from 'rxjs/Subject';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NewReportComponent } from '../../components/new-report/new-report.component';
import {Router} from "@angular/router";
import { OrderAdminShort } from '@shared/backend/data-types/order.types';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.styl']
})
export class ReportsComponent implements OnInit, AfterViewInit {

  isLoading = true;
  docRequested = false;
  private reportsSource: Report[] = [];
  rows: Report[] = [];
  columns = [];
  searchSubject = new Subject<string>();

  types: string[] = [];
  statuses: string[] = [];

  search = '';
  type = '';
  status = '';

  inDelete: number[] = [];

  private openedDialog: NgbModalRef;

  constructor(private rest: RestApiService, private modalService: NgbModal, private router: Router) { }

  @ViewChild('linkTemplate') linkTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  ngOnInit() {
    this.loadReports();
    this.searchSubject.asObservable()
      .debounceTime(300)
      .subscribe(value => {
        this.search = value;
        this.applyFilter();
      });
  }

  ngAfterViewInit() {
    this.columns = [
      { name: 'ID', prop: 'id' },
      { name: 'Name', prop: 'name' },
      { name: 'Type', prop: 'report_type_name' },
      { name: 'Date of creation', prop: 'created_at', pipe: new CustomDatePipe() },
      { name: 'Status', prop: 'status' },
      { name: 'Links', cellTemplate: this.linkTemplate },
      { name: 'Action', cellTemplate: this.actionTemplate }
    ];
  }

  /**
   * Perform filtering
   */
  applyFilter() {
    this.rows = this.reportsSource.filter(report => {
      let statusFilter = !this.status.length || report.status == this.status;
      let typeFilter = !this.type.length || this.type == report.report_type_name;
      let searchString = this.search.toLowerCase();
      let searchFilter = !this.search.length
                      || (-1 !== report.id.toString().indexOf(searchString))
                      || (-1 !== report.name.toLowerCase().indexOf(searchString));
      return typeFilter && statusFilter && searchFilter;
    });
  }

  /**
   * Load reports list
   */
  private loadReports() {
    this.rest.getReports().then(reports => {
      this.reportsSource = reports;
      reports.forEach(report => {
        if(!this.types.includes(report.report_type_name)) this.types.push(report.report_type_name);
        if(!this.statuses.includes(report.status)) this.statuses.push(report.status);
      });
      this.applyFilter();
      this.isLoading = false;
    });
  }

  /**
   * Open dialog to create report
   */
  openNewReportDialog() {
    this.openedDialog = this.modalService.open(NewReportComponent);
    this.openedDialog.result
      .then(() => {
        this.isLoading = true;
        this.loadReports();
      })
      .catch(err => console.log(err));
  }

  /**
   * Redirect to report view page
   *
   * @param event
   */
  showReport(event) {
    if(this.docRequested) {
      this.docRequested = false;
    } else {
      if (event.length) {
         this.router.navigate([`/admin/report/${event[0].id}`]);
      }
    }
  }

  /**
   * Callback to check if row is selectable
   *
   * @param {OrderAdminShort} row
   * @returns {boolean}
   */
  checkSelectable = (row: Report) => {
    return 'ready' == row.status;
  };

  /**
   * Delete operation
   *
   * @param event
   * @param id
   */
  async delete(event, id: number) {
    event.stopPropagation();
    if (confirm('Are you sure?')) {
      this.inDelete.push(id);
      await this.rest.deleteReport(id);
      this.loadReports();
      const index = this.inDelete.indexOf(id);
      this.inDelete.splice(index, 1);
    }
  }

}
