import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomCurrencyPipe, CustomDatePipe } from '../operations/custom.pipes';

@Component({
  selector: 'app-report-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.styl']
})
export class ReportViewComponent implements OnInit {

  reportData = [];
  rows: any[] = [];
  columns = [];
  isLoading = true;

  filteredColumns = [
    {id: 'name',        type: 'list', additional: null},
    {id: 'city',        type: 'list', additional: null},
    {id: 'country',     type: 'list', additional: null},
    {id: 'suppliers',   type: 'array-list', additional: ';'},
    {id: 'created_at',  type: 'range', additional: null},
    {id: 'activities',  type: 'range', additional: null},
    {id: 'amount',      type: 'range', additional: null},
    {id: 'visitors',    type: 'range', additional: null},
  ];

  filters = {};

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.rest.getReportData(id)
      .then(report => {
        this.reportData = report.data;
        this.isLoading = false;
        this.buildHeader(report.header);
        this.applyFilter({});
      })
      .catch((err) => {
        alert('Cannot get report data');
        this.router.navigate(['/admin/reports']);
      });
  }

  /**
   * Fill the filter all filtered variants
   *
   * @param data
   */
  private populateFilterFromRow = (data: any) => {
    this.filteredColumns.forEach(filter => {
      if(data.hasOwnProperty(filter.id)) {
        switch (filter.type) {
          case 'list':
            this.populateFilterList(filter.id, data[filter.id]);
            break;
          case 'array-list':
            this.populateFilterArrayList(filter.id, data[filter.id], filter.additional);
            break;
          case 'range':
            this.populateFilterRange(filter.id, data[filter.id]);
            break;
        }
      }
    });
  };

  /**
   * Fill filter field
   *
   * @param column
   * @param data
   */
  private populateFilterList = (column, data) => {
    if (this.filters.hasOwnProperty(column)) {
      if (!this.filters[column].includes(data)) {
        this.filters[column].push(data);
      }
    } else {
      this.filters[column] = [data];
    }
  };

  /**
   * Fill filter array field
   *
   * @param column
   * @param data
   * @param splitter
   */
  private populateFilterArrayList = (column, data, splitter) => {
    const dataArray = data.split(splitter);
    dataArray.forEach(dataValue => {
      if (this.filters.hasOwnProperty(column)) {
        if (!this.filters[column].includes(dataValue) && dataValue.length) {
          this.filters[column].push(dataValue);
        }
      } else {
        this.filters[column] = [dataValue];
      }
    });
  };

  /**
   * Populate filter for range
   *
   * @param column
   * @param data
   */
  private populateFilterRange = (column, data) => {
    if (this.filters.hasOwnProperty(column)) {
      if (this.filters[column].min > data) {
        this.filters[column].min = parseInt(data);
      }
      if (this.filters[column].max < data) {
        this.filters[column].max = parseInt(data);
      }
    } else {
      this.filters[column] = {min: parseInt(data), max: parseInt(data)};
    }
  };

  private buildHeader(header) {
    this.columns = [];
    for (const id in header) {
      let definition: any = { name: header[id], prop: id};
      if (id == 'created_at') definition.pipe = new CustomDatePipe();
      if (id == 'amount') definition.pipe = new CustomCurrencyPipe();
      this.columns.push(definition);
    }
  }

  /**
   * Perform filtering
   */
  applyFilter(filter) {
    this.rows = this.reportData.filter(row => {
      let passed = true;
      // apply fields to filter
      for (const field in filter) {
          // 1. Find filter params for field
          let filterParams = null;
          for (let i = 0; i < this.filteredColumns.length; i++) {
            if (this.filteredColumns[i].id == field) {
              filterParams = this.filteredColumns[i];
              break;
            }
          }

          // 2. If filter parms are found apply them
          if (filterParams) {
            switch (filterParams.type) {
              case 'list':
                passed = passed && (row[field] === filter[field]);
                break;
              case 'array-list':
                const rowArray = row[field].split(filterParams.additional);
                passed = passed && (rowArray.includes(filter[field]));
                break;
              case 'range':
                passed = passed && (row[field] >= filter[field].low) && (row[field] <= filter[field].high);
                break;
            }
          }
        }
      return passed;
    });
    this.rows.forEach(this.populateFilterFromRow);
  }

}
