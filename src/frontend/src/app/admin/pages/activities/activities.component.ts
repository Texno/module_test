import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Activity } from '@shared/backend/data-types/activity.types';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UploadReviewsComponent } from '../../components/upload-reviews/upload-reviews.component';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.styl']
})
export class ActivitiesComponent implements OnInit {

  activitiesSource$: Promise<Activity[]>;
  filteredActivities: Activity[] = [];
  activities: Activity[] = [];
  isLoading = true;
  savingActivities: number[] = [];
  deletingActivities: number[] = [];

  readonly pageSize = 10;
  page = 1;
  search = '';

  constructor(private rest: RestApiService, private modalService: NgbModal) { }

  async ngOnInit() {
    this.activitiesSource$ = this.rest.getActivities(true);
    await this.updateActivities();
  }

  /**
   * Update activities list according to filtering
   */
  async updateActivities() {
    const activities = await this.activitiesSource$;
    this.isLoading = false;
    this.filteredActivities = activities.filter(v => {
      const searchText = this.search.toLowerCase();
      return (searchText == v.id.toString()) || (v.name.toLowerCase().indexOf(searchText) != -1)
    });
    this.activities = this.filteredActivities.slice((this.page -1) * this.pageSize, this.page * this.pageSize);
  }

  async approve(idActivity: number) {
    this.savingActivities.push(idActivity);
    await this.rest.setActivityDraftStatus(idActivity, false);
    this.activitiesSource$ = this.rest.getActivities(true);
    await this.updateActivities();
    const index = this.savingActivities.indexOf(idActivity);
    this.savingActivities.splice(index, 1);
  }

  async toDraft(idActivity: number) {
    this.savingActivities.push(idActivity);
    await this.rest.setActivityDraftStatus(idActivity, true);
    this.activitiesSource$ = this.rest.getActivities(true);
    await this.updateActivities();
    const index = this.savingActivities.indexOf(idActivity);
    this.savingActivities.splice(index, 1);
  }

  async delete(idActivity: number) {
    if (confirm('Are you sure?')) {
      this.deletingActivities.push(idActivity);
      await this.rest.deleteActivity(idActivity);
      this.activitiesSource$ = this.rest.getActivities(true);
      await this.updateActivities();
      const index = this.deletingActivities.indexOf(idActivity);
      this.deletingActivities.splice(index, 1);
    }

  }

  openUploadReview() {
    this.modalService.open(UploadReviewsComponent);
  }
}
