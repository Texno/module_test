import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Review } from '@shared/backend/data-types/review.types';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-activity-reviews',
  templateUrl: './activity-reviews.component.html',
  styleUrls: ['./activity-reviews.component.styl']
})
export class ActivityReviewsComponent implements OnInit {

  isLoading = true;

  // reviews list params
  private reviewSource$: Promise<Review[]>;
  reviews: Review[] = [];
  filteredReviews: Review[] = [];
  search = '';
  activityId: number;
  deletingReviews: number[] = [];
  savingReviews: number[] = [];

  // Pagination params
  readonly pageSize = 10;
  page = 1;

  constructor(private rest: RestApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(async params => {
      if (params.hasOwnProperty('id')) {
        this.reviewSource$ = this.rest.getReviewsForActivity(params.id, true);
        this.activityId = params.id;
        await this.updateReviews();
      }
    });
  }

  async updateReviews() {
    const reviews = await this.reviewSource$;
    this.isLoading = false;
    this.filteredReviews = reviews.filter(v => {
      const searchText = this.search.toLowerCase();
      return  (v.header.toLowerCase().indexOf(searchText) != -1)
              || (v.text.toLowerCase().indexOf(searchText) != -1)
              || (v.user_name.toLowerCase().indexOf(searchText) != -1)
    });
    this.reviews = this.filteredReviews.slice((this.page - 1) * this.pageSize, this.page * this.pageSize);
  }

  /**
   * Remove review
   *
   * @param idReview
   * @param index
   */
  async deleteReview(idReview, index) {
    if (confirm('¿Estás seguro?')) {
      try {
        this.deletingReviews.push(idReview);
        await this.rest.deleteReview(idReview);
        this.reviews.splice(index, 1);
        const idx = this.deletingReviews.indexOf(idReview);
        this.deletingReviews.splice(idx, 1);
      } catch (err) {
        alert(err.message);
      }
    }
  }

  /**
   * Change review approve status
   *
   * @param {number} idReview
   * @param status
   * @returns {Promise<void>}
   */
  async approve(idReview: number, status: boolean) {
    this.savingReviews.push(idReview);
    await this.rest.setReviewApproveStatus(idReview, status);
    this.reviewSource$ = this.rest.getReviewsForActivity(this.activityId, true);
    await this.updateReviews();
    const index = this.savingReviews.indexOf(idReview);
    this.savingReviews.splice(index, 1);
  }

}
