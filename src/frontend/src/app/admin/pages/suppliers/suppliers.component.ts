import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Supplier } from '@shared/backend/data-types/supplier.types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.styl']
})
export class SuppliersComponent implements OnInit {

  suppliersSource$: Promise<Supplier[]>;
  filteredSuppliers: Supplier[] = [];
  suppliers: Supplier[] = [];
  isLoading = true;

  readonly pageSize = 10;
  page = 1;
  search = '';

  deletingSuppliers: number[] = [];

  constructor(private rest: RestApiService, private router: Router) { }

  ngOnInit() {
    this.suppliersSource$ = this.rest.getSuppliers();
    this.updateSuppliers();
  }

  /**
   * Update activities list according to filtering
   */
  async updateSuppliers() {
    const suppliers = await this.suppliersSource$;
    this.isLoading = false;
    this.filteredSuppliers = suppliers.filter(v => {
      const searchText = this.search.toLowerCase();
      return (searchText == v.id.toString()) || (v.name.toLowerCase().indexOf(searchText) != -1)
    });
    this.suppliers = this.filteredSuppliers.slice((this.page -1) * this.pageSize, this.page * this.pageSize);
  }

  /**
   * Redirect to edit supplier with data
   *
   * @param {number} index
   */
  editSupplier(index: number) {
    let supplier = this.suppliers[index];
    this.router.navigateByData({
      url: [`/admin/suppliers/${supplier.id}/edit`],
      data: supplier
    });
  }

  /**
   * Remove review
   *
   * @param idSupplier
   * @param index
   */
  async deleteSupplier(idSupplier, index) {
    if (confirm('¿Estás seguro?')) {
      try {
        this.deletingSuppliers.push(idSupplier);
        await this.rest.deleteSupplier(idSupplier);
        this.suppliers.splice(index, 1);
        const idx = this.deletingSuppliers.indexOf(idSupplier);
        this.deletingSuppliers.splice(idx, 1);
      } catch (err) {
        alert(err.message);
      }
    }
  }

}
