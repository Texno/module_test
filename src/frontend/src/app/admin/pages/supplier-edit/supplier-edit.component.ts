import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SupplierForm } from '@shared/backend/data-types/supplier.types';

@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.styl']
})
export class SupplierEditComponent implements OnInit {

  isLoading = false;
  isSaving = false;

  form = new SupplierForm();
  serverErrors = new SupplierForm();
  tryToSubmit = false;
  supplierError = '';

  @ViewChild('submitForm') submitForm;

  private supplierId: number = null;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if(params.has('id')) {
        this.supplierId = parseInt(params.get('id'));
        const data = this.router.getNavigatedData();
        if('object' == typeof(data) && Object.keys(data).length) {
          // Check passed data to speedup loading page
          this.form.fillFromSupplier(data);
        } else {
          this.isLoading = true;
          // Load data from the rest
          this.rest.getSupplier(parseInt(params.get('id'))).then(supplier => {
            this.isLoading = false;
            this.form.fillFromSupplier(supplier);
          }).catch(err => {
            alert(err.error.message);
            this.router.navigate(['/admin/suppliers']);
          })
        }
      }
    });
  }

  onSubmit() {
    this.tryToSubmit = true;
    this.supplierError = '';
    if (!this.submitForm.valid) {
      return;
    }
    this.isSaving = true;
    let promise;
    if(this.supplierId) {
      promise = this.rest.updateSupplier(this.supplierId, this.form);
    } else {
      promise = this.rest.createSupplier(this.form);
    }
    promise.then(()=> {
      this.router.navigate(['/admin/suppliers'])
    }).catch(err => {
      this.isSaving = false;
      if (err.error.hasOwnProperty('errors')) {
        Object.assign(this.serverErrors, err.error.errors);
      }
      this.supplierError = err.error.message;
    });
  }

}
