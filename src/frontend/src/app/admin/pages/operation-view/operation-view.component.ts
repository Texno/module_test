import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderAdminFull } from '@shared/backend/data-types/order.types';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-operation-view',
  templateUrl: './operation-view.component.html',
  styleUrls: ['./operation-view.component.styl']
})
export class OperationViewComponent implements OnInit {

  isLoading = false;
  operation: OrderAdminFull;
  adminSaving = false;

  commentSubject = new Subject<string>();
  private id: number;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if(params.has('id')) {
        this.isLoading = true;
        this.id = parseInt(params.get('id'));
        this.rest.getOperation(this.id).then(operation => {
          this.isLoading = false;
          this.operation = operation;
        }).catch(err => {
          if (err.hasOwnProperty('error')) {
            alert(err.error.message);
          }
          this.router.navigate(['/admin/operations']);
        })
      }
    });

    this.commentSubject.debounceTime(500).subscribe(value => {
      this.adminSaving = true;
      this.rest.saveAdminNote(this.id, value)
        .then(() => this.adminSaving = false)
        .catch(err => {
          if (err.hasOwnProperty('error')) {
            alert(err.error.message);
            this.adminSaving = false
          }
        });
    })
  }

}
