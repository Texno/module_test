import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { OrderAdminShort } from '@shared/backend/data-types/order.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { CustomCurrencyPipe, CustomDatePipe } from './custom.pipes';
import { Subject } from 'rxjs/Subject';
import { AppConfig } from '@shared/config/app-config.service';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.styl']
})
export class OperationsComponent implements OnInit, AfterViewInit {

  @ViewChild('statusTemplate') statusTemplate: TemplateRef<any>;
  @ViewChild('actionTemplate') actionTemplate: TemplateRef<any>;

  isLoading = true;
  private operationsSource: OrderAdminShort[] = [];

  rows: OrderAdminShort[] = [];
  columns = [];
  searchSubject = new Subject<string>();

  statuses = [
    'paid',
    'confirmed',
    'cancelled',
    'refund'
  ];

  countries: string[] = [];
  cities: string[] = [];

  //filter
  search = '';
  country = '';
  city = '';
  status = '';

  inDelete: number[] = [];
  inChange: number[] = [];
  private stopSelectList: number[] = [];

  emailUrl = '';

  constructor(private router: Router, private rest: RestApiService) {
    this.emailUrl = AppConfig.getConfiguration().backendUrl + 'admin/operations/emails/unfinished';
  }

  ngOnInit() {
    this.rest.getOperations().then(operations => {
      this.operationsSource = operations;
      operations.forEach(op => {
        if(!this.countries.includes(op.country)) this.countries.push(op.country);
        if(!this.cities.includes(op.city)) this.cities.push(op.city);
      });
      this.applyFilter();
      this.isLoading = false;
    });
    this.searchSubject.asObservable()
      .debounceTime(300)
      .subscribe(value => {
        this.search = value;
        this.applyFilter();
      });
  }

  ngAfterViewInit() {
    this.columns = [
      { name: 'Número de reserva', prop: 'code' },
      { name: 'Actividad', prop: 'activity' },
      { name: 'País', prop: 'country' },
      { name: 'Ciudad', prop: 'city' },
      { name: 'Fecha de reserva', prop: 'created_at', pipe: new CustomDatePipe() },
      { name: 'Fecha de la actividad', prop: 'date' },
      { name: 'Precio', prop: 'price', pipe: new CustomCurrencyPipe() },
      { name: 'Status', prop: 'status', cellTemplate: this.statusTemplate },
      { name: 'Action', cellTemplate: this.actionTemplate }
    ];
  }

  onSelect(event) {
    if(event.length) {
      this.router.navigate([`admin/operation/${event[0].id}`]);
    }
  }

  /**
   * Change order status handler
   *
   * @param event
   * @param {OrderAdminShort} row
   */
  changeStatus(event, row: OrderAdminShort) {

    if (confirm('Status will be changed for whole order')) {
      this.inChange.push(row.id);
      this.rest.updateOrderStatus(row.code, event.target.value)
        .then(() => {
          let idx = this.inChange.indexOf(row.id);
          this.inChange.splice(idx, 1);
          this.updateOrderStatuses(row.code, event.target.value);
        })
        .catch(err => {
          let idx = this.inChange.indexOf(row.id);
          this.inChange.splice(idx, 1);
          if (err.hasOwnProperty('error')) {
            alert(err.error.message);
          }
        });
    }
  }

  /**
   * Callback to check if row is selectable
   *
   * @param {OrderAdminShort} row
   * @returns {boolean}
   */
  checkSelectable = (row: OrderAdminShort) => {
    let status = !this.stopSelectList.includes(row.id);
    let idx = this.stopSelectList.indexOf(row.id);
    this.stopSelectList.splice(idx, 1);
    return status;
  };

  /**
   * Mark row to stop select propagation
   *
   * @param {OrderAdminShort} row
   */
  stopSelect(row: OrderAdminShort) {
    this.stopSelectList.push(row.id);
  }

  /**
   * Update table with new status
   *
   * @param {string} code
   * @param {string} newStatus
   */
  private updateOrderStatuses(code: string, newStatus: string) {
    this.operationsSource.forEach( row => {if(row.code == code) row.status = newStatus});
    this.applyFilter();

  }

  /**
   * Perform filtering
   */
  applyFilter() {
    this.rows = this.operationsSource.filter(op => {
      let countryFilter = !this.country.length || op.country == this.country;
      let cityFilter = !this.city.length || op.city == this.city;
      let statusFilter = !this.status.length || op.status == this.status;
      let searchString = this.search.toLowerCase();
      let searchFilter = !this.search.length  || (-1 !== op.code.toLowerCase().indexOf(searchString))
                                              || (-1 !== op.activity.toLowerCase().indexOf(searchString))
                                              || (-1 !== op.country.toLowerCase().indexOf(searchString))
                                              || (-1 !== op.city.toLowerCase().indexOf(searchString))
                                              || (-1 !== op.status.toLowerCase().indexOf(searchString));
      return countryFilter && cityFilter && statusFilter && searchFilter;
    });
  }

  /**
   * Delete operation
   *
   * @param event
   * @param id
   */
  async delete(event, id: number) {
    event.stopPropagation();
    if (confirm('Are you sure?')) {
      this.inDelete.push(id);
      await this.rest.deleteOperation(id);
      this.operationsSource = await this.rest.getOperations();
      this.applyFilter();
      const index = this.inDelete.indexOf(id);
      this.inDelete.splice(index, 1);
    }
  }
}
