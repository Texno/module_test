import { CurrencyPipe, DatePipe } from '@angular/common';

export class CustomDatePipe extends DatePipe {

  constructor() {
    super('en-US');
  }

  transform(value: any): string | null {
    return super.transform(value * 1000, 'mediumDate');
  }
}

export class CustomCurrencyPipe extends CurrencyPipe {

  constructor() {
    super('en-US');
  }

  transform(value: any): string | null {
    return super.transform(value, 'EUR', 'symbol', '1.2-2' );
  }
}
