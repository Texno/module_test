import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { Review } from '@shared/backend/data-types/review.types';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-review-edit',
  templateUrl: './review-edit.component.html',
  styleUrls: ['./review-edit.component.styl']
})
export class ReviewEditComponent implements OnInit {

  isLoading = true;
  activity: ActivityFull;
  review: Review;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      let promises = [];
      if(params.hasOwnProperty('idReview')) {
        this.rest.getReview(params.idReview)
          .then(review => {
            this.review = review;
            this.rest.getActivityFull(review.activity, true)
              .then(activity => {
                this.activity = activity;
                this.isLoading = false;
              });
          })
      }
      if(params.hasOwnProperty('idActivity')) {
        this.rest.getActivityFull(params.idActivity, true).then(activity => {
          this.activity = activity;
          this.isLoading = false;
        });
      }
    })
  }

  onEditComplete(result: boolean) {
    const idActivity = this.review ? this.review.activity : this.activity.id;
    this.router.navigate([`/admin/activities/${idActivity}/reviews`])
  }

}
