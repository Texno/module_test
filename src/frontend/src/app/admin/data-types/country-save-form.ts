import { Country, LocaleNames } from '@shared/backend/data-types/geo.types';

export class CountrySaveForm {

  constructor(public name: string, public route_name: string, public localeNames: LocaleNames, public imageFile = '', public imageFileSmall = '') {}

  public static FromCountry(country: Country) {
    return new CountrySaveForm(country.name, country.route_name, country.locale_name);
  }
}
