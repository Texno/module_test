import { ActivityFull, ActivitySchedule, ActivityType, GalleryItem } from '@shared/backend/data-types/activity.types';
import { Supplier } from '@shared/backend/data-types/supplier.types';

export class ActivityForm {
  constructor(
    public name = '',
    public route_name = '',
    public avatar = '',
    public avatar_small = '',
    public country = 0,
    public city = 0,
    public category = 0,
    public shortDescription = '',
    public description = '',
    public duration = '',
    public language = '',
    public included = '',
    public travellers = 0,
    public notIncluded = '',
    public whenToBook = '',
    public accessibility = '',
    public ticket = '',
    public howToBook = '',
    public meetingPointLatitude = 41.3895201, // Default to Barselona
    public meetingPointLongitude = 2.1691647, // Default to Barselona
    public meetingPointText = '',
    public cancelationHoursBefore = 0,
    public cancelationDescription = '',
    public book_limit = '',
    public types: ActivityType[] = [],
    public gallery: GalleryItemEdit[] = [],
    public schedule: ActivitySchedule = {time: [], offset: 0},
    public suppliers: number[] = [],
    public blocks = new ActivityBlocks(),
    public voucher = new ActivityVoucher(),
  ) {}

  public fillFromActivity(activity: ActivityFull) {
    this.name = activity.name;
    this.route_name = activity.route_name;
    this.city = activity.city;
    this.country = activity.country;
    this.category = activity.category;
    this.shortDescription = activity.short_description;
    this.description = activity.description;
    this.duration = activity.duration;
    this.language = activity.language;
    this.included = activity.included;
    this.travellers = activity.travellers;
    this.notIncluded = activity.not_included;
    this.whenToBook = activity.when_to_book;
    this.accessibility = activity.acessibility;
    this.ticket = activity.ticket;
    this.howToBook = activity.how_to_book;
    this.meetingPointLatitude = parseFloat(activity.meeting_point_latitude.toString()); // For google maps component
    this.meetingPointLongitude = parseFloat(activity.meeting_point_longitude.toString()); // For google maps component
    this.meetingPointText = activity.meeting_point_text;
    this.cancelationHoursBefore = activity.cancelation_hours_before;
    this.cancelationDescription = activity.cancelation_descriprion;
    this.book_limit = activity.book_limit;
    this.types = activity.types;
    this.gallery = [];
    this.blocks.fillFromObject(activity.blocks);
    activity.gallery.forEach(item => this.gallery.push(new GalleryItemEdit(item)));
    this.schedule = activity.schedule;
    activity.suppliers.forEach(supplier => {
      this.suppliers.push(supplier.id);
    });
    if (activity.voucher) {
      this.voucher.text = activity.voucher.text;
    }
  }
}

class ActivityBlocks {
  constructor(
    public included = false,
    public notIncluded = false,
    public whenToBook = false,
    public ticket = false,
    public accessibility = false,
    public howToBook = false,
    public cancelationDescription = false,
    public questions = false,
    public cancelationHours = false,
    public meetingPoint = false,
    public language = false,
  ){}

  public fillFromObject(obj : any) {
    this.included =               obj.included || this.included;
    this.notIncluded =            obj.notIncluded || this.notIncluded;
    this.whenToBook =             obj.whenToBook || this.whenToBook;
    this.ticket =                 obj.ticket || this.ticket;
    this.accessibility =          obj.accessibility || this.accessibility;
    this.howToBook =              obj.howToBook || this.howToBook;
    this.cancelationDescription = obj.cancelationDescription || this.cancelationDescription;
    this.questions =              obj.questions || this.questions;
    this.cancelationHours =       obj.cancelationHours || this.cancelationHours;
    this.meetingPoint =           obj.meetingPoint || this.meetingPoint;
    this.language =               obj.language || this.language;
  }
}

class ActivityVoucher {
  constructor(
    public image = '',
    public text  = ''
  ) {}
}

export class GalleryItemEdit implements GalleryItem {
  path: string;
  description: string;
  is_main: boolean;

  constructor(item: GalleryItem, public upload: string = '') {
    this.path = item.path;
    this.description = item.description;
    this.is_main = item.is_main;
  }
}
