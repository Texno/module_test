import { City, LocaleNames } from '@shared/backend/data-types/geo.types';

export class CitySaveForm {

  constructor(
    public name: string,
    public route_name: string,
    public travellers: number,
    public localeNames: LocaleNames,
    public imageFile = '',
    public imageFileSmall = ''
  ) {}

  public static FromCity(city: City) {
    return new CitySaveForm(city.name, city.route_name, city.travellers, city.locale_name);
  }
}