import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { GeoEditComponent } from './pages/geo-edit/geo-edit.component';
import { ActivitiesComponent } from './pages/activities/activities.component';
import { ActivityEditComponent } from './pages/activity-edit/activity-edit.component';
import { ActivityViewComponent } from './pages/activity-view/activity-view.component';
import { ActivityReviewsComponent } from './pages/activity-reviews/activity-reviews.component';
import { ReviewEditComponent } from './pages/review-edit/review-edit.component';
import { SuppliersComponent } from './pages/suppliers/suppliers.component';
import { SupplierEditComponent } from './pages/supplier-edit/supplier-edit.component';
import { OperationsComponent } from './pages/operations/operations.component';
import { OperationViewComponent } from './pages/operation-view/operation-view.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { ReportViewComponent } from './pages/report-view/report-view.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Admin area'
      }
    },
    children: [
      {path: '',           redirectTo: 'geo'},
      {path: 'geo',        component: GeoEditComponent,     data: { meta: { title: 'Admin - Manage geo locations'}}},
      {path: 'activities/create',   component: ActivityEditComponent,  data: { meta: { title: 'Admin - Manage activities - New'}}},
      {path: 'activities/:id/edit', component: ActivityEditComponent,  data: { meta: { title: 'Admin - Manage activities - Edit'}}},
      {path: 'activities/:id/view', component: ActivityViewComponent,  data: { meta: { title: 'Admin - Manage activities - View'}}},
      {path: 'activities/:id/reviews', component: ActivityReviewsComponent,  data: { meta: { title: 'Admin - Manage activities - Reviews'}}},
      {path: 'activities', component: ActivitiesComponent,  data: { meta: { title: 'Admin - Manage activities'}}},
      {path: 'suppliers/create',   component: SupplierEditComponent,  data: { meta: { title: 'Admin - Manage suppliers - New'}}},
      {path: 'suppliers/:id/edit', component: SupplierEditComponent,  data: { meta: { title: 'Admin - Manage suppliers - Edit'}}},
      {path: 'suppliers', component: SuppliersComponent,  data: { meta: { title: 'Admin - Manage suppliers'}}},
      {path: 'operation/:id', component: OperationViewComponent,  data: { meta: { title: 'Admin - View operation'}}},
      {path: 'operations', component: OperationsComponent,  data: { meta: { title: 'Admin - Manage operations'}}},
      {path: 'reviews/:idReview/edit', component: ReviewEditComponent,  data: { meta: { title: 'Admin - Manage reviews - Edit'}}},
      {path: 'reviews/create/:idActivity', component: ReviewEditComponent,  data: { meta: { title: 'Admin - Manage reviews - Create'}}},
      {path: 'report/:id', component: ReportViewComponent,  data: { meta: { title: 'Admin - View report'}}},
      {path: 'reports', component: ReportsComponent,  data: { meta: { title: 'Admin - Manage reports'}}},
    ]
  }
];

export const AdminRoutes = RouterModule.forChild(routes);