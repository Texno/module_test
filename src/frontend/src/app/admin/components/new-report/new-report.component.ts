import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ReportType } from '@shared/backend/data-types/report.types';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-new-report',
  templateUrl: './new-report.component.html',
  host: {'class': 'modal-wrapper'},
  styleUrls: ['./new-report.component.styl']
})
export class NewReportComponent implements OnInit {

  reportTypes: ReportType[] = [];

  form = {
    report_type: null,
    name: null
  };

  tryToSubmit = false;
  inCreate = false;

  constructor(public activeModal: NgbActiveModal, private rest: RestApiService) { }

  ngOnInit() {
    this.rest.getReportTypes().then(types => this.reportTypes = types);
  }

  submitData(form: NgForm) {
    this.tryToSubmit = true;
    if (!form.valid) {
      return;
    }
    this.inCreate = true;
    this.rest.createNewReport(this.form)
      .then(()=>{
        this.activeModal.close();
      })
      .catch(err => {
        alert('Cannot create report');
        console.log(err);
        this.inCreate = false;
      });

  }

}
