import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Country, Language } from '@shared/backend/data-types/geo.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { CountrySaveForm } from '../../data-types/country-save-form';
import { UploaderComponent } from '@shared/components/uploader/uploader.component';

@Component({
  selector: 'app-edit-country',
  templateUrl: './edit-country.component.html',
  styleUrls: ['./edit-country.component.styl']
})
export class EditCountryComponent implements OnInit, OnChanges {

  @Input() country: Country;
  @Output() onCountrySaved = new EventEmitter<boolean>();
  languageList$: Promise<Language[]>;

  form: CountrySaveForm;
  error: string = null;
  isUploading = false;
  uploadProgress = 0;
  uploadError = '';
  saveError = '';
  isSaving = false;
  uploadData: UploadResult;
  uploadDataSmall: UploadResult;

  uploader = UploaderComponent;

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.languageList$ = this.rest.getLanguages();
  }

  ngOnChanges(changes) {
    this.form = CountrySaveForm.FromCountry(this.country);
    this.uploadData = null;
    this.uploadDataSmall = null;
  }

  onImageUploaded(uploadData: UploadResult) {
    this.uploadData = uploadData;
    this.form.imageFile = this.uploadData.imageName;
  }

  onImageUploadedSmall(uploadData: UploadResult) {
    this.uploadDataSmall = uploadData;
    this.form.imageFileSmall = this.uploadDataSmall.imageName;
  }

  /**
   * Perform country saving
   */
  saveCountry() {
    this.isSaving = true;
    this.saveError = '';
    this.rest.saveCountryData(this.country.id, this.form)
      .then(() => {
        this.isSaving = false;
        this.form.imageFile = '';
        this.onCountrySaved.next(true);
      })
      .catch(err => {
        this.isSaving = false;
        this.saveError = err.error.message;
      })
  }

}
