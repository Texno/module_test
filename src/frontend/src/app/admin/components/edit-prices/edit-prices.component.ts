import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { ActivityType } from '@shared/backend/data-types/activity.types';

@Component({
  selector: 'app-edit-prices',
  templateUrl: './edit-prices.component.html',
  styleUrls: ['./edit-prices.component.styl']
})
export class EditPricesComponent implements OnInit, OnChanges {

  typesValue: ActivityType[] = [];

  set types(types: ActivityType[]) {
    this.typesValue = types;
  }

  @Input() get types() {
    return this.typesValue;
  }

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes) {
    if (0 == this.typesValue.length) {
      //insert empty first column
      this.typesValue.push({
        id: null,
        name: '',
        tickets: [
          {
            id: null,
            name: '',
            price: 0,
            is_adult: true
          }
        ]
      });
    }
  }

  addRow() {
    let type = {
      id: null,
      name: '',
      tickets: []
    };
    this.typesValue[0].tickets.forEach(ticket => type.tickets.push(Object.assign({}, ticket)));
    this.typesValue.push(type);
  }

  removeRow(index: number) {
    this.typesValue.splice(index, 1);
  }

  addColumn() {
    let ticket = {
      id: null,
      name: '',
      price: 0,
      is_adult: true
    };
    this.typesValue.forEach(type => type.tickets.push(Object.assign({}, ticket)));
  }

  removeColumn(index: number) {
    this.typesValue.forEach(type => {
      type.tickets.splice(index, 1);
    });
  }
}
