import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { City, Language } from '@shared/backend/data-types/geo.types';
import { CitySaveForm } from '../../data-types/city-save-form';
import { RestApiService } from '@shared/backend/rest-api.service';
import { UploaderComponent } from '@shared/components/uploader/uploader.component';

@Component({
  selector: 'app-edit-city',
  templateUrl: './edit-city.component.html',
  styleUrls: ['./edit-city.component.styl']
})
export class EditCityComponent implements OnInit, OnChanges {

  @Input() city: City;
  @Output() onCitySaved = new EventEmitter<boolean>();
  languageList$: Promise<Language[]>;

  form: CitySaveForm;
  error: string = null;
  saveError = '';
  isSaving = false;
  uploadData: UploadResult;
  uploadDataSmall: UploadResult;

  uploader = UploaderComponent;

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.languageList$ = this.rest.getLanguages();
  }

  ngOnChanges(changes) {
    this.form = CitySaveForm.FromCity(this.city);
    this.uploadData = null;
    this.uploadDataSmall = null;
  }

  onImageUploaded(uploadData: UploadResult) {
    this.uploadData = uploadData;
    this.form.imageFile = this.uploadData.imageName;
  }

  onImageUploadedSmall(uploadData: UploadResult) {
    this.uploadDataSmall = uploadData;
    this.form.imageFileSmall = this.uploadDataSmall.imageName;
  }

  /**
   * Perform city saving
   */
  saveCity() {
    this.isSaving = true;
    this.saveError = '';
    this.rest.saveCityData(this.city.id, this.form)
      .then(() => {
        this.isSaving = false;
        this.form.imageFile = '';
        this.onCitySaved.next(true);
      })
      .catch(err => {
        this.isSaving = false;
        this.saveError = err.error.message;
      })
  }

}
