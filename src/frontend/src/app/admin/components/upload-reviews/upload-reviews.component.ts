import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-upload-reviews',
  templateUrl: './upload-reviews.component.html',
  host: {'class': 'modal-wrapper'},
  styleUrls: ['./upload-reviews.component.styl']
})
export class UploadReviewsComponent implements OnInit {

  selectedFile: File;
  uploadError = '';
  uploadMessages: string[] = [];
  inUpload = false;

  constructor(public activeModal: NgbActiveModal, private rest: RestApiService) { }

  ngOnInit() {
  }

  submit() {
    this.uploadError = '';
    if(!this.selectedFile) {
      this.uploadError = 'Please select file';
      return;
    }
    this.inUpload = true;
    this.uploadMessages = [
      'Uploading file...'
    ];
    this.rest.uploadReviews(this.selectedFile)
      .then(result => {
        this.uploadMessages = result;
        this.inUpload = false;
      })
      .catch(err => {
        this.uploadMessages = [];
        this.uploadError = err.error.message;
        this.inUpload = false;
      })
  }

}
