import { Component, Input, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Supplier } from '@shared/backend/data-types/supplier.types';

@Component({
  selector: 'app-edit-suppliers',
  templateUrl: './edit-suppliers.component.html',
  styleUrls: ['./edit-suppliers.component.styl']
})
export class EditSuppliersComponent implements OnInit {

  suppliersValue: number[] = [];
  allSuppliers: Supplier[] = [];
  suppliersLoading = true;

  set suppliers(suppliers: number[]) {
    this.suppliersValue = suppliers;
  }

  @Input() get suppliers(): number[] {
    return this.suppliersValue;
  }

  constructor(private rest: RestApiService) { }

  ngOnInit() {
    this.rest.getSuppliers().then(suppliers => {
      this.allSuppliers = suppliers;
      this.suppliersLoading = false;
    });
  }

  toggleSupplier(supplier: Supplier) {
    if (!this.suppliersValue.includes(supplier.id)) {
      this.suppliersValue.push(supplier.id);
    } else {
      let idx = this.suppliersValue.indexOf(supplier.id);
      this.suppliersValue.splice(idx, 1);
    }
  }

  removeSupplier(id: number) {
    let idx = this.suppliersValue.indexOf(id);
    this.suppliersValue.splice(idx, 1);
  }

}
