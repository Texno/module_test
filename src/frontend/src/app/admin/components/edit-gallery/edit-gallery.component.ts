import { Component, Input, OnInit } from '@angular/core';
import { GalleryItemEdit } from '../../data-types/activity-form';

@Component({
  selector: 'app-edit-gallery',
  templateUrl: './edit-gallery.component.html',
  styleUrls: ['./edit-gallery.component.styl']
})
export class EditGalleryComponent implements OnInit {

  galleryValue: GalleryItemEdit[] = [];

  set gallery(gallery: GalleryItemEdit[]) {
    this.galleryValue = gallery;
  }

  @Input() get gallery() {
    return this.galleryValue;
  }

  constructor() { }

  ngOnInit() {
  }

  onImageUpload(uploadData: UploadResult) {
    this.galleryValue.push({
      path: uploadData.imageUrl,
      is_main: false,
      description: '',
      upload: uploadData.imageName
    })
  }

  deleteImage(index: number) {
    this.galleryValue.splice(index, 1);
  }

  selectItem(index: number) {
    this.galleryValue.forEach(item => item.is_main = false);
    this.galleryValue[index].is_main = true;
  }

}
