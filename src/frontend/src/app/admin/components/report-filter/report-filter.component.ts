import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NumberFormatter, PriceFormatter } from '@shared/components/activity-filter/tooltip.formatter';

@Component({
  selector: 'app-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.styl']
})
export class ReportFilterComponent implements OnInit {

  @Input() filters;

  @Output() filtersChanged = new EventEmitter<any>();

  private selected = {};

  pricesTooltip = [
    new PriceFormatter(),
    new PriceFormatter()
  ];

  numberTooltip = [
    new NumberFormatter(),
    new NumberFormatter()
  ];

  constructor() { }

  ngOnInit() {
  }

  updateFilterValue(type, value) {
    if ('all' !== value) {
      this.selected[type] = value;
    } else {
      if (this.selected.hasOwnProperty(type)) {
        delete this.selected[type];
      }
    }
    this.filtersChanged.next(this.selected);
  }

  updateRangeValue(type, range) {
    const [min, max] = range;
    if (min == this.filters[type].min && max == this.filters[type].max) {
      if (this.selected.hasOwnProperty(type)) {
        delete this.selected[type];
      }
    } else {
      this.selected[type] = { low: min, high: max };
    }
    this.filtersChanged.next(this.selected);
  }

}
