import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivitySchedule } from '@shared/backend/data-types/activity.types';
import { NgbDatepicker, NgbDateStruct, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { DateHelper } from '@shared/helpers/date-helper';

@Component({
  selector: 'app-edit-calendar',
  templateUrl: './edit-calendar.component.html',
  styleUrls: ['./edit-calendar.component.styl']
})
export class EditCalendarComponent implements OnInit {

  static readonly MIN_YEAR = 2018;
  static readonly MAX_YEAR = 2099;

  dayTimes: NgbTimeStruct[] = [];
  selectedYear = EditCalendarComponent.MIN_YEAR;

  scheduleValue: ActivitySchedule;
  selectedDates: NgbDateStruct[] = [];

  self = EditCalendarComponent;
  dateHelper = DateHelper;

  @ViewChild('calendar') calendar: NgbDatepicker;

  set schedule(schedule: ActivitySchedule) {
    this.scheduleValue = schedule;
  }

  @Input() get schedule() {
    return this.scheduleValue;
  }

  constructor() { }

  ngOnInit() {
  }

  /**
   * Call back for date check
   * @param {NgbDateStruct} date
   * @returns {boolean}
   */
  isDisabled = (date: NgbDateStruct) => {
    let calendar = DateHelper.parseNgbDateStruct(date, this.schedule.offset);
    let now = moment().utcOffset(this.schedule.offset);
    return calendar.isBefore(now, 'day');
  };

  /**
   * callback to switch forward current year
   */
  selectNextYear() {
    if (this.selectedYear < EditCalendarComponent.MAX_YEAR) {
      this.selectedYear++;
      this.calendar.navigateTo({year: this.selectedYear, month:1});
      this.selectedDates = [];
      this.dayTimes = [];
    }
  }

  /**
   * callback to switch backward current year
   */
  selectPreviousYear() {
    if (this.selectedYear > EditCalendarComponent.MIN_YEAR) {
      this.selectedYear--;
      this.calendar.navigateTo({year: this.selectedYear, month:1});
      this.selectedDates = [];
      this.dayTimes = [];
    }
  }

  /**
   * Show times for selected date
   */
  private showTimes() {
    if (!this.selectedDates.length) {
      return;
    }
    const calendar = DateHelper.parseNgbDateStruct(this.selectedDates[0], this.schedule.offset);
    const unixStartOfDay = calendar.startOf('day').unix();
    const unixEndOfDay = calendar.endOf('day').unix();
    const filteredDates = this.scheduleValue.time.filter(val => val >= unixStartOfDay && val <= unixEndOfDay);
    this.dayTimes = [];
    filteredDates.forEach(timestamp => {
      let day = moment.unix(timestamp).utcOffset(this.scheduleValue.offset);
      this.dayTimes.push({
        hour: day.get('hours'),
        minute: day.get('minutes'),
        second: 0
      });
    });
  }

  /**
   * Date click event handler
   *
   * @param {MouseEvent} event
   * @param {NgbDateStruct} date
   */
  dateClicked(event: MouseEvent, date: NgbDateStruct) {
    if (event.ctrlKey) {
      const index = this.selectedDates.indexOf(date);
      if (-1 === index) {
        this.selectedDates.push(date);
      } else {
        this.selectedDates.splice(index, 1);
      }
    } else {
      this.selectedDates = [];
      this.selectedDates.push(date);
    }
    this.showTimes();
  }

  /**
   * Callback for date hovering
   * @param event
   * @param {NgbDateStruct} date
   */
  hoveredDate(event, date: NgbDateStruct) {
    if(1 == event.buttons && !this.selectedDates.includes(date)) {
        this.selectedDates.push(date);
        this.showTimes();
    }
  }

  /**
   * Perform sync between dates block and schedule model
   */
  syncDates(date: NgbDateStruct) {
    const calendar = DateHelper.parseNgbDateStruct(date, this.schedule.offset);
    const unixStartOfDay = calendar.startOf('day').unix();
    const unixEndOfDay = calendar.endOf('day').unix();
    // remove old dates for day
    this.scheduleValue.time = this.scheduleValue.time.filter(val => val < unixStartOfDay || val > unixEndOfDay);

    // fill the dates from array
    this.dayTimes.forEach(dayTime => {
      const timestamp = unixStartOfDay + dayTime.hour * 3600 + dayTime.minute * 60 + dayTime.second;
      this.scheduleValue.time.push(timestamp);
    });
  }

  /**
   * Remove date from list
   * @param {number} index
   */
  removeTime(index: number) {
    this.dayTimes.splice(index, 1);
    this.syncDates(this.selectedDates[0]);
  }

  /**
   * Add new empty date
   */
  addTime() {
    this.dayTimes.push({
      hour: 12,
      minute: 0,
      second: 0
    });
    this.syncDates(this.selectedDates[0]);
  }

  /**
   * Check date has schedule
   *
   * @param {NgbDateStruct} date
   */
  hasSchedule(date: NgbDateStruct){
    const calendar = DateHelper.parseNgbDateStruct(date, this.schedule.offset);
    const unixStartOfDay = calendar.startOf('day').unix();
    const unixEndOfDay = calendar.endOf('day').unix();
    for(let i =0; i < this.scheduleValue.time.length; i++) {
      if (this.scheduleValue.time[i] >= unixStartOfDay && this.scheduleValue.time[i] <= unixEndOfDay) {
        return true;
      }
    }
    return false;
  }

  /**
   *
   */
  copyToAll() {
    const copyArray = this.selectedDates.slice(1);
    copyArray.forEach(date => {
     this.syncDates(date);
    });
  }
}
