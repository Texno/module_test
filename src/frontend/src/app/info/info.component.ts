import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.styl']
})
export class InfoComponent implements OnInit {

  block = '';

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(data => {
      this.block = data.id;
    });
  }

  ngOnInit() {
  }

}
