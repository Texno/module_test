import { Component, OnInit } from '@angular/core';
import { AuthService } from '@shared/auth/auth.service';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.styl']
})
export class DashboardComponent implements OnInit {

  constructor(public auth: AuthService, public service: DashboardService) { }

  ngOnInit() {
    this.service.init();
  }

}
