import { Component, Input, OnInit } from '@angular/core';
import { OrderInfo, OrderInfoPrices } from '@shared/backend/data-types/order.types';
import { FormatHelper } from '@shared/helpers/format-helper';
import { RouteHelper } from '@shared/helpers/route-helper';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.styl']
})
export class OrderCardComponent implements OnInit {

  @Input() orderInfo: OrderInfo;
  @Input() passed = false;

  formatHelper = FormatHelper;
  routeHelper = RouteHelper;

  constructor() { }

  ngOnInit() {
  }

  getTotalPrice(prices: OrderInfoPrices[]):number {
    let sum = 0;
    prices.forEach(price => sum += +price.price);
    return sum;
  }

}
