import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Review } from '@shared/backend/data-types/review.types';
import { FormatHelper } from '@shared/helpers/format-helper';

@Component({
  selector: 'app-user-review-card',
  templateUrl: './review-card.component.html',
  styleUrls: ['./review-card.component.styl']
})
export class ReviewCardComponent implements OnInit {

  @Input() review: Review;

  @Output() deleteClicked = new  EventEmitter<boolean>();

  formatHelper = FormatHelper;

  constructor() { }

  ngOnInit() {
  }

}
