import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { OrdersHistoryComponent } from './pages/orders-history/orders-history.component';
import { OrdersUpcomingComponent } from './pages/orders-upcoming/orders-upcoming.component';
import { AccountComponent } from './pages/account/account.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { CreateReviewComponent } from './pages/create-review/create-review.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Dashboard'
      }
    },
    children: [
      {path: '',                        redirectTo: 'history'},
      {path: 'history',                 component: OrdersHistoryComponent,   data: { meta: { title: 'Dashboard - History of booking'}}},
      {path: 'upcoming',                component: OrdersUpcomingComponent,  data: { meta: { title: 'Dashboard - Upcoming booking'}}},
      {path: 'account',                 component: AccountComponent,         data: { meta: { title: 'Dashboard - My Account'}}},
      {path: 'reviews',                 component: ReviewsComponent,         data: { meta: { title: 'Dashboard - My Feedback'}}},

    ]
  },
  {path: 'review/:activity/:order', component: CreateReviewComponent,    data: { meta: { title: 'Dashboard - Create Feedback'}}},
];

export const DashboardRoutes = RouterModule.forChild(routes);
