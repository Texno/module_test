import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { AuthService } from '@shared/auth/auth.service';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Observable } from 'rxjs/Observable';
import { City, Country } from '@shared/backend/data-types/geo.types';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { RestApiService } from '@shared/backend/rest-api.service';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.styl']
})
export class AccountComponent implements OnInit {


  showOldPassword = false;
  showPassword = false;
  showPasswordRepeat = false;

  passwordChangeForm: FormGroup;
  accountForm: FormGroup;

  checkPasswordFields = false;
  inPasswordChange = false;
  passwordChangeError = '';

  countryError = false;
  cityError = false;

  country = '';
  city = '';

  checkFields = false;
  inAccountUpdate = false;
  accountUpdateError = '';

  serverErrors = {
    email: [],
    name: [],
    surname: [],
    phone: [],
    country: [],
    city: [],
  };

  @ViewChild('cityInput') cityInput;
  @ViewChild('countryInput') countryInput;

  private countries$: Promise<Country[]>;

  constructor(private auth: AuthService, private rest: RestApiService) {
    this.createForms();
  }

  ngOnInit() {
    this.countries$ = this.rest.getCountryList();
  }

  /**
   * Create user forms
   */
  private createForms() {
    this.passwordChangeForm = new FormGroup({
      oldPassword:      new FormControl('',   [
        Validators.required
      ]),
      newPassword:   new FormControl('',   [
        Validators.required,
        Validators.pattern(/^(?=.*[A-Z]).{8,}$/)
      ]),
    }, { updateOn: 'change'});

    const passwordConfirm = new FormControl('',   [
      Validators.required,
      this.sameValueAs(this.passwordChangeForm, 'newPassword')
    ]);

    this.passwordChangeForm.addControl('passwordConfirm', passwordConfirm);

    let userData = this.auth.getTokenData();
    this.country = userData.userCountryName;
    this.city = userData.userCityName;
    this.accountForm  = new FormGroup({
      name:       new FormControl(userData.userName,    [
        Validators.required,
        Validators.maxLength(150)
      ]),
      surname:       new FormControl(userData.userSurname,    [
        Validators.required,
        Validators.maxLength(150)
      ]),
      email:      new FormControl(userData.email,   [
        Validators.required,
        Validators.email
      ]),
      phone:      new FormControl(userData.phone,   [
        Validators.required,
        Validators.pattern(/^\+\d{11,15}$/)
      ]),
      country:  new FormControl(userData.userCountry),
      city:  new FormControl(userData.userCity),
      news:  new FormControl(false),
    }, { updateOn: 'change'});

  }

  /**
   * Callback for password update
   */
  public updatePassword() {
    this.checkPasswordFields = true;
    if (this.passwordChangeForm.invalid) {
      return;
    }
    this.inPasswordChange = true;
    this.auth.updatePassword(this.passwordChangeForm.value)
      .then(() => {
        this.inPasswordChange = false;
        this.auth.renewToken();
        this.passwordChangeForm.reset();
        this.checkPasswordFields = false;
        this.passwordChangeError = '';
        alert('Password was changed');
      })
      .catch(err => {
        this.inPasswordChange = false;
        this.passwordChangeError = err.error.message;
      });
  }

  /**
   * Callback for account update
   */
  public updateAccount() {
    this.checkFields = true;
    if (this.accountForm.invalid) {
      return;
    }
    this.inAccountUpdate = true;
    this.auth.updateAccount(this.accountForm.value)
      .then(() => {
        this.inAccountUpdate = false;
        this.auth.renewToken();
        this.checkFields = false;
        this.accountUpdateError = '';
        alert('Account was updated');
        this.serverErrors = {
          email: [],
          name: [],
          surname: [],
          phone: [],
          country: [],
          city: [],
        };
      })
      .catch(err => {
        this.inAccountUpdate = false;
        this.accountUpdateError = err.error.message;
        Object.assign(this.serverErrors, err.error.errors);
      });
  }

  /**
   * Custom validation function
   *
   * @param {FormGroup} group
   * @param {string} controlName
   * @returns {ValidatorFn}
   */
  sameValueAs(group: FormGroup, controlName: string): ValidatorFn {
    return (control: AbstractControl) => {
      const myValue = control.value;
      const compareValue = group.controls[controlName].value;
      return (myValue === compareValue) ? null : {valueDifferent: true};
    };
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    let country: Country = event.item;
    this.accountForm.get('country').setValue(country.id);
    this.countryError = false;
    this.country = country.name;
  };

  /**
   * Callback to filter cities
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any[]>}
   */
  searchCity = (text$: Observable<string>) => {
    return text$.debounceTime(200).distinctUntilChanged()
      .flatMap(searchText => {
        if (!this.accountForm.value.country || !searchText.length) {
          return of([]);
        }
        return this.rest.searchCitiesByCountry(this.accountForm.value.country, searchText);
      });

  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  cityFormatter = (result: City) => {
    return result.name;
  };

  /**
   * City select handler
   * @param event
   */
  onSelectCity = (event) => {
    let city: City = event.item;
    this.accountForm.get('city').setValue(city.id);
    this.cityError = false;
    this.city = city.name;
  };

  onBlur() {
    this.cityInput.nativeElement.value = this.city;
    this.countryInput.nativeElement.value = this.country;
  }

  /**
   * Set focus to element
   * @param {string} id
   */
  setFocus(id: string) {
    document.getElementById(id).focus();
  }

  get oldPassword() { return this.passwordChangeForm.get('oldPassword'); }
  get newPassword() { return this.passwordChangeForm.get('newPassword'); }
  get passwordConfirm() { return this.passwordChangeForm.get('passwordConfirm'); }

  get name()    {return this.accountForm.get('name');}
  get surname() {return this.accountForm.get('surname');}
  get email()   {return this.accountForm.get('email');}
  get phone()   {return this.accountForm.get('phone');}

}
