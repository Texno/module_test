import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../dashboard.service';
import { Review } from '@shared/backend/data-types/review.types';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.styl']
})
export class ReviewsComponent implements OnInit {

  isLoading = true;
  reviews: Review[] = [];
  deleted: number[] = [];

  constructor(private service: DashboardService, private rest: RestApiService) { }

  ngOnInit() {
    this.service.getUserReviews().then(reviews => {
      this.reviews = reviews;
      this.isLoading = false;
    });
  }

  deleteReview(id: number) {
    if(confirm('¿Estás seguro?')) {
      this.deleted.push(id);
      this.rest.deleteReview(id)
        .then(() => {
          this.service.init();
          this.service.getUserReviews().then(reviews => this.reviews = reviews);
        })
        .catch(err => {
          alert(err.error.message);
          this.deleted.splice(this.deleted.indexOf(id), 1)
        });
    }
  }

}
