import { Component, OnInit } from '@angular/core';
import { OrderInfo } from '@shared/backend/data-types/order.types';
import { DashboardService } from '../../dashboard.service';
import { DateHelper } from '@shared/helpers/date-helper';
import * as moment from 'moment';

@Component({
  selector: 'app-orders-upcoming',
  templateUrl: './orders-upcoming.component.html',
  styleUrls: ['./orders-upcoming.component.styl']
})
export class OrdersUpcomingComponent implements OnInit {

  isLoading = true;
  orders: OrderInfo[] = [];

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.service.getUserOrders().then(orders => {
      this.orders = orders.filter(order => {
        let orderMoment = DateHelper.getMomentFromOrder(order);
        console.log(orderMoment);
        return orderMoment.isAfter(moment());
      });
      this.isLoading = false;
    });
  }

}
