import { Component, OnInit } from '@angular/core';
import { ActivityFull } from '@shared/backend/data-types/activity.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-review',
  templateUrl: './create-review.component.html',
  styleUrls: ['./create-review.component.styl']
})
export class CreateReviewComponent implements OnInit {

  isLoading = true;
  activity: ActivityFull;
  orderCode: string;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if(params.has('order')) {
        this.orderCode = params.get('order');
      }
      if(params.has('order')) {
        this.rest.getActivityFull(parseInt(params.get('activity'))).then(activity => {
          this.activity = activity;
          this.isLoading = false;
        });
      }
    });
  }

  onEditComplete(result: boolean) {
    this.router.navigate([`/dashboard/reviews`])
  }

}
