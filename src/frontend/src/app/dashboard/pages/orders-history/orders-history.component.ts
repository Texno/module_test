import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../dashboard.service';
import { OrderInfo } from '@shared/backend/data-types/order.types';
import { DateHelper } from '@shared/helpers/date-helper';
import * as moment from 'moment';

@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
  styleUrls: ['./orders-history.component.styl']
})
export class OrdersHistoryComponent implements OnInit {

  isLoading = true;
  orders: OrderInfo[] = [];

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.service.getUserOrders().then(orders => {
      this.orders = orders.filter(order => {
        let orderMoment = DateHelper.getMomentFromOrder(order);
        return orderMoment.isBefore(moment());
      });
      this.isLoading = false;
    });
  }

}
