import { Injectable } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { OrderInfo } from '@shared/backend/data-types/order.types';
import { Review } from '../shared/backend/data-types/review.types';

@Injectable()
export class DashboardService {

  private orders$: Promise<OrderInfo[]> = null;
  private reviews$: Promise<Review[]> = null;

  constructor(private rest: RestApiService) {
  }

  /**
   * Initialize service for user
   */
  public init() {
    this.orders$ = this.rest.getUserOrders();
    this.reviews$ = this.rest.getUserReviews();
  }

  /**
   * Get user orders
   *
   * @returns {Promise<OrderInfo[]>}
   */
  public getUserOrders(): Promise<OrderInfo[]> {
    return this.orders$;
  }

  /**
   * Get user reviews
   *
   * @returns {Promise<Review[]>}
   */
  public getUserReviews(): Promise<Review[]> {
    return this.reviews$;
  }

}
