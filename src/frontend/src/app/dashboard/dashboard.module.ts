import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { OrdersHistoryComponent } from './pages/orders-history/orders-history.component';
import { OrdersUpcomingComponent } from './pages/orders-upcoming/orders-upcoming.component';
import { AccountComponent } from './pages/account/account.component';
import { ReviewsComponent } from './pages/reviews/reviews.component';
import { RouterModule } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { OrderCardComponent } from './components/order-card/order-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateReviewComponent } from './pages/create-review/create-review.component';
import { ComponentsModule } from '@shared/components/components.module';
import { ReviewCardComponent } from './components/review-card/review-card.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutes,
    RouterModule,
    NgbModule,
    ComponentsModule,
    ReactiveFormsModule
  ],
  providers: [
    DashboardService
  ],
  declarations: [
    DashboardComponent,
    OrdersHistoryComponent,
    OrdersUpcomingComponent,
    AccountComponent,
    ReviewsComponent,
    OrderCardComponent,
    CreateReviewComponent,
    ReviewCardComponent
  ]
})
export class DashboardModule { }
