import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountryComponent } from './country.component';
import { CountryRoutes } from './country.routing';
import { ComponentsModule } from '@shared/components/components.module';
import { BookingCalendarModule } from '@shared/booking-calendar/booking-calendar.module';
import { HeroComponent } from './component/hero/hero.component';

@NgModule({
  imports: [
    CommonModule,
    CountryRoutes,
    BookingCalendarModule,
    ComponentsModule
  ],
  declarations: [CountryComponent, HeroComponent]
})
export class CountryModule { }
