import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DestinationCountry } from '@shared/backend/data-types/destination.types';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.styl']
})
export class HeroComponent implements OnInit {

  @Input() destinationCountry: DestinationCountry;
  @Output() onCountrySelected = new  EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  selectCountry(route_name: string) {
    this.onCountrySelected.next(route_name);
  }

}
