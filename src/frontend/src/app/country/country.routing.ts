import { Routes, RouterModule } from '@angular/router';
import { CountryComponent } from './country.component';

const routes: Routes = [
  {
    path: '', component: CountryComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Destinations'
      }
    },
  },
];

export const CountryRoutes = RouterModule.forChild(routes);
