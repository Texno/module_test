import { Component, OnInit } from '@angular/core';
import { RestApiService } from '@shared/backend/rest-api.service';
import { DestinationCountry } from '@shared/backend/data-types/destination.types';
import { ActivatedRoute, Router } from '@angular/router';
import { Country } from '@shared/backend/data-types/geo.types';
import { RouteHelper } from '@shared/helpers/route-helper';
import { AuthService } from '@shared/auth/auth.service';

@Component({
  selector: 'app-destinations',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.styl']
})
export class CountryComponent implements OnInit {

  country = '';
  destinationInfo: Promise<DestinationCountry>;
  isLoading = true;

  constructor(private rest: RestApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if(params.has('country')) {
        this.country = params.get('country');
      }
      this.loadCountryInformation();
    })
  }

  /**
   * Reload destination information
   */
  private loadCountryInformation () {
    this.isLoading = true;
    this.destinationInfo = this.rest.getDestinationCountry(this.country).then(res => {
      this.isLoading = false;
      return res;
    });
    this.destinationInfo.catch(err => {
      if (err.hasOwnProperty('status') && err.status == 404) {
        this.router.navigate([AuthService.FALLBACK_URL]);
      }
      console.log(err);
    });
  }

  /**
   * Country change handler
   *
   * @param country
   */
  selectCountry(country: Country) {
    if (this.country !== country.route_name) {
      this.router.navigate([RouteHelper.buildCountryRoute(country)]);
    }
  }
}
