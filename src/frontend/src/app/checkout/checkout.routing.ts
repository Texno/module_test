import { Routes, RouterModule } from '@angular/router';
import { CheckoutComponent } from './checkout.component';
import { ThankYouComponent } from './pages/thank-you/thank-you.component';
import { ReviewComponent } from './pages/review/review.component';
import { DetailsComponent } from './pages/details/details.component';
import { PaymentComponent } from './pages/payment/payment.component';


const routes: Routes = [
  {
    path: 'thank-you/:code', component: ThankYouComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Thank you'
      }
    },
  },
  {
    path: '', component: CheckoutComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Checkout'
      }
    },
    children: [
      { path: 'review', component: ReviewComponent},
      { path: 'details', component: DetailsComponent},
      { path: 'payment', component: PaymentComponent},
      { path: '', redirectTo: 'review', pathMatch: 'full'}
    ]
  },
];

export const CheckoutRoutes = RouterModule.forChild(routes);