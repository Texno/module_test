import { Component, OnInit } from '@angular/core';
import { BasketService } from '@shared/basket/basket.service';
import { BasketOrder } from '@shared/basket/data-types/basket-order';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.styl']
})
export class CheckoutComponent implements OnInit {

  order: BasketOrder = null;
  step1Complete = false;
  step2Complete = false;

  currentStep = null;

  constructor(private basketService: BasketService, private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        switch (event.url) {
          case '/checkout/review':
            this.currentStep = 1;
            break;

          case '/checkout/details':
            this.currentStep = 2;
            break;

          case '/checkout/payment':
            this.currentStep = 3;
            break;
        }
      }
    })
  }

  ngOnInit() {
    this.basketService.getOrderObservable().subscribe(order => {
      this.order = order;
      this.step1Complete = null !== this.order;
      this.step2Complete = this.order && this.order.isVerified;
    });
  }

}
