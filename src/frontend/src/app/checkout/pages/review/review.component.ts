import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { Review } from '@shared/backend/data-types/review.types';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import { BasketService } from '@shared/basket/basket.service';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.styl']
})
export class ReviewComponent implements OnInit {

  isReviewLoading = false;
  reviews: Review[] = [];
  basketItems: BasketItem[] = [];

  pricefixed = true;

  openFaq = false;

  questions = [
    {o: false, q:'¿Dónde inicia la actividad?', a:'En cada actividad te detallamos el punto de encuentro, el cual es el lugar donde inicia la actividad. Una vez realices la reserva, recibirás por correo electrónico tu confirmación con el detalle específico del punto de encuentro e información del proveedor.'},
    {o: false, q:'¿Puedo reservar una actividad para otra persona?', a:'Si es posible. Es importante que cuando reserves para otra persona indiques los datos personales de la persona que realizará la actividad. La información de pago sí debe ser tus datos para finalizar la transacción.'},
    {o: false, q:'¿Cómo realizo el pago de mi reserva?', a:'Realizar el pago con nosotros es muy fácil y rápido. Puedes realizarlo a través de PayPal o bien una tarjeta Visa o Mastercard. Todos los pagos de las actividades deben ser realizados a través de la página web.'},
    {o: false, q:'¿El pago es seguro?', a:'Contamos con un seguro de pago online 100% seguro y libre de fraudes o transacciones no autorizadas. Como empresa, no tenemos acceso a tus datos bancarios ni datos de tarjeta.'}
  ];

  constructor(private basketService: BasketService, private rest: RestApiService, private router: Router) { }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = window.scrollY;
    if (number > 1005) {
      this.pricefixed = false;
    } else {
      this.pricefixed = true;
    }
  }

  ngOnInit() {
    this.basketService.getBasketObservable().subscribe(items => {
      this.basketItems = items;
      this.reviews = [];
      this.isReviewLoading = true;
      let promises = [];
      items.forEach(item => {
        promises.push(this.rest.getReviewsForActivity(item.activity.id).then(reviews => this.reviews = this.reviews.concat(reviews)));
      });
      Promise.all(promises).then(() => this.isReviewLoading = false);
      (<any>window).dataLayer.push({'content_grouping_1' : 'checkout'});
    });

  }

  getTotal() {
    let total = 0;
    this.basketItems.forEach(item => total += item.getTicketData().total);
    return total;
  }

  clickComment(index, event) {
    if (!event.target.checked) {
      this.basketItems[index].order.comment = '';
    }
  }

  /**
   * Submit handler
   */
  submit() {
    this.basketService.getOrCreateOrder();
    this.basketService.saveOrder();
    this.router.navigate(['/checkout/details']);
  }

  deleteClicked(id: number) {
    if (confirm('¿Estás seguro?')) {
      this.basketService.removeItem(id);
    }
  }

}
