import { Component, HostListener, OnInit } from '@angular/core';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import { BasketService } from '@shared/basket/basket.service';
import * as moment from 'moment';
import { BasketOrder, InvoiceRequest } from '@shared/basket/data-types/basket-order';
import { Observable } from 'rxjs/Observable';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Country } from '@shared/backend/data-types/geo.types';
import { RestApiService } from '@shared/backend/rest-api.service';
import { Router } from '@angular/router';
import { RouteHelper } from '@shared/helpers/route-helper';
import { GoogleTracking } from '@shared/helpers/google-tracking';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.styl']
})
export class DetailsComponent implements OnInit {

  basketItems: BasketItem[] = [];
  order: BasketOrder;
  termsStatus = false;
  tryToSubmit = false;

  routeHelper = RouteHelper;

  pricefixed = true;

  private countries$: Promise<Country[]>;

  constructor(private basketService: BasketService, private rest: RestApiService, private router: Router, private ga: GoogleTracking) { }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = window.scrollY;
    if (number > 110 && !this.order.invoice || number > 720 && this.order.invoice) {
      this.pricefixed = false;
    } else {
      this.pricefixed = true;
    }
  }

  ngOnInit() {
    this.basketService.getBasketObservable().subscribe(items => {
      this.basketItems = items;
      (<any>window).dataLayer.push({'content_grouping_1' : 'checkout'});
      this.ga.fireCheckoutStep(1, items);
    });
    this.order = this.basketService.getOrCreateOrder();
    this.countries$ = this.rest.getCountryList();
  }

  formatDate(item: BasketItem): string {
    return moment.unix(item.order.time).utcOffset(item.activity.schedule.offset).format("DD.MM.YY");
  }

  getTotal() {
    let total = 0;
    this.basketItems.forEach(item => total += item.getTicketData().total);
    return total;
  }

  changeNeedInvoice(event) {
    const status = event.target.checked;
    if (status) {
      this.order.invoice = new InvoiceRequest();
    } else {
      this.order.invoice = null;
    }
  }

  /**
   * Handler for form submitting
   *
   * @param form
   */
  submitData(form) {
    this.tryToSubmit = true;
    if(form.invalid) {
      return;
    }
    this.order.isVerified = true;
    this.rest.createOrder(this.order)
      .then((response: any) => {
        this.order.code = response.code;
        this.order.verifiedTotal = response.total;
        this.basketService.saveOrder();
        this.router.navigate(['/checkout/payment']);
      })
      .catch(err => alert('Cannot create order. Please try again later'));
  }

  /**
   * Callback to filter countries
   *
   * @param {Observable<string>} text$
   * @returns {Observable<any>}
   */
  searchCountry = (text$: Observable<string>) => {
    return combineLatest(fromPromise(this.countries$), text$.debounceTime(200).distinctUntilChanged())
      .map(data => {
        let countryList = data[0];
        let searchText = data[1];
        return countryList.filter(v => v.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1).slice(0, 10);
      });
  };

  /**
   * Format string for country
   *
   * @param {Country} result
   * @returns {string}
   */
  countryFormatter = (result: Country) => {
    return result.name;
  };

  /**
   * Country select handler
   * @param event
   */
  onSelectCountry = (event) => {
    let country: Country = event.item;
    this.order.invoice.country = country.name;
  };

  /**
   * Return to page with activity selection
   */
  backToSelect() {
    const backLocation = this.basketService.getBeforeCheckoutRoute();
    if(!backLocation) {
      this.router.navigate(['/destinations']);
    } else {
      this.router.navigate([backLocation]);
    }
  }

}
