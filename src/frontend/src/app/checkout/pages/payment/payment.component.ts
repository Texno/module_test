import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { BasketItem } from '@shared/basket/data-types/basket-item';
import { BasketService } from '@shared/basket/basket.service';
import * as moment from 'moment';
import { BasketOrder } from '@shared/basket/data-types/basket-order';
import { RestApiService } from '@shared/backend/rest-api.service';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';
import { IPayPalPaymentCompleteData } from 'ngx-paypal/ngx-dist/src/models/paypal-models';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteHelper } from '@shared/helpers/route-helper';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { AppConfig } from '@shared/config/app-config.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { IframeModalComponent } from '@shared/components/iframe-modal/iframe-modal.component';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.styl']
})
export class PaymentComponent implements OnInit, AfterViewInit {

  basketItems: BasketItem[] = [];
  order: BasketOrder;
  paypal: Promise<void>;

  payPalConfig?: PayPalConfig;

  @ViewChild('redsysButton') redsysButton;

  verifyPayment = false;
  inPay = false;
  routeHelper = RouteHelper;

  stripe: any;
  stripeError = '';
  cardNumber: any;

  iframe3dsRef: NgbModalRef = null;

  constructor(
    private basketService: BasketService,
    private rest: RestApiService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: NgbModal,
    private ga: GoogleTracking
  ) { }

  ngOnInit() {
    this.basketService.getBasketObservable().subscribe(items => {
      this.basketItems = items;
      (<any>window).dataLayer.push({'content_grouping_1' : 'checkout'});
      this.ga.fireCheckoutStep(2, items);
    });
    this.basketService.getOrderObservable().subscribe(order => {
      if (!order) return;
      this.order = order;
    });
  }

  ngAfterViewInit() {
    // stripe
    this.setupStripe();
  }

  /**
   * Setup stripe payment
   */
  private setupStripe() {
    this.stripe = (<any>window).Stripe(AppConfig.getConfiguration().stripe.publicKey);
    const elements = this.stripe.elements({
      locale: 'es',
    });

    const elementStyles = {
      base: {
        color: '#32325D',
        fontWeight: 500,
        fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
        fontSize: '16px',
        fontSmoothing: 'antialiased',

        '::placeholder': {
          color: '#CFD7DF',
        },
        ':-webkit-autofill': {
          color: '#e39f48',
        },
      },
      invalid: {
        color: '#E25950',

        '::placeholder': {
          color: '#FFCCA5',
        },
      },
    };

    const elementClasses = {
      focus: 'focused',
      empty: 'empty',
      invalid: 'invalid',
    };

    this.cardNumber = elements.create('cardNumber', {
      style: elementStyles,
      classes: elementClasses,
    });
    this.cardNumber.mount('#card-number');

    const cardExpiry = elements.create('cardExpiry', {
      style: elementStyles,
      classes: elementClasses,
    });
    cardExpiry.mount('#card-expiry');

    const cardCvc = elements.create('cardCvc', {
      style: elementStyles,
      classes: elementClasses,
    });
    cardCvc.mount('#card-cvc');

    this.cardNumber.addEventListener('change', ({error}) => {
      this.stripeError = error ? error.message : '';
    });
    cardExpiry.addEventListener('change', ({error}) => {
      this.stripeError = error ? error.message : '';
    });
    cardCvc.addEventListener('change', ({error}) => {
      this.stripeError = error ? error.message : '';
    });
  }

  /**
   * Stripe error handler
   *
   * @param error
   */
  private handleStripeError = (error) => {
    if (error) {
      this.inPay = false;
      this.stripeError = error.message;
      return true;
    }
    return false;
  };

  /**
   * Perform stripe payment
   */
  async payStripe() {
    if (this.stripeError) {
      return;
    }
    this.inPay = true;
    this.submitStripe(); // GA

    const {source, error} = await this.stripe.createSource(this.cardNumber);
    if (this.handleStripeError(error)) return;

    if (source.card.three_d_secure == 'not_supported') {
      // perform usual payment
      this.makeStripePayment(source);
    } else {
      // perform 3ds payment
      this.makeStripe3dsPayment(source);
    }
  }

  /**
   * Make payment with the source
   *
   * @param source
   */
  private makeStripePayment(source: any) {
    this.rest.payStripe(this.order.code, source.id)
      .then(()=> {
        this.basketService.clear();
        this.router.navigate([`order/thank-you/${this.order.code}`]);
      })
      .catch(err => {
        console.log(err.message);
        alert('Cannot pay with stripe. Please contact support.');
        this.inPay = false;
      })
  }

  /**
   * Perform 3ds check
   *
   * @param cardSource
   */
  private async makeStripe3dsPayment(cardSource: any) {
    const { source , error} = await this.stripe.createSource({
      type: 'three_d_secure',
      amount: this.order.verifiedTotal * 100,
      currency: 'eur',
      three_d_secure: {
        card: cardSource.id
      },
      redirect: {
        return_url: 'https://civitours.com/assets/waitPayment.html'
      }
    });
    if (this.handleStripeError(error)) return;

    if (source.status == 'chargeable') {
      // 3ds is not acceptable. perform pay.
      this.makeStripePayment(source);
      return;
    }

    // open 3ds iframe
    this.iframe3dsRef = this.modalService.open(IframeModalComponent);
    this.iframe3dsRef.componentInstance.url = source.redirect.url;
    this.pollForSourceStatus(source);
  }

  /**
   * Perform server status polling
   *
   * @param source3ds
   */
  private async pollForSourceStatus(source3ds) {
    const {source, error} = await this.stripe.retrieveSource({id: source3ds.id, client_secret: source3ds.client_secret});
    if (error) {
      console.log(error);
      return;
    }
    if (source.status === 'chargeable') {
      this.makeStripePayment(source);
      this.iframe3dsRef.close();
    } else if (source.status === 'pending') {
      // Try again in a second, if the Source is still `pending`:
      setTimeout(this.pollForSourceStatus.bind(this, source3ds), 500);
    } else if (source.status === 'failed') {
      this.inPay = false;
      this.iframe3dsRef.close();
      alert('Payment is failed');
    } else if (source.status === 'canceled') {
      this.inPay = false;
      this.iframe3dsRef.close();
      alert('Payment is canceled');
    }
  }



  formatDate(item: BasketItem): string {
    return moment.unix(item.order.time).utcOffset(item.activity.schedule.offset).format("DD.MM.YY");
  }

  getTotal() {
    let total = 0;
    this.basketItems.forEach(item => total += item.getTicketData().total);
    return total;
  }

  submitRedsys() {
    this.ga.fireEvent(GoogleTracking.PAYMENT_OPTIONS_CATEGORY, 'Redsys', '/checkout/payment');
    document.getElementsByClassName('visa-btn')[0].getElementsByTagName('form')[0].submit();
  }

  submitPaypal() {
    this.ga.fireEvent(GoogleTracking.PAYMENT_OPTIONS_CATEGORY, 'PayPal', '/checkout/payment');
  }

  submitStripe() {
    this.ga.fireEvent(GoogleTracking.PAYMENT_OPTIONS_CATEGORY, 'Stripe', '/checkout/payment');
  }

}
