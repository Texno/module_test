import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@shared/auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '@shared/backend/rest-api.service';
import { OrderInfo } from '@shared/backend/data-types/order.types';
import { FormatHelper } from '@shared/helpers/format-helper';
import { GoogleTracking } from '@shared/helpers/google-tracking';
import { AppStorage } from '@shared/for-storage/universal.inject';
import { BasketService } from '@shared/basket/basket.service';

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.styl']
})
export class ThankYouComponent implements OnInit {

  orderInfo: OrderInfo[] = null;
  isLoading = false;

  formatHelper = FormatHelper;

  constructor(
    public auth: AuthService,
    private basketService: BasketService,
    private rest: RestApiService,
    private route: ActivatedRoute,
    private router: Router,
    private ga: GoogleTracking,
    @Inject(AppStorage) private appStorage: Storage
  ) { }

  parseFloat = parseFloat;

  ngOnInit() {
    this.route.params.subscribe(params => {
      if(params.hasOwnProperty('code')) {
        this.isLoading = true;
        this.rest.getOrderInfo(params.code)
          .then(info => {
            if (info[0].viewed) {
              this.router.navigate(['not-found']);
            } else {
              this.basketService.clear();
              this.rest.makeOrderViewed(params.code);
              this.ga.firePurchase(info);
              this.orderInfo = info;
              this.isLoading = false;
              window.setTimeout(()=> {
                if(this.auth.isAuthenticated()) {
                  this.router.navigate(['dashboard/upcoming']);
                } else {
                  this.router.navigate(['']);
                }

              }, 30000);
            }
          })
          .catch(() => {
            alert('Order not found');
            this.router.navigate(['/']);
          });
      }
    });
  }

}
