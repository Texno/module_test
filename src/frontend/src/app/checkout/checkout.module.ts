import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckoutComponent } from './checkout.component';
import { ReviewComponent } from './pages/review/review.component';
import { DetailsComponent } from './pages/details/details.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ThankYouComponent } from './pages/thank-you/thank-you.component';
import { CheckoutRoutes } from './checkout.routing';
import { ComponentsModule } from '@shared/components/components.module';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { AppConfig } from '@shared/config/app-config.service';
import { NgxPayPalModule } from 'ngx-paypal';

@NgModule({
  imports: [
    CommonModule,
    CheckoutRoutes,
    ComponentsModule,
    FormsModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: AppConfig.getConfiguration().maps.google
    }),
    NgxPayPalModule
  ],
  declarations: [CheckoutComponent, ReviewComponent, DetailsComponent, PaymentComponent, ThankYouComponent]
})
export class CheckoutModule { }
