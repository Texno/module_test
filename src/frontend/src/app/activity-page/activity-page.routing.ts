import { Routes, RouterModule } from '@angular/router';
import { ActivityComponent } from '@shared/activity/activity.component';

const routes: Routes = [
  {
    path: '', component: ActivityComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Activity'
      }
    },
  },
];

export const ActivityRoutes = RouterModule.forChild(routes);