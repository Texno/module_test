import { Component, OnInit } from '@angular/core';
import { NotFoundService } from './not-found.service';
import { DestinationCity } from '@shared/backend/data-types/destination.types';
import { RestApiService } from '@shared/backend/rest-api.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.styl']
})
export class NotFoundComponent implements OnInit {
  public status: { code: number, message: string };

  constructor(private _notFoundService: NotFoundService, private rest: RestApiService) {
  }

  mainDestinationList: Promise<DestinationCity[]>;

  ngOnInit(): void {
    this.mainDestinationList = this.rest.getMainDestinationsList();
    this._notFoundService.setStatus(404, 'Not Found');
  }
}
