import { Component, OnInit } from '@angular/core';
import { ContactForm } from '../../data-types/contact.form';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from '@shared/backend/rest-api.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.styl']
})
export class ContactFormComponent implements OnInit {

  public static readonly ALREADY_BOOKED_TYPE = 1;
  public static readonly BEFORE_BOOKED_TYPE = 2;
  public static readonly OTHER_TYPE = 3;

  selected = false;
  termsAccepted = false;
  tryToSubmit = false;

  form = new ContactForm();
  selectedFile: File;
  selectedType: number;

  th = ContactFormComponent;

  get isAlready() { return ContactFormComponent.ALREADY_BOOKED_TYPE == this.selectedType;}
  get isBefore() { return ContactFormComponent.BEFORE_BOOKED_TYPE == this.selectedType;}
  get isOther() { return ContactFormComponent.OTHER_TYPE == this.selectedType;}

  constructor(route: ActivatedRoute, private rest: RestApiService) {
    this.selectedType = route.snapshot.data.type;
    switch (this.selectedType) {
      case ContactFormComponent.ALREADY_BOOKED_TYPE:
        this.form.type = 'YA HE RESERVADO'; break;
      case ContactFormComponent.BEFORE_BOOKED_TYPE:
        this.form.type = 'ANTES DE RESERVAR'; break;
      case ContactFormComponent.OTHER_TYPE:
        this.form.type = 'OTROS TEMAS'; break;
    }

  }

  ngOnInit() {
  }

  submitData(form: NgForm) {
    this.tryToSubmit = true;
    if(!this.form.isValid(this.selectedType == ContactFormComponent.ALREADY_BOOKED_TYPE) || !this.termsAccepted) {
      return false;
    }
    const formData = new FormData();
    formData.append('data', JSON.stringify(this.form));
    if(this.selectedFile) {
      formData.append('file', this.selectedFile);
    }
    this.rest.requestContact(formData)
      .then(() => {
        this.tryToSubmit = false;
        this.selectedFile = null;
        const oldVal = this.form.hasReservation;
        alert('Solicitud aceptada.Nos pondremos en contacto con usted pronto.');
        this.form = new ContactForm();
        form.reset();
        this.form.hasReservation = oldVal;
      })
      .catch((err) => {
        alert('La petición falló. Por favor, inténtelo de nuevo más tarde.');
      });
  }
}
