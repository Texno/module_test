export class ContactForm {
  constructor(
    public hasReservation = true,
    public number =  '',
    public name = '',
    public email = '',
    public subject = '',
    public destination = '',
    public message = '',
    public type = ''
  ) {}

  isValid(forAlready = false) {
    if(forAlready) {
      if (this.hasReservation){
        return this.checkEmail() && this.number.length > 0;
      }
    }
    return this.checkEmail() && this.name.length > 0 && this.message.length > 0;
  }

  private checkEmail() {
    return /.+@.+\..+/.test(this.email);
  }
}