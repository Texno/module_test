import { Routes, RouterModule } from '@angular/router';
import { ContactsComponent } from './contacts.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

const routes: Routes = [
  {
    path: '', component: ContactsComponent,
    data: {
      meta: {
        title: 'Civitours',
        override: true,
        description: 'Contacts'
      }
    },
    children: [
      { path: 'already-booked', component: ContactFormComponent, data: {type: ContactFormComponent.ALREADY_BOOKED_TYPE}},
      { path: 'before-booking', component: ContactFormComponent, data: {type: ContactFormComponent.BEFORE_BOOKED_TYPE}},
      { path: 'other', component: ContactFormComponent, data: {type: ContactFormComponent.OTHER_TYPE}},
      { path: '', redirectTo: 'already-booked', pathMatch: 'full'}
    ]
  }
];

export const ContactsRoutes = RouterModule.forChild(routes);