import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsRoutes } from './contacts.routing';
import { FormsModule } from '@angular/forms';
import { ContactsComponent } from './contacts.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';
import { ComponentsModule } from '@shared/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    ContactsRoutes,
    FormsModule,
    ComponentsModule
  ],
  declarations: [ContactsComponent, ContactFormComponent]
})
export class ContactsModule { }
