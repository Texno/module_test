var gulp = require('gulp');
var clean = require('gulp-clean');
var rename = require('gulp-rename');

gulp.task('copy-backend', function(){
    return gulp.src('src/backend/**/*', {
        dot: true
    }).pipe(gulp.dest('dist/api'))
});

gulp.task('copy-frontend-client', function(){
    return gulp.src('src/frontend/dist/**/*')
        .pipe(gulp.dest('dist/angular-app'))
});
gulp.task('copy-frontend-client-prod', function(){
    return gulp.src('src/frontend/dist/**/*')
        .pipe(gulp.dest('dist/'))
});

gulp.task('copy-frontend-server', function(){
    return gulp.src('src/frontend/dist-server/**/*')
        .pipe(gulp.dest('dist/angular-server'))
});

gulp.task('copy-frontend-server-app', function(){
    return gulp.src('src/frontend/server.js')
        .pipe(gulp.dest('dist/'))
});

gulp.task('copy-backend-settings', function(){
    return gulp.src('src/.htaccess')
        .pipe(gulp.dest('dist/'))
});

gulp.task('copy-backend-settings-prod-main', function(){
    return gulp.src('src/.htaccess.prod')
        .pipe(rename('.htaccess'))
        .pipe(gulp.dest('dist/'))
});

gulp.task('copy-backend-settings-prod', function(){
    return gulp.src('dist/api/public/.htaccess.prod')
        .pipe(rename('.htaccess'))
        .pipe(gulp.dest('dist/api/public'));
});

gulp.task('clean', function () {
    return gulp.src('dist', {allowEmpty: true})
        .pipe(clean());
});

gulp.task('copy-ssr', gulp.series('clean',
    gulp.parallel(
        'copy-backend',
        'copy-frontend-client',
        'copy-frontend-server',
        'copy-frontend-server-app',
        'copy-backend-settings')
    ));

gulp.task('copy-prod', gulp.series('clean',
    gulp.parallel(
        'copy-backend',
        'copy-frontend-client-prod'),
    'copy-backend-settings-prod',
    'copy-backend-settings-prod-main'
));
