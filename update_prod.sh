#!/bin/sh

git checkout . 
git pull origin master
npm install
npm run build-prod

mv dist/api/public/img/gallery dist/api/public/img/gallery.old
ln -s /home/ubuntu/upload/gallery dist/api/public/img/gallery
cp parameters.php dist/api/app/config

mv dist/api/app/logs dist/api/app/logs.old
ln -s /home/ubuntu/logs dist/api/app/logs